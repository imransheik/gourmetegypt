<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */

namespace Amasty\Checkout\Api\Data;

interface TotalsInterface
{
    const TOTALS = 'totals';
    const IMAGE_DATA = 'imageData';
    const OPTIONS_DATA = 'options';

    /**
     * @return \Magento\Quote\Api\Data\TotalsInterface
     */
    public function getTotals();

    /**
     * @return string Json encoded data
     */
    public function getImageData();

    /**
     * @return string Json encoded data
     */
    public function getOptionsData();
}
