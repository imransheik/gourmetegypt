<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */


namespace Amasty\Checkout\Api;

interface GuestItemManagementInterface
{
    /**
     * @param string $cartId
     * @param int    $itemId
     *
     * @return \Magento\Quote\Api\Data\TotalsInterface|boolean
     */
    public function remove($cartId, $itemId);

    /**
     * @param string $cartId
     * @param int    $itemId
     * @param string $formData
     *
     * @return \Amasty\Checkout\Api\Data\TotalsInterface|boolean
     */
    public function update($cartId, $itemId, $formData);
}
