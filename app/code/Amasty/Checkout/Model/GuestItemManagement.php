<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */

namespace Amasty\Checkout\Model;

use Amasty\Checkout\Api\GuestItemManagementInterface;
use Amasty\Checkout\Api\ItemManagementInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;

class GuestItemManagement implements GuestItemManagementInterface
{
    /** @var QuoteIdMaskFactory */
    protected $quoteIdMaskFactory;
    /**
     * @var ItemManagementInterface
     */
    protected $itemManagement;

    public function __construct(
        QuoteIdMaskFactory $quoteIdMaskFactory,
        ItemManagementInterface $itemManagement
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->itemManagement = $itemManagement;
    }

    public function remove($cartId, $itemId)
    {
        /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->itemManagement->remove(
            $quoteIdMask->getQuoteId(),
            $itemId
        );
    }

    public function update($cartId, $itemId, $formData)
    {
        /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->itemManagement->update(
            $quoteIdMask->getQuoteId(),
            $itemId,
            $formData
        );
    }
}
