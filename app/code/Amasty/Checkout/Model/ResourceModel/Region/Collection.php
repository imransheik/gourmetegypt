<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */


namespace Amasty\Checkout\Model\ResourceModel\Region;

class Collection extends \Magento\Directory\Model\ResourceModel\Region\Collection
{
    protected function _construct()
    {
        $this->_init('Magento\Directory\Model\Region', 'Magento\Directory\Model\ResourceModel\Region');
    }

    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => $this->getMainTable()], ['code', 'region_id']);
        
        return $this;
    }
    
    public function fetchPairs()
    {
        return $this->getResource()->getConnection()->fetchPairs($this->getSelect());
    }
}
