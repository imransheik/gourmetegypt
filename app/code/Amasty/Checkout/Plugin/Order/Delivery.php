<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */


namespace Amasty\Checkout\Plugin\Order;

class Delivery
{
    public function afterToHtml(
        \Magento\Sales\Block\Items\AbstractItems $subject, $result
    ) {
        foreach ($subject->getLayout()->getUpdate()->getHandles() as $handle) {
            if (substr($handle, 0, 12) === 'sales_email_') {
                if ($subject->getOrder() && $subject->getOrder()->getId()) {
                    $deliveryBlock = $subject->getLayout()
                        ->createBlock(
                            'Amasty\Checkout\Block\Sales\Order\Email\Delivery', 'amcheckout.delivery',
                            [
                                'data' => [
                                    'order_id' => $subject->getOrder()->getId()
                                ]
                            ]
                        );

                    return $deliveryBlock->toHtml() . $result;
                }
            }
        }

        return $result;
    }
}
