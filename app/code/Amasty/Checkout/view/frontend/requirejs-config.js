/*jshint browser:true jquery:true*/
/*global alert*/
var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/place-order': {
                'Amasty_Checkout/js/model/place-order-mixin': true
            },
              'Magento_Checkout/js/model/address-converter': {
                'Amasty_Checkout/js/model/address-converter-mixin': true
            }
        }
    }
};
