<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


namespace Amasty\Preorder\Plugin\Bundle\Model\Product;

class Type
{
    /**
     * @var \Amasty\Preorder\Helper\Data
     */
    private $helper;
    
    public function __construct(
        \Amasty\Preorder\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }
    
    public function aroundIsSalable(
        $subject,
        callable $proceed,
        $product
    ) {
        if ($this->helper->getIsProductPreorder($product, true)) {
            $product->setData('all_items_salable', true);

            return true;
        }

        return $proceed($product);
    }
}
