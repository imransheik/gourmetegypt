<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Xsearch
 */


namespace Amasty\Xsearch\Block;

use Magento\Framework\View\Element\Template;

class CssFileInclude extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $context->getPageConfig()->addPageAsset('Amasty_Xsearch::css/source/mkcss/am-xsearch.css');
    }
}