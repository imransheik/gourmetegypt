<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Xsearch
 */


namespace Amasty\Xsearch\Block\Search;

class Brand extends AbstractSearch
{
    const BRAND_BLOCK_PAGE = 'brand';
    const SPLIT_SYMBOLS = '/( |&|:|-|\*|=)/';

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @inheritdoc
     */
    public function getBlockType()
    {
        return self::BRAND_BLOCK_PAGE;
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->dataObjectFactory = $this->getData('dataObjectFactory');
    }

    /**
     * @inheritdoc
     */
    protected function prepareCollection()
    {
        $collection = $this->getSearchCollection();
        $query = $this->explodeString($this->getQuery()->getQueryText());
        foreach ($this->getBrands() as $item) {
            if (array_intersect($this->explodeString($item['label']), $query)
                || array_intersect($this->explodeString($item['description']), $query)
            ) {
                $dataObject = $this->dataObjectFactory->create();
                $dataObject->setData($item);
                $collection->addItem($dataObject);
                if (count($collection) == $this->getLimit()) {
                    break;
                }
            }
        }

        return $collection;
    }

    /**
     * @param $string
     * @return array
     */
    private function explodeString($string)
    {
        return preg_split(self::SPLIT_SYMBOLS, strtolower($string), -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @return array
     */
    public function getBrands()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getSearchUrl(\Magento\Framework\DataObject $item)
    {
        return $item->getUrl();
    }

    /**
     * @inheritdoc
     */
    public function getName(\Magento\Framework\DataObject $item)
    {
        return $this->generateName($item->getLabel());
    }

    /**
     * @inheritdoc
     */
    public function showDescription(\Magento\Framework\DataObject $item)
    {
        return $this->stringUtils->strlen($item->getDescription()) > 0;
    }

    /**
     * @param \Magento\Framework\DataObject $item
     * @return string
     */
    public function getDescription(\Magento\Framework\DataObject $item)
    {
        $descStripped = $this->stripTags($item->getDescription(), null, true);
        $this->replaceVariables($descStripped);
        $text = $this->stringUtils->strlen($descStripped) > $this->getDescLength()
            ? $this->stringUtils->substr($descStripped, 0, $this->getDescLength()) . '...'
            : $descStripped;

        return $this->highlight($text);
    }
}
