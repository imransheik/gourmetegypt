<?php

namespace Gourmet\Alert\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class GuestAlert extends AbstractDb
{
	protected function _construct()
	{
		$this->_init('gourmet_alert_stock', 'id');
	}
}