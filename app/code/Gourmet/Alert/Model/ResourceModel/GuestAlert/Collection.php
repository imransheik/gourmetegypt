<?php

namespace Gourmet\Alert\Model\ResourceModel\GuestAlert;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

	protected $_idFieldName = 'id';

    protected function _construct()
    {
    	$this->_init('Gourmet\Alert\Model\GuestAlert', 'Gourmet\Alert\Model\ResourceModel\GuestAlert');
    }

    public function setEmailOrder($sort = 'ASC')
    {
        $this->getSelect()->order('email ' . $sort);
        return $this;
    }
}