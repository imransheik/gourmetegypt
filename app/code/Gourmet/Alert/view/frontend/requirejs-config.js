var config = {
    map: {
        '*': {
        	gourmetAlertCheck: 'Gourmet_Alert/js/check',
            gourmetAlert: 'Gourmet_Alert/js/stock'
        }
    },
	shim:{
        gourmetAlert:{
            'deps':['jquery']
        }
    }
};
