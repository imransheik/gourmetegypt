<?php

namespace Gourmet\Assist\Block\Amasty\Shopby\Navigation\Top;


class State extends \Magento\LayeredNavigation\Block\Navigation\State
{
    public function getActiveFilters()
    {
        return [];
    }

    /*getting active filters for display*/
    public function getCurrentActiveFilters()
    {
        return parent::getActiveFilters();
    }

}
