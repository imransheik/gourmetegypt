<?php

namespace Gourmet\Assist\Block\Catalog\Product\ProductList;

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{

    protected $_eavAttribute;
    protected $_storeid;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Catalog\Model\Product\ProductList\Toolbar $toolbarModel,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Catalog\Helper\Product\ProductList $productListHelper,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        array $data = []
    ) {
        $this->_eavAttribute = $eavAttribute;
        parent::__construct($context, $catalogSession, $catalogConfig, $toolbarModel, $urlEncoder, $productListHelper, $postDataHelper, $data);
    }

    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        if ($this->getCurrentOrder()) {
            if($this->getCurrentOrder() == 'discount'){
                $this->_collection->getSelect()->order(
                    new \Zend_Db_Expr('CAST(
                      CASE WHEN `price_index`.`price` > `price_index`.`final_price`
                      THEN ((`price_index`.`price` - `price_index`.`final_price`)/`price_index`.`price`)*100
                      ELSE 0
                      END AS DECIMAL(4,2)) '. $this->getCurrentDirection()
                    )
                );
            } else if($this->getCurrentOrder() == 'newest'){

                //$storeid = $this->_storeManager->getStore()->getId();
                $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'news_from_date');
                $this->_collection->getSelect()->joinLeft(
                    ['cped' => $this->_collection->getTable('catalog_product_entity_datetime')],
                    new \Zend_Db_Expr('`e`.`entity_id` = `cped`.`entity_id` AND `cped`.`attribute_id`='.$attributeId.' AND `cped`.`store_id`=0'),
                    ['news_from_date' =>  new \Zend_Db_Expr('IF(cped.value_id > 0, cped.value, cped.value)')]
                )->order('news_from_date '. $this->getCurrentDirection());

            } else{
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }
        return $this;
    }

}