<?php

namespace Gourmet\Assist\Block\Category;

class Menus extends \Magento\Catalog\Block\Navigation
{

	protected $_current_cat_id;
    protected $breadArr = [];

    public function listCategories()
    {
    	$catArr = [];
        $root_id = $this->_storeManager->getStore()->getRootCategoryId();
    	if($this->getCategory()){

            // skip for 1st level
            if($root_id == $this->getCategory()->getParentId()){
               return []; 
            }

    		$this->_current_cat_id = $this->getCategory()->getId();

    		$storeCats = $this->_catalogCategory->getStoreCategories();
    		foreach ($storeCats as $catId => $cat) {
    			if($cat->getIsActive()){
	    			$curr_cat = array(
	    				'name' => $cat->getName(),
	    				'url' => $this->_catalogCategory->getCategoryUrl($cat),
	    				'active' => ($this->_current_cat_id == $catId) ? 1 : 0 
	    			);

	    			if($cat->getChildrenCount()){
	    				$curr_cat['children'] = $this->getCategoryChildren($cat);
	    			}

	    			$catArr[$catId] = $curr_cat;
    			}   
    		}

    		foreach($catArr as $cat_id => $sub_cat){
    			if(!$sub_cat['active'] && array_key_exists('children', $sub_cat)){
    				$child_activated_cat = $this->setActiveFlags($sub_cat['children']);
    				if(!empty($child_activated_cat)){
						$catArr[$cat_id]['active'] = 1;
						$catArr[$cat_id]['children'] = $child_activated_cat;    					
    				}
    			}
    		}
    	}

    	return $catArr;
    }

    public function traverseCategories($categories)
    {
    	$html = '';
    	if(!empty($categories)){
    		$html .= '<ul class="cat-sub-menu-nav">';
    		foreach ($categories as $id => $data) {
    			$html .= '<li' . ($data['active']?' class="active"':'') . '><a href="'. $data['url'] .'">'. $data['name'] .'</a>';

    			if(array_key_exists('children', $data)){
					$html .= $this->traverseCategories($data['children']);    				
    			}

    			$html .= '</li>';
    		}
    		$html .= '</ul>';
    	}

    	return $html;
    }
	
	public function traverseCategoriesRoot($categories, $catId)
    {
    	$html = '';
    	if(!empty($categories)){
    		$html .= '<ul>';
			if(count($categories) > 1){
				$html .= '<li><a href="'. $catId .'">'. __('All').'</a></li>';
			}
    		foreach ($categories as $id => $data) {
    			$html .= '<li' . ($data['active']?' class="active"':'') . '><a href="'. $data['url'] .'">'. $data['name'] .'</a>';

    			if(array_key_exists('children', $data) && $data['active']){
					$childhtml = $this->traverseCategories($data['children']);    				
    			}

    			$html .= '</li>';
    		}
    		$html .= '</ul>';
			if(isset($childhtml))
			$html .= $childhtml;
    	}

    	return $html;
    }

    public function breadCategories($categories)
    {

        foreach ($categories as $curr) {

            if(array_key_exists('active', $curr) && $curr['active']){
                $this->breadArr[] = array(
                    'name' => $curr['name'],
                    'url' => $curr['url'],
                );

                if(array_key_exists('children', $curr) && is_array($curr['children'])){
                    $this->breadCategories($curr['children']);
                }

                break;
            }
        }

        return $this->breadArr;
    }

    protected function getCategoryChildren($cat)
    {
    	$childArr = array();
    	$subcats = $cat->getChildren();

		foreach ($subcats as $childId => $childCat) {
			if($childCat->getIsActive()){
				$sub_cat = array(
					'name' => $childCat->getName(),
					'url' => $this->_catalogCategory->getCategoryUrl($childCat),
					'active' => ($this->_current_cat_id == $childId) ? 1 : 0 
				);

				if($childCat->getChildrenCount()){
					$sub_cat['children'] = $this->getCategoryChildren($childCat);
				}				
			}
			$childArr[$childId] = $sub_cat;
		}

		return $childArr;
    }


    protected function setActiveFlags(array $category)
    {
    	foreach ($category as $id => $data) {
    		if($data['active']){
    			return $category;
    		} elseif(array_key_exists('children', $data)){
    			$child_cats =  $this->setActiveFlags($data['children']);
    		
    			if(!empty($child_cats)){
    				$category[$id]['active']=1;
    				$category[$id]['children']=$child_cats;
    				return $category;
    			}
    		}
    	}
    	
    	return array();
    }
}