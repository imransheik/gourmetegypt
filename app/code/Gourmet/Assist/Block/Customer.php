<?php

namespace Gourmet\Assist\Block;


use Magento\Framework\View\Element\AbstractBlock;

use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;

class Customer extends AbstractBlock
{
    private $_customer;
	private $_customerSession;
    private $_customerRepository;
    
    public function __construct(\Magento\Framework\View\Element\Context $context, Session $customerSession, CustomerRepository $customerRepository, array $data = [])
    {
        $this->_customerSession = $customerSession;
        $this->_customerRepository = $customerRepository;

        parent::__construct($context, $data);
    }

    /*
        use in template as 
        $assistBlock =  $block->getLayout()->createBlock('Gourmet\Assist\Block\Customer');
    */

    public function isCustomerLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }

    public function getCustomer()
    {
        if (!$this->_customer) {
            if ($this->isCustomerLoggedIn()) {
                $this->_customer = $this->_customerRepository->getById($this->_customerSession->getCustomerId());
            } else {
                return null;
            }
        }

        return $this->_customer;
    }
}