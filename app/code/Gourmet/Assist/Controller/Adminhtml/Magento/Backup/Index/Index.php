<?php

namespace Gourmet\Assist\Controller\Adminhtml\Magento\Backup\Index;

class Index extends \Magento\Backup\Controller\Adminhtml\Index\Index
{
    protected function _isAllowed()
    {
    	// default attribute is not returning true, quick fix, enabled for all
        return true;
    }
}
