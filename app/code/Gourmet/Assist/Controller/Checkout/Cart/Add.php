<?php

namespace Gourmet\Assist\Controller\Checkout\Cart;

class Add extends \Magento\Checkout\Controller\Cart\Add
{
	public function execute()
	{
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        if(isset($params['deduct'])){
        	try{
        		$product = $this->_initProduct();
        		if(
        			($quoteItem = $this->cart->getQuote()->getItemByProduct($product)) && $quoteItem->getQty() && ($itemId = $quoteItem->getItemId())){
    				unset($params['deduct']);
        			$itemQty = ((int) $quoteItem->getQty()) - 1;

        			$itemData = [$itemId => ['qty' => $itemQty]];
		            $this->cart->updateItems($itemData)->save();
    			
		            $this->_eventManager->dispatch(
		                'checkout_cart_update_item_complete',
		                ['item' => $quoteItem, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
		            );
    			
		            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
		                if (!$this->cart->getQuote()->getHasError()) {
		                    $message = __(
		                        'You deducted %1 from your shopping cart.',
		                        $product->getName()
		                    );
		                    $this->messageManager->addSuccessMessage($message);
		                }
		            }
        		}
        		return $this->goBack(null, $product);
	        } catch (\Magento\Framework\Exception\LocalizedException $e) {
	            if ($this->_checkoutSession->getUseNotice(true)) {
	                $this->messageManager->addNotice(
	                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
	                );
	            } else {
	                $messages = array_unique(explode("\n", $e->getMessage()));
	                foreach ($messages as $message) {
	                    $this->messageManager->addError(
	                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
	                    );
	                }
	            }

	            $url = $this->_checkoutSession->getRedirectUrl(true);

	            if (!$url) {
	                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
	                $url = $this->_redirect->getRedirectUrl($cartUrl);
	            }

	            return $this->goBack($url);

	        } catch (\Exception $e) {
	            $this->messageManager->addException($e, __('We can\'t deduct this item to your shopping cart right now.'));
	            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
	            return $this->goBack();
	        }
        }else{
        	return parent::execute();
        }
	}
}