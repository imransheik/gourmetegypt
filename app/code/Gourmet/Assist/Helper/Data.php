<?php

namespace Gourmet\Assist\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_devState;
    public function __construct(\Magento\Framework\App\State $devState, \Magento\Framework\App\Helper\Context $context)
    {
        $this->_devState = $devState;
        parent::__construct($context);
    }
    /*
    - shows remaining prodcuts to show, upto 100
    */
    public function getRemainingProductsCount($total_count, $displayed_count)
    {
        try{
            $total_count = (int) $total_count;
            $displayed_count = (int) $displayed_count;
            $diff = $total_count - $displayed_count;

            return $diff > 100 ? '100+' : $diff;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function isDeveloperMode()
    {
        return $this->_devState->getMode() == \Magento\Framework\App\State::MODE_DEVELOPER;
    }
}
