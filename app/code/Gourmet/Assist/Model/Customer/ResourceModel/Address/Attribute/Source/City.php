<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Customer country attribute source
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Magento\Customer\Model\ResourceModel\Address\Attribute\Source;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Api\StoreResolverInterface;

class City extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $_countriesFactory;

    /**
     * @var StoreResolverInterface
     */
    private $storeResolver;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countriesFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countriesFactory
    ) {
        $this->_countriesFactory = $countriesFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all options
     *
     * @return array
     */
    public function getAllOptions()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager= $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$storeurl    = $storeManager->getStore()->getUrl('stores/store/switch/');
		$model = $objectManager->create('\Gourmet\Location\Model\Location');
		$locationCollection = $model->getCollection();
		
		$city = array();
		$city[0] = array("value" => "", "label" => "Please select city.", "is_region_required"=>"1");
		
		$i = 1;
		foreach($locationCollection as $data){
			$cityid = $data->getCityId();
			$cityname = $data->getCity();
			$city[$i] = array("value"=>"$cityid", "label"=>"$cityname", "is_region_required"=>"1");
			$i++;
		}
		
		return $city;
    }

    /**
     * @return \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected function _createCountriesCollection()
    {
        return $this->_countriesFactory->create();
    }

    /**
     * Retrieve Store Resolver
     *
     * @deprecated
     * @return StoreResolverInterface
     */
    private function getStoreResolver()
    {
        if (!$this->storeResolver) {
            $this->storeResolver = ObjectManager::getInstance()->get(StoreResolverInterface::class);
        }

        return $this->storeResolver;
    }
}
