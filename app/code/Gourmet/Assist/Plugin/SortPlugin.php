<?php

namespace Gourmet\Assist\Plugin;

class SortPlugin
{
    protected $_attribute;

    public function __construct( \Magento\Eav\Model\ResourceModel\Entity\Attribute $attribute) {
        $this->_attribute = $attribute;
    }

    public function afterGetAvailableOrders(\Magento\Catalog\Block\Product\ProductList\Toolbar $subject, array $result)
    {

        if(!array_key_exists('discount', $result)){
            $result['discount'] = 'By Discount';
        }

        if(!array_key_exists('newest', $result)){
            $result['newest'] = 'Newest';
        }

        return $result;
    }

    public function aroundSetCollection(\Magento\Catalog\Block\Product\ProductList\Toolbar $subject, \Closure $proceed, $collection)
    {
        if($current_order = $subject->getCurrentOrder()) {
             if(in_array($current_order, array('discount','newest'))){
                $subject->setData('_current_grid_order', null);
             } else{
                $current_order = null;
             }
        }
        $result = $proceed($collection);

        if(!is_null($current_order)){
            $collection = $result->getCollection();
            switch ($current_order) {
                case 'discount':
                    $collection->getSelect()->order(
                        new \Zend_Db_Expr('CAST(
                          CASE WHEN `price_index`.`price` > `price_index`.`final_price`
                          THEN ((`price_index`.`price` - `price_index`.`final_price`)/`price_index`.`price`)*100
                          ELSE 0
                          END AS DECIMAL(4,2)) '. $result->getCurrentDirection()
                        )
                    );
                break;
                case 'newest':
                    $attributeId = $this->_attribute->getIdByCode('catalog_product', 'news_from_date');
                    $collection->getSelect()->joinLeft(
                        ['cped' => $collection->getTable('catalog_product_entity_datetime')],
                        new \Zend_Db_Expr('`e`.`entity_id` = `cped`.`row_id` AND `cped`.`attribute_id`='.$attributeId.' AND `cped`.`store_id`=0'),
                        ['news_from_date' =>  new \Zend_Db_Expr('IF(cped.value_id > 0, cped.value, cped.value)')]
                    )->order('news_from_date '. $result->getCurrentDirection());
                break;
                default: break;
            }
        }

        return $result;
    }
}
