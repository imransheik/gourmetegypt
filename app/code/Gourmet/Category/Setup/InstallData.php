<?php
 
namespace Gourmet\Category\Setup;
 
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallDataInterface;
 
class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
 
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
 
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
 
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
 
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'rms_id',
			[
				'type' => 'int',
				'label' => 'Rms Id',
				'input' => 'text',
				'required' => false,
				'sort_order' => 4,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
				'wysiwyg_enabled' => false,
				'is_html_allowed_on_front' => false,
				'group' => 'General Information',
			]
		);
 
        $setup->endSetup();
    }
}