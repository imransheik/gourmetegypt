<?php
namespace Gourmet\CustomReport\Console;
 
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
 
class CustomerReport extends Command
{
	
    const NAME_ARGUMENT = "csv_name";
    
	protected $_directory;
	protected $_customerFactory;
	protected $_customerModel;
	
	public function __construct(
        DirectoryList $directory,
        CustomerFactory $customerFactory,
        Customer $customerModel
        
    ) {
        $this->_directory = $directory;
        $this->_customerFactory = $customerFactory;
        $this->_customerModel = $customerModel;
        
        parent::__construct();
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$name = $input->getArgument(self::NAME_ARGUMENT);
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

		$rootPath  =  $directory->getPath('var');
		
		$output->writeln("Exporting CSV " . $name);
		if(empty($name)){
			$name = 'customerwithnoorder.csv';
		}
		$jsonFilePath = $rootPath.'/export/'.$name;
		$fp = fopen($jsonFilePath, 'w');
		$columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
		fputcsv($fp, $header);
		
		$customerCollections = $this->_customerModel->getCollection()->addAttributeToSelect("*")->load();
		foreach ($customerCollections as $customerCollection) {
			//print_r($customerCollection->getData());
			$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
			$orderCollection = $OrderFactory->create()->addFieldToSelect(array('*'));
			$orderCollection->addFieldToFilter('customer_id', $customerCollection->getEntity_id());
			$customerArray =[];
			if($orderCollection->getSize() <= 0){
				$customerArray['email']=$customerCollection->getEmail();
				$customerArray['fristname']=$customerCollection->getFirstname();
				$customerArray['lastname']=$customerCollection->getLastname();
				$customerArray['phone']=$customerCollection->getPrimaryMobileNumber();
				fputcsv($fp, $customerArray);
			}
			//exit;
		}
		fclose($fp);

        $output->writeln("Exporting done pls check the csv file  " . $jsonFilePath);
    }
 
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("report:customer");
        $this->setDescription("Export customer list in CSV with no order");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Csv Name")
        ]);
        parent::configure();
    }
	public function getColumnHeader() {
        $headers = ['Email','First Name','Last Name','Phone'];
        return $headers;
    }
}