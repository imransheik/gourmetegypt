<?php
namespace Gourmet\CustomReport\Console;
 
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
 
class OrderReport extends Command
{
	
    const NAME_ARGUMENT = "csv_name";
    
	protected $_directory;
	protected $_customerFactory;
	protected $_customerModel;
	
	public function __construct(
        DirectoryList $directory,
        CustomerFactory $customerFactory,
        Customer $customerModel
        
    ) {
        $this->_directory = $directory;
        $this->_customerFactory = $customerFactory;
        $this->_customerModel = $customerModel;
        
        parent::__construct();
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$name = $input->getArgument(self::NAME_ARGUMENT);
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

		$rootPath  =  $directory->getPath('var');
		
		$output->writeln("Exporting CSV " . $name);
		
		if(empty($name)){
			$name = 'orderreport.csv';
		}
		$jsonFilePath = $rootPath.'/export/'.$name;
		$fp = fopen($jsonFilePath, 'w');
		$columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
		fputcsv($fp, $header);
		$now = new \DateTime();
		$fromDate = $now->format('2022-03-02');
		$toDate = $now->format('2022-03-03');
		//print_r($toDate);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
		$orderCollection = $OrderFactory->create()->addAttributeToSelect('*');
		$orderCollection->addFieldToFilter('created_at', array('gteq' => $fromDate))
            ->addFieldToFilter('created_at', array('lt' => $toDate));
		$totalorder = count($orderCollection);
		$appOrder = $websiteOrder=0;
		foreach($orderCollection as $order){
			$source = "";
			$histories = $order->getStatusHistories();
			$latestHistoryComment = array_pop($histories);
			if($latestHistoryComment){
				$comment = $latestHistoryComment->getComment();
				if($comment =='Order was placed using Mobile App.'){
					$appOrder = $appOrder + 1;
					$source = "APP";
				}else{
					$websiteOrder = $websiteOrder + 1;
					$source = "WEB";
				}
			}
			$orderArray["increment_id"] = $order->getIncrementId();
			$orderArray["created_at"] = $order->getCreatedAt();
			if($order->getStoreId() == 1){
				$store = "English";
			}else{
				$store = "Arabic";
			}
			$orderArray["website"] = $store;
			$orderArray["source"] = $source;
			fputcsv($fp, $orderArray);
		}
		fclose($fp);
        $output->writeln("Exporting done pls check the csv file  " . $jsonFilePath);
    }
 
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("report:order");
        $this->setDescription("Export customer list in CSV with no order");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Csv Name")
        ]);
        parent::configure();
    }
	public function getColumnHeader() {
        $headers = ['Email','First Name','Last Name','Phone'];
        return $headers;
    }
}