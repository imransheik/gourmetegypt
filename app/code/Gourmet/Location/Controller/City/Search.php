<?php
namespace Gourmet\Location\Controller\City;
class Search extends \Magento\Framework\App\Action\Action
{
	public function execute()
    {
		$getKeyword = $_GET["keyword"];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager= $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$storeurl    = $storeManager->getStore()->getUrl('stores/store/switch/');
		$model = $objectManager->create('\Gourmet\Location\Model\Location');
		$locationCollection = $model->getCollection();
		$locationCollection->getSelect()->where(" city LIKE '%".$getKeyword."%' OR city LIKE '%".$getKeyword."' OR city LIKE '".$getKeyword."%' ");
		?>
		<ul id="country-list">
		<?php
		foreach($locationCollection as $data){
		?>
		<?php
			$cityid = $data->getCityId();
			$storeid = $data->getStoreId();
			$regionid = $data->getRegionId();
		?>
		<li onClick="selectCountry('<?php echo $data->getCity(); ?>','<?php echo $storeurl."?location=".$cityid."&store=".$storeid."&region=".$regionid ?>');" url="<?php echo $storeurl."?location=".$cityid."&store=".$storeid."&region=".$regionid ?>"><?php echo $data->getCity(); ?></li>
		<?php } ?>
		</ul>
		<?php
		return;
    }
}
?>