<?php
namespace Gourmet\Location\Controller\City;
class View extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $getId = $_GET['id'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$model = $objectManager->create('\Gourmet\Location\Model\Location');
		$locationCollection = $model->getCollection()->addFieldToFilter('region_id', $getId);
		$html = '<option value="">'. __('Please select a Location.').'</option>';		
		foreach($locationCollection as $data){
			$html .= '<option value="' .$data->getCity(). '"> ' . __($data->getCity()) . '</option>';
		}
		echo $html;
    }
}