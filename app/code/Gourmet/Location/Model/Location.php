<?php
 
namespace Gourmet\Location\Model;
 
//use Magento\Framework\Model\AbstractModel;
 
class Location extends \Magento\Framework\Model\AbstractModel
{
	/**
	 * Define resource model
	 */
	protected function _construct()
	{
		$this->_init('Gourmet\Location\Model\ResourceModel\Location');
	}
}