<?php
 
namespace Gourmet\Location\Model\ResourceModel;
 
//use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Location extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	/**
	 * Define main table
	 */
	protected function _construct()
	{
		$this->_init('directory_region_city', 'city_id');
	}
}