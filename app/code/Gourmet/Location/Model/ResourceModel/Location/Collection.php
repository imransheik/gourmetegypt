<?php
 
namespace Gourmet\Location\Model\ResourceModel\Location;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
      protected $_idFieldName = 'city_id';
	/**
	 * Define model & resource model
	 */
	protected function _construct()
	{
		$this->_init(
			'Gourmet\Location\Model\Location',
			'Gourmet\Location\Model\ResourceModel\Location'
		);
	}
}