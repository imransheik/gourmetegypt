<?php

namespace Gourmet\MultiWishlist\Block\Account;

class Listing extends \Magento\Framework\View\Element\Template
{
	protected $_wishlistModel;
	protected $_gourmetWishlistModel;
    protected $_gourmetMapResource;
	protected $_currentCustomer;
    protected $_formKey;

    public function __construct(
    	\Magento\Framework\View\Element\Template\Context $context,
    	\Magento\Wishlist\Model\ResourceModel\Wishlist\CollectionFactory $wishlistModel,
    	\Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlist\CollectionFactory $gourmetWishlistModel,
        \Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap $gourmetMapResource,
    	\Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\Data\Form\FormKey $formKey,
    	array $data = []
	){
		$this->_wishlistModel = $wishlistModel;
		$this->_gourmetWishlistModel = $gourmetWishlistModel;
        $this->_gourmetMapResource = $gourmetMapResource;
        $this->_currentCustomer = $currentCustomer;
        $this->_formKey = $formKey;

        parent::__construct($context, $data);
    }


    public function getCustomerWishlists()
    {
    	$wishlists = null;
    	if($customerId = $this->_currentCustomer->getCustomerId()){
    		$wishtable = $this->_wishlistModel->create()->getResource()->getMainTable();
    		$wishlists = $this->_gourmetWishlistModel->create();
    		$wishlists->getSelect()
    					->joinInner(array('wish' => $wishtable), 'wish.wishlist_id=main_table.wishlist_id', array())
    					->where('wish.customer_id=?', $customerId);
    	}
    	
    	return $wishlists;
    }

    public function isRedirectToCartEnabled()
    {
        return $this->_scopeConfig->getValue(
            'checkout/cart/redirect_to_cart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getFormKey()
    {
        return $this->_formKey->getFormKey();
    }
}