<?php

namespace Gourmet\MultiWishlist\Block\Account;

class Products extends \Magento\Catalog\Block\Product\AbstractProduct
{
	protected $_wishId;
	protected $_currentCustomer;
	protected $_productCollection;
	protected $_productStatus;
	protected $_productVisibility;
	protected $_wishMapResource;

    public function __construct(
    	\Magento\Catalog\Block\Product\Context $context,
    	\Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
    	\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
    	\Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
    	\Magento\Catalog\Model\Product\Visibility $productVisibility,
    	\Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap $wishMapResource,
        \Magento\Framework\Url\Helper\Data $urlHelper,
    	array $data = []
	){
		$this->_currentCustomer = $currentCustomer;
		$this->_productCollection = $productCollection;
		$this->_productStatus = $productStatus;
		$this->_productVisibility = $productVisibility;
		$this->_wishMapResource = $wishMapResource;
        $this->urlHelper = $urlHelper;

        parent::__construct($context, $data);
    }

    public function getWishlistProducts()
    {
    	$wishId = $this->getWishlistId();
    	$customerId = $this->_currentCustomer->getCustomerId();
		$wishMapTable = $this->_wishMapResource->getMainTable();

		if($customerId && $wishId){
			$collection = $this->_productCollection->create()
							->addFinalPrice()
							->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()])
    						->setVisibility($this->_productVisibility->getVisibleInSiteIds())
    						->addAttributeToSort('name', 'asc');
			
			$collection->getSelect()
				    		->joinInner(array('wishmap' => $wishMapTable), 'wishmap.product_id=e.entity_id AND wishmap.multi_id='.$wishId, array());

			return $collection;
		}
		return null;
    }

    public function setWishlistId($wishId)
    {
    	$this->_wishId = $wishId;
    	return $this;
    }

    public function getWishlistId()
    {
    	return $this->_wishId;
    }

    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
}