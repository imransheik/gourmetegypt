<?php

namespace Gourmet\MultiWishlist\Block\Account;

class Shared extends \Magento\Framework\View\Element\Template
{
    protected $_gourmetWishlist;
    protected $_formKey;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlist $gourmetWishlist,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    ){
        $this->_gourmetWishlist = $gourmetWishlist;
        $this->_formKey = $formKey;

        parent::__construct($context, $data);
    }

    public function getCurrentWishlist()
    {
        $share_code = $data = $this->getRequest()->getParam('code',0);
        return $this->_gourmetWishlist->loadCode($share_code);
    }

    public function isRedirectToCartEnabled()
    {
        return $this->_scopeConfig->getValue( 'checkout/cart/redirect_to_cart', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFormKey()
    {
        return $this->_formKey->getFormKey();
    }
}