<?php

namespace Gourmet\MultiWishlist\Block;

use Magento\Wishlist\Block\Catalog\Product\ProductList\Item\AddTo\Wishlist;
use Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlist\CollectionFactory as GourmetWishCollectionFactory;
use Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap\CollectionFactory as GourmetWishMapCollectionFactory;


class GourmetWishlist extends Wishlist
{
	private $_wishHelper;
    private $_multiwish;
    private $_multiwishmap;
    private $_labels;

	public function __construct(\Magento\Catalog\Block\Product\Context $context, 
        \Gourmet\MultiWishlist\Helper\Data $wishHelper, 
        \Gourmet\Assist\Block\Labels $labels,
        GourmetWishCollectionFactory $multiwish, 
        GourmetWishMapCollectionFactory $multiwishmap, 
        array $data = []
    ){
        $this->_wishHelper = $wishHelper;
        $this->_multiwish = $multiwish;
        $this->_multiwishmap = $multiwishmap;
        $this->_labels = $labels;
        parent::__construct($context, $data);	 	
	 }


    public function shouldShow()
    {
        return $this->_wishHelper->isEnabled();
    }

    public function isCustomerLoggedIn()
    {
        return $this->_wishHelper->isCustomerLoggedIn();
    }

    public function getAddToWishlistParams($product)
    {
        return $this->_wishHelper->getAddParams($product);
    }

    public function isWishlistExist($product_id)
    {
        return $this->_labels->isWishlistItem($product_id);
    }


    public function getProduct()
    {
        $product = parent::getProduct();
        if (null === $product) {
            if($prd = $this->_coreRegistry->registry('product')){
                $this->setProduct($prd);
                return $prd;
            }
        }
        return $product;
    }

    public function getUserList()
    {
        if($this->isCustomerLoggedIn()){
            $wishlistId = $this->_wishHelper->getWishlist()->getId();    

            $multiCollection = $this->_multiwish->create();
            $items = $multiCollection->addFieldToFilter('wishlist_id', $wishlistId)->getItems();

            if(empty($items)){
                // create dafault wishlist
                $wishModel = $this->_multiwish->create()->getNewEmptyItem()
                                        ->setData('wishlist_id', $wishlistId)
                                        ->setData('multi_wishlist_name', 'My Wishlist')
                                        ->save();
            
                return $this->getUserList();
            }

            return $items;
        } else{
            return null;
        }
        
    }
    
}