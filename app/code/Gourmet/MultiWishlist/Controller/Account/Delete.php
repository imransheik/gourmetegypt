<?php

namespace Gourmet\MultiWishlist\Controller\Account;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;

class Delete extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    protected $_multiwishlist;
    protected $_mapwishlist;

    public function __construct(
        Context $context, 
        \Magento\Customer\Model\Session $customerSession,
        \Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlist $multiwishlist,
        \Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap $mapwishlist
    ){
        $this->_customerSession = $customerSession;
        $this->_multiwishlist = $multiwishlist;
        $this->_mapwishlist = $mapwishlist;
        parent::__construct($context);
    }
    
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $reponse['error'][] = "You session has expired. Please login again";;
        }
        return parent::dispatch($request);
    }

	public function execute()
	{
        $data = $this->getRequest()->getParams();
        if ($this->getRequest()->isXmlHttpRequest() && $data) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $reponse = array();
            if($wishid = (int) $data['wishid']){
                $deleteItems = $this->_mapwishlist->checkAndDelete($wishid);
                $deleteWish = $this->_multiwishlist->deleteWishlist($wishid);

                $reponse['success'][] = "Wishlist successfully deleted";
            }

            if(empty($reponse)){
                $reponse['error'][] = 'Sorry, there was an error deleting wishlist. Please try later';
            }

            $result->setData($reponse);
            return $result;
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('');
        return $resultRedirect;
	}
}