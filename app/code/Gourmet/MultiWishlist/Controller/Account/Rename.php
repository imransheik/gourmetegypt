<?php

namespace Gourmet\MultiWishlist\Controller\Account;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;

class Rename extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    protected $_multiwishlist;

    public function __construct(
        Context $context, 
        \Magento\Customer\Model\Session $customerSession,
        \Gourmet\MultiWishlist\Model\MultiWishlistFactory $multiwishlist
    ){
        $this->_customerSession = $customerSession;
        $this->_multiwishlist = $multiwishlist;
        parent::__construct($context);
    }
    
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $reponse['error'][] = "You session has expired. Please login again";;
        }
        return parent::dispatch($request);
    }

	public function execute()
	{
        $data = $this->getRequest()->getParams();
        if ($this->getRequest()->isXmlHttpRequest() && $data) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $reponse = array();
            if($wishid = (int) $data['wishid']){
                $name = trim($data['name']);
                $name = preg_replace('/[^a-z\d ]/i', '', $name);
                if(!empty($name)){
                    $wishModel = $this->_multiwishlist->create()->load($wishid);
                    if($wishModel->getId()){
                        $old_name = $wishModel->getMultiWishlistName();
                        $wishModel->setData('multi_wishlist_name', $name)->save();
                        $reponse['name'] = $name;
                        $reponse['success'][] = $old_name . ' changed to ' . $name;
                    }
                }
            }

            if(empty($reponse)){
                $reponse['error'][] = 'Sorry, name can\'t be changed now';
            }

            $result->setData($reponse);
            return $result;
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('');
        return $resultRedirect;
	}
}