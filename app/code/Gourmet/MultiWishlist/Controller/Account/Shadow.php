<?php

namespace Gourmet\MultiWishlist\Controller\Account;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;

class Shadow extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    protected $_multiwishlist;
    protected $_mapwishlist;

    public function __construct(
        Context $context, 
        \Magento\Customer\Model\Session $customerSession,
        \Gourmet\MultiWishlist\Model\MultiWishlistFactory $multiwishlist,
        \Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap $mapwishlist
    ){
        $this->_customerSession = $customerSession;
        $this->_multiwishlist = $multiwishlist;
        $this->_mapwishlist = $mapwishlist;
        parent::__construct($context);
    }
    
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $reponse['error'][] = "You session has expired. Please login again";;
        }
        return parent::dispatch($request);
    }

	public function execute()
	{
        $data = $this->getRequest()->getParams();
        if ($this->getRequest()->isXmlHttpRequest() && $data && isset($data['type'])) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $reponse = array();

            switch ($data['type']) {
                case 'copy':
                    $wish_id = (int) $data['wishid'];
                    $prod_id = (int) $data['prodid'];  
                    $wish_to = (int) $data['wish_to'];

                    if($wish_id && $prod_id && $wish_to){
                        if($this->_mapwishlist->insertMapItem($wish_id, $wish_to, $prod_id)){
                            $reponse['success'][] = "Product successfully copied";
                        }
                    }
                break;

                case 'move':
                    $wish_id = (int) $data['wishid'];
                    $prod_id = (int) $data['prodid'];  
                    $wish_to = (int) $data['wish_to'];

                    if($wish_id && $prod_id && $wish_to){
                        if($this->_mapwishlist->updateMapItem($wish_id, $wish_to, $prod_id)){
                            $reponse['success'][] = "Product successfully moved";
                        }
                    }
                break;                
                
                case 'remove':
                    $wish_id = (int) $data['wishid'];
                    $prod_id = (int) $data['prodid'];                    
                    if($wish_id && $prod_id){
                        if($this->_mapwishlist->deleteMapItem($wish_id, $prod_id)){
                            $reponse['success'][] = "Product successfully removed";
                        }
                    }
                break;

                default:
                break;
            }


            if(empty($reponse)){
                $reponse['error'][] = 'Sorry, there was an error. Please try later.';
            }

            $result->setData($reponse);
            return $result;
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('');
        return $resultRedirect;
	}
}