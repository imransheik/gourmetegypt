<?php

namespace Gourmet\MultiWishlist\Controller\Account;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Layout as ResultLayout;
use Magento\Framework\App\Action\Context;

class Share extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    protected $_customerHelperView;
    protected $_storeManager;
    protected $_url;
    protected $_multiwishlist;
    protected $_scopeConfig;
    protected $_transportBuilder;

    const XML_PATH_WISHLIST_SHARE_TEMPLATE = 'gourmet_multiwishlist/general/template';

    public function __construct(
        Context $context, 
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Helper\View $customerHelperView,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Gourmet\MultiWishlist\Model\MultiWishlist $multiwishlist
    ){
        $this->_customerSession = $customerSession;
        $this->_customerHelperView = $customerHelperView;
        $this->_multiwishlist = $multiwishlist;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }
    
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $reponse['error'][] = "You session has expired. Please login again";;
        }
        return parent::dispatch($request);
    }

	public function execute()
	{
        $data = $this->getRequest()->getParams();
        if ($this->getRequest()->isXmlHttpRequest() && $data) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $reponse = array();
            if($wishid = (int) $data['wishid']){
                $wishlist = $this->_multiwishlist->load($wishid);
                if($wishlist->getId()){
                    if(is_null($wishlist->getShareCode())){
                        $share_code = \uniqid();
                        $wishlist->setShareCode($share_code);
                        $wishlist->save();
                    } else{
                        $share_code = $wishlist->getShareCode();
                    }

                    $emails = $data['emails'];
                    $message = \strip_tags($data['msg']);
                    $emails = array_unique($emails);

                    $customer = $this->_customerSession->getCustomerDataObject();
                    $customerName = $this->_customerHelperView->getCustomerName($customer);

                    $store_id = $this->_storeManager->getStore()->getId();
                    $templateId = $this->_scopeConfig->getValue(self::XML_PATH_WISHLIST_SHARE_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);

                    $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
                    $this->addLayoutHandles($resultLayout);

                    $items = $resultLayout->getLayout()->getBlock('gourmet.wishlist.email.items')->setWishlistId($wishid)->toHtml();
                    try {
                        foreach ($emails as $email) {
                            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                        'store' => $store_id,
                                    ]
                                )->setTemplateVars(
                                    [
                                        'customer' => $customer,
                                        'customerName' => $customerName,
                                        'viewOnSiteLink' => $this->_url->getUrl('*/shared/wishlist', ['code' => $share_code]),
                                        'items' => $items,
                                        'message' => $message,
                                        'store' => $this->_storeManager->getStore(),
                                    ]
                                )->setFrom(
                                    $this->_scopeConfig->getValue(
                                        'wishlist/email/email_identity',
                                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                    )
                                )->addTo($email)
                                ->getTransport();

                            $transport->sendMessage();
                        }
                        $reponse['success'][] = "Emails sent successfully";
                    } catch (\Exception $e) {
                        //echo $e->getMessage();
                        $reponse['error'][] = 'Sorry, there was an error sharing wishlist. Please try later';
                    }    

                }
            }

            if(empty($reponse)){
                $reponse['error'][] = 'Sorry, there was an error sharing wishlist. Please try later';
            }

            $result->setData($reponse);
            return $result;
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('');
        return $resultRedirect;
	}

    protected function addLayoutHandles(ResultLayout $resultLayout)
    {
        $resultLayout->addHandle('gourmet_wishlist_account_shared');
    }

}