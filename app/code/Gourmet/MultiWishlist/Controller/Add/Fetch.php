<?php

namespace Gourmet\MultiWishlist\Controller\Add;

use Magento\Framework\Controller\ResultFactory;

class Fetch extends \Magento\Framework\App\Action\Action
{
	protected $_wishhelper;
	protected $_layoutFactory;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Gourmet\MultiWishlist\Helper\Data $wishhelper, \Magento\Framework\View\LayoutFactory $layoutFactory)
    {
    	$this->_wishhelper = $wishhelper;
    	$this->_layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

	public function execute()
	{

		$result = '';
		if($this->getRequest()->isAjax() && $this->_wishhelper->isEnabled()){
			
			$result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
	        $output = $this->_layoutFactory->create()
			            ->createBlock('Gourmet\MultiWishlist\Block\GourmetWishlist')
			            ->setTemplate('Gourmet_MultiWishlist::fetch.phtml')
			            ->toHtml();

            $result->setContents($output);
	        return $result;
		}

		return $result;
	}
}