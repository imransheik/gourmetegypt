<?php

namespace Gourmet\MultiWishlist\Controller\Add;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Wishlist\Controller\Index\Add
{

	public function execute()
	{

		if($this->_objectManager->get('Gourmet\MultiWishlist\Helper\Data')->isCustomerLoggedIn() &&
			$this->_objectManager->get('Gourmet\MultiWishlist\Helper\Data')->isEnabled()
		){
			$result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
			$parent = parent::execute();

			$reponse = array();
	        if($this->messageManager->hasMessages()){
	        	//$msgs = $this->messageManager->getMessages(true)->getItemsByType(\Magento\Framework\Message\MessageInterface::TYPE_SUCCESS);
	        	if($this->messageManager->getMessages(true)->getMessageByIdentifier('addProductSuccessMessage')){
			        if($this->_objectManager->get('Magento\Framework\Registry')->registry('gourmet_wish_map')) {
			            $success = $this->_objectManager->get('Magento\Framework\Registry')->registry('gourmet_wish_map');
			        } 
					if($this->_objectManager->get('Magento\Framework\Registry')->registry('gourmet_wish_already_map')) {
			            $success = $this->_objectManager->get('Magento\Framework\Registry')->registry('gourmet_wish_already_map');
			        }
	  				$reponse['success'][] = $success;
	        	} else{
	        		$errors = $this->messageManager->getMessages(true)->getErrors();
	        		foreach ($errors as $error) {
	        			$reponse['error'][] = $error;
	        		}
	        	}
	        }

	        $result->setData($reponse);
	        return $result;
		} else{
			return parent::execute(); 
		}

	}
}