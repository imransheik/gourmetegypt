<?php

namespace Gourmet\MultiWishlist\Controller\Index;
use Magento\Framework\App\RequestInterface;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $customerSession) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
    }
    
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $this->_redirect('customer/account/login');
        }
        return parent::dispatch($request);
    }

	public function execute()
	{
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('My Mutli Wishlists'));
        $this->_view->getPage()->getConfig()->addBodyClass('page-products');
        $this->_view->renderLayout();
	}
}