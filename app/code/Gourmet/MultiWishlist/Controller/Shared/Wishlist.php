<?php

namespace Gourmet\MultiWishlist\Controller\Shared;
use Magento\Framework\App\RequestInterface;

class Wishlist extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $customerSession) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
    }

	public function execute()
	{
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Shared Wishlist'));
        $this->_view->getPage()->getConfig()->addBodyClass('page-products');
        $this->_view->renderLayout();
	}
}