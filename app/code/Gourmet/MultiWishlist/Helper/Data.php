<?php

namespace Gourmet\MultiWishlist\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Wishlist\Helper\Data
{

    public function isEnabled()
    {
        return $this->scopeConfig->getValue('gourmet_multiwishlist/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    public function isCustomerLoggedIn()
    {
        return $this->_isCustomerLogIn();
    }


    public function getAddParams($item, array $params = [])
    {
        $productId = null;
        if ($item instanceof \Magento\Catalog\Model\Product) {
            $productId = $item->getEntityId();
        }
        if ($item instanceof \Magento\Wishlist\Model\Item) {
            $productId = $item->getProductId();
        }

        $url = $this->_getUrlStore($item)->getUrl('gourmet-wishlist/add/index');
        if ($productId) {
            $params['product'] = $productId;
        }

        return $this->_postDataHelper->getPostData($url, $params);
    }
}
