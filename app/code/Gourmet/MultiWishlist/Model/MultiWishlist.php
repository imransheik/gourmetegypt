<?php

namespace Gourmet\MultiWishlist\Model;

use Magento\Framework\Model\AbstractModel;


class MultiWishlist extends AbstractModel
{
	protected function _construct()
	{
		$this->_init(\Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlist::class);
	}	
}