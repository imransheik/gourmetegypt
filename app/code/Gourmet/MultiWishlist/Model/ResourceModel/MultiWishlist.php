<?php

namespace Gourmet\MultiWishlist\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class MultiWishlist extends AbstractDb
{
	protected $_mapTable;

	protected function _construct()
	{
		$this->_init('gourmet_multiwishlist', 'id');
		$this->_mapTable = $this->getTable('gourmet_multiwishlist');
	}

	public function deleteWishlist($wish_id)
	{
		if($wish_id){
			$connection = $this->getConnection();
            $condition = [
                'id = ?' => $wish_id
            ];
			$connection->delete($this->_mapTable, $condition);

			return true;
		}
		return false;
	}

	public function loadCode($code)
	{
		$wishlist = array();
		if($code){
			$connection = $this->getConnection();

			$bind = ['share_code' => trim($code)];
			$select = $connection->select()
									->from($this->_mapTable)
									->where('share_code = :share_code');
			$wishlist = $connection->fetchRow($select, $bind);
		}

		return $wishlist;
	}
}