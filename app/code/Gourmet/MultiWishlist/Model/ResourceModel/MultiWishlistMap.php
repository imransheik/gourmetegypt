<?php

namespace Gourmet\MultiWishlist\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class MultiWishlistMap extends AbstractDb
{
	protected $_mapTable;
	protected $_wishlistItem;
	protected $_wishlistOption;

    public function __construct(
    	\Magento\Framework\Model\ResourceModel\Db\Context $context,
    	\Magento\Wishlist\Model\ResourceModel\Item $wishlistItem,
    	\Magento\Wishlist\Model\ResourceModel\Item\Option $wishlistOption
    ){
    	$this->_wishlistItem = $wishlistItem;
    	$this->_wishlistOption = $wishlistOption;
        parent::__construct($context);
    }

	protected function _construct()
	{
		$this->_init('gourmet_multiwishlist_map', 'id');
		$this->_mapTable = $this->getTable('gourmet_multiwishlist_map');
	}

	public function insertMapItem($wish_id, $wish_to, $prod_id)
	{
		if($wish_id != $wish_to){
			$this->noCheckDeleteMapItem($wish_to, $prod_id);

			$connection = $this->getConnection();
	        $bind = [
	            'multi_id' => $wish_to,
	            'product_id' => $prod_id
	        ];

	        return $connection->insert($this->_mapTable, $bind);
		}
		return true;
	}

	public function updateMapItem($wish_id, $wish_to, $prod_id)
	{
		if($wish_id != $wish_to){
			$this->noCheckDeleteMapItem($wish_to, $prod_id);
			$connection = $this->getConnection();
	        $where = [
	            'multi_id = ?' => $wish_id,
	            'product_id = ?' => $prod_id
	        ];

	        return $connection->update($this->_mapTable, ['multi_id' => $wish_to], $where);
		}
		return true;
	}

	public function noCheckDeleteMapItem($wish_id, $prod_id)
	{
		if($wish_id && $prod_id){
			$connection = $this->getConnection();

            $condition = [
                'multi_id = ?' => $wish_id,
                'product_id = ?' => $prod_id
            ];
			$connection->delete($this->_mapTable, $condition);
			return true;
		}

		return false;
	}

	/*
	* currently skipping store check
	*/
	public function deleteMapItem($wish_id, $prod_id)
	{
		if($wish_id && $prod_id){
			$connection = $this->getConnection();

            $condition = [
                'multi_id = ?' => $wish_id,
                'product_id = ?' => $prod_id
            ];
			$connection->delete($this->_mapTable, $condition);

			// check if product id still exists else remove from wishlist too
			$bind = ['prod_id' => (int)$prod_id];
			$select = $connection->select()
									->from($this->_mapTable, 'COUNT(*)')
									->where('product_id = :prod_id');
			$count = $connection->fetchOne($select, $bind);
			if(!$count){
				// delete from wishlist_item and wishlist_item_option
	            $condition = [
	                'product_id = ?' => $prod_id
	            ];
	            $wishlistItemTable = $this->_wishlistItem->getMainTable();
				$connection->delete($wishlistItemTable, $condition);

				$wishlistOptionTable = $this->_wishlistOption->getMainTable();
				$connection->delete($wishlistOptionTable, $condition);
			}

			return true;
		}

		return false;
	}

	// delete wishlist product, remove references
	public function checkAndDelete($wish_id)
	{
		if($wish_id){
			$connection = $this->getConnection();

			$bind = ['multi_id' => (int)$wish_id];
			$select = $connection->select()
									->from($this->_mapTable, 'product_id')
									->where('multi_id = :multi_id');
			$products = $connection->fetchCol($select, $bind);

			foreach($products as $prod_id){
				$this->deleteMapItem($wish_id, $prod_id);
			}

			return true;
		}

		return false;
	}

	public function deleteDuplicates()
	{


		exit('rascal');
	}
}