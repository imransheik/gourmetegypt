<?php

namespace Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

	protected $_idFieldName = 'id';

    protected function _construct()
    {
    	$this->_init('Gourmet\MultiWishlist\Model\MultiWishlistMap', 'Gourmet\MultiWishlist\Model\ResourceModel\MultiWishlistMap');
    }
}