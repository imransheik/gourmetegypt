<?php

namespace Gourmet\MultiWishlist\Observer;

use Gourmet\MultiWishlist\Model\MultiWishlistFactory;
use Gourmet\MultiWishlist\Model\MultiWishlistMapFactory;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;

class ProductAdded implements ObserverInterface
{

    protected $_multiWishlist;
    protected $_mapWishlist;
    protected $_request;
    protected $_wishRegistry;

    public function __construct(MultiWishlistFactory $multiWishlistFactory, MultiWishlistMapFactory $multiWishlistMapFactory, RequestInterface $request, Registry $wishRegistry) {
        $this->_multiWishlist = $multiWishlistFactory;
        $this->_mapWishlist = $multiWishlistMapFactory;
        $this->_request = $request;
        $this->_wishRegistry = $wishRegistry;
    }

    public function execute(EventObserver $observer)
    {


      $wishlist = $observer->getWishlist();
      $product = $observer->getProduct();
      $item = $observer->getItem();

      $wish = (int) $this->_request->getParam('wish');
      // add wishname
      if(!$wish){
        $name = $this->_request->getParam('wishname','Custom Wishlist');

        $wishCollection = $this->_multiWishlist->create()->getCollection()
                            ->addFieldToFilter('wishlist_id',$wishlist->getId())
                            ->addFieldToFilter('multi_wishlist_name', $name)
                            ->getFirstItem();

        if($wishCollection->getId()){
            $wish = $wishCollection->getId();
        }else{
          $wishModel = $this->_multiWishlist->create();
          $wishModel->setData('wishlist_id', $wishlist->getId())
                     ->setData('multi_wishlist_name', $name)
                     ->save();

          $wish = $wishModel->getId();          
        }
      }

      // assign product
        $wishMapCollection = $this->_mapWishlist->create()->getCollection()
                                ->addFieldToFilter('multi_id',$wish)
                                ->addFieldToFilter('product_id', $product->getId())
                                ->getFirstItem();
		
		if(count($wishMapCollection->getData()) > 0){
			$this->_wishRegistry->register('gourmet_wish_already_map', 'This product already added to this wishlist!');
			return false;
		}

        if(!$wishMapCollection->getId()){
            $wishMapMpdel = $this->_mapWishlist->create();
            $wishMapMpdel->setData('multi_id', $wish)
                       ->setData('product_id', $product->getId())
                       ->save();
        }
		
        if (!$this->_wishRegistry->registry('gourmet_wish_map')) {
            $this->_wishRegistry->register('gourmet_wish_map', $product->getName() . ' added to wishlist!');
        }
		
		
		

      return $this;
    }
}
