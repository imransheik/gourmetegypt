<?php

namespace Gourmet\MultiWishlist\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
	
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		
	// multi wishlist
		$table = $setup->getConnection()->newTable(
			$setup->getTable('gourmet_multiwishlist')
		)->addColumn(
			'id',
			Table::TYPE_INTEGER,
			null,
			['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
		)->addColumn(
			'wishlist_id',
			Table::TYPE_INTEGER,
			11,
			['unsigned' => true, 'nullable' => false],
			'Wishlist Id'
		)->addColumn(
			'multi_wishlist_name',
			Table::TYPE_TEXT,
			200,
			['nullable' => true],
			'Wishlist Name'
		)->setComment('Multiple Wishlist');
		$setup->getConnection()->createTable($table);

	// multi wishlist map
		$table = $setup->getConnection()->newTable(
			$setup->getTable('gourmet_multiwishlist_map')
		)->addColumn(
			'multi_id',
			Table::TYPE_INTEGER,
			10,
			['unsigned' => true, 'nullable' => false],
			'Multi Wishlist Id'
		)->addColumn(
			'product_id',
			Table::TYPE_INTEGER,
			null,
			['unsigned' => true, 'nullable' => false],
			'Product Id'
		)->addIndex(
			// generates unique index name
			$setup->getIdxName('gourmet_multiwishlist_map', ['multi_id']), ['multi_id']
		)->addForeignKey(
            $setup->getFkName($setup->getTable('gourmet_multiwishlist_map'), 'multi_id', $setup->getTable('gourmet_multiwishlist'), 'id'),
            'multi_id',
            $setup->getTable('gourmet_multiwishlist'),
            'id',
            Table::ACTION_CASCADE
        )->setComment('Multiple Wishlist Map');
		$setup->getConnection()->createTable($table);

		$setup->endSetup();	
	}	
}