<?php

namespace Gourmet\MultiWishlist\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		if(version_compare($context->getVersion(), '1.0.1', '<')){
			$setup->getConnection()->addColumn(
				$setup->getTable('gourmet_multiwishlist'),
				'share_code',
				[
					'type' => Table::TYPE_TEXT,
					'nullable' => true,
					'comment' => 'Shared Code'
				]
			);
		}

		if(version_compare($context->getVersion(), '1.0.3', '<')){
			$setup->getConnection()->addForeignKey(
	            $setup->getFkName('gourmet_multiwishlist_map','product_id','wishlist_item', 'product_id'),
	            $setup->getTable('gourmet_multiwishlist_map'),
	            'product_id',
	            $setup->getTable('wishlist_item'),
	            'product_id',
	            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
	        );

		}

		if(version_compare($context->getVersion(), '1.0.4', '<')){
            $connection = $setup->getConnection();

            $connection->addIndex(
                $setup->getTable('gourmet_multiwishlist_map'),
                $setup->getIdxName(
                    'gourmet_multiwishlist_map',
                    ['multi_id', 'product_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['multi_id', 'product_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            );
		}
		$setup->endSetup();
	}
}