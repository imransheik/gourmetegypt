var config = {
	deps: [
		"Gourmet_MultiWishlist/js/multiwishlist"
	],
    map: {
        '*': {
        	gourmetAccount: 'Gourmet_MultiWishlist/js/account'
        }
    },
	shim:{
        gourmetAccount:{
            'deps':['jquery']
        }
    }
};