define([
  'jquery',
  'Magento_Ui/js/modal/modal',
  'Magento_Ui/js/modal/alert',
  'Magento_Ui/js/modal/confirm',
  'Magento_Ui/js/lib/spinner',
  'jquery/ui',
  "domReady!"
], 
function($, modal, uialert, uiconfirm, loader) {
	'use strict';

	var config = {};

	var registry = {
        data: [],
        set: function (key, value) {
            this.data[key] = value;
        },
        get: function (key) {
            return this.data[key];
        }
    };

    function populateAlterList(type)
    {
    	if(type === 1){
	    	$('#copy-wrapper .msgs').empty();
	    	$('#copy-wrapper select').empty();
			$.each(config.wishlists, function(v,t) {
			    $('<option>').val(v).text(t).appendTo($('#copy-wrapper select'));
			});
    	}else{
			$('#move-wrapper .msgs').empty();
			$('#move-wrapper select').empty();
			$.each(config.wishlists, function(v,t) {
			    $('<option>').val(v).text(t).appendTo($('#move-wrapper select'));
			});
    	}

    }

    function reloadWishlist(wish_id)
    {
    	setTimeout(function(){ 
    		window.location.reload();
    	}, 5000);
    }

    // wishlist section
	$('.customer-wishlist-wrapper').on('click', '.actions-toolbar button', function(){
		var t = $(this).attr('data-action');
		registry.set('current_wish', $(this).closest('.customer-wishlist'));

		if(t == 'rename'){
			$('#list-rename').modal({
				title: 'Rename wishlist',
				modalClass: 'confirm wishlist-popup',
	            buttons: [{
	                text: 'Cancel',
	                class: 'action-secondary',
	                click: function () {
	                    this.closeModal();
	                }
	            }, {
	                text: 'Rename',
	                class: 'action-primary',
	                click: function () {
	                	var self = this;
	                	var rm = this.element;
	                	var inp = $(rm).find('input[name="rename"]').val();
	                	var current_wish = registry.get('current_wish');
	                	var wishid = $(current_wish).attr('data-id');

	                	if(inp === ''){
	                		uialert({content: "Please enter name"});
	                	} else{
	                        $.ajax({
	                            url: config.rename,
	                            method: 'POST',
	                            data: {"wishid":wishid, "name":inp, "formkey":config.formkey},
	                            showLoader: true
	                        }).done(function (data) {
	                            if(data.success) {
	                            	$(current_wish).find('h1').html(data.name);
	                            	$(data.success).each(function(i,msg){
	                            		$('<span/>',{'class':'success'}).html(msg).appendTo($(rm).find('.msgs'));
	                            	});
	                            	config.wishlists[wishid] = data.name;
	                            }

								if(data.error) {
	                            	$(data.error).each(function(i,msg){
	                            		$('<span/>',{'class':'error'}).html(msg).appendTo($(rm).find('.msgs'));
	                            	});
	                            }

	                            //setTimeout(function(){ self.closeModal(); }, 3000);
	                        }).fail(function (jqXHR, textStatus) {
	                            if (window.console) {
	                                console.log(textStatus);
	                                uialert({content: "Sorry there was an error processing. Please try later."});
	                            }
	                        });	
	                	}
	                }
	            }],
                closed: function (e, modal) {
                	$('#list-rename .msgs').empty();
                	$('#list-rename input').val('');
                }
			}).trigger('openModal');
		} else if(t == 'delete'){
	        var wishlist_id = $(this).closest('.customer-wishlist').attr('data-id');
	        uiconfirm({
	            content: 'Are you sure you want to remove wishlist and all its products?',
	            actions: {
	                confirm: function () {
                        $.ajax({
                            url: config.delete,
                            method: 'POST',
                            data: {"wishid":wishlist_id, "formkey":config.formkey},
                            showLoader: true
                        }).done(function (data) {
                            if(data.success) {
                            	uialert({content: "Wishlist successfully removed"});
                            	reloadWishlist(wishlist_id);
                            }

							if(data.error) {
                            	uialert({content: "Sorry there was an error processing. Please try later."});
                            }

                        }).fail(function (jqXHR, textStatus) {
                            if (window.console) {
                                console.log(textStatus);
                                uialert({content: "Sorry there was an error processing. Please try later."});
                            }
                        });
	                }
	            }
	        });
		} else if(t == 'share'){
			$('#list-share').modal({
				title: 'Share wishlist',
				modalClass: 'confirm wishlist-popup',
	            buttons: [{
	                text: 'Cancel',
	                class: 'action-secondary',
	                click: function () {
	                    this.closeModal();
	                }
	            }, {
	                text: 'Share',
	                class: 'action-primary',
	                click: function () {
	                	var self = this;
	                	var rm = this.element;
	                	var msg = '';

	                	var current_wish = registry.get('current_wish');
	                	var wishid = $(current_wish).attr('data-id');

	                	var share_msg = $(rm).find('textarea[name="share-msg"]').val();
	                	share_msg = $.trim(share_msg);

						var share_em = $(rm).find('textarea[name="share-email"]').val();
	                	var share_split = share_em.split(",");
	                	var emails = [];
	                	var reg = /\S+@\S+\.\S+/;
	                	$.each(share_split, function(i,v){
	                		var ml=$.trim(v);
	                		if(reg.test(ml)){
	                			emails.push(ml);
	                		}
	                	});

	                	if(emails.length && (share_msg !== '')){
	                		var joined = emails.join();
	                		$(rm).find('textarea[name="share-email"]').val(joined);
	                        $.ajax({
	                            url: config.share,
	                            method: 'POST',
	                            data: {"wishid":wishid, "emails":emails, "msg":share_msg, "formkey":config.formkey},
	                            showLoader: true
	                        }).done(function (data) {
	                            if(data.success) {
	                            	$(data.success).each(function(i,msg){
	                            		$('<span/>',{'class':'success'}).html(msg).appendTo($(rm).find('.msgs'));
	                            	});
	                            }

								if(data.error) {
	                            	$(data.error).each(function(i,msg){
	                            		$('<span/>',{'class':'error'}).html(msg).appendTo($(rm).find('.msgs'));
	                            	});
	                            }

								//setTimeout(function(){ self.closeModal(); }, 3000);
	                        }).fail(function (jqXHR, textStatus) {
	                            if (window.console) {
	                                console.log(textStatus);
	                                uialert({content: "Sorry there was an error processing. Please try later."});
	                            }
	                        });	
	                	}else{
	                		if(!emails.length){
	                			msg = 'Please enter valid emails';
	                		} else{
	                			msg = 'Please enter message';
	                		}
	                		uialert({content:msg});
	                	}
	                }
	            }],
                closed: function (e, modal) {
                	$('#list-share .msgs').empty();
                	$('#list-share textare').html('');
                }
			}).trigger('openModal');
		}
	});

	// products section
	$('.customer-wishlist-wrapper').on('click', '#wishlist-actions', function(){
		var select_dd = this;
		var wishlist_id = $(this).closest('.customer-wishlist').attr('data-id');
		var prod = $(this).closest('li.product-item');
		var prodmatch = $(prod).attr("class").match(/\prod\-swatch\-([\d]*)\b/);
		
		if(prodmatch[1]){
			var prodid = parseInt(prodmatch[1]);

			registry.set('alter_wish_wish', wishlist_id);
			registry.set('alter_wish_prod', prodid);
			var sel = $(this).val();
			switch(sel){
				case 'copy':
					$('#list-copy').modal({
						title: 'Copy Product',
						modalClass: 'confirm wishlist-popup',
		                opened: function () {
		                	populateAlterList(1);
		                },
			            buttons: [{
			                text: 'Cancel',
			                class: 'action-secondary',
			                click: function () {
			                    this.closeModal();
			                }
			            }, {
			                text: 'Copy',
			                class: 'action-primary',
			                click: function () {
			                	var self = this;
			                	var rm = this.element;
			                	var wish_to = $(rm).find('select').val();

			                	if(wish_to === ''){
			                		uialert({content: "Please select wishlist"});
			                	}else{
			                		var dt = {
			                			"type":"copy", 
			                			"wishid":registry.get('alter_wish_wish'), 
			                			"prodid":registry.get('alter_wish_prod'), 
			                			"wish_to":wish_to, 
			                			"formkey":config.formkey
			                		};
			                        $.ajax({
			                            url: config.shadow,
			                            method: 'POST',
			                            data: dt,
			                            showLoader: true
			                        }).done(function (data) {
			                            if(data.success) {
			                            	$(data.success).each(function(i,msg){
			                            		$('<span/>',{'class':'success'}).html(msg).appendTo($(rm).find('.msgs'));
			                            	});
			                            	wishlist_id = registry.get('alter_wish_wish');
			                            	reloadWishlist(wishlist_id);
			                            }

										if(data.error) {
			                            	$(data.error).each(function(i,msg){
			                            		$('<span/>',{'class':'error'}).html(msg).appendTo($(rm).find('.msgs'));
			                            	});
			                            }

			                            //setTimeout(function(){ self.closeModal(); }, 3000);
			                        }).fail(function (jqXHR, textStatus) {
			                            if (window.console) {
			                                console.log(textStatus);
			                                uialert({content: "Sorry there was an error processing. Please try later."});
			                            }
			                        });
			                	}
			                }
			            }]
					}).trigger('openModal');
				break;
				case 'move':
					$('#list-move').modal({
						title: 'Move Product',
						modalClass: 'confirm wishlist-popup',
		                opened: function () {
		                	populateAlterList(2);
		                },
			            buttons: [{
			                text: 'Cancel',
			                class: 'action-secondary',
			                click: function () {
			                    this.closeModal();
			                }
			            }, {
			                text: 'Move',
			                class: 'action-primary',
			                click: function () {
			                	var self = this;
			                	var rm = self.element;
			                	var wish_to = $(rm).find('select').val();

			                	if(wish_to === ''){
			                		uialert({content: "Please select wishlist"});
			                	}else{
			                		var dt = {
			                			"type":"move", 
			                			"wishid":registry.get('alter_wish_wish'), 
			                			"prodid":registry.get('alter_wish_prod'), 
			                			"wish_to":wish_to, 
			                			"formkey":config.formkey
			                		};
			                        $.ajax({
			                            url: config.shadow,
			                            method: 'POST',
			                            data: dt,
			                            showLoader: true
			                        }).done(function (data) {
			                            if(data.success) {
			                            	$(data.success).each(function(i,msg){
			                            		$('<span/>',{'class':'success'}).html(msg).appendTo($(rm).find('.msgs'));
			                            	});
			                            	wishlist_id = registry.get('alter_wish_wish');
			                            	reloadWishlist(wishlist_id);
			                            }

										if(data.error) {
											$(data.error).each(function(i,msg){
												$('<span/>',{'class':'error'}).html(msg).appendTo($(rm).find('.msgs'));
											});
			                            }

			                            //setTimeout(function(){ self.closeModal(); }, 3000);
			                        }).fail(function (jqXHR, textStatus) {
			                            if (window.console) {
			                                console.log(textStatus);
			                                uialert({content: "Sorry there was an error processing. Please try later."});
			                            }
			                        });
			                	}
			                }
			            }]
					}).trigger('openModal');
				break;
				case 'remove':
			        uiconfirm({
						modalClass:'modal__small',
			            content: 'Are you sure you want to delete this product?',
			            actions: {
			                confirm: function () {
		                        $.ajax({
		                            url: config.shadow,
		                            method: 'POST',
		                            data: {"type":"remove", "wishid":wishlist_id, "prodid":prodid, "formkey":config.formkey},
		                            showLoader: true
		                        }).done(function (data) {
		                            if(data.success) {
		                            	uialert({modalClass:'modal__small',content: "Product removed"});
		                            	reloadWishlist(wishlist_id);
		                            }

									if(data.error) {
										uialert({modalClass:'modal__small', content: "Sorry there was an error processing. Please try later."});
		                            }

		                        }).fail(function (jqXHR, textStatus) {
		                            if (window.console) {
		                                console.log(textStatus);
		                                uialert({modalClass:'modal__small', content: "Sorry there was an error processing. Please try later."});
		                            }
		                        });
			                }
			            }
			        });
				break;
			}
		}

		$(select_dd).get(0).selectedIndex = 0;
		$(select_dd).selectpicker("refresh");
	});

	return function(options){
		config = options;
	};
});