define([
  'jquery',
  'jquery/ui',
  'Magento_Ui/js/modal/alert',
  'Magento_Ui/js/modal/modal',
  "domReady!"
], 
function($, modal, alert) {
	'use strict';

	var registry = {
        data: [],
        set: function (key, value) {
            this.data[key] = value;
        },
        get: function (key) {
            return this.data[key];
        }
    };

    function reloadWishlist(wishurl)
    {
        $.ajax({
            url: wishurl,
            method: 'GET',
            showLoader: true
        }).done(function (data) {
            $('#multiwishlist-created').html(data);
        }).fail(function (jqXHR, textStatus) {
            if (window.console) {
                console.log(textStatus);
            }
        });
    }

	var mageJsComponent = function(config)
	{

		if($('body .multiwishlist').length){			
			
			$(document).on('click','body #multiwishlist-modal input[name="gmw-name"]',function(){
				
				if($('input[name="gmw-name"]').is(":checked") && $(this).attr('id') == 'gmw-new' ){
					$('body #multiwishlist-modal .new-wishlist').show();
				} 
				else{
					$('#multiwishlist-modal .new-wishlist').hide();
					$(".new-wishlist #new-wishlist-invalid").remove();
					$('#multiwishlist-modal .new-wishlist input[name="new_wishlist"]').val('');
				}			
			});
			
			$('#multiwishlist-modal input[name="new_wishlist"]').on('input', function() {
				$(this).val($(this).val().replace(/[^A-Za-z0-9 ]+/gi,''));
			});
			
			/* $(document).click(function(){
				$('#multiwishlist-modal').modal("closeModal");   
			});
			 */

			/* $(document).on('click','.products-grid .product-items li .product-item-info a.multiwishlist',function(){
			if(!config.loggedIn){
				socialLogin.open();
				//window.location.href = config.logInUrl;
				//alert({content: "Redirecting to login page"});
				return false;
			} */
			
			$(document).on('click','body .multiwishlist',function(){
			
			if(!config.loggedIn){
				openSignInWindow();
				//window.location.href = config.logInUrl;
				//alert({content: "Redirecting to login page"});
				return false;
			}

				var multiself = $(this);
				var proid = $(this).attr('wishlist-id');
				var postparams = $(this).attr('wishlist-post');
				var formData =  typeof postparams == "object" ? postparams : JSON.parse(postparams);

				var formkey = $('#multiwishlist-modal input[name="form_key"]').val();
				formData.data.form_key = formkey;
				formData.fetchWishUrl = config.fetchWishUrl;
				registry.set('fd', formData);

				$('#multiwishlist-modal').modal({
					title: 'Add to Favourites',
					modalClass: 'modal__small multiwishlist-modal',
		            buttons: [{
		                text: 'Add',
		                class: 'action-primary',
		                click: function () { 
							
		                	var self = this;
		                    (function ($, node, alert) {
		                    	var fmdata = registry.get('fd');

			                    var wish = $(node).find('input[name="gmw-name"]:checked').val();
			                    var wishname;
			                    if(wish == 'new'){
			                    	wish=0;
			                    	wishname = $(node).find('input[name="new_wishlist"]').val();

									if(wishname === ''){
										$("#new-wishlist-invalid").remove();
										$(".new-wishlist").append("<div id='new-wishlist-invalid'>Please select/create wishlist</div>");
										//alert({modalClass:'modal__small',content: "Please select/create wishlist"});
									} else{
										$(".new-wishlist #new-wishlist-invalid").remove();
										fmdata.data.wishname = wishname;
									}
			                    }								

			                    fmdata.data.wish = wish;

		                    	if(wish || (wishname !== '')){
			                        $.ajax({
			                            url: fmdata.action,
			                            method: 'POST',
			                            data: fmdata.data,
			                            showLoader: true
			                        }).done(function (data) {
			                            if(data.success) {
			                            	$(multiself).addClass('active');
											$( 'ol .prod-swatch-' + fmdata.data.product + ' .multiwishlist').addClass( 'active' );
			                            	$(data.success).each(function(i,msg){
			                            		$('<span/>',{'class':'success'}).html(msg).appendTo($(node).find('.msgs'));
			                            	});
			                            }

										if(data.error) {
			                            	$(data.error).each(function(i,msg){
			                            		$('<span/>',{'class':'error'}).html(msg).appendTo($(node).find('.msgs'));
			                            	});
			                            }

			                            if(!wish){
			                            	reloadWishlist(fmdata.fetchWishUrl);
			                            }
			
			                            setTimeout(function(){ self.closeModal(); }, 1000);
			                        }).fail(function (jqXHR, textStatus) {
			                            if (window.console) {
			                                console.log(textStatus);
											
			                                alert({modalClass:'modal__small',content: "Sorry there was an error processing. Please try later."});
			                            }
			                            location.reload();
			                        });
		                    	}	                    	
		                    })(jQuery, this.element, alert);
		                }
		            }/*,{
		                text: 'Cancel',
		                class: 'action-secondary',
		                click: function () {
		                    this.closeModal();
		                }
		            }*/],
	                closed: function (modal) {
	                	$('#multiwishlist-wrapper .msgs').empty();
	                	$('#multiwishlist-wrapper input[name="new_wishlist"]').val('');
						$("#new-wishlist-invalid").html('');
						
	                }
				}).trigger('openModal');

				return false;
		  	});
		}
	};

	return mageJsComponent;
});