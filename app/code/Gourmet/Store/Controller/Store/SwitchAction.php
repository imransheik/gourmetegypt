<?php

/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Gourmet\Store\Controller\Store;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context as ActionContext;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\StoreCookieManagerInterface as StoreCookieManagerInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreIsInactiveException;
use Magento\Store\Model\StoreResolver;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Switch current store view.
 */
class SwitchAction extends \Magento\Store\Controller\Store\SwitchAction {

    /**
     * @var StoreCookieManagerInterface
     */
    protected $storeCookieManager;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    public function execute() {
        $currentActiveStore = $this->storeManager->getStore();
        $location = $this->getRequest()->getParam('location');
        $store = $this->getRequest()->getParam('store');
        $region = $this->getRequest()->getParam('region');
        $lconfirm = $this->getRequest()->getParam('lconfirm');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$redCities = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/red_city', $storeScope);
		$redCitiesArray = explode(',', $redCities);
		$cutOffTimeSameday ="";
		
        if (isset($location)) {
            setcookie("Gourmet_Location", $location, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);


            $helper = $objectManager->get('\Gourmet\Location\Helper\Data');
            $data = $helper->getLocationById($location);

            $customerSession = $objectManager->create('Magento\Customer\Model\Session');

            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            if ($customerSession->isLoggedIn()) {
                $shipingID = $customerSession->getCustomer()->getDefaultShipping();
                if ($shipingID) {
                    $address = $objectManager->create('Magento\Customer\Model\Address')->load($shipingID);
                    $addcity = $address->getCity();
                }
            }
            if ($customerSession->isLoggedIn() && (!isset($_SESSION["locationconfirm"]) || (isset($_SESSION["locationconfirm"]) && $_SESSION["locationconfirm"] != 1)) && isset($addcity)) {
                $sqlcookie = "Select city_id FROM directory_region_city WHERE city = '" . $addcity . "'";
                $resultcookie = $connection->fetchRow($sqlcookie);
                $dest_city = $resultcookie['city_id'];
                $data = $helper->getLocationById($dest_city);
            }

            $tableName = $resource->getTableName('webshopapps_matrixrate');
            $sql = "Select * FROM " . $tableName . " WHERE dest_city = '" . $data->getCity() . "'";
            $result = $connection->fetchAll($sql);
            $totalResult = count($result);
            $date=$objectManager->create('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');
            // $timenow = date("Y-m-d H:i:00", time() + 1200);
            $timenow = $date->date()->format('Y-m-d H:i:00');
            $i = 0;
            $slot="";
            $dateLabel="";
            foreach ($result as $row) {
                // echo "<pre>";
                // print_r($row);
				if (in_array($data->getCity(), $redCitiesArray)) {
					$cutOffTimeSameday = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/red_cut_off_time', $storeScope);
				}
				
                $slotbreak = explode('-', $row['shipping_method']);
                $slotbreak1 = date("Y-m-d ") . trim($slotbreak[0]);
                $slot = date("Y-m-d H:i:00", strtotime($slotbreak1));

                $dest_city = $data->getCity();
                if (
                   $dest_city == "Ain Sokhna"
                ) {
                    $nextWeekDay = strtotime('next friday');
                    $slot = "(Friday-" . date('d/m', $nextWeekDay) . ")";
                }else if($dest_city == "Madeinty" || $dest_city == "Shoruk City" || $dest_city == "Almaza Bay"){
                    $slot = $row['shipping_method'];
					$slot = str_replace("(FREE DELIVERY)", "",$slot);
                }else if(in_array($dest_city, $redCitiesArray)){
					$date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('m/d/y H:i:s');
					if((strtotime($date) <= strtotime($cutOffTimeSameday)) ){
						$slot = $row['shipping_method'];
						$slot = str_replace("(FREE DELIVERY)", "",$slot);
					} else{
						$slot =' Next Day-'.$row['shipping_method'];
					}
                }else {
                    if ($slot > $timenow) {

                        $slot = $row['shipping_method'];
                        break;
                    }
                    $mystring = $slot;

                    $findme = '(';
                    $pos = strpos($mystring, $findme);
                    if ($pos) {
                        $slot1 = explode('(', $slot);
                        $slot = $slot1[0];
                    }
                }
            }
            //var_dump($slot); exit;
            setcookie("Gourmet_Knockout_Location", $data->getCity(), time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
            setcookie("Gourmet_Knockout_Slot", $slot, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
            $_SESSION["shippinglist"] = 0;
            $_SESSION["locationconfirm"] = 1;
            $_SESSION["store"] = $store;
            $_SESSION["region"] = $region;

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('webshopapps_matrixrate');

            $sqlcookie = "Select city FROM directory_region_city WHERE city_id = " .str_replace("gourmet_dev","gourmet_production",$location);
            $resultcookie = $connection->fetchRow($sqlcookie);
            $dest_city = $resultcookie['city'];

            $cartQuote = $objectManager->get('\Magento\Checkout\Model\Session')->getQuote();
            $oldCouponCode = $cartQuote->getCouponCode();
            $coupon = $objectManager->get('Magento\SalesRule\Model\CouponFactory')->create();
            $coupon->load($oldCouponCode, 'code');
            $_rule = $this->_objectManager->create('Magento\SalesRule\Model\Rule')->load($coupon->getRuleId());
            if (in_array("9", $_rule->getCustomerGroupIds()) && $region != '512') {
                $itemsCount = $cartQuote->getItemsCount();
                if ($itemsCount) {

                    $cartQuote->getShippingAddress()->setCollectShippingRates(true);
                    $cartQuote->setCouponCode('')->collectTotals();
                    $quoteRepository = $objectManager->create('Magento\Quote\Api\CartRepositoryInterface');
                    $quoteRepository->save($cartQuote);
                    $this->messageManager->addError(__('Applied coupon code applicable for Alexandria location only.'));
                }
            }
//            var_dump($resultcookie); exit;
            //Select Data from table
            $sql = "Select * FROM " . $tableName . " WHERE dest_city = '" . $dest_city . "'";
            $result = $connection->fetchAll($sql);
			$slotId ="";
            $timenow = date("Y-m-d H:i:00", time() + 1200);
            $i = 0;
            foreach ($result as $row) {
                $slotbreak = explode('-', $row['shipping_method']);
                $slotbreak1 = date("Y-m-d ") . trim($slotbreak[0]);

                $slot = date("Y-m-d H:i:00", strtotime($slotbreak1));
                if ($slot > $timenow) {
                    $slotId = $row['pk'];
                    break;
                }
                $i++;
                if ($i == count($result)) {
                    foreach ($result as $row) {
                        $slotbreak = explode('-', $row['shipping_method']);
                        $slotbreak1 = date("Y-m-d ") . trim($slotbreak[0]);
                        $slot1 = date("Y-m-d H:i:00", strtotime($slotbreak1));
                        $slot = date("Y-m-d H:i:00", strtotime($slot1) + 86400);
                        if ($slot > $timenow) {
                            $slotId = $row['pk'];
                            break;
                        }
                    }
                }
            }

            setcookie("Slot_Id", $slotId, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
        } else {
            setcookie("Gourmet_Location", "", time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
            setcookie("Gourmet_Knockout_Location", "", time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
            setcookie("Gourmet_Knockout_Slot", "", time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);
            $_SESSION["shippinglist"] = 1;
        }
        if(isset($_COOKIE["Gourmet_Location"])){
            $cookie = $_COOKIE["Gourmet_Location"];
        }
        /*
          //$data = $helper->getLocationById($cookie);
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $helper = $objectManager->get('\Gourmet\Location\Helper\Data');
          $data = $helper->getLocationById($cookie);
          $customerSession = $objectManager->create('Magento\Customer\Model\Session');
          if ($customerSession->isLoggedIn()) {
          $shipingID = $customerSession->getCustomer()->getDefaultShipping();
          if ($shipingID) {
          $address = $objectManager->create('Magento\Customer\Model\Address')->load($shipingID);
          $addcity = $address->getCity();
          }
          }
          if ($customerSession->isLoggedIn() && (!isset($_SESSION["locationconfirm"]) || (isset($_SESSION["locationconfirm"]) && $_SESSION["locationconfirm"] != 1)) && isset($addcity)) {
          $sqlcookie = "Select city_id FROM directory_region_city WHERE city = '" . $addcity . "'";
          $resultcookie = $connection->fetchRow($sqlcookie);
          $dest_city = $resultcookie['city_id'];
          $data = $helper->getLocationById($dest_city);
          }
          setcookie("Gourmet_Knockout_Location", $data->getCity(), time() + (10 * 365 * 24 * 60 * 60), "/","", 0);
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $helper = $objectManager->get('\Gourmet\Location\Helper\Data');
          $data = $helper->getLocationById($cookie); */


        if (isset($lconfirm) && $lconfirm == 1) {
            $_SESSION["reorderlocationconfirm"] = 1;
        } else {
            $_SESSION["reorderlocationconfirm"] = 0;
        }

        $storeCode = $this->_request->getParam(
            StoreResolver::PARAM_NAME, $this->storeCookieManager->getStoreCodeFromCookie()
        );

        /* $storeCode = 'default'; */
        if ($storeCode == NULL)
            $storeCode = 'default';

        try {
            $store = $this->storeRepository->getActiveStoreByCode($storeCode);
        } catch (StoreIsInactiveException $e) {
            $error = __('Requested store is inactive');
        } catch (NoSuchEntityException $e) {
            $error = __('Requested store is not found');
        }

        if (isset($error)) {
            $this->messageManager->addError($error);
            $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
            return;
        }

        $defaultStoreView = $this->storeManager->getDefaultStoreView();
        if ($defaultStoreView->getId() == $store->getId()) {
            $this->storeCookieManager->deleteStoreCookie($store);
        } else {
            $this->httpContext->setValue(Store::ENTITY, $store->getCode(), $defaultStoreView->getCode());
            $this->storeCookieManager->setStoreCookie($store);
        }

        if ($store->isUseStoreInUrl()) {
            // Change store code in redirect url
            if (strpos($this->_redirect->getRedirectUrl(), $currentActiveStore->getBaseUrl()) !== false) {
                $this->getResponse()->setRedirect(
                    str_replace(
                        $currentActiveStore->getBaseUrl(), $store->getBaseUrl(), $this->_redirect->getRedirectUrl()
                    )
                );
            } else {
                $this->getResponse()->setRedirect($store->getBaseUrl());
            }
        } else {
            // echo; echo $_SERVER['QUERY_STRING'];

            if (strpos($this->_redirect->getRedirectUrl(), 'amasty_quickview') !== false) {


                $productId = substr( $this->_redirect->getRedirectUrl(), strpos( $this->_redirect->getRedirectUrl(), "?id=")+4);

                $product  = $objectManager->get('Magento\Catalog\Model\ProductRepository')->getById($productId);

                $this->getResponse()->setRedirect($product->getUrlModel()->getUrl($product));

            } else {
                $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
            }
        }
    }

}
