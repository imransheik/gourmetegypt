<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Edit
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */

namespace I95Dev\AnnouncementsNotification\Block\Adminhtml\Category\Edit;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress DeprecatedClass
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_wysiwygConfig;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_systemStore;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $optionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param array $data
     * @psalm-suppress DeprecatedClass
     * @psalm-suppress UndefinedFunction
     */
    public function __construct(
        \I95Dev\AnnouncementsNotification\Ui\Component\Listing\Column\Options $optionFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->optionFactory = $optionFactory;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress DeprecatedClass
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('wysiwyg_form');
        $this->setTitle(__('Wysiwyg Editor'));
    }

    /**
     * Retrieve template object
     *
     * @return \Magento\Newsletter\Model\Template
     */
    public function getModel()
    {
        return $this->_coreRegistry->registry('_category_template');
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @psalm-suppress UndefinedFunction
     */
    protected function _prepareForm()
    {
        $model = $this->getModel();

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']] //phpcs:ignore
        );

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Announcements/Category Notifications '), 'class' => 'fieldset-wide']
        );
        $fieldset->addField(
            'push_notification',
            'select',
            [
            'name' => 'push_notification',
            'label' => __('Push Notification Status'),
            'title' => __('Push Notification Status'),
            'required' => true,
            'value' => $model->getNotificationType(),
            'values' => $this->optionFactory->getPushNotification(),
                ]
        );

        $fieldset->addField(
            'title',
            'text',
            [
            'name' => 'title',
            'label' => __('Name'),
            'title' => __('Name'),
            'required' => true,
            'value' => $model->getTitle(),
                ]
        );
        $fieldset->addField(
            'message',
            'textarea',
            [
            'name' => 'message',
            'label' => __('Description'),
            'title' => __('Description'),
            'required' => true,
            'value' => $model->getMessage(),
                ]
        );
        $fieldset->addField(
            'image',
            'image',
            [
            'title' => __('Image'),
            'label' => __('Select Image'),
            'name' => 'image',
            'required' => true,
            'value' => 'images/' . $model->getImage(),
            'note' => 'Allow image type: jpg, jpeg, gif, png',
                ]
        );

        $fieldset->addType(
            'categories',
            \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Category::class
        );
        $fieldset->addField(
            'categories_id',
            'categories',
            [
            'name' => 'categories_id',
            'label' => __('Categories'),
            'title' => __('Categories'),
            'value' => $model->getCategoryId(),
                ]
        );
        $fieldset->addField(
            'device_type',
            'select',
            [
            'name' => 'device_type',
            'label' => __('Device Type'),
            'title' => __('Device Type'),
            'required' => true,
            'value' => $model->getDeviceType(),
            'values' => $this->optionFactory->getDeviceType(),
                ]
        );
        $fieldset->addField(
            'scheduled_time',
            'date',
            [
            'name' => 'scheduled_time',
            'label' => __('Schedule Time'),
            'title' => __('Schedule Time'),
            'required' => true,
            'value' => $model->getScheduledTime(),
            'date_format' => 'yyyy-MM-dd',
            'time_format' => 'HH:mm:ss'
                ]
        );
        $form->setAction($this->getUrl('*/*/save'));
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
