<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Admin Block
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
 
namespace I95Dev\AnnouncementsNotification\Block\Adminhtml;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Template extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    protected $_template = 'template/edit.phtml';

    /**
     * @return $this
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress UndefinedMethod
     */
    protected function _prepareLayout()
    {
        $this->getToolbar()->addChild(
            'add_button',
            \Magento\Backend\Block\Widget\Button::class,
            [
                'label' => __('Add New Announcements Notification'),
                'onclick' => "window.location='" . $this->getCreateUrl() . "'",
                'class' => 'add primary add-template'
            ]
        );

        return parent::_prepareLayout();
    }

    /**
     * Get the url for create
     *
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     * @psalm-suppress UndefinedFunction
     */
    public function getHeaderText()
    {
        return __('Announcements Notification');
    }
}
