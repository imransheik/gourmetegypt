<?php
/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Index controller for grid
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev\AnnouncementsNotification::notification_Tab1');
        $resultPage->addBreadcrumb(__('Announcements Category Notification List'), __('Announcements Category Notification List')); //phpcs:ignore
        $resultPage->addBreadcrumb(__('Announcements Category Notification List'), __('Announcements Category Notification List')); //phpcs:ignore
        $resultPage->getConfig()->getTitle()->prepend(__('Announcements/Category Notifications '));
        return $resultPage;
    }
}
