<?php
/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description NewAction Controller
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category;

class NewAction extends \I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category
{
    /**
     * Create new Newsletter Template
     *
     * @return void
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
