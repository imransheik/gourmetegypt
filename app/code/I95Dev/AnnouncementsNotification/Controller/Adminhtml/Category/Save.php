<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Save Controller
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */

namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends \I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category
{

    /**
     * Save Newsletter Template
     *
     * @return void
     *
     */
    protected $_fileUploaderFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_filesystem;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param UploaderFactory $fileUploaderFactory
     * @param \Magento\Framework\Filesystem $fileSystem
     * @psalm-suppress UndefinedClass
     */
    public function __construct(Action\Context $context, UploaderFactory $fileUploaderFactory, \Magento\Framework\Filesystem $fileSystem) //phpcs:ignore
    {
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $fileSystem;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $categoryId='';
        $request = $this->getRequest();
        if ($request->getParam('categories_id')) {
            $categoryId = implode(',', $request->getParam('categories_id'));
        }
 
//   var_dump($categoryId);exit;
//        echo"<pre>";var_dump($request->getParams());exit;
        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/category'));
        }

        $template = $this->_objectManager->create(\I95Dev\AnnouncementsNotification\Model\Category::class);

        $id = (int) $request->getParam('id');
        
        if ($id) {
             $template->load($id);
        }
       // var_dump($request->getParam('image')); exit;
            /*
             * Save image upload
             */
        try {
            if (isset($_FILES['image']) && isset($_FILES['image']['name']) && strlen($_FILES['image']['name'])) { //phpcs:ignore
                $data = $request->getParams();
      
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);

                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                $uploader->setAllowRenameFiles(false);

                $uploader->setFilesDispersion(false);

                $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
                        ->getAbsolutePath('images/');

                $imgPath = $uploader->save($path);
                 $template->setData('image', $imgPath['file']);
            } else {
               
                  $delete= $request->getParam('image');
                if (array_key_exists("delete", $delete)) {
                    if ($delete['delete']='1') {
                        $imgFilename = null;
                        $template->setData('image', $imgFilename);
                    }
                }
            }
// $categoryId = implode(',',$request->getParam('categories_id'));
////    $categoryId = implode(",",$request('categories_id'));
//   var_dump($categoryId);exit;
            $template->setData('categories_id', $categoryId);
            $template->setData('push_notification', $request->getParam('push_notification'));
            $template->setData('type', $request->getParam('type'));
            $template->setData('title', $request->getParam('title'));
            $template->setData('message', $request->getParam('message'));
            $template->setData('device_type', $request->getParam('device_type'));
            $template->setData('scheduled_time', $request->getParam('scheduled_time'));
            $template->setData('status', '0');
            $template->save();

            $this->messageManager->addSuccess(__('The Category Notification has been saved.'));
            $this->_getSession()->setFormData(false);
        } catch (LocalizedException $e) {

            $this->messageManager->addError(nl2br($e->getMessage()));
            $this->_getSession()->setData('category_template_form_data', $this->getRequest()->getParams());
            return $resultRedirect->setPath('*/*/edit', ['id' => $template->getCategorytemplateId(), '_current' => true]);//phpcs:ignore
        } catch (\Exception $e) {

            $this->messageManager->addException($e, __('Something went wrong while saving this images.'));
            $this->_getSession()->setData('category_template_form_data', $this->getRequest()->getParams());
            return $resultRedirect->setPath('*/*/edit', ['id' => $template->getCategorytemplateId(), '_current' => true]); //phpcs:ignore
        }
      
            return $resultRedirect->setPath('*/*/');
    }
}
