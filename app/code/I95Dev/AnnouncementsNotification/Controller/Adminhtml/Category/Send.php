<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description NewAction Controller
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */

namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

class Send extends \I95Dev\AnnouncementsNotification\Controller\Adminhtml\Category
{
    /**
     * Create new Newsletter Template
     *
     * @return void
     *
     */

    /**
     * @var \I95Dev\Fcm\lib\FcmAPI
     */
    protected $_api;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_fcmCollection;
    /**
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        Action\Context $context,
        \I95Dev\Fcm\lib\FcmAPI $api,
        \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection
    ) {
        $this->_api = $api;
        $this->_fcmCollection = $fcmCollection;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $id = (int) $request->getParam('id');

        $template = $this->_objectManager->create(\I95Dev\AnnouncementsNotification\Model\Category::class);
        if ($id) {
            $template->load($id);
        }

        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $currentStore = $storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        if ($template->getData('image')) {
            $orderStatusMessage = [
                'title' => $template->getData('title'),
                'body' => $template->getData('message'),
                'categoryIdentifier' => 'offer_category',
                "summaryArgument" => $template->getData('categories_id'),
                "launchImageName" => $mediaUrl . 'images/' . $template->getData('image'),
            ];
        } else {
            $orderStatusMessage = [
                'title' => $template->getData('title'),
                'body' => $template->getData('message'),
                'categoryIdentifier' => 'offer_category',
                "summaryArgument" => $template->getData('categories_id'),
            ];
        }

        if ($template->getData('device_type') == 1) {
            $fcmCollection = $this->_fcmCollection->create();
            $fcmCollectionIos = $fcmCollection->addFieldToFilter('platform', 'ios');
            $fcmIos = $fcmCollectionIos->getData();
            $fcmIosArr = [];
            foreach ($fcmIos as $fcmData) {
                $fcmIosArr[] = $fcmData['fcmtoken'];
            }

            $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
            $this->messageManager->addSuccess(__('The Offer Category Announcements Notification has been sent for IOS.')); //phpcs:ignore
        } else {
            $fcmCollection = $this->_fcmCollection->create();
            $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('platform', 'ANDROID');
            $fcmAndroid = $fcmCollectionAndroid->getData();
            $fcmAndroidArr = [];
            foreach ($fcmAndroid as $fcmData) {
                $fcmAndroidArr[] = $fcmData['fcmtoken'];
            }

            $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
            $this->messageManager->addSuccess(__('The Offer Category Announcements Notification has been sent for ANDROID.')); //phpcs:ignore
        }

        return $resultRedirect->setPath('*/*/edit', ['id' => $id, '_current' => true]);
    }
}
