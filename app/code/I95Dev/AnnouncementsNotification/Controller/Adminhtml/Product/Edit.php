<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @product    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Edit record controller
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
 
namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Product;

class Edit extends \I95Dev\AnnouncementsNotification\Controller\Adminhtml\Product
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry)
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Edit Newsletter Template
     *
     * @return void
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        $model = $this->_objectManager->create(\I95Dev\AnnouncementsNotification\Model\Product::class);
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $model->load($id);
        }
        $this->_coreRegistry->register('_product_template', $model);

        $this->_view->loadLayout();
        $this->_setActiveMenu('Sample_product::product_template');

        if ($model->getId()) {
            $breadcrumbTitle = __('Edit Announcements Product Notification');
            $breadcrumbLabel = $breadcrumbTitle;
        } else {
            $breadcrumbTitle = __('New Announcements Product Notification');
            $breadcrumbLabel = __('Create Announcements Product Notification');
        }
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Announcements Product Notification'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            $model->getId() ? $model->getTemplateId() : __('New Announcements Product Notification')
        );
 
        $this->_addBreadcrumb($breadcrumbLabel, $breadcrumbTitle);

        // restore data
        $values = $this->_getSession()->getData('product_template_form_data', true);
        if ($values) {
            $model->addData($values);
        }

        $this->_view->renderLayout();
    }
}
