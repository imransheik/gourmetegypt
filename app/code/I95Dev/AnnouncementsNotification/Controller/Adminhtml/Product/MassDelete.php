<?php
/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @product    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description MassDelete Controller
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
namespace I95Dev\AnnouncementsNotification\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use I95Dev\AnnouncementsNotification\Model\ResourceModel\Product\CollectionFactory;

class MassDelete extends \I95Dev\AnnouncementsNotification\Controller\Adminhtml\Product
{
    
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    /**
     * Delete action
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $template) {
            $template->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
