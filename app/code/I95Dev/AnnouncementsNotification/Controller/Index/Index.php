<?php

namespace I95Dev\AnnouncementsNotification\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Index extends Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;
    }
}
