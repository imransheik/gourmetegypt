<?php

namespace I95Dev\AnnouncementsNotification\Cron;

class SendNotification
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_categoryCollection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_productCollection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_dateFactory;

    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_helper;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_fcmCollection;

    /**
     * @var \I95Dev\Fcm\lib\FcmAPI
     */
    protected $_api;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_generalCollection;

    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $category;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $product;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $template;

    /**
     * SendNotification constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateFactory
     * @param \I95Dev\AnnouncementsNotification\Model\ResourceModel\Template\Collection $generalCollection
     * @param \I95Dev\AnnouncementsNotification\Model\ResourceModel\Category\Collection $categoryCollection
     * @param \I95Dev\AnnouncementsNotification\Model\ResourceModel\Product\Collection $productCollection
     * @param \I95Dev\Fcm\Helper\Data $helepr
     * @param \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection
     * @param \I95Dev\Fcm\lib\FcmAPI $api
     * @param \I95Dev\AnnouncementsNotification\Model\Category $category
     * @param \I95Dev\AnnouncementsNotification\Model\Product $product
     * @param \I95Dev\AnnouncementsNotification\Model\Template $template
     * @psalm-suppress   UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    // phpcs:disable
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateFactory,
        \I95Dev\AnnouncementsNotification\Model\ResourceModel\Template\Collection $generalCollection,
        \I95Dev\AnnouncementsNotification\Model\ResourceModel\Category\Collection $categoryCollection,
        \I95Dev\AnnouncementsNotification\Model\ResourceModel\Product\Collection $productCollection,
        \I95Dev\Fcm\Helper\Data $helepr,
        \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection,
        \I95Dev\Fcm\lib\FcmAPI $api,
        \I95Dev\AnnouncementsNotification\Model\Category $category,
        \I95Dev\AnnouncementsNotification\Model\Product $product,
        \I95Dev\AnnouncementsNotification\Model\Template $template
    ) {
        $this->_storeManager = $storeManager;
        $this->_dateFactory = $dateFactory;
        $this->_categoryCollection = $categoryCollection;
        $this->_productCollection = $productCollection;
        $this->_generalCollection = $generalCollection;
        $this->_helper = $helepr;
        $this->_fcmCollection = $fcmCollection;
        $this->_api = $api;
        $this->_category = $category;
        $this->_product = $product;
        $this->_template = $template;
    }
    // phpcs:disable

    /**
     * @psalm-suppress MissingReturnType
     */
    public function Send()
    {
        $this->catNotification();
        $this->productNotification();
        $this->generalNotification();
    }
    // phpcs:enable
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function catNotification()
    {
        $scheduledLists = $this->_categoryCollection
                ->addFieldToFilter('push_notification', "Enable")
                ->addFieldToFilter('scheduled_time', ['lteq' => $this->_dateFactory->date()->format('Y-m-d H:i:s')])
                ->addFieldToFilter('status', ['eq' => '', 'neq' => '2', 'neq' => '1'])
                ->getItems();
        foreach ($scheduledLists as $notification) {
            // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //$template = $objectManager->create(\I95Dev\AnnouncementsNotification\Model\Category::class);
            $template = $this->_category;
            $template->load($notification->getData("notification_id"))->setData('status', '1')->save();
            $currentStore = $this->_storeManager->getStore();
            $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            if ($notification->getData('image')) {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => 'offer_category',
                    "summaryArgument" => $notification->getData('categories_id'),
                    "launchImageName" => $mediaUrl . 'images/' . $notification->getData('image'),
                ];
            } else {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => 'offer_category',
                    "summaryArgument" => $notification->getData('categories_id'),
                ];
            }
            if ($notification->getData('device_type') == 1) {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionIos = $fcmCollection->addFieldToFilter('platform', 'ios');
                $fcmIos = $fcmCollectionIos->getData();
                $fcmIosArr = [];
                foreach ($fcmIos as $fcmData) {
                    $fcmIosArr[] = $fcmData['fcmtoken'];
                }
                $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
            } else {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('platform', 'ANDROID');
                $fcmAndroid = $fcmCollectionAndroid->getData();
                // var_dump($fcmAndroid); exit;
                $fcmAndroidArr = [];
                foreach ($fcmAndroid as $fcmData1) {
                    $fcmAndroidArr[] = $fcmData1['fcmtoken'];
                }

                $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
            }
            $template->load($notification->getData("notification_id"))->setData('status', '2')->save();
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function productNotification()
    {
        $scheduledLists = $this->_productCollection
                ->addFieldToFilter('push_notification', "Enable")
                ->addFieldToFilter('scheduled_time', ['lteq' => $this->_dateFactory->date()->format('Y-m-d H:i:s')])
                ->addFieldToFilter('status', ['eq' => '', 'neq' => '2', 'neq' => '1'])
                ->getItems();
        foreach ($scheduledLists as $notification) {
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $template = $objectManager->create(\I95Dev\AnnouncementsNotification\Model\Product::class);
            $template = $this->_product;
            $template->load($notification->getData("notification_id"))->setData('status', '1')->save();
            $currentStore = $this->_storeManager->getStore();
            $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            if ($notification->getData('image')) {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => 'product_details',
                    "summaryArgument" => $notification->getData('products_id'),
                    "launchImageName" => $mediaUrl . 'images/' . $notification->getData('image'),
                ];
            } else {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => 'product_details',
                    "summaryArgument" => $notification->getData('products_id'),
                ];
            }
            if ($notification->getData('device_type') == 1) {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionIos = $fcmCollection->addFieldToFilter('platform', 'ios');
                $fcmIos = $fcmCollectionIos->getData();
                $fcmIosArr = [];
                foreach ($fcmIos as $fcmData) {
                    $fcmIosArr[] = $fcmData['fcmtoken'];
                }
                $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
            } else {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('platform', 'ANDROID');
                $fcmAndroid = $fcmCollectionAndroid->getData();
                // var_dump($fcmAndroid); exit;
                $fcmAndroidArr = [];
                foreach ($fcmAndroid as $fcmData1) {
                    $fcmAndroidArr[] = $fcmData1['fcmtoken'];
                }

                $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
            }
            $template->load($notification->getData("notification_id"))->setData('status', '2')->save();
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function generalNotification()
    {
        $scheduledLists = $this->_generalCollection
                ->addFieldToFilter('push_notification', "Enable")
                ->addFieldToFilter('scheduled_time', ['lteq' => $this->_dateFactory->date()->format('Y-m-d H:i:s')])
                ->addFieldToFilter('status', ['eq' => '', 'neq' => '2', 'neq' => '1'])
                ->getItems();
        foreach ($scheduledLists as $notification) {
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $template = $objectManager->create(\I95Dev\AnnouncementsNotification\Model\Template::class);
            $template =$this->_template;
            $template->load($notification->getData("notification_id"))->setData('status', '1')->save();
            $currentStore = $this->_storeManager->getStore();
            $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            if ($notification->getData('image')) {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => $notification->getData('type'),
                    "launchImageName" => $mediaUrl . 'images/' . $notification->getData('image'),
                ];
            } else {
                $orderStatusMessage = [
                    'title' => $notification->getData('title'),
                    'body' => $notification->getData('message'),
                    'categoryIdentifier' => $notification->getData('type'),
                ];
            }
            if ($notification->getData('device_type') == 1) {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionIos = $fcmCollection->addFieldToFilter('platform', 'ios');
                $fcmIos = $fcmCollectionIos->getData();
                $fcmIosArr = [];
                foreach ($fcmIos as $fcmData) {
                    $fcmIosArr[] = $fcmData['fcmtoken'];
                }
                $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
            } else {
                $fcmCollection = $this->_fcmCollection->create();
                $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('platform', 'ANDROID');
                $fcmAndroid = $fcmCollectionAndroid->getData();
                // var_dump($fcmAndroid); exit;
                $fcmAndroidArr = [];
                foreach ($fcmAndroid as $fcmData1) {
                    $fcmAndroidArr[] = $fcmData1['fcmtoken'];
                }

                $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
            }
            $template->load($notification->getData("notification_id"))->setData('status', '2')->save();
        }
    }
}
