<?php


/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Model for Status
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
 
 
namespace I95Dev\AnnouncementsNotification\Model\Theme;

/**
 * @psalm-suppress UndefinedClass
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @psalm-suppress PropertyNotSetInConstructor
     * @psalm-suppress UndefinedClass
     */
    protected $_template;

    /**
     * Constructor
     *
     * @param \Sample\gridpartimage\Model\Template $template
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MismatchingDocblockParamType
     * @psalm-suppress InvalidPropertyAssignmentValue
     */
    public function __construct(\I95Dev\AnnouncementsNotification\Model\Template $template)
    {
        $this->_template =  $template;
    }

    /**
     * Get options
     *
     * @return array
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress UndefinedClass
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_template->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
