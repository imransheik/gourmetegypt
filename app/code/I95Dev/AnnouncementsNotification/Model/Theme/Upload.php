<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Model to upload file.
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
 
namespace I95Dev\AnnouncementsNotification\Model\Theme;

use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\File\Uploader;

class Upload
{
    /**
     * uploader factory
     * @psalm-suppress UndefinedClass
     */
    protected $uploaderFactory;

    /**
     * constructor
     *
     * @param UploaderFactory $uploaderFactory
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidPropertyAssignmentValue
     */
    public function __construct(UploaderFactory $uploaderFactory)
    {
        $this->uploaderFactory = $uploaderFactory;
    }

    /**
     * @psalm-suppress MissingParamType
     * @param $input
     * @param $destinationFolder
     * @param $data
     * @return mixed|string
     * @throws \Exception
     * @psalm-suppress UndefinedClass
     */
    public function uploadFileAndGetName($input, $destinationFolder, $data)
    {
        // phpcs:disable
        try {

            if (isset($data[$input]['delete'])) {
                return '';
            } else {
                $uploader = $this->uploaderFactory->create(['fileId' => $input]);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result = $uploader->save($destinationFolder);
                return $result['file'];
            }
        } catch (Exception $e) {

            if ($e->getCode() != Uploader::TMP_NAME_EMPTY) {
                throw new \Exception('The file was not uploaded.');
            } else {
                if (isset($data[$input]['value'])) {
                    return $data[$input]['value'];
                }
            }
            // phpcs:enable
        }
        return '';
    }
}
