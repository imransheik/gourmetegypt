<?php

/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description Creation of tables
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */

namespace I95Dev\AnnouncementsNotification\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        // phpcs:disable
//        $gridpartimagetemplate = $installer->getConnection()->newTable(
//                                $installer->getTable('announcements_notification'))
//                        ->addColumn('notification_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array('identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true), 'Code ID'
//                        )->addColumn('image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Post Logo File'
//                )->addColumn('type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Notification Type'
//                )->addColumn('push_notification', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Push Notification'
//                )->addColumn('title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Name'
//                )->addColumn('message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Description'
//                )->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created At'
//                )->addColumn('updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated At'
//        );
//        $gridpartimagecategory = $installer->getConnection()->newTable(
//                                $installer->getTable('announcements_category_notification'))
//                        ->addColumn('notification_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array('identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true), 'Code ID'
//                        )->addColumn('image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Post Logo File'
//                     )->addColumn('push_notification', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Push Notification'
//                )->addColumn('categories_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Category'
//                )->addColumn('title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Name'
//                )->addColumn('message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Description'
//                )->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created At'
//                )->addColumn('updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated At'
//        );
//        $installer->getConnection()->createTable($gridpartimagetemplate);
//        $installer->getConnection()->createTable($gridpartimagecategory);
        // phpcs:enable
        $installer->endSetup();
    }
}
