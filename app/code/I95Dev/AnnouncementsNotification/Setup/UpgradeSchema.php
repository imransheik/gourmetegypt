<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Base
 */


namespace I95Dev\AnnouncementsNotification\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $this->addExtraField($setup);
        }
         $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @psalm-suppress MissingReturnType
     * @psalm-suppress TooManyArguments
     */
    private function addExtraField(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_notification'),
            'device_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Device Type'
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_notification'),
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Status'
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_notification'),
            'scheduled_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true],
            'Scheduled Time'
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_category_notification'),
            'device_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Device Type'
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_category_notification'),
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Status'
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable('announcements_category_notification'),
            'scheduled_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true],
            'Scheduled Time'
        );
    }
}
