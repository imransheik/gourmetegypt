<?php


/**
 * i95Dev.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.i95dev.com/LICENSE-M1.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sub@i95dev.com so we can send you a copy immediately.
 *
 * @category    I95Dev
 * @package     I95Dev_AnnouncementsNotification
 * @Description For save and delete Urls
 * @author      i95Dev
 * @copyright   Copyright (c) 2016 i95Dev
 * @license     http://store.i95dev.com/LICENSE-M1.txt
 */
 
 
 
namespace I95Dev\AnnouncementsNotification\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Background extends Column
{
    /** Url path */
    const BLOG_URL_PATH_EDIT = 'announcementsnotification/lists/edit';
    const BLOG_URL_PATH_DELETE = 'announcementsnotification/lists/delete';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;
    /**
     * @var \I95Dev\AnnouncementsNotification\Model\Theme\Background
     * @psalm-suppress MissingPropertyType
     */
    protected $backGround;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     * @param \I95Dev\AnnouncementsNotification\Model\Theme\Background $backGround
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storemanager,
        \I95Dev\AnnouncementsNotification\Model\Theme\Background $backGround,
        array $components = [],
        array $data = [],
        $editUrl = self::BLOG_URL_PATH_EDIT
    ) {
        $this->_storeManager = $storemanager;
        $this->backGround=$backGround;
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function prepareDataSource(array $dataSource)
    {
        
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       
        $backgroundModel = $this->backGround;
//        $backgroundModel = $objectManager->get('I95Dev\AnnouncementsNotification\Model\Theme\Background');
        
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $template = new \Magento\Framework\DataObject($item);
                $imageUrl =  $mediaDirectory.'gridpartimage/background/image'.$template->getBackground();
                $item[$fieldName . '_src'] = $imageUrl;
                $item[$fieldName . '_alt'] = $template->getName();
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                    'gridpartimage/template/edit',
                    [
                        'id' => $template->getgridpartimagetemplateId(),
                        'store' => $this->context->getRequestParam('store')
                    ]
                );
                $item[$fieldName . '_orig_src'] = $imageUrl;
            }
        }

        return $dataSource;
    }
}
