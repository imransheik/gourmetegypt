<?php
namespace I95Dev\AnnouncementsNotification\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class DeviceTypeCategory implements OptionSourceInterface
{
    /**
     * @psalm-suppress MissingReturnType
     */
    public function toArray()
    {
        return [

        ];
    }

    /**
     * @psalm-suppress UndefinedFunction
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 1,
                'label' => __("IOS")
            ],
            [
                'value' => 0,
                'label' => __("Android")
            ],
            
        ];
    }
}
