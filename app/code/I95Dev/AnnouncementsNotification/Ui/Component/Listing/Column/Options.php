<?php
namespace I95Dev\AnnouncementsNotification\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Options extends Column
{
    /**
     * @var UrlInterface
     * @psalm-suppress MissingPropertyType
     */
    protected $urlBuilder;
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $actionUrlBuilder;
    /**
     * @psalm-suppress MissingDocblockType
     */
    protected $_storeManager;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress MissingReturnType
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        UrlBuilder $actionUrlBuilder,
        PriceCurrencyInterface $priceFormatter,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
            $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        $this->priceFormatter = $priceFormatter;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @psalm-suppress MissingReturnType
     *
     */
    public function getNotificationType()
    {
        return [
            ['value' => 'normal', 'label' => 'Normal'],
            ['value' => 'offer', 'label' => 'Offer']
        ];
    }
    /**
     * @psalm-suppress MissingReturnType
     *
     */
    public function getPushNotification()
    {
        return [
           ['value' => 'Enable', 'label' => 'Enable'],
           ['value' => 'Disable', 'label' => 'Disable']
        ];
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function getDeviceType()
    {
        return [
           ['value' => '0', 'label' => 'Android'],
           ['value' => '1', 'label' => 'IOS']
            
        ];
    }
}
