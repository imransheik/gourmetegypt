<?php



namespace I95Dev\AnnouncementsNotification\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class StatusProduct implements OptionSourceInterface
{
    /**
     * @psalm-suppress MissingReturnType
     */
    public function toArray()
    {
        return [

        ];
    }

    /**
     * @psalm-suppress UndefinedFunction
     */
    public function toOptionArray()
    {
        return [
        [
                'value' => 2,
                'label' => __("Sent")
            ],
            [
                'value' => 1,
                'label' => __("Processing")
            ],
            [
                'value' => 0,
                'label' => __("Pending")
            ],
            
        ];
    }
}
