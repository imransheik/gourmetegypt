<?php

namespace I95Dev\AppCoupon\Controller\Magento\Checkout\Cart;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class CouponPost extends \Magento\Checkout\Controller\Cart\CouponPost
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\SalesRule\Model\CouponFactory $couponFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @codeCoverageIgnore
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\SalesRule\Model\CouponFactory $couponFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $couponFactory,
            $quoteRepository
        );
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Initialize coupon
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress InvalidReturnStatement
     * @psalm-suppress TooManyArguments
     * @psalm-suppress DeprecatedMethod
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $scopeConfig = $this->_objectManager->create(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $ecouponUrl = $scopeConfig->getValue("example_section/general/url", $storeScope);
        $ecouponSecretCode = $scopeConfig->getValue("example_section/general/secretCode", $storeScope);
        $ecouponPrefix = $scopeConfig->getValue("example_section/general/preForCoupon", $storeScope);

        $escaper = $this->_objectManager->get(\Magento\Framework\Escaper::class);
        $customerSession = $this->_objectManager->create(\Magento\Customer\Model\Session::class);

        if ($this->getRequest()->getParam('remove') == 1) {
            setcookie('store-coupon', '', 0); //phpcs:ignore
        }
        if ($this->getRequest()->getParam('from_minicart') == "1") {
            $resultJson = $this->resultJsonFactory->create();
            $message = "";
            if (!$this->getRequest()->isPost()) {
                $message = 'Page not found.';
                $response = ['success' => 'false', 'message' => $message];
                $resultJson->setData($response);
                return $resultJson;
            }

            $couponCode = $this->getRequest()->getParam('remove') == 1 ? '' : trim($this->getRequest()->getParam('coupon_code'));  //phpcs:ignore
            $cartQuote = $this->cart->getQuote();
            $codeLength = strlen($couponCode);
            $originalCode = $couponCode;
            
            if (substr($couponCode, 0, 2) === $ecouponPrefix) {
                //$couponCode = preg_replace('/^'.$ecouponPrefix.'/', '', $couponCode);
                $couponUrl = $ecouponUrl.'?couponNumber='.$couponCode.'&msisdn='.$ecouponSecretCode.'&customerMSISDN=&Burn=false';  //phpcs:ignore
                $xml = simplexml_load_string(file_get_contents($couponUrl)); //phpcs:ignore
                $discountPercentage = (float)$xml->DiscountPercentage;
                $discountValue = (float)$xml->DiscountValue;
                $remainingUsage = (float)$xml->RemainingUsage;
                $discountType = (string)$xml->DiscountValueType;
                if ((string)$xml->Message == "Valid coupon") {
                    if ($remainingUsage > 0) {
                        $cartQuote ->setCouponCode($originalCode);
                        $cartQuote ->setEcouponValue($discountValue);
                        $cartQuote ->setEcouponPercentage($discountPercentage);
                        $cartQuote ->setEcoupon($originalCode);
                        $cartQuote ->setEcouponValueType($discountType);
                        $customerSession->setEcoupon(1);
                        $cartQuote ->save($cartQuote->collectTotals());
                        $message = __("Coupon code '%1' was applied!", $escaper->escapeHtml($originalCode));
                    } else {
                        $message = __("This coupon has exhausted.".(string)$xml->Message);
                        $response = ['success' => 'false', 'message' => $message];
                    }
                    
                } else {
                    $message = __("This coupon is not valid. ".(string)$xml->Message);
                    $response = ['success' => 'false', 'message' => $message];
                }
                
                $response = ['success' => 'true', 'message' => $message];
                $resultJson->setData($response);
                return $resultJson;
            }
            
            try {
                $isCodeLengthValid = $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH;  //phpcs:ignore
                $resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
                $connection = $resource->getConnection();
                $cookie = '';
                if (isset($_COOKIE['Gourmet_Location'])) { //phpcs:ignore
                    $cookie = $_COOKIE["Gourmet_Knockout_Location"]; //phpcs:ignore
                } else {
                    $message = __('Please select the location.');
                    $response = ['success' => 'false', 'message' => $message];
                }
                $sqlcookie = "Select region_id FROM directory_region_city WHERE city='" . $cookie . "'"; //phpcs:ignore
                $resultcookie = $connection->fetchRow($sqlcookie);
                $region_id = $resultcookie['region_id'];
                $coupon = $this->couponFactory->create();
                $coupon->load($couponCode, 'code');
                $_rule = $this->_objectManager->create(\Magento\SalesRule\Model\Rule::class)->load($coupon->getRuleId());

                if (in_array("8", $_rule->getCustomerGroupIds())) {
                    $response = ['success' => 'false', 'message' => "We cannot apply the coupon code on the website, applicable only for the mobile app."]; //phpcs:ignore
                    $resultJson->setData($response);
                    return $resultJson;
                } elseif (in_array("9", $_rule->getCustomerGroupIds()) && $region_id != '512') {

                    $response = ['success' => 'false', 'message' => "This coupon code only applicable for Alexandria location"]; //phpcs:ignore
                    $resultJson->setData($response);
                    return $resultJson;
                }
                if (in_array("9", $_rule->getCustomerGroupIds())) {
                    setcookie("store-coupon", 9, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0); //phpcs:ignore
                } else {
                    setcookie('store-coupon', '', 0); //phpcs:ignore
                }
                $itemsCount = $cartQuote->getItemsCount();
                if ($itemsCount) {
                    $cartQuote->getShippingAddress()->setCollectShippingRates(true);
                    $cartQuote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals();
                    $cartQuote ->setEcouponValue(null);
                    $cartQuote ->setEcouponPercentage(null);
                    $cartQuote ->setEcoupon(null);
                    $cartQuote ->setEcouponValueType(null);
                    $customerSession->setEcoupon(0);
                    $cartQuote ->collectTotals();
                    $cartQuote->save();
                }

                if ($codeLength) {
                    $coupon = $this->couponFactory->create();
                    $coupon->load($couponCode, 'code');
                    if (!$itemsCount) {
                        if ($isCodeLengthValid && $coupon->getId()) {
                            $this->_checkoutSession->getQuote()->setCouponCode($couponCode)->save();
                            $message = __(
                                "Coupon code '%1' was applied!",
                                $escaper->escapeHtml($couponCode)
                            );
                            $response = ['success' => 'true', 'message' => $message];
                        } else {
                            $message = __(
                                'Enter Valid Code',
                                $escaper->escapeHtml($couponCode)
                            );
                            $response = ['success' => 'false', 'message' => $message];
                        }
                    } else {
                        if ($isCodeLengthValid && $coupon->getId() && $couponCode == $cartQuote->getCouponCode()) {
                            $message = __(
                                "Coupon code '%1' was applied!",
                                $escaper->escapeHtml($couponCode)
                            );
                            $response = ['success' => 'true', 'message' => $message];
                        } else {
                            $message = __(
                                'Enter Valid Code',
                                $escaper->escapeHtml($couponCode)
                            );
                            $response = ['success' => 'false', 'message' => $message];
                        }
                    }
                } else {
                    $message = __('You canceled the coupon code.');
                    $response = ['success' => 'false', 'message' => $message];
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $message = $e->getMessage();
                $response = ['success' => 'false', 'message' => $message];
            } catch (\Exception $e) {
                $message = __('We cannot apply the coupon code.');
                $response = ['success' => 'false', 'message' => $message];
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }

            $resultJson->setData($response);
            return $resultJson;
        } else {

            $couponCode = $this->getRequest()->getParam('remove') == 1 ? '' : trim($this->getRequest()->getParam('coupon_code')); //phpcs:ignore
            
            $cartQuote = $this->cart->getQuote();
            $oldCouponCode = $cartQuote->getCouponCode();
            $codeLength = strlen($couponCode);
            $oldCouponCodeLength = strlen($oldCouponCode);
            if (!$codeLength && !$oldCouponCodeLength) {
                return $this->_goBack();
            }
            $originalCode = $couponCode;
            if (substr($couponCode, 0, 2) === $ecouponPrefix) {
                //$couponCode = preg_replace('/^'.$ecouponPrefix.'/', '', $couponCode);
                 
                $couponUrl = $ecouponUrl.'?couponNumber='.$couponCode.'&msisdn='.$ecouponSecretCode.'&customerMSISDN=&Burn=false'; //phpcs:ignore
                $xml = simplexml_load_string(file_get_contents($couponUrl)); //phpcs:ignore
                $discountPercentage = (float)$xml->DiscountPercentage;
                $discountValue = (float)$xml->DiscountValue;
                $remainingUsage = (float)$xml->RemainingUsage;
                $discountType = (string)$xml->DiscountValueType;
                if ((string)$xml->Message == "Valid coupon") {
                    if ($remainingUsage > 0) {
                        $cartQuote ->setCouponCode($originalCode);
                        $cartQuote ->setEcouponValue($discountValue);
                        $cartQuote ->setEcouponPercentage($discountPercentage);
                        $cartQuote ->setEcoupon($originalCode);
                        $cartQuote ->setEcouponValueType($discountType);
                        
                        $cartQuote ->save($cartQuote->collectTotals());
                        $customerSession->setEcoupon(1);
                        $this->messageManager->addSuccess(__('You used coupon code "%1".', $escaper->escapeHtml($originalCode))); //phpcs:ignore
                        
                    } else {
                        $this->messageManager->addError(__('This coupon has exhausted'.(string)$xml->Message));
                    }
                    
                } else {
                    $this->messageManager->addError(__('This coupon is not valid.'.(string)$xml->Message));
                }
                return $this->_goBack();
            }

            try {
                $isCodeLengthValid = $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH; //phpcs:ignore
                $resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
                $connection = $resource->getConnection();
                $cookie = '';
                if (isset($_COOKIE['Gourmet_Location'])) { //phpcs:ignore
                    $cookie = $_COOKIE["Gourmet_Knockout_Location"]; //phpcs:ignore
                }
                $sqlcookie = "Select region_id FROM directory_region_city WHERE city='" . $cookie . "'"; //phpcs:ignore
                $resultcookie = $connection->fetchRow($sqlcookie);
                $region_id = $resultcookie['region_id'];

                $itemsCount = $cartQuote->getItemsCount();
                $coupon = $this->couponFactory->create();
                $coupon->load($couponCode, 'code');
                $_rule = $this->_objectManager->create(\Magento\SalesRule\Model\Rule::class)->load($coupon->getRuleId());

                if (in_array("8", $_rule->getCustomerGroupIds())) {
                    $this->messageManager->addError(__('We cannot apply the coupon code on the website, applicable only for the mobile app.')); //phpcs:ignore

                    return $this->_goBack();
                } elseif (in_array("9", $_rule->getCustomerGroupIds()) && $region_id != '512') {
                    $this->messageManager->addError(__('This coupon code only applicable for Alexandria location'));
                    return $this->_goBack();
                }
                if (in_array("9", $_rule->getCustomerGroupIds())) {
                    setcookie("store-coupon", 9, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0); //phpcs:ignore
                } else {
                    setcookie('store-coupon', '', 0); //phpcs:ignore
                }
                if ($itemsCount) {
                    $cartQuote->getShippingAddress()->setCollectShippingRates(true);
                    $cartQuote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals();
                    $cartQuote ->setEcouponValue(null);
                    $cartQuote ->setEcouponPercentage(null);
                    $cartQuote ->setEcoupon(null);
                    $cartQuote ->setEcouponValueType(null);
                    $customerSession->setEcoupon(0);
                    $cartQuote ->save($cartQuote->collectTotals());
                    
                }

                if ($codeLength) {
                    $coupon = $this->couponFactory->create();
                    $coupon->load($couponCode, 'code');
                    if (!$itemsCount) {
                        if ($isCodeLengthValid && $coupon->getId()) {
                            $this->_checkoutSession->getQuote()->setCouponCode($couponCode)->save();
                            $this->messageManager->addSuccess(
                                __('You used coupon code "%1".', $escaper->escapeHtml($couponCode))
                            );
                        } else {
                            $this->messageManager->addError(__('The coupon code "%1" is not valid.', $escaper->escapeHtml($couponCode))); //phpcs:ignore
                        }
                    } else {
                        if ($isCodeLengthValid && $coupon->getId() && $couponCode == $cartQuote->getCouponCode()) {
                            $this->messageManager->addSuccess(__('You used coupon code "%1".', $escaper->escapeHtml($couponCode))); //phpcs:ignore
                        } else {
                            $this->messageManager->addError(__('The coupon code "%1" is not valid.', $escaper->escapeHtml($couponCode))); //phpcs:ignore
                        }
                    }
                } else {
                    $this->messageManager->addSuccess(__('You canceled the coupon code.'));
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('We cannot apply the coupon code.'));
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }

            return $this->_goBack();
        }
    }
}
