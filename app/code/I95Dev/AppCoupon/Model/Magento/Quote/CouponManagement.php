<?php

namespace I95Dev\AppCoupon\Model\Magento\Quote;

use \Magento\Quote\Api\CouponManagementInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class CouponManagement extends \Magento\Quote\Model\CouponManagement
{
    
     /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $scopeConfig;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customerSession;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $couponFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $requestInterface;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resourceConnection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $rule;

    /**
     * Constructs a coupon read service object.
     * @psalm-suppress UndefinedClass
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository Quote repository.
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\SalesRule\Model\CouponFactory $couponFactory,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\SalesRule\Model\Rule $rule

    ) {
        $this->quoteRepository = $quoteRepository;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->couponFactory = $couponFactory;
        $this->requestInterface = $requestInterface;
        $this->resourceConnection = $resourceConnection;
        $this->rule = $rule;

    }
    /**
     * @param int $cartId
     * @param string $couponCode
     * @return bool
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress TooManyArguments
     * @psalm-suppress DeprecatedMethod
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress InvalidScalarArgument
     * @psalm-suppress UndefinedClass
     */
    public function set($cartId, $couponCode)
    {
           /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        
        $couponCode = trim($couponCode);
        $codeLength = strlen($couponCode);
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $scopeConfig = $this->scopeConfig;
        $ecouponUrl = $scopeConfig->getValue("example_section/general/url", $storeScope);
        $ecouponSecretCode = $scopeConfig->getValue("example_section/general/secretCode", $storeScope);
        $ecouponPrefix = $scopeConfig->getValue("example_section/general/preForCoupon", $storeScope);

        $originalCode = $couponCode;
        $customerSession = $this->customerSession;

        if (substr($couponCode, 0, 2) === $ecouponPrefix) {
                
            $couponUrl = $ecouponUrl.'?couponNumber='.$couponCode.'&msisdn='.$ecouponSecretCode.'&customerMSISDN=&Burn=false'; //phpcs:ignore
            $xml = simplexml_load_string(file_get_contents($couponUrl)); //phpcs:ignore
            $discountPercentage = (float)$xml->DiscountPercentage;
            $discountValue = (float)$xml->DiscountValue;
            $remainingUsage = (float)$xml->RemainingUsage;
            $discountType = (string)$xml->DiscountValueType;
            if ((string)$xml->Message == "Valid coupon") {
                if ($remainingUsage > 0) {
                    $quote ->setCouponCode($originalCode);
                    $quote ->setEcouponValue($discountValue);
                    $quote ->setEcouponPercentage($discountPercentage);
                    $quote ->setEcouponValueType($discountType);
                    $quote ->setEcoupon($originalCode);
                    $customerSession->setEcoupon(0);
                    $quote ->save($quote->collectTotals());
                } else {
                    throw new CouldNotSaveException(__('This coupon has exhausted'.(string)$xml->Message));
                    //$this->messageManager->addError(__('This coupon has exhausted'.(string)$xml->Message));
                }
                    
            } else {
                throw new CouldNotSaveException(__('This coupon is not valid'.(string)$xml->Message));
                //$this->messageManager->addError(__('This coupon is not valid'.(string)$xml->Message));
            }

            //$this->messageManager->addSuccess(__('You used coupon code "%1".', $escaper->escapeHtml($originalCode)));
            return true;
        }
            $coupon =$this->couponFactory->create()->load($couponCode, 'code'); //phpcs:ignore
            $requestInterface = $this->requestInterface;
               
            $query=$requestInterface->getQuery();
            $resource = $this->resourceConnection;
            $connection = $resource->getConnection();
            $cookie = '';
        if (isset($_COOKIE['Gourmet_Location'])) { //phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Knockout_Location"]; //phpcs:ignore
        }
            $sqlcookie = "Select region_id FROM directory_region_city WHERE city='" . $cookie . "'"; //phpcs:ignore
            $resultcookie = $connection->fetchRow($sqlcookie);
            $region_id = $resultcookie['region_id'];
            $checkoutregion_id ='';
        if (isset($_COOKIE['checkout-regionId'])) { //phpcs:ignore
            $checkoutregion_id = $_COOKIE["checkout-regionId"]; //phpcs:ignore
        }
              
                $_rule =$this->rule->load($coupon->getRuleId());
        if (in_array("9", $_rule->getCustomerGroupIds())) {
            setcookie("store-coupon", 9, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0); //phpcs:ignore
        } else {
            setcookie('store-coupon', '', 0); //phpcs:ignore
        }
        if (in_array("8", $_rule->getCustomerGroupIds()) && !isset($query->isapp)) {
                                     
            throw new CouldNotSaveException(__('We cannot apply the coupon code on the website, applicable only for the mobile app.')); //phpcs:ignore
        }
                 
        if (isset($query->isapp)) {
             $regionId='';
            if (isset($query->regionId)) {
                     $regionId=$query->regionId;
            }
            if (in_array("9", $_rule->getCustomerGroupIds()) && $regionId!='512') {
                 
                        throw new CouldNotSaveException(__('This coupon code only applicable for Alexandria location')); //phpcs:ignore
            }
        } else {
            if (in_array("9", $_rule->getCustomerGroupIds()) && $checkoutregion_id!='512') {
                 
                throw new CouldNotSaveException(__('This coupon code only applicable for Alexandria location'));
            }
                    
        }
     
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
        }
        $quote->getShippingAddress()->setCollectShippingRates(true);

        try {
            $quote->setCouponCode($couponCode);
            $this->quoteRepository->save($quote->collectTotals());
            
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not apply coupon code'));
        }
        if ($quote->getCouponCode() != $couponCode) {
            throw new NoSuchEntityException(__('Coupon code is not valid or expired'));
        }
        return true;
    }
    /**
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress TooManyArguments
     * @psalm-suppress DeprecatedMethod
     * @psalm-suppress UndefinedFunction
     */
    public function remove($cartId)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        
        $couponCode = trim($quote->getCouponCode());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $scopeConfig = $this->scopeConfig;
        $ecouponUrl = $scopeConfig->getValue("example_section/general/url", $storeScope);
        $ecouponSecretCode = $scopeConfig->getValue("example_section/general/secretCode", $storeScope);
        $ecouponPrefix = $scopeConfig->getValue("example_section/general/preForCoupon", $storeScope);
        $ecouponCustomerCode = $scopeConfig->getValue("example_section/general/customerMsisdn", $storeScope);
        $customerSession = $this->customerSession;
        $originalCode = $couponCode;
        if (substr($couponCode, 0, 2) === $ecouponPrefix) {
            $customerSession->setEcoupon(1);
            $quote->setCouponCode('');
            $quote ->setEcouponValue(null);
            $quote ->setEcouponPercentage(null);
            $quote ->setEcoupon(null);
            $quote ->setEcouponValueType(null);
            $customerSession->setEcoupon(1);
            $quote ->save($quote->collectTotals());
            
            return true;
        }
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
        }
        $quote->getShippingAddress()->setCollectShippingRates(true);
        try {
            $quote->setCouponCode('');
            $this->quoteRepository->save($quote->collectTotals());
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete coupon code'));
        }
        if ($quote->getCouponCode() != '') {
            throw new CouldNotDeleteException(__('Could not delete coupon code'));
        }
        return true;
    }
}
