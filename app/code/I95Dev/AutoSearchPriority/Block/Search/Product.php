<?php
namespace I95Dev\AutoSearchPriority\Block\Search;

use Magento\Framework\DB\Select;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorage;

/**
 * @psalm-suppress UndefinedClass
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Product extends \Amasty\Xsearch\Block\Search\Product
{
    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     * @psalm-suppress MissingReturnType
     */
    protected function _construct()
    {
        $this->_template = 'search/product.phtml';
        parent::_construct();
    }

    /**
     * @return $this
     * @psalm-suppress PropertyNotSetInConstructor
     * @psalm-suppress DocblockTypeContradiction
     * @psalm-suppress NullReference
     */
    public function prepareCollection()
    {
        if ($this->_productCollection === null) {
            $this->_getProductCollection();
            $this->_productCollection->clear();
            $this->_productCollection->setPageSize($this->getLimit());
            $this->_productCollection->getSelect()
                ->reset(Select::ORDER)
                ->order('search_result.'. TemporaryStorage::FIELD_SCORE . ' ' . Select::SQL_DESC);

            $this->_eventManager->dispatch(
                'catalog_block_product_list_collection',
                ['collection' => $this->_productCollection
                ->addAttributeToSelect('auto_search_priority_product')
                ->addAttributeToSort("auto_search_priority_product", "desc")
                ]
            );

            $this->_productCollection->load();
        }

        return $this;
    }
}
