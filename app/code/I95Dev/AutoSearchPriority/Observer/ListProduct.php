<?php
namespace I95Dev\AutoSearchPriority\Observer;

use Magento\Framework\Event\ObserverInterface;

class ListProduct implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $productCollection = $observer->getEvent()->getCollection();

        $productCollection
            ->addAttributeToSelect('auto_search_priority_product')
            ->addAttributeToSort("auto_search_priority_product", "desc");
        return $this;
    }
}
