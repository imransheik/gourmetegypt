<?php

namespace I95Dev\AutoSearchPriority\Setup;

 use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Resource\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 *
 * @psalm-suppress UndefinedClass
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * Init
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        /* assign object to class global variable for use in other class methods */
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            
        /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /**
         * Add attributes to the eav/attribute
       */
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'auto_search_priority_product',
                [
                'type' => 'int',
        
                'label' => 'Auto Search Priority',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'used_for_sort_by'=>1,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'General'
                ]
            );

        }

        $setup->endSetup();
    }
}
