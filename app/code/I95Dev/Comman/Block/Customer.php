<?php
namespace I95Dev\Comman\Block;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class Customer extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * Customer constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     * @psalm-suppress PropertyNotSetInConstructor
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customer,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        $this->customer = $customer;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     * @psalm-suppress UndefinedConstant
     */
    //phpcs:disable
    public function customerHasNewCario()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomer()->getId();
            $customerObj = $this->customer->load($customerId);
            $customerAddress = [];
            $hascity = false;
            foreach ($customerObj->getAddresses() as $address) {
                $customerAddress[] = $address->toArray();
            }
            foreach ($customerAddress as $customerAddres) {
                if ($customerAddres['city'] == "New Cairo") {
                    $hascity = true;
                }
            }
            return $hascity;
        } else {
            return false;
        }
    }
}
