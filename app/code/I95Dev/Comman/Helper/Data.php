<?php

namespace I95Dev\Comman\Helper;

/**
 * Returns base helper
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CACHE_NEW_ID = 'assist_label_new';
    const CACHE_OUTSTOCK_ID = 'assist_label_stock';
    const CACHE_WISHLIST_ID = 'assist_label_wishlist';
    const CACHE_SPECIAL_ID = 'assist_label_special';
    const CACHE_LOWSTOCK_ID = 'assist_label_lowstock';
    const CACHE_LABEL_PRODUCT_LABEL = 'assist_label_product_label';
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_productCollection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_storeid;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customer;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $wishlist;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_assisthelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_eavEntityAttribute;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_collectionCache;
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productData;

    /**
     * Data constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Wishlist\Model\Wishlist $wishlist
     * @param \Magento\Framework\App\Cache\Type\Collection $collectionCache
     * @param \Gourmet\Assist\Helper\Data $assisthelper
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavEntityAttribute
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\Product $productData
     * @param array $data
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Model\Wishlist $wishlist,
        \Magento\Framework\App\Cache\Type\Collection $collectionCache,
        \Gourmet\Assist\Helper\Data $assisthelper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavEntityAttribute,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\Product $productData,
        array $data = []
    ) {
        $this->_productCollection = $collectionFactory;
        $this->_storeManager = $storeManager;
        $this->_storeid = $storeManager->getStore()->getId();
        $this->_customerSession = $customerSession;
        $this->wishlist = $wishlist;
        $this->_assisthelper = $assisthelper;
        $this->_eavEntityAttribute = $eavEntityAttribute;
        $this->stockRegistry = $stockRegistry;
        $this->resourceConnection = $resourceConnection;
        $this->productData = $productData;
        $this->_collectionCache = $collectionCache;

        //if ($this->_assisthelper->isDeveloperMode()) {
        //$this->_collectionCache->clean();
        //}
    }

    /* return product based on priority
      - Out of stock
      - Special Price
      - Low Stock
      - New
      - In Wishlist
      - Low quantity
     */
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $inventory
     * @return int|\Magento\Framework\Phrase|mixed|string
     * @psalm-suppress  MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MissingParamType
     * @psalm-suppress TooManyArguments
     * @psalm-suppress UndefinedFunction
     */
    public function fetchLabel(\Magento\Catalog\Model\Product $product, $inventory)
    {
        $productId = $product->getId();

        if ($this->isProductOutofStock($productId, $inventory)) {
            return __('Out Of Stock');
        }

        if ($this->isCatalogRulePrice($productId)) {
            return 'Special Offer';
        }

        if ($this->isSpecialPrice($productId)) {
            return 'Special Offer';
        }

        if ($prodLabel = $this->fetchProductLabel($productId)) {
            return $prodLabel;
        }

        /* if($this->isLowInQuantity($productId, $inventory)){
          return 'Low Quantity';
          } */

        if ($this->isProductNew($productId)) {
            return 'New Product';
        }

//
        if ($this->isWishlistItem($productId)) {
            return 'In Your Wishlist';
        }
//


        return '';
    }

    /**
     * @param $product_id
     * @param $inventory
     * @return bool
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    // phpcs:disable
    protected function isProductOutofStock($product_id, $inventory)
    {
        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); //phpcs:ignore
        // $productStockObj = $objectManager->get(\Magento\CatalogInventory\Api\StockRegistryInterface::class)->getStockItem($product_id); //phpcs:ignore
        $productStockObj = $this->stockRegistry->getStockItem($product_id);
        $minQty = $productStockObj->getMinQty();

        if ((isset($inventory) && $inventory <= $minQty) || (!isset($inventory))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $product_id
     * @return bool
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingReturnType
     */
    // phpcs:disable
    public function isProductNew($product_id)
    {
        $newArrSerial = $this->_collectionCache->load(self::CACHE_NEW_ID);
        if (!$newArrSerial) {

            $newCollection = $this->_productCollection->create()->addStoreFilter($this->_storeid);
            $dateformat = new \DateTime();
            $today = $dateformat->format('Y-m-d H:i:s');

            $newArr = $newCollection->addAttributeToFilter('news_from_date', [
                'date' => true,
                'to' => $today
            ])->getAllIds();

            $newArrSerial = serialize($newArr); //phpcs:ignore
            $this->_collectionCache->save($newArrSerial, self::CACHE_NEW_ID);
        }

        $newArr = unserialize($newArrSerial); //phpcs:ignore
        $result = in_array($product_id, $newArr);

        if ($result) {
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager //phpcs:ignore
//            $resource = $objectManager->get(\Magento\Framework\App\ResourceConnection::class);  //phpcs:ignore
            $resource = $this->resourceConnection;
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('catalog_product_new_end_date'); //gives table name with prefix
            //Select Data from table
            $sql = "Select * FROM " . $tableName . " WHERE end_date >= CURDATE() AND product_id=" . $product_id; //phpcs:ignore
            $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
            $count = count($result);
            if ($count > 0) {
                $result = true;
            } else {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @return array
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function isWishlistItem()
    {
        $productArray = [];
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = $this->_customerSession->getId();

            $wishlist_collection = $this->wishlist->loadByCustomerId($customerId, true)
                ->getItemCollection()->addFieldToFilter('store_id', ['eq' => $this->_storeid]);

            $customerArr = [];
            foreach ($wishlist_collection as $item) {
                $productArray[] = $item->getProductId();
            }

            return $productArray;
        } else {
            return $productArray;
        }
    }

    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    protected function isSpecialPrice($product_id)
    {
        $specialArrSerial = $this->_collectionCache->load(self::CACHE_SPECIAL_ID);
        if (!$specialArrSerial) {
            $now = date('Y-m-d H:i:s');
            $specialCollection = $this->_productCollection->create()
                ->addStoreFilter($this->_storeid)
                ->addFinalPrice()->addAttributeToSelect('special_from_date')
                ->addAttributeToSelect('special_to_date')
                ->addAttributeToFilter('special_price', ['neq' => ''])
                ->addAttributeToFilter(
                    'special_from_date',
                    ['lteq' => date('Y-m-d H:i:s', strtotime($now))]
                )
                ->addAttributeToFilter(
                    'special_to_date',
                    ['gteq' => date('Y-m-d H:i:s', strtotime($now))]
                );
            ;
            $specialCollection->getSelect()->reset(
                \Magento\Framework\DB\Select::COLUMNS
            )->columns([
                'entity_id',
                'price_index.price',
                'price_index.final_price'
            ])->where(new \Zend_Db_Expr('`price_index`.`final_price` < ?'), new \Zend_Db_Expr('`price_index`.`price`'));

            $specialArr = [];
            foreach ($specialCollection as $item) {
                $specialArr[] = $item->getId();
            }

            $specialArrSerial = serialize($specialArr); //phpcs:ignore
            $this->_collectionCache->save($specialArrSerial, self::CACHE_SPECIAL_ID);
        }

        $specialArr = unserialize($specialArrSerial); //phpcs:ignore

        return in_array($product_id, $specialArr);
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     */
    protected function isCatalogRulePrice($product_id)
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); //phpcs:ignore
//        $product = $objectManager->create(\Magento\Catalog\Model\Product::class)->load($product_id);//phpcs:ignore
        $product = $this->productData->load($product_id);
        $ctPrice = $product->getPriceModel()->getFinalPrice(1, $product);
        $FPrice = $product->getFinalPrice();
        $newPrice = $product->getPrice();
        if (($FPrice != '' && $ctPrice != '')):
            $percentChange = (1 - $FPrice / $newPrice) * 100;
            if (round($percentChange) > 0):
                //echo round($percentChange) . "% Off";
                return true;

            endif;
        endif;
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     */
    protected function isLowInQuantity($product_id, $inventory)
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); //phpcs:ignore
//        $product = $objectManager->create(\Magento\Catalog\Model\Product::class)->load($product_id); //phpcs:ignore
        $product = $this->productData->load($product_id);
        $lowQty = $product->getResource()->getAttribute('low_inventory')->getFrontend()->getValue($product);

        if (isset($inventory) && $inventory <= $lowQty) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     */
    protected function fetchProductLabel($product_id)
    {
        $prod_label = 0;
        $labelArrSerial = $this->_collectionCache->load(self::CACHE_LABEL_PRODUCT_LABEL);
        if (!$labelArrSerial) {
            $labelArr = [];

            if ($attributeId = $this->_eavEntityAttribute->getIdByCode('catalog_product', 'product_label')) {
                $labelCollection = $this->_productCollection->create()->addStoreFilter($this->_storeid);

                $labelCollection->getSelect()
                    ->joinInner(
                        ['cpei' => 'catalog_product_entity_int'],
                        'cpei.row_id=e.entity_id AND cpei.store_id=0 AND cpei.attribute_id=' . $attributeId,
                        []
                    )->joinInner(
                        ['eaov' => 'eav_attribute_option_value'],
                        'cpei.value=eaov.option_id',
                        ['eaov.value']
                    );

                foreach ($labelCollection as $item) {
                    $labelArr[$item->getEntityId()] = $item->getValue();
                }
            }

            $labelArrSerial = serialize($labelArr); //phpcs:ignore
            $this->_collectionCache->save($labelArrSerial, self::CACHE_LABEL_PRODUCT_LABEL); //phpcs:ignore
        }

        $labelArr = unserialize($labelArrSerial); //phpcs:ignore
        if (array_key_exists($product_id, $labelArr)) {
            $prod_label = $labelArr[$product_id];
        }

        return $prod_label;
    }
}
