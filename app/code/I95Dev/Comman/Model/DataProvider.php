<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Comman\Model;

use Magento\Framework\App\ObjectManager;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @api
 * @since 100.0.2
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class DataProvider extends \Magento\Customer\Model\Customer\DataProvider
{
    /**
     * @return mixed
     * @psalm-suppress PropertyNotSetInConstructor
     * @psalm-suppress UndefinedClass
     * @psalm-suppress LessSpecificImplementedReturnType
     */
    public function getMeta()
    {
        $meta = parent::getMeta();
        $allowedUsers = ["abdel@gmail.com", "abdelrahman.ismail@gourmetegypt.com","loay.mohamed@gourmetegypt.com","noureldin.ashraf@gourmetegypt.com", "noureldin.salheya@gourmetegypt.com"]; //phpcs:ignore
        if ($this->getParamId()) {
            $customer = $this->collection->addAttributeToFilter('entity_id', $this->getParamId())->addAttributeToSelect("*"); //phpcs:ignore
            $primary_mobile_number = $customer->getFirstItem()->getData("primary_mobile_number");
            $isd_code = $customer->getFirstItem()->getData("isd_code");
            if (!in_array($this->getCurrentUser()->getEmail(), $allowedUsers)) {
                if ($primary_mobile_number) {
                    $meta['customer']['children']['primary_mobile_number']['arguments']['data']['config']['disabled'] = 1; //phpcs:ignore
                }
                if ($isd_code) {
                    $meta['customer']['children']['isd_code']['arguments']['data']['config']['disabled'] = 1;
                }
            }
        }
        return $meta;
    }

    /**
     * @return mixed
     * @psalm-suppress UndefinedClass
     */
    public function getCurrentUser()
    {
        return ObjectManager::getInstance()->get(
            \Magento\Backend\Model\Auth\Session::class
        )->getUser();
    }

    /**
     * @return mixed
     * @psalm-suppress UndefinedClass
     */
    public function getParamId()
    {
        return ObjectManager::getInstance()->get(
            \Magento\Framework\App\Request\Http::class
        )->getParam('id');
    }
}
