<?php

namespace I95Dev\Comman\Model;

use Magento\Framework\Locale\Bundle\DataBundle;

class Format extends \Magento\Framework\Locale\Format
{
    /**
     * @var string
     */
    private static $defaultNumberSet = 'latn';

    /**
     * @param null $localeCode
     * @param null $currencyCode
     * @return array
     * @psalm-suppress MoreSpecificImplementedParamType
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress DocblockTypeContradiction
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress InvalidArrayAccess
     * @psalm-suppress PossiblyFalseOperand
     */
    public function getPriceFormat($localeCode = null, $currencyCode = null)
    {
        $localeCode = $localeCode ?: $this->_localeResolver->getLocale();
        if ($currencyCode) {
            $currency = $this->currencyFactory->create()->load($currencyCode);
            
        } else {
            $currency = $this->_scopeResolver->getScope()->getCurrentCurrency();
            
        }
        $localeData = (new DataBundle())->get($localeCode);
        $defaultSet = $localeData['NumberElements']['default'] ?: self::$defaultNumberSet;
        $format = $localeData['NumberElements'][$defaultSet]['patterns']['currencyFormat']
                ?: ($localeData['NumberElements'][self::$defaultNumberSet]['patterns']['currencyFormat']
                ?: explode(';', $localeData['NumberPatterns'][1])[0]);
        $decimalSymbol = '.';
        $groupSymbol = ',';
        $pos = strpos($format, ';');
        if ($pos !== false) {
            $format = substr($format, 0, $pos);
            
        }
        $format = preg_replace("/[^0\#\.,]/", "", $format);
        $totalPrecision = 0;
        $decimalPoint = strpos($format, '.');
        if ($decimalPoint !== false) {
            $totalPrecision = strlen($format) - (strrpos($format, '.') + 1);
            
        } else {
            $decimalPoint = strlen($format);
            
        }
        $requiredPrecision = $totalPrecision;
        $t = substr($format, $decimalPoint);
        $pos = strpos($t, '#');
        if ($pos !== false) {
            $requiredPrecision = strlen($t) - $pos - $totalPrecision;
            
        }
        if (strrpos($format, ',') !== false) {
            $group = $decimalPoint - strrpos($format, ',') - 1;
            
        } else {
            $group = strrpos($format, '.');
            
        }
        $integerRequired = strpos($format, '.') - strpos($format, '0');
        $result = [
            'pattern' => $currency->getOutputFormat(),
            'precision' => $totalPrecision,
            'requiredPrecision' => $requiredPrecision,
            'decimalSymbol' => $decimalSymbol,
            'groupSymbol' => $groupSymbol,
            'groupLength' => 8,
            'integerRequired' => $integerRequired,
            ];
        return $result;
    }
}
