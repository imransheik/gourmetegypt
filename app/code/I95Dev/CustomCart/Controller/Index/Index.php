<?php
namespace I95Dev\CustomCart\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;

/**
 * @psalm-suppress InvalidReturnType
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     * @psalm-suppress UndefinedClass
     */
    private $productRepository;
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $imageHelperFactory;
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $priceHelper;
    /**
     * Index constructor.
     * @param Context $context
     * @param Cart $Cart
     * @param Product $Product
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param Product\Option $productOption
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        Context $context,
        Cart $Cart,
        Product $Product,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product\Option $productOption,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->Product = $Product;
        $this->ProductOption = $productOption;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->Cart = $Cart;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->priceHelper = $priceHelper;
        parent::__construct($context);
    }
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedConstant
     */
    //phpcs:disable
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        $resultJson = $this->resultJsonFactory->create();
        $product_id = $post['product_id'];
        $main_product_id = $post['main_product_id'];
        $product = $this->Product->load($main_product_id);
        $customOptions = $this->ProductOption->getProductOptionCollection($product);
        $response=[];
        foreach ($customOptions as $optionKey => $optionVal) {
            foreach ($optionVal->getValues() as $valuesKey => $valuesVal) {
                if ($product_id == $valuesVal->getData('option_type_id')) {
                    $sku = $valuesVal->getData('sku');
                    if ($sku){
                        $productId = $this->Product->getIdBySku($sku);
                    $custom_product = $this->Product->load($productId);
                    $custom_weight = $custom_product->getData('product_weight');
                    $imageUrl = $this->imageHelperFactory->create()
                        ->init($product, 'product_base_image')->getUrl();
//                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//                        $priceHelper = $objectManager->create(\Magento\Framework\Pricing\Helper\Data::class);
                    $response['weight']=$custom_weight;
                    $response['imageUrl']=$imageUrl;
                    $formattedPrice = $this->priceHelper->currency($custom_product->getFinalPrice(), true, false);
                    $response['finalPrice']=$formattedPrice;
                    $response['finalPriceWc']=$custom_product->getFinalPrice();
                    return $resultJson->setData(json_encode($response));
                }
            }
        }
    }
}
}
