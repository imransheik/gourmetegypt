<?php

namespace I95Dev\CustomCart\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class Option extends \Magento\Framework\App\Action\Action
{

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     * @psalm-suppress MissingPropertyType
     * @psalm-suppress UndefinedClass
     */
    private $productRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $imageHelperFactory;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        Context $context,
        Cart $Cart,
        Product $Product,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product\Option $productOption,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->Product = $Product;
        $this->ProductOption = $productOption;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->Cart = $Cart;
        $this->imageHelperFactory = $imageHelperFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        $resultJson = $this->resultJsonFactory->create();
        $product_id = $post['product_id'];

        $product = $this->Product->load($product_id);
        $customOptions = $this->ProductOption->getProductOptionCollection($product);
        
        $html='';
         $response=[];
        foreach ($customOptions as $optionKey => $optionVal) {
            if ($optionVal->getData("type") == "drop_down") {
                $html = '<div class="field required">
    <label class="label" for="select_">
        <span>' . $optionVal->getData("title") . '</span>
    </label>
    <div class="control"><select name="options[' . $optionVal->getData("option_id") . ']" id="select_' . $optionVal->getData("option_id") . '" class=" required product-custom-option admin__control-select" title="" data-selector="options[' . $optionVal->getData("option_id") . ']" aria-required="true">'; //phpcs:ignore

                foreach ($optionVal->getValues() as $valuesKey => $valuesVal) {
 
                    $html .= '<option value="' . $valuesVal->getData('option_type_id') . '" price="' . number_format((float)$valuesVal->getData('price'), 2, ".", '') . '">' . $valuesVal->getData('title') . '</option>'; //phpcs:ignore
                }
                $html .= "</select>   </div></div>";
            }
        }
        $response["htmloption"] = $html;
        return $resultJson->setData(json_encode($response));
    }
}
