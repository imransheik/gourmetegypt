<?php

namespace I95Dev\CustomCart\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CustomData implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getEvent()->getData('quote_item');
        $quote = $item->getQuote();
    }
}
