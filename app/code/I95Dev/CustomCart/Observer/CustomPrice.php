<?php

namespace I95Dev\CustomCart\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CustomPrice implements ObserverInterface
{
    /**
     * @var RequestInterface
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @psalm-suppress UndefinedMethod
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $price=$this->_request->getPost("custom-price");
        if ($price) {
       
            $item = $observer->getEvent()->getData('quote_item');
            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
       
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->getProduct()->setIsSuperMode(true);
        }
    }
}
