<?php


namespace I95Dev\CustomerGroupRules\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CustomergrouprulesRepositoryInterface
{

    /**
     * Save customergrouprules
     * @param \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
    );

    /**
     * Retrieve customergrouprules
     * @param string $customergrouprulesId
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($customergrouprulesId);

    /**
     * Retrieve customergrouprules matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete customergrouprules
     * @param \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
    );

    /**
     * Delete customergrouprules by ID
     * @param string $customergrouprulesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customergrouprulesId);
}
