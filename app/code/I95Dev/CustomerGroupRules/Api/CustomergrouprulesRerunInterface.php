<?php

namespace I95Dev\CustomerGroupRules\Api;
 
interface CustomergrouprulesRerunInterface
{
    /**
     * @param string $customerid.
     * @return \I95Dev\CustomerGroupRules\Api\CustomergrouprulesRerunInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     */
    public function segmenatationrerun($customerid);
}
