<?php


namespace I95Dev\CustomerGroupRules\Api\Data;

interface CustomergrouprulesInterface
{

    const GROUP_ID = 'group';
    const NAME = 'Name';
    const ID = 'id';

    /**
     * Get customergrouprules_id
     * @return string|null
     */
    public function getCustomergrouprulesId();

    /**
     * Set customergrouprules_id
     * @param string $customergrouprulesId
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     */
    public function setCustomergrouprulesId($customergrouprulesId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     */
    public function setId($id);

    /**
     * Get Name
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     * @param string $name
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     */
    public function setName($name);
}
