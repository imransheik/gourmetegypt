<?php


namespace I95Dev\CustomerGroupRules\Api\Data;

interface CustomergrouprulesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get customergrouprules list.
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
