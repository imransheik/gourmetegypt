<?php


namespace I95Dev\CustomerGroupRules\Block\Adminhtml\Customergrouprules\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save Peer Registration Group Mapping'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
