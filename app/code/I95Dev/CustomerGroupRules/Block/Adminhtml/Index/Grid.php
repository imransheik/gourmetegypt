<?php


namespace I95Dev\CustomerGroupRules\Block\Adminhtml\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Grid extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    //phpcs:disable
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    //phpcs:enable
    /**
     * @return string
     * @psalm-suppress UndefinedFunction
     */
    public function test()
    {
        //Your block code
        return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts')); //phpcs:ignore
    }
}
