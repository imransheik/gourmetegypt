<?php
namespace I95Dev\CustomerGroupRules\Block;

use I95Dev\CustomerGroupRules\Model\Config\Source\Subject;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class PeerRegister extends \Magento\Framework\View\Element\Template
{

        /**
         * @var Subject
         */
    protected $subject;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $request;

    /**
     * ContactForm constructor.
     */
    public function __construct(Subject $subject, \Magento\Framework\App\Request\Http $request)
    {
        $this->subject = $subject;
        $this->request = $request;
    }

    /**
     * @return array|\string[][]
     */
    public function getSubject()
    {
        $subject = $this->subject->toOptionArray();
        return $subject;
    }

    /**
     * @return mixed
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function getAllOptions()
    {
        $this->options[] = ['value' => 211, 'label' => "CAll"];
        $this->options[] = ['value' => 212, 'label' => "SMS"];
        $this->options[] = ['value' => 213, 'label' => "Email"];

        return $this->options;
    }

    /**
     * @return mixed
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function getPrefferedLanguage()
    {
        $this->_options[] = ['value' => 209, 'label' => "English"];
        $this->_options[] = ['value' => 210, 'label' => "اللغة العربية"];
        return $this->_options;
    }

    /**
     * @return string
     */
    public function controllerName()
    {
        $controller = $this->request->getControllerName();
        return $controller;
    }
}
