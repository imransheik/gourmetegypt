<?php

namespace I95Dev\CustomerGroupRules\Controller\Account;

use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @psalm-suppress PropertyNotSetInConstructor
 */
class EditPost extends \Magento\Customer\Controller\Account\EditPost
{
    /**
     * Form code for data extractor
     */
    const FORM_DATA_EXTRACTOR_CODE = 'customer_account_edit';

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var CustomerExtractor
     */
    protected $customerExtractor;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Magento\Customer\Model\EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    public $customerGroupRule;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_collectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customer;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $url;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $accountManagement;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     * @param CustomerExtractor $customerExtractor
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\AccountManagement $accountManagement
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        CustomerExtractor $customerExtractor,
        \I95Dev\CustomerGroupRules\Model\CustomergrouprulesFactory $customerGroupRule,
        \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\AccountManagement $accountManagement
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $customerAccountManagement,
            $customerRepository,
            $formKeyValidator,
            $customerExtractor); //phpcs:ignore
        $this->customerGroupRule = $customerGroupRule;
        $this->_collectionFactory = $collectionFactory;
        $this->customer = $customer;
        $this->url = $url;
        $this->accountManagement = $accountManagement;
    }

    /**
     * Get authentication
     *
     * @return AuthenticationInterface
     * @psalm-suppress RedundantConditionGivenDocblockType
     */
    private function getAuthentication()
    {
        if (!($this->authentication instanceof AuthenticationInterface)) {
            return ObjectManager::getInstance()->get(
                \Magento\Customer\Model\AuthenticationInterface::class
            );
        } else {
            return $this->authentication;
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     * @psalm-suppress RedundantConditionGivenDocblockType
     */
    private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * Change customer email or password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress DeprecatedMethod
     * @deprecated
     * @psalm-suppress PossiblyNullArgument
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if ($validFormKey && $this->getRequest()->isPost()) {
            $currentCustomerDataObject = $this->getCustomerDataObject($this->session->getCustomerId());
            $customerCandidateDataObject = $this->populateNewCustomerDataObject(
                $this->_request,
                $currentCustomerDataObject
            );

            try {
                $email = $this->getRequest()->getPost('email');

                $emailNew = strstr($email, '@');
                $collectionNew = $this->_collectionFactory->create();
                $data = $collectionNew->addFieldToFilter('domain', $emailNew);
                $confirmationStatus = $this->customerAccountManagement->getConfirmationStatus($customerCandidateDataObject->getId()); //phpcs:ignore

                if (($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMED) && (!empty($data->getData())) || (($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_NOT_REQUIRED) && (!empty($data->getData())))) { //phpcs:ignore
                    $domainGroup = $data->getData()[0]['group'];
                    $customerCandidateDataObject->setGroupId($domainGroup);
                    $customerCandidateDataObject->setEmail($email);
                    $customerCandidateDataObject->setConfirmation(md5(uniqid())); //phpcs:ignore

                    $this->customerRepository->save($customerCandidateDataObject);
//                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//                    $customer = $objectManager->get(\Magento\Customer\Model\Customer::class)
//->load($customerCandidateDataObject->getId()); //phpcs:ignore
                    $customer = $this->customer->load($customerCandidateDataObject->getId()); //phpcs:ignore
                    //$customerUrl = $objectManager->create(\Magento\Customer\Model\Url::class); //phpcs:ignore
                    $customerUrl = $this->url;
                    $email = $customerUrl->getEmailConfirmationUrl($customer->getEmail());
                    $accountManagementObj = $this->accountManagement;

                    // @codingStandardsIgnoreStart
                    $this->messageManager->addSuccess(
                        __(
                            'For login you must confirm your account. Please check your email for the confirmation link or <a href="%1">click here</a> for a new link.',
                            $email
                        )
                    );

                    $accountManagementObj->resendConfirmation($customerCandidateDataObject->getEmail(),$customer->getStoreId(),$email);

                    return $resultRedirect->setPath('customer/account/logout');
                }else{
                    $customerCandidateDataObject->setGroupId(1);
                }

                // whether a customer enabled change email option
                $this->processChangeEmailRequest($currentCustomerDataObject);

                // whether a customer enabled change password option
                $isPasswordChanged = $this->changeCustomerPassword($currentCustomerDataObject->getEmail());

                $this->customerRepository->save($customerCandidateDataObject);
                $this->getEmailNotification()->credentialsChanged(
                    $customerCandidateDataObject,
                    $currentCustomerDataObject->getEmail(),
                    $isPasswordChanged
                );
                $this->dispatchSuccessEvent($customerCandidateDataObject);
                $this->messageManager->addSuccess(__('You saved the account information.'));
                $language=$this->getRequest()->getPost('prefered_language');
                if($language=="210"){
                    return $resultRedirect->setPath('customer/account/?___store=arabic_store&___from_store=default&');
                }else{
                    return $resultRedirect->setPath('customer/account/?___store=default&___from_store=arabic_store&');
                }
            } catch (InvalidEmailOrPasswordException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (UserLockedException $e) {
                $message = __(
                    'You did not sign in correctly or your account is temporarily disabled.'
                );
                $this->session->logout();
                $this->session->start();
                $this->messageManager->addError($message);
                return $resultRedirect->setPath('customer/account/login');
            } catch (InputException $e) {
                $this->messageManager->addError($e->getMessage());
                foreach ($e->getErrors() as $error) {
                    $this->messageManager->addError($error->getMessage());
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t save the customer.'));
            }

            $this->session->setCustomerFormData($this->getRequest()->getPostValue());
        }

        return $resultRedirect->setPath('*/*/edit');
    }

    /**
     * Account editing action completed successfully event
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject
     * @return void
     */
    private function dispatchSuccessEvent(\Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject)
    {
        $this->_eventManager->dispatch(
            'customer_account_edited',
            ['email' => $customerCandidateDataObject->getEmail()]
        );
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }

    /**
     * Create Data Transfer Object of customer candidate
     *
     * @param \Magento\Framework\App\RequestInterface $inputData
     * @param \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @psalm-suppress DeprecatedMethod
     * @psalm-suppress PossiblyNullArgument
     */
    private function populateNewCustomerDataObject(
        \Magento\Framework\App\RequestInterface $inputData,
        \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
    ) {
        $attributeValues = $this->getCustomerMapper()->toFlatArray($currentCustomerData);
        $customerDto = $this->customerExtractor->extract(
            self::FORM_DATA_EXTRACTOR_CODE,
            $inputData,
            $attributeValues
        );
        $customerDto->setId($currentCustomerData->getId());
        if (!$customerDto->getAddresses()) {
            $customerDto->setAddresses($currentCustomerData->getAddresses());
        }
        if (!$inputData->getParam('change_email')) {
            $customerDto->setEmail($currentCustomerData->getEmail());
        }

        return $customerDto;
    }

    /**
     * Change customer password
     *
     * @param string $email
     * @return boolean
     * @throws InvalidEmailOrPasswordException|InputException
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedFunction
     */
    protected function changeCustomerPassword($email)
    {
        $isPasswordChanged = false;
        if ($this->getRequest()->getParam('change_password')) {
            $currPass = $this->getRequest()->getPost('current_password');
            $newPass = $this->getRequest()->getPost('password');
            $confPass = $this->getRequest()->getPost('password_confirmation');
            if ($newPass != $confPass) {
                throw new InputException(__('Password confirmation doesn\'t match entered password.'));
            }

            $isPasswordChanged = $this->customerAccountManagement->changePassword($email, $currPass, $newPass);
        }

        return $isPasswordChanged;
    }

    /**
     * Process change email request
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $currentCustomerDataObject
     * @return void
     * @throws InvalidEmailOrPasswordException
     * @throws UserLockedException
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyNullArgument
     * @psalm-suppress UndefinedFunction
     */
    private function processChangeEmailRequest(\Magento\Customer\Api\Data\CustomerInterface $currentCustomerDataObject)
    {
        if ($this->getRequest()->getParam('change_email')) {
            // authenticate user for changing email
            try {
                $this->getAuthentication()->authenticate(
                    $currentCustomerDataObject->getId(),
                    $this->getRequest()->getPost('current_password')
                );
            } catch (InvalidEmailOrPasswordException $e) {
                throw new InvalidEmailOrPasswordException(__('The password doesn\'t match this account.'));
            }
        }
    }

    /**
     * Get Customer Mapper instance
     *
     * @return Mapper
     *
     * @deprecated 100.1.3
     * @psalm-suppress DocblockTypeContradiction
     */
    private function getCustomerMapper()
    {
        if ($this->customerMapper === null) {
            $this->customerMapper = ObjectManager::getInstance()->get(\Magento\Customer\Model\Customer\Mapper::class);
        }
        return $this->customerMapper;
    }
}
