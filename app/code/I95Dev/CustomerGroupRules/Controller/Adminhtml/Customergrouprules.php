<?php


namespace I95Dev\CustomerGroupRules\Controller\Adminhtml;

abstract class Customergrouprules extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'I95Dev_CustomerGroupRules::top_level';
    /**
     * @psalm-suppress  MissingPropertyType
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedFunction
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('I95Dev'), __('I95Dev'))
            ->addBreadcrumb(__('Peer Registration Mapping'), __('Peer Registration Mapping'));
        return $resultPage;
    }
}
