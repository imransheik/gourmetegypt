<?php


namespace I95Dev\CustomerGroupRules\Controller\Adminhtml\Customergrouprules;

class Delete extends \I95Dev\CustomerGroupRules\Controller\Adminhtml\Customergrouprules
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\I95Dev\CustomerGroupRules\Model\Customergrouprules::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Customergrouprules.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Customergrouprules to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
