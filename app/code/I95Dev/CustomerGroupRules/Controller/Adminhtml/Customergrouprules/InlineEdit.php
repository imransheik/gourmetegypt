<?php


namespace I95Dev\CustomerGroupRules\Controller\Adminhtml\Customergrouprules;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $jsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Inline edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        // phpcs:disable
        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelid) {
                    /** @var \I95Dev\CustomerGroupRules\Model\Customergrouprules $model */
                    $model = $this->_objectManager->create(\I95Dev\CustomerGroupRules\Model\Customergrouprules::class)->load($modelid); //phpcs:ignore
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$modelid])); //phpcs:ignore                        $model->setData(array_merge($model->getData(), $postItems[$modelid]));

                        $model->save();
                    } catch (\Exception $e) {
                        $messages[] = "[Customergrouprules ID: {$modelid}]  {$e->getMessage()}";
                        $error = true;
                    }
                }
            }
        }
        
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
