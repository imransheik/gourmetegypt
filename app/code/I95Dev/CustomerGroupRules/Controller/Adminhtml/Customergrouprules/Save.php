<?php


namespace I95Dev\CustomerGroupRules\Controller\Adminhtml\Customergrouprules;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataPersistor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customerGroupRules;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $customerGroupRules
    ) {
        $this->collectionFactory = $customerGroupRules;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $this->getRequest()->getParam('id');
        
            $model = $this->_objectManager->create(\I95Dev\CustomerGroupRules\Model\Customergrouprules::class)
                ->load($id);
            
            $postDomainName = $this->getRequest()->getParam('domain');
            $incetiveTypes = $this->collectionFactory->create();
            $incetiveTypes->addFieldToFilter('domain', ['finset' =>  $postDomainName]);
            $incetiveTypes->addFieldToFilter('active', 0);

            $incentiveData = $incetiveTypes->getData();
            
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Customergrouprules no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
        
            try {
                if (empty($incentiveData) || ($incentiveData[0]['id'] == $id)) {
                    $model->setData($data);
                    $model->save();
                } else {
                    $this->messageManager->addError('Domain Name should be unique.');
                    return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                }
                $this->messageManager->addSuccessMessage(__('You saved the Customergrouprules.'));
                $this->dataPersistor->clear('i95dev_customergrouprules_customergrouprules');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Customergrouprules.')); //phpcs:ignore
            }
        
            $this->dataPersistor->set('i95dev_customergrouprules_customergrouprules', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
