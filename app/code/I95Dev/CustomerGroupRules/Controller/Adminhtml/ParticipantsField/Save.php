<?php

namespace I95Dev\CustomerGroupRules\Controller\Adminhtml\ParticipantsField;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customerGroupRules;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @psalm-suppress MismatchingDocblockParamType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress TypeCoercion
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField\CollectionFactory $customerGroupRules,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->collectionFactory = $customerGroupRules;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress DeprecatedMethod
     */
    public function execute()
    {

        $data = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create(\I95Dev\CustomerGroupRules\Model\ParticipantsField::class)->load($id);

        $value = $data['provider'];
        $fieldvalue = '@'.$value;

        $incetiveTypes = $this->collectionFactory->create();
        $incetiveTypes->addFieldToFilter('field_value', ['finset' =>  $fieldvalue]);

        $incentiveData = $incetiveTypes->getData();
     //      var_dump($incentiveData); exit;
      // $value = $data['provider'];
       // $fieldvalue = '@'.$value;
        try {
            if (empty($incentiveData) || ($incentiveData[0]['id'] == $id)) {
                $model->setData('field_type', $data['type']);
                $model->setData('field_value', $fieldvalue);
                $model->save();
            } else {
                $this->messageManager->addError('Domain Name should be unique.');
                //return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
            $result->setData(['type'=>'success','message' => 'Data saved Successfully !!']);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result->setData(['message' => $e->getMessage()]);
        } catch (\RuntimeException $e) {
            $result->setData(['type'=>'error','message' => $e->getMessage()]);
        } catch (\Exception $e) {
            $result->setData(['type'=>'error','message' => "Something went wrong while saving the Data."]);
            $this->_getSession()->setFormData(false);
        }
        return $result;
    }
}
