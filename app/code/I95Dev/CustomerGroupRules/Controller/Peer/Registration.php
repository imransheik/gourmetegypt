<?php

namespace I95Dev\CustomerGroupRules\Controller\Peer;

use Magento\Framework\App\Action\Context;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Registration extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_resultPageFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
    
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }
}
