<?php

namespace I95Dev\CustomerGroupRules\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
 
class CustomSection implements SectionSourceInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    public $session;
    /**
     * @psalm-suppress MissingPropertyType
     */
    public $customerGroupRule;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $segmentName;
    /**
     * @psalm-suppress MissingPropertyType
     */
    public $customerGroupCollection;

    /**
     * CustomSection constructor.
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Customer\Model\Session $session
     * @param \I95Dev\CustomerGroupRules\Model\CustomergrouprulesFactory $customerGroupRule
     * @param \Magento\Customer\Model\Customer $customerModel
     * @param \Magento\CustomerSegment\Model\ResourceModel\Segment\Collection $segmentName
     * @param \Magento\Customer\Model\Group $customerGroupCollection
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Customer\Model\Session $session,
        \I95Dev\CustomerGroupRules\Model\CustomergrouprulesFactory $customerGroupRule,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\CustomerSegment\Model\ResourceModel\Segment\Collection $segmentName,
        \Magento\Customer\Model\Group $customerGroupCollection
    ) {
        $this->resource = $resource;
        $this->session = $session;
        $this->customerGroupRule = $customerGroupRule;
        $this->customerModel = $customerModel;
        $this->segmentName = $segmentName;
        $this->customerGroupCollection = $customerGroupCollection;
    }
    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function getSectionData()
    {
        if ($this->session->isLoggedIn()) {
            $customerId = $this->session->getCustomer()->getId();
            $customer = $this->customerModel->load($customerId);
            if ($customerId) {
                $websiteId = $this->session->getCustomer()->getWebsiteId();
                $tableName = $this->resource->getTableName('magento_customersegment_customer');
                $connection = $this->resource->getConnection();
                $sql = $connection->select()
                ->from($tableName) // to select some particular fields
                ->where('customer_id = ?', $customerId)
                ->where('website_id = ?', $websiteId); // adding WHERE condition with AND
                $result = $connection->fetchAll($sql);
                if (empty($result)) {
                    $segment_id = 0;
                } else {
                    $segment_id = $result['0']['segment_id'];
                }
                if (isset($segment_id) && $segment_id != 0) {
                    $customerRule = $this->customerGroupRule->create();
                    $i95devmodel = $customerRule->load($segment_id, "segment");
                    $NewCustomerGroupId = $i95devmodel->getGroup();
                    $segment = $this->segmentName->addFieldToFilter('segment_id', $segment_id)->getData();
                    $segment_name = $segment[0]['name'];
                } else {
                    $customerRule = $this->customerGroupRule->create();
                    $i95devmodel = $customerRule->load($segment_id, "segment");
                    $NewCustomerGroupId = $i95devmodel->getGroup();
                    if ($NewCustomerGroupId == '') {
                        $NewCustomerGroupId = $customer->getGroupId();
                    }
                    $segment = $this->segmentName->addFieldToFilter('segment_id', $segment_id)->getData();
                    $segment_name = '';
                }
                $group_name = $this->customerGroupCollection->load($NewCustomerGroupId);
                return [
                    'group_id' => $NewCustomerGroupId,
                    'segment_id' => $segment_id,
                    'group_name' => $group_name->getCustomerGroupCode(),
                    'segment_name' => $segment_name,
                    'customer_id' => $customerId,
                    'default_address_id' => $this->session->getDefaultAddressId()
                ];
            }
        }
        // Need to return an array
        return [];
    }
}
