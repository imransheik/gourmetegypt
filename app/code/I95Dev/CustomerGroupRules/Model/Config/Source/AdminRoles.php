<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\CustomerGroupRules\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class AdminRoles implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_roleFactory;

    /**
     * AdminRoles constructor.
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Authorization\Model\RoleFactory $roleFactory
    ) {
         $this->_roleFactory = $roleFactory;
    }
    /**
     * Options getter
     *
     * @return array
     * @psalm-suppress UndefinedClass
     */
    public function toOptionArray()
    {
        $RoleData=$this->_roleFactory->create()->getCollection()->addFieldToFilter('role_type', 'G')->getData();
        
        $options = [];
        foreach ($RoleData as $key => $user) {
                $options[$key] = [
                    'label' => $user['role_name'],
                    'value' => $user['role_id'],
                ];
        }
        return $options;
    }
}
