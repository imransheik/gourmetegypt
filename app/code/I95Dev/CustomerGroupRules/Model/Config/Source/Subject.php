<?php
/**
 * Module Contact Subject Source Model
 * @package MODULE\Contact
 */
declare(strict_types=1);

namespace I95Dev\CustomerGroupRules\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Subject implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_collectionFactory;

    /**
     * @var array|null
     */
    protected $_options;

    /**
     * Subject constructor.
     * @param \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $collectionFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     */
    public function toOptionArray()
    {
        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('active', 1);
        $options = [['label' => 'Please select Domain', 'value' => '']];
        foreach ($collection as $domain) {
                
            $options[] = [
                'label' => __('%1 ', $domain->getDomain()),
                'value' => $domain->getDomain()
            ];
        }

        return $options;
    }
}
