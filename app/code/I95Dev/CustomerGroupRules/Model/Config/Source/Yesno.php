<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\CustomerGroupRules\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Yesno implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Enabled')], ['value' => 0, 'label' => __('Disabled')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function toArray()
    {
        return [0 => __('Disabled'), 1 => __('Enabled')];
    }
}
