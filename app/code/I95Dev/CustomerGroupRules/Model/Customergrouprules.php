<?php


namespace I95Dev\CustomerGroupRules\Model;

use I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Customergrouprules extends \Magento\Framework\Model\AbstractModel implements CustomergrouprulesInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_eventPrefix = 'i95dev_customergrouprules_customergrouprules';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules::class);
    }

    /**
     * Get id
     * @return string
     */
    public function getCustomergrouprulesId()
    {
        return $this->getData(self::GROUP_ID);
    }

    /**
     * Set id
     * @param string $customergrouprulesId
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     */
    public function setCustomergrouprulesId($customergrouprulesId)
    {
        return $this->setData(self::ID, $customergrouprulesId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress MoreSpecificImplementedParamType
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get Name
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set Name
     * @param string $name
     * @return \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}
