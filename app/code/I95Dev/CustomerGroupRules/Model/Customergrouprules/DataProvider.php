<?php


namespace I95Dev\CustomerGroupRules\Model\Customergrouprules;

use I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $collection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataPersistor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     * @psalm-suppress UndefinedClass
     * @psalm-suppress NullableReturnStatement
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }
        $data = $this->dataPersistor->get('i95dev_customergrouprules_customergrouprules');
        
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('i95dev_customergrouprules_customergrouprules');
        }
        
        return $this->loadedData;
    }
}
