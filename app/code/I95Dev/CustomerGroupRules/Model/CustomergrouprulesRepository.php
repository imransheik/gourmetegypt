<?php


namespace I95Dev\CustomerGroupRules\Model;

use Magento\Framework\Api\DataObjectHelper;
use I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules as ResourceCustomergrouprules;
use Magento\Framework\Reflection\DataObjectProcessor;
use I95Dev\CustomerGroupRules\Api\CustomergrouprulesRepositoryInterface;
use I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory as CustomergrouprulesCollectionFactory; // phpcs:disable
use I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesSearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
/**
 * @psalm-suppress MissingPropertyType
 */
class CustomergrouprulesRepository implements CustomergrouprulesRepositoryInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataCustomergrouprulesFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customergrouprulesFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $searchResultsFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customergrouprulesCollectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;

    /**
     * @param ResourceCustomergrouprules $resource
     * @param CustomergrouprulesFactory $customergrouprulesFactory
     * @param CustomergrouprulesInterfaceFactory $dataCustomergrouprulesFactory
     * @param CustomergrouprulesCollectionFactory $customergrouprulesCollectionFactory
     * @param CustomergrouprulesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ResourceCustomergrouprules $resource,
        CustomergrouprulesFactory $customergrouprulesFactory,
        CustomergrouprulesInterfaceFactory $dataCustomergrouprulesFactory,
        CustomergrouprulesCollectionFactory $customergrouprulesCollectionFactory,
        CustomergrouprulesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->customergrouprulesFactory = $customergrouprulesFactory;
        $this->customergrouprulesCollectionFactory = $customergrouprulesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCustomergrouprulesFactory = $dataCustomergrouprulesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function save(
        \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
    ) {
        /* if (empty($customergrouprules->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $customergrouprules->setStoreId($storeId);
        } */
        try {
            $this->resource->save($customergrouprules);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the customergrouprules: %1',
                $exception->getMessage()
            ));
        }
        return $customergrouprules;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getById($customergrouprulesId)
    {
        $customergrouprules = $this->customergrouprulesFactory->create();
        $this->resource->load($customergrouprules, $customergrouprulesId);
        if (!$customergrouprules->getId()) {
            throw new NoSuchEntityException(__('customergrouprules with id "%1" does not exist.', $customergrouprulesId)); // phpcs:disable
        }
        return $customergrouprules;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyNullIterator
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->customergrouprulesCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyNullIterator
     * @psalm-suppress UndefinedFunction
     */
    public function delete(
        \I95Dev\CustomerGroupRules\Api\Data\CustomergrouprulesInterface $customergrouprules
    ) {
        try {
            $this->resource->delete($customergrouprules);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the customergrouprules: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($customergrouprulesId)
    {
        return $this->delete($this->getById($customergrouprulesId));
    }
}
