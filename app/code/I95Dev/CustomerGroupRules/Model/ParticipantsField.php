<?php
namespace I95Dev\CustomerGroupRules\Model;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ParticipantsField extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField');
    }
}
