<?php


namespace I95Dev\CustomerGroupRules\Model\ResourceModel;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Customergrouprules extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('i95dev_customergrouprules_customergrouprules', 'id');
    }
}
