<?php


namespace I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \I95Dev\CustomerGroupRules\Model\Customergrouprules::class,
            \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules::class
        );
    }
}
