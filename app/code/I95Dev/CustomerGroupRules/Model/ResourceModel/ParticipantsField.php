<?php
namespace I95Dev\CustomerGroupRules\Model\ResourceModel;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ParticipantsField extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('i95_domain_field', 'id');
    }
}
