<?php

namespace I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('I95Dev\CustomerGroupRules\Model\ParticipantsField', 'I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField'); // phpcs:disable
    }
}
