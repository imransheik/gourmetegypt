<?php

namespace I95Dev\CustomerGroupRules\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Segmentgrouplog extends AbstractDb
{
    public function _construct()
    {
        $this->_init('efi_autoauth_segment_group_log', 'entity_id');
    }
}
