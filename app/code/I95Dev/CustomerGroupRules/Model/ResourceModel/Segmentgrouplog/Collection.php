<?php

namespace I95Dev\CustomerGroupRules\Model\ResourceModel\Segmentgrouplog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init("I95Dev\CustomerGroupRules\Model\Segmentgrouplog", "I95Dev\CustomerGroupRules\Model\ResourceModel\Segmentgrouplog"); // phpcs:disable
    }
}
