<?php

namespace I95Dev\CustomerGroupRules\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Segmentgrouplog extends AbstractModel
{
    public function _construct()
    {
        $this->_init('I95Dev\CustomerGroupRules\Model\ResourceModel\Segmentgrouplog');
    }
}
