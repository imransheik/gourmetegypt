<?php

namespace I95Dev\CustomerGroupRules\Observer\Frontend\Customer;

use Magento\Framework\Exception\CouldNotSaveException;

/**
 * @psalm-suppress MissingPropertyType
 */
class RegisterSuccess implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     *
     */
    const CUSTOMER_GROUP_ID = 1;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerSegment;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerRepoInterface;
    /**
     * @psalm-suppress MissingPropertyType
     */
    public $customerGroupRule;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_collectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerRepositoryInterface;

    /**
     * RegisterSuccess constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\CustomerSegment\Model\Customer $customerSegment
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepoInterface
     * @param \I95Dev\CustomerGroupRules\Model\CustomergrouprulesFactory $customerGroupRule
     * @param \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $collectionFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\CustomerSegment\Model\Customer $customerSegment,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepoInterface,
        \I95Dev\CustomerGroupRules\Model\CustomergrouprulesFactory $customerGroupRule,
        \I95Dev\CustomerGroupRules\Model\ResourceModel\Customergrouprules\CollectionFactory $collectionFactory
    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_customerSegment = $customerSegment;
        $this->_customerRepoInterface = $customerRepoInterface;
        $this->customerGroupRule = $customerGroupRule;
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     * @psalm-suppress PossiblyFalseArgument
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $customer = $observer->getEvent()->getCustomer();

        $email = $customer->getEmail();
        $emailNew = strstr($email, '@');

        $collectionNew = $this->_collectionFactory->create();

        $data = $collectionNew->addFieldToFilter('domain', $emailNew);

        if (!empty($data->getData())) {
            $domainGroup = $data->getData()[0]['group'];
            $customer->setGroupId($domainGroup);
            $this->_customerRepositoryInterface->save($customer);
        } else {
            $customer->setGroupId(self::CUSTOMER_GROUP_ID);
            $customer->setConfirmation(null);
			$customer->save();
            //$this->_customerRepositoryInterface->save($customer);
        }
    }
}
