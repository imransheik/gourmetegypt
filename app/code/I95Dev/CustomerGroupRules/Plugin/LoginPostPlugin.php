<?php

namespace I95Dev\CustomerGroupRules\Plugin;

use Magento\Framework\App\Helper\Context as Context;

class LoginPostPlugin
{

    /**
     * Change redirect after login to home instead of dashboard.
     *
     * @param \Magento\Customer\Controller\Account\LoginPost $subject
     * @param \Magento\Framework\Controller\Result\Redirect $result
     */
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $context;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;

    /**
     * LoginPostPlugin constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Request\Http $request
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->context = $context;
        $this->_request = $request;
    }

    /**
     * @param \Magento\Customer\Controller\Account\LoginPost $subject
     * @param $result
     * @return mixed
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MissingParamType
     */
    public function afterExecute(
        \Magento\Customer\Controller\Account\LoginPost $subject,
        $result
    ) {
        if (array_key_exists('peer_login', $_COOKIE)) { // phpcs:disable

            if ($_COOKIE['peer_login'] == 'peer') { // phpcs:disable
                $result->setPath('customer/account/edit');
            }
        }
        return $result;
    }
}
