<?php

namespace I95Dev\CustomerGroupRules\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        //Your install script

        $table_i95dev_customergrouprules_customergrouprules = $setup->getConnection()->newTable($setup->getTable('i95dev_customergrouprules_customergrouprules')); //phpcs:ignore

        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
            'Entity ID'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1000,
            [],
            'Name'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'group',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1000,
            [],
            'Group'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'domain',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1000,
            [],
            'Domain'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'active',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1000,
            [],
            'Active'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        );
        $table_i95dev_customergrouprules_customergrouprules->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        );

        $setup->getConnection()->createTable($table_i95dev_customergrouprules_customergrouprules);
    }
}
