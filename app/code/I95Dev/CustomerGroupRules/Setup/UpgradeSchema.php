<?php

namespace I95Dev\CustomerGroupRules\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $domainProviderField = $installer->getConnection()->newTable(
                $installer->getTable('i95_domain_field')
            )
                    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'id') //phpcs:ignore
                    ->addColumn('field_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Provider type')
                    ->addColumn('field_value', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Provider Value');
            $installer->getConnection()->createTable($domainProviderField);
        }
    }
}
