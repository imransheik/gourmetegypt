<?php

namespace I95Dev\CustomerGroupRules\Ui\Component\Listing\Column;

class Domain implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_collectionFactory;

    /**
     * @var array|null
     */
    protected $_options;

    /**
     * Domain constructor.
     * @param \I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField\CollectionFactory $collectionFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\CustomerGroupRules\Model\ResourceModel\ParticipantsField\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function toOptionArray()
    {
        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('field_type', ['eq' => 'domain']);
        $options = [];
        foreach ($collection as $domain) {
            $options[] = [
                'label' => __('%1 ', $domain->getFieldValue()),
                'value' => $domain->getFieldValue()
            ];
        }

        return $options;
    }
}
