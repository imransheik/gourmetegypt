<?php

/*
 * I95Dev_DeliverySlot
 */

namespace I95Dev\DeliverySlot\Controller\Adminhtml\DeliverySlot;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action {

    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    public function __construct(
    Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return true;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev_DeliverySlot::deliveryslot')
                ->addBreadcrumb(__('I95Dev Delivery Slot'), __('I95Dev Delivery Slot'))
                ->addBreadcrumb(__('Manage Delivery Slot'), __('Manage Delivery Slot'));
        return $resultPage;
    }

    public function execute() {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('pk');
        $model = $this->_objectManager->create('I95Dev\DeliverySlot\Model\DeliverySlot');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        // 3. Set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        $this->_coreRegistry->register('deliveryslot', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(__('I95Dev'), __('I95Dev'));
        $resultPage->addBreadcrumb(
                $id ? __('Edit Delivery Slot') : __('Add Delivery Slot'), $id ? __('Edit Delivery Slot') : __('Add Delivery Slot')
        );
        $resultPage->getConfig()->getTitle()->prepend($id ? __('Edit Delivery Slot') : __('Add Delivery Slot'));
                //$resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Item'));

        return $resultPage;
    }

}
