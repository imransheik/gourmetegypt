<?php
/*
 * I95Dev_DeliverySlot
 */
namespace I95Dev\DeliverySlot\Controller\Adminhtml\DeliverySlot;

use I95Dev\DeliverySlot\Controller\Adminhtml\DeliverySlot;

class Index extends DeliverySlot
{
     protected function _isAllowed() {
    return $this->_authorization->isAllowed('I95Dev_DeliverySlot::deliveryslot');
    }
    
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        
        //return $this->resultPageFactory->create();
         $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev\DeliverySlot::deliveryslot_Tab');
        $resultPage->addBreadcrumb(__('Delivery Slot'), __('Delivery Slot'));
        $resultPage->addBreadcrumb(__('Delivery Slot'), __('Delivery Slot'));
        $resultPage->getConfig()->getTitle()->prepend(__('Delivery Slot'));
        return $resultPage;
    }
}
