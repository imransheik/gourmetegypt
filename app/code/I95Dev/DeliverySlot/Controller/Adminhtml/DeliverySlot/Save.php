<?php
/**
 * I95Dev_DeliverySlot
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2016 I95Dev, Inc.
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace I95Dev\DeliverySlot\Controller\Adminhtml\DeliverySlot;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action {
 protected $taxClassifiication;
    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, \I95Dev\DeliverySlot\Model\ResourceModel\DeliverySlot\CollectionFactory $taxClassifiication) {
        $this->collectionFactory = $taxClassifiication;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $request = $this->getRequest();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/deliveryslot'));
        }

        $model = $this->_objectManager->create('I95Dev\DeliverySlot\Model\DeliverySlot');
        $data = $request->getParams();

        $id = (int) $request->getParam('pk');
        if ($id) {
            $model->load($id);
        }

        try {
            
                $model->setData('website_id', $request->getParam('website_id'));
                $model->setData('dest_country_id', $request->getParam('dest_country_id'));
                $model->setData('dest_region_id', $request->getParam('dest_region_id'));
                $model->setData('dest_city', $request->getParam('dest_city'));
                $model->setData('dest_zip', $request->getParam('dest_zip'));
                $model->setData('dest_zip_to', $request->getParam('dest_zip_to'));
                $model->setData('condition_name', $request->getParam('condition_name'));
                $model->setData('condition_from_value', $request->getParam('condition_from_value'));
                $model->setData('condition_to_value', $request->getParam('condition_to_value'));
                $model->setData('price', $request->getParam('price'));
                $model->setData('cost', $request->getParam('cost'));
                $model->setData('shipping_method', $request->getParam('shipping_method'));
                $model->save();
            
                $this->messageManager->addSuccess(__('The Delivery Slot is saved successfully.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['pk' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the Delivery Slot'));
            $this->_getSession()->setFormData(false);
            return $resultRedirect->setPath('*/*/edit', ['pk' => $this->getRequest()->getParam('pk')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
