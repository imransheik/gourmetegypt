<?php
/**
 * I95Dev_DeliverySlot
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2016 I95Dev, Inc.
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace I95Dev\DeliverySlot\Ui\Component\Listing\Column;

class LocationId implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$locationManagers= $objectManager->get('I95Dev\Locationgroup\Model\Data')->getCollection();

		$location[] = ['value' => "", 'label' => __('Select Location')];
		foreach($locationManagers as $locationManager){
			$location[] = ['value' => $locationManager->getDataId(), 'label' => __($locationManager->getDataId().' - '. $locationManager->getGroupName())];
		}
        return $location;
    }
}