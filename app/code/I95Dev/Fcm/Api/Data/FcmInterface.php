<?php


namespace I95Dev\Fcm\Api\Data;

interface FcmInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CREATED_AT = 'created_at';
    const FCM_ID = 'fcm_id';
    const DEVICEID = 'deviceid';
    const ENVIRONMENT = 'environment';
    const PLATFORMVERSION = 'platformversion';
    const NAME = 'name';
    const CUSTOMERID = 'customerid';
    const MODEL = 'model';
    const FCMTOKEN = 'fcmtoken';
    const APPVERSION = 'appversion';
    const ID = 'id';
    const PLATFORM = 'platform';
    const UPDATED_AT = 'updated_at';

    /**
     * Get fcm_id
     * @return string|null
     */
    public function getFcmId();

    /**
     * Set fcm_id
     * @param string $fcmId
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setFcmId($fcmId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setId($id);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\Fcm\Api\Data\FcmExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \I95Dev\Fcm\Api\Data\FcmExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \I95Dev\Fcm\Api\Data\FcmExtensionInterface $extensionAttributes
    );

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid();

    /**
     * Set customerid
     * @param string $customerid
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setCustomerid($customerid);

    /**
     * Get fcmtoken
     * @return string|null
     */
    public function getFcmtoken();

    /**
     * Set fcmtoken
     * @param string $fcmtoken
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setFcmtoken($fcmtoken);

    /**
     * Get deviceid
     * @return string|null
     */
    public function getDeviceid();

    /**
     * Set deviceid
     * @param string $deviceid
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setDeviceid($deviceid);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setName($name);

    /**
     * Get model
     * @return string|null
     */
    public function getModel();

    /**
     * Set model
     * @param string $model
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setModel($model);

    /**
     * Get platformversion
     * @return string|null
     */
    public function getPlatformversion();

    /**
     * Set platformversion
     * @param string $platformversion
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setPlatformversion($platformversion);

    /**
     * Get platform
     * @return string|null
     */
    public function getPlatform();

    /**
     * Set platform
     * @param string $platform
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setPlatform($platform);

    /**
     * Get appversion
     * @return string|null
     */
    public function getAppversion();

    /**
     * Set appversion
     * @param string $appversion
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setAppversion($appversion);

    /**
     * Get environment
     * @return string|null
     */
    public function getEnvironment();

    /**
     * Set environment
     * @param string $environment
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setEnvironment($environment);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setUpdatedAt($updatedAt);
}
