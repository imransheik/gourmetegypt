<?php


namespace I95Dev\Fcm\Api\Data;

interface FcmSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get fcm list.
     * @return \I95Dev\Fcm\Api\Data\FcmInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \I95Dev\Fcm\Api\Data\FcmInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
