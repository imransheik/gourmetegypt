<?php


namespace I95Dev\Fcm\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface FcmRepositoryInterface
{

    /**
     * Save fcm
     * @param \I95Dev\Fcm\Api\Data\FcmInterface $fcm
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\I95Dev\Fcm\Api\Data\FcmInterface $fcm);

    /**
     * Retrieve fcm
     * @param string $fcmId
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($fcmId);

    /**
     * Retrieve fcm matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \I95Dev\Fcm\Api\Data\FcmSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete fcm
     * @param \I95Dev\Fcm\Api\Data\FcmInterface $fcm
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\I95Dev\Fcm\Api\Data\FcmInterface $fcm);

    /**
     * Delete fcm by ID
     * @param string $fcmId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($fcmId);
}
