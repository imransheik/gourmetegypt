<?php

namespace I95Dev\Fcm\Helper;

/**
 * Returns base helper
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \I95Dev\Fcm\lib\FcmAPI
     * @psalm-suppress UndefinedClass
     */
    protected $_api;

    /**
     *
     * @psalm-suppress UndefinedClass
     */
    protected $_fcmCollection;
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_guestCollection;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     * @psalm-suppress UndefinedClass
     */
    protected $_customerRepo;

    /**
     * Data constructor.
     * @param \I95Dev\Fcm\lib\FcmAPI $api
     * @param \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection
     * @param \Gourmet\Alert\Model\ResourceModel\GuestAlert\CollectionFactory $guestCollection
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\Fcm\lib\FcmAPI $api,
        \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection,
        \Gourmet\Alert\Model\ResourceModel\GuestAlert\CollectionFactory $guestCollection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo
    ) {
        $this->_api = $api;
        $this->_fcmCollection = $fcmCollection;
        $this->_guestCollection = $guestCollection;
        $this->_customerRepo = $customerRepo;
    }

    /**
     * Order status change notification
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedClass
     */

    public function sendOrderStatusNotificationTo($order)
    {
        // Prepare message
        $orderStatusMessage = [
            'title' => 'Your Order status',
            'categoryIdentifier' => 'order_status',
            'summaryArgument' => $order->getId(),
            'body' => 'Your order is now in ' . $order->getStatusLabel() . ' status',
        ];
        $customerId = $order->getCustomerId();
     
        $fcmCollection = $this->_fcmCollection->create();

        $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('customerid', $customerId)->addFieldToFilter('platform', 'ANDROID'); //phpcs:ignore
        $fcmAndroid = $fcmCollectionAndroid->getData();
        $fcmAndroidArr = [];
        foreach ($fcmAndroid as $fcmData) {
            $fcmAndroidArr[] = $fcmData['fcmtoken'];
        }

        $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
        $fcmCollection = $this->_fcmCollection->create();
        $fcmCollectionIos = $fcmCollection->addFieldToFilter('customerid', $customerId)->addFieldToFilter('platform', 'ios'); //phpcs:ignore
        $fcmIos = $fcmCollectionIos->getData();
        $fcmIosArr = [];
        foreach ($fcmIos as $fcmData) {
            $fcmIosArr[] = $fcmData['fcmtoken'];
        }
        $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
    }

    /**
     * @param $product
     * @param $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MissingParamType
     */
    public function sendProductNotificationTo($product, $email)
    {
        // Prepare message
        $orderStatusMessage = [
            'title' => $product->getName() . ' is available now',
            'categoryIdentifier' => 'notify_me',
            'summaryArgument' => $product->getId(),
            'body' => $product->getName() . ' is available, you can place an order from cart',
        ];

        $customer = $this->_customerRepo->get($email, $websiteId = 1);

        $fcmCollection = $this->_fcmCollection->create();

        $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('customerid', $customer->getId())->addFieldToFilter('platform', 'ANDROID'); //phpcs:ignore
        $fcmAndroid = $fcmCollectionAndroid->getData();
        $fcmAndroidArr = [];
        foreach ($fcmAndroid as $fcmData) {
            $fcmAndroidArr[] = $fcmData['fcmtoken'];
        }

        $responce = $this->_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
        $fcmCollection = $this->_fcmCollection->create();
        $fcmCollectionIos = $fcmCollection->addFieldToFilter('customerid', $customer->getId())->addFieldToFilter('platform', 'ios'); //phpcs:ignore
        $fcmIos = $fcmCollectionIos->getData();
        $fcmIosArr = [];
        foreach ($fcmIos as $fcmData) {
            $fcmIosArr[] = $fcmData['fcmtoken'];
        }
        $responceios = $this->_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
        ;
    }

    /**
     * @param $message
     * @param $id
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function sendAbandonedCart($message, $id)
    {
       
        $fcmCollection = $this->_fcmCollection->create();

        $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('customerid', $id)->addFieldToFilter('platform', 'ANDROID'); //phpcs:ignore
        $fcmAndroid = $fcmCollectionAndroid->getData();
        $fcmAndroidArr = [];
        foreach ($fcmAndroid as $fcmData) {
            $fcmAndroidArr[] = $fcmData['fcmtoken'];
        }

        $responce = $this->_api->getApiCall($fcmAndroidArr, $message);
        $fcmCollection = $this->_fcmCollection->create();
        $fcmCollectionIos = $fcmCollection->addFieldToFilter('customerid', $id)->addFieldToFilter('platform', 'ios');
        $fcmIos = $fcmCollectionIos->getData();
        $fcmIosArr = [];
        foreach ($fcmIos as $fcmData) {
            $fcmIosArr[] = $fcmData['fcmtoken'];
        }
        $responceios = $this->_api->getApiCallforIos($fcmIosArr, $message);
    }
}
