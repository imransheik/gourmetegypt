<?php


namespace I95Dev\Fcm\Model\Data;

use I95Dev\Fcm\Api\Data\FcmInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Fcm extends \Magento\Framework\Api\AbstractExtensibleObject implements FcmInterface
{

    /**
     * Get fcm_id
     * @return string|null
     */
    public function getFcmId()
    {
        return $this->_get(self::FCM_ID);
    }

    /**
     * Set fcm_id
     * @param string $fcmId
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setFcmId($fcmId)
    {
        return $this->setData(self::FCM_ID, $fcmId);
    }

    /**
     * Get id
     * @return string|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress InvalidReturnStatement
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \I95Dev\Fcm\Api\Data\FcmExtensionInterface $extensionAttributes
     * @return $this
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidArgument
     */
    public function setExtensionAttributes(
        \I95Dev\Fcm\Api\Data\FcmExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid()
    {
        return $this->_get(self::CUSTOMERID);
    }

    /**
     * Set customerid
     * @param string $customerid
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setCustomerid($customerid)
    {
        return $this->setData(self::CUSTOMERID, $customerid);
    }

    /**
     * Get fcmtoken
     * @return string|null
     */
    public function getFcmtoken()
    {
        return $this->_get(self::FCMTOKEN);
    }

    /**
     * Set fcmtoken
     * @param string $fcmtoken
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setFcmtoken($fcmtoken)
    {
        return $this->setData(self::FCMTOKEN, $fcmtoken);
    }

    /**
     * Get deviceid
     * @return string|null
     */
    public function getDeviceid()
    {
        return $this->_get(self::DEVICEID);
    }

    /**
     * Set deviceid
     * @param string $deviceid
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setDeviceid($deviceid)
    {
        return $this->setData(self::DEVICEID, $deviceid);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get model
     * @return string|null
     */
    public function getModel()
    {
        return $this->_get(self::MODEL);
    }

    /**
     * Set model
     * @param string $model
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setModel($model)
    {
        return $this->setData(self::MODEL, $model);
    }

    /**
     * Get platformversion
     * @return string|null
     */
    public function getPlatformversion()
    {
        return $this->_get(self::PLATFORMVERSION);
    }

    /**
     * Set platformversion
     * @param string $platformversion
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setPlatformversion($platformversion)
    {
        return $this->setData(self::PLATFORMVERSION, $platformversion);
    }

    /**
     * Get platform
     * @return string|null
     */
    public function getPlatform()
    {
        return $this->_get(self::PLATFORM);
    }

    /**
     * Set platform
     * @param string $platform
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setPlatform($platform)
    {
        return $this->setData(self::PLATFORM, $platform);
    }

    /**
     * Get appversion
     * @return string|null
     */
    public function getAppversion()
    {
        return $this->_get(self::APPVERSION);
    }

    /**
     * Set appversion
     * @param string $appversion
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setAppversion($appversion)
    {
        return $this->setData(self::APPVERSION, $appversion);
    }

    /**
     * Get environment
     * @return string|null
     */
    public function getEnvironment()
    {
        return $this->_get(self::ENVIRONMENT);
    }

    /**
     * Set environment
     * @param string $environment
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setEnvironment($environment)
    {
        return $this->setData(self::ENVIRONMENT, $environment);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \I95Dev\Fcm\Api\Data\FcmInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
