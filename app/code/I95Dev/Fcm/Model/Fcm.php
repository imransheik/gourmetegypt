<?php

namespace I95Dev\Fcm\Model;

use I95Dev\Fcm\Api\Data\FcmInterface;
use I95Dev\Fcm\Api\Data\FcmInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class Fcm extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $fcmDataFactory;
    /**
     * @var string
     */
    protected $_eventPrefix = 'i95dev_fcm_fcm';
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param FcmInterfaceFactory $fcmDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \I95Dev\Fcm\Model\ResourceModel\Fcm $resource
     * @param \I95Dev\Fcm\Model\ResourceModel\Fcm\Collection $resourceCollection
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        FcmInterfaceFactory $fcmDataFactory,
        DataObjectHelper $dataObjectHelper,
        \I95Dev\Fcm\Model\ResourceModel\Fcm $resource,
        \I95Dev\Fcm\Model\ResourceModel\Fcm\Collection $resourceCollection,
        array $data = []
    ) {
        $this->fcmDataFactory = $fcmDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve fcm model with fcm data
     * @return FcmInterface
     * @psalm-suppress UndefinedClass
     */
    public function getDataModel()
    {
        $fcmData = $this->getData();

        $fcmDataObject = $this->fcmDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $fcmDataObject,
            $fcmData,
            FcmInterface::class
        );

        return $fcmDataObject;
    }
}
