<?php

namespace I95Dev\Fcm\Model;

use I95Dev\Fcm\Api\FcmRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use I95Dev\Fcm\Model\ResourceModel\Fcm as ResourceFcm;
use I95Dev\Fcm\Api\Data\FcmInterfaceFactory;
use I95Dev\Fcm\Api\Data\FcmSearchResultsInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory as FcmCollectionFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\DataObjectHelper;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class FcmRepository implements FcmRepositoryInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $fcmFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensibleDataObjectConverter;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $collectionProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $fcmCollectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $searchResultsFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataFcmFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @param ResourceFcm $resource
     * @param FcmFactory $fcmFactory
     * @param FcmInterfaceFactory $dataFcmFactory
     * @param FcmCollectionFactory $fcmCollectionFactory
     * @param FcmSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ResourceFcm $resource,
        FcmFactory $fcmFactory,
        FcmInterfaceFactory $dataFcmFactory,
        FcmCollectionFactory $fcmCollectionFactory,
        FcmSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->fcmFactory = $fcmFactory;
        $this->fcmCollectionFactory = $fcmCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataFcmFactory = $dataFcmFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function save(\I95Dev\Fcm\Api\Data\FcmInterface $fcm)
    {
        /* if (empty($fcm->getStoreId())) {
          $storeId = $this->storeManager->getStore()->getId();
          $fcm->setStoreId($storeId);
          } */

        $fcmData = $this->extensibleDataObjectConverter->toNestedArray(
            $fcm,
            [],
            \I95Dev\Fcm\Api\Data\FcmInterface::class
        );
        $deviceid = $fcmData['deviceid'];
        $fcmModel = $this->fcmFactory->create()->load($deviceid, 'deviceid');
        // var_dump($fcmModel->getId()); exit;
        //if($fcmModel->getId()){
        $fcmModel->addData($fcmData);
        // }
        try {
            $this->resource->save($fcmModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the fcm: %1',
                $exception->getMessage()
            ));
        }
        return $fcmModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getById($fcmId)
    {
        $fcm = $this->fcmFactory->create();
        $this->resource->load($fcm, $fcmId);
        if (!$fcm->getId()) {
            throw new NoSuchEntityException(__('fcm with id "%1" does not exist.', $fcmId));
        }
        return $fcm->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->fcmCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \I95Dev\Fcm\Api\Data\FcmInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function delete(\I95Dev\Fcm\Api\Data\FcmInterface $fcm)
    {
        try {
            $fcmModel = $this->fcmFactory->create();
            $this->resource->load($fcmModel, $fcm->getFcmId());
            $this->resource->delete($fcmModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the fcm: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($fcmId)
    {
        return $this->delete($this->getById($fcmId));
    }
}
