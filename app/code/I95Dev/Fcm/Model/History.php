<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Acart
 */

namespace I95Dev\Fcm\Model;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 * @psalm-suppress UndefinedClass
 */
class History extends \Amasty\Acart\Model\History
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * History constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory
     * @param \Magento\Framework\Mail\MessageFactory $messageFactory
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Amasty\Acart\Model\RuleQuoteFactory $ruleQuoteFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\SalesRule\Model\RuleFactory $salesRuleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Amasty\Acart\Model\Config $config
     * @param \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $newsletterSubscriberCollection
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     * @psalm-suppress  UndefinedClass
     * @psalm-suppress MisplacedRequiredParam
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory,
        \Magento\Framework\Mail\MessageFactory $messageFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Amasty\Acart\Model\RuleQuoteFactory $ruleQuoteFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\SalesRule\Model\RuleFactory $salesRuleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Amasty\Acart\Model\Config $config,
        \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $newsletterSubscriberCollection,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        $this->_objectManager = $objectmanager;
        parent::__construct(
            $context,
            $registry,
            $date,
            $dateTime,
            $mailTransportFactory,
            $messageFactory,
            $quoteFactory,
            $ruleQuoteFactory,
            $stockRegistry,
            $salesRuleFactory,
            $storeManager,
            $config,
            $newsletterSubscriberCollection,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @param bool $testMode
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress UndefinedClass
     */
    //phpcs:disable
    protected function _sendEmail($testMode = false)
    {
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $configData = $this->_objectManager->create(\Amasty\Acart\Model\Config::class);
        $senderName = $configData->getSenderName($this->getStoreId());
        $senderEmail = $configData->getSenderEmail($this->getStoreId());
        $bcc =$configData->getBcc($this->getStoreId());
        $safeMode = $configData->isSafeMode($this->getStoreId());
        $recipientEmail = $configData->getTestingRecipientEmail($this->getStoreId());
        $replyToEmail =$configData->getReplyToEmail($this->getStoreId());
        $replyToName = $configData->getReplyToName($this->getStoreId());

        $name = [
            $this->getCustomerFirstname(),
            $this->getCustomerLastname(),
        ];

        $to = $this->getCustomerEmail();

        if (($testMode || $safeMode) && $recipientEmail) {
            $to = $recipientEmail;
        }
        $messageFactory = $this->_objectManager->create(\Magento\Framework\Mail\MessageFactory::class);
        $message = $messageFactory->create();
        $customerId = $this->_objectManager->create(\Magento\Customer\Model\Customer::class)->setWebsiteId('1')->loadByEmail($to)->getId(); //phpcs:ignore

        if ($customerId) {

            $fcmMessage = [
                'title' => 'Gourmet Egypt: Items in your shopping bag',
                'body' => 'Dear ' . $this->getCustomerFirstname() . ' ' . $this->getCustomerLastname() . ',You have items in your shopping bag.Save money with coupon code ' . $this->getSalesRuleCoupon() . ', coupon_expiration_date: ' . $this->getSalesRuleCouponExpirationDate(), //phpcs:ignore
                'couponcode' => $this->getSalesRuleCoupon(),
                'categoryIdentifier' => 'abandon_cart',
            ];

            $this->_objectManager->create(\I95Dev\Fcm\Helper\Data::class)->sendAbandonedCart($fcmMessage, $customerId);
        }
        $message
            ->addTo($to, implode(' ', $name))
            ->setFrom($senderEmail, $senderName)
            ->setMessageType(\Magento\Framework\Mail\MessageInterface::TYPE_HTML)
            ->setBody($this->getEmailBody())
            ->setSubject(html_entity_decode($this->getEmailSubject(), ENT_QUOTES)); //phpcs:ignore

        if (!empty($bcc) && !$testMode && !$safeMode) {
            $message->addBcc(explode(',', $bcc));
        }

        if ($replyToEmail) {
            $message->setReplyTo($replyToEmail, $replyToName ?: '');
        }
        $mailTransportFactory = $this->_objectManager->create(\Magento\Framework\Mail\TransportInterfaceFactory::class);

        $mailTransport =$mailTransportFactory->create(
            [
                'message' => clone $message
            ]
        );
        $mailTransport->sendMessage();
    }
}
