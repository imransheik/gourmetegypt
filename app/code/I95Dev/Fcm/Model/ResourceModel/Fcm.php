<?php


namespace I95Dev\Fcm\Model\ResourceModel;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Fcm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('i95dev_fcm_fcm', 'id');
    }
}
