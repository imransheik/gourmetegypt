<?php


namespace I95Dev\Fcm\Model\ResourceModel\Fcm;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \I95Dev\Fcm\Model\Fcm::class,
            \I95Dev\Fcm\Model\ResourceModel\Fcm::class
        );
    }
}
