<?php

namespace I95Dev\Fcm\Observer\Order;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class OrderStateChange implements ObserverInterface
{
    /**
     * Productsaveafter constructor.
     *
     */

    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_helper;
    /**
     * MissingPropertyType
     */
    protected $_checkoutSession;

    public function __construct(\I95Dev\Fcm\Helper\Data $helepr, \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helepr;
    }

    /**
     * reindex after creation of new customer
     * @param Observer $observer
     * @psalm-suppress InvalidReturnType
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
////        var_dump($order->getStatus());exit;
        $orderSate = $this->_checkoutSession->getFcmOrderState();
      
        if (($orderSate != $order->getStatus()) && ($order->getStatus()!="pending_paymob")) {
            $this->_helper->sendOrderStatusNotificationTo($order);
            
        }
        $this->_checkoutSession->setFcmOrderState($order->getStatus());
//        if ($order) {

        return true;
//        }
//
        // Simple Text Log
//        exit;
    }
}
