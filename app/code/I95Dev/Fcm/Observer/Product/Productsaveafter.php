<?php
namespace I95Dev\Fcm\Observer\Product;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class Productsaveafter implements ObserverInterface
{

   /**
    * Productsaveafter constructor.
    *
    */

    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_helper;

    public function __construct(\I95Dev\Fcm\Helper\Data $helepr)
    {
        $this->_helper = $helepr;
    }

    /**
     * reindex after creation of new customer
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
          
        $_product = $observer->getProduct();  //Get product object
//        $this->_helper->sendProductNotificationTo($_product);
    }
}
