<?php

namespace I95Dev\Fcm\Plugin;

class OrderStatePlugin
{
  /**
   * Productsaveafter constructor.
   *
   */

    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_helper;

    public function __construct(\I95Dev\Fcm\Helper\Data $helepr)
    {
      
        $this->_helper = $helepr;
    }
    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $result
     * @return mixed
     * @throws \Exception
     */
    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        $result
    ) {

          $this->_helper->sendOrderStatusNotificationTo($result);
        return $result;
    }
}
