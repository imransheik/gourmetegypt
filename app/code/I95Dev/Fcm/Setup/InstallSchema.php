<?php


namespace I95Dev\Fcm\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        //Your install script

        $table_i95dev_fcm_fcm = $setup->getConnection()->newTable($setup->getTable('i95dev_fcm_fcm'));

        $table_i95dev_fcm_fcm->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'customerid',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'customerid'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'fcmtoken',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'fcmtoken'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'deviceid',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'deviceid'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'name'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'model',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'model'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'platformversion',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'platformversion'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'platform',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'platform'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'appversion',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'appversion'
        );

        $table_i95dev_fcm_fcm->addColumn(
            'environment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'environment'
        );

            $table_i95dev_fcm_fcm->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            );
           $table_i95dev_fcm_fcm->addColumn(
               'updated_at',
               \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
               null,
               ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
               'Update Time'
           );

        $setup->getConnection()->createTable($table_i95dev_fcm_fcm);
    }
}
