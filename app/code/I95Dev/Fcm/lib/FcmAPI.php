<?php

namespace I95Dev\Fcm\lib;

class FcmAPI
{

    const API_URL = 'https://fcm.googleapis.com/fcm/send';
    const API_ACCESS_KEY = 'AAAAgnjOwGI:APA91bFI75IR47GEJrtJ7vRiesmDB25-kR6U3urjy4JRjEMCn9lCb8KzRkZMYNIKfpyJFns-JavrX6z_s3UVT5dRHS5QOYEONgFURsbXJ4xRzVi0QaOJ4HU0L5kMH_LaxpBD1L7zT5ixkqSSB2xhQclL_hTdLpKz2A';// phpcs:disable

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;

    public function __construct(\Magento\Framework\HTTP\Client\Curl $curl)
    {
        $this->_curl = $curl;
    }

    /**
     * @param $fcmIds
     * @param array $message
     * @return bool
     * @psalm-suppress MissingParamType
     */
    public function getApiCall($fcmIds, array $message)
    {
        $url = self::API_URL;
//
        // Passing All FCM ids
        $registrationIds = $fcmIds;
        // prepare the fields value
        $fields = [
            'registration_ids' => $registrationIds,
            'data' => $message
        ];

        $postDatas = json_encode($fields);
        $headers = [
            'Authorization: key=' . self::API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDatas);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
       
        if ($err) {
//            return "cURL Error #:" . $err;
            return true;
        } else {
            return true;
        }
    }

    /**
     * @param $fcmIds
     * @param array $message
     * @return bool
     * @psalm-suppress MissingParamType
     */
    public function getApiCallforIos($fcmIds, array $message)
    {
        $responce= [];
        foreach ($fcmIds as $fcmId) {
            $url = self::API_URL;
            $token = $fcmId;
            $body = '';
            $serverKey = self::API_ACCESS_KEY;
            
            //var_dump($message); exit;
//            $notification = array('title' => $message['title'], 'text' => $message, 'sound' => 'default', 'badge' => '1'); // phpcs:disable
            $message['sound']='default';
            $message['badge']='1';
         
            $arrayToSend = ['to' => $token, 'notification' => $message, 'priority' => 'high'];
            $json = json_encode($arrayToSend);

            $headers = [];
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key=' . $serverKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Send the request
            $responce[]= curl_exec($ch);
     
//Close request

            curl_close($ch);

        }

        return true;
    }
}
