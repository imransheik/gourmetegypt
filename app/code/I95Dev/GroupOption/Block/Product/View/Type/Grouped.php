<?php

namespace I95Dev\GroupOption\Block\Product\View\Type;

use Magento\Catalog\Api\ProductRepositoryInterface;
use I95Dev\GroupOption\Block\Product\View\Type\Configurable;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Grouped extends \Magento\GroupedProduct\Block\Product\View\Type\Grouped
{
    /**
     * I95Dev grouped product option helper.
     *
     * @var \I95Dev\GroupOption\Helper\Data
     */
    private $helperI95Dev;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    private $localeFormat;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $dataObjectFactory;

    /**
     * Grouped constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \I95Dev\GroupOption\Helper\Data $helperI95Dev
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \I95Dev\GroupOption\Helper\Data $helperI95Dev,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $arrayUtils,
            $data
        );
        $this->jsonEncoder = $jsonEncoder;
        $this->helperI95Dev = $helperI95Dev;
        $this->productRepository = $productRepository;
        $this->localeFormat = $localeFormat;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @param $item
     * @return bool|string
     * @psalm-suppress MissingParamType
     * @psalm-suppress TypeCoercion
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress PossiblyNullOperand
     * @psalm-suppress InvalidMethodCall
     * @psalm-suppress RedundantCondition
     */
    public function renderBlockProduct($item)
    {
        $html = '';
        $product = $this->getProductInfo($item);
        $typeProduct = $product->getTypeId();
        if ($typeProduct == \I95Dev\GroupOption\Helper\Data::PRODUCT_TYPE_CONFIGURABLE) {
            $block = $this->_addConfigurableBlock($product);
        }

        if (isset($block)) {
            $html .= '<div class="I95Dev-option-configurable-info fieldset">'.
                        $block->toHtml().
                     '</div>';
        }

        $customOption = $this->_addCustomOption($product);
        if (isset($customOption)) {
            $html .= '<div class="I95Dev-option-custom-option" data-product-id="'.$product->getId().'">'
                    . '<div class="fieldset">'
                    . $customOption->toHtml()
                    . '</div></div>';
        }

        if (isset($html) && $html != '') {
            return $html;
        }

        return false;
    }

    /**
     * Get JSON encoded configuration array which can be used for JS dynamic
     * price calculation depending on product options
     *
     * @return string
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyNullArrayOffset
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedClass
     */
    public function getJsonConfig()
    {
        /* @var $product \Magento\Catalog\Model\Product */
        $associatedProducts = $this->getAssociatedProducts();
        $config = [];
        foreach ($associatedProducts as $item) {
            $product = $this->getProductInfo($item);
            $productId = $product->getId();

            $tierPrices = [];
            $tierPricesList = $product->getPriceInfo()->getPrice('tier_price')->getTierPriceList();
            foreach ($tierPricesList as $tierPrice) {
                $tierPrices[] = $tierPrice['price']->getValue();
            }
            $config[$productId] = [
                'productId'   => $productId,
                'priceFormat' => $this->localeFormat->getPriceFormat(),
                'prices'      => [
                    'oldPrice'   => [
                        'amount'      => $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue(),
                        'adjustments' => []
                    ],
                    'basePrice'  => [
                        'amount'     => $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount(),
                        'adjustments' => []
                    ],
                    'finalPrice' => [
                        'amount'      => $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),
                        'adjustments' => []
                    ]
                ],
                'idSuffix'    => '_clone',
                'tierPrices'  => $tierPrices
            ];

            $responseObject = $this->dataObjectFactory->create();
            $this->_eventManager->dispatch('catalog_product_view_config', ['response_object' => $responseObject]);
            if (is_array($responseObject->getAdditionalOptions())) {
                foreach ($responseObject->getAdditionalOptions() as $option => $value) {
                    $config[$productId][$option] = $value;
                }
            }
        }
        return $this->jsonEncoder->encode($config);
    }

    /**
     * Add configurable block to layout.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedMethod
     */
    protected function _addConfigurableBlock($product)
    {
        $layout = $this->_layout->createBlock(Configurable::class)
            ->setProduct($product);

        if (isset($layout)) {
            return $layout;
        }

        return false;
    }

    /**
     * Add custom option block to layout.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     * @psalm-suppress TypeCoercion
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress UndefinedMethod
     */
    protected function _addCustomOption($product)
    {
        $layout = $this->_layout->getBlock('I95Dev.option.product.info.options')->setProduct($product);

        if (isset($layout)) {
            return $layout;
        }

        return false;
    }

    /**
     * @param $item
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @psalm-suppress MissingParamType
     */
    public function getProductInfo($item)
    {
        return $this->productRepository->getById($item->getId());
    }

    /**
     * Get grouped info html.
     *
     * @param \Magento\Catalog\Model\Product $item
     * @return string
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyNullOperand
     * @psalm-suppress TypeCoercion
     */
    public function getProductInfooption($item)
    {
        $html = '';
        $product = $this->getProductInfo($item);
        if ($this->helperI95Dev->getConfig('show_link') && $product->getVisibility() != 1) {
            $html .= '<a href = "'.$product->getProductUrl().'" class="I95Dev-option-img">';
        }

        $html .= '<img src="'.$this->getProductImage($product).'"  alt='.$product->getName().' />';
        if ($this->helperI95Dev->getConfig('show_link') && $product->getVisibility() != 1) {
            $html .='</a>';
        }

        return $html;
    }

    /**
     * Get product image.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool|string
     */
    protected function getProductImage($product)
    {
        $imageSize = 135;
        $productImage = $this->_imageHelper->init(
            $product,
            'category_page_list',
            ['height' => $imageSize, 'width'=> $imageSize]
        )->getUrl();

        if (!$productImage) {
            return false;
        }

        return $productImage;
    }

    /**
     * Get I95Dev grouped product option helper.
     *
     * @return \I95Dev\GroupOption\Helper\Data
     */
    public function getI95DevHelper()
    {
        return $this->helperI95Dev;
    }
}
