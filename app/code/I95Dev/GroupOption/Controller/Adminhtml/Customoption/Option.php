<?php
namespace I95Dev\GroupOption\Controller\Adminhtml\Customoption;

use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\Product;

class Option extends \Magento\Backend\App\Action
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_resultPageFactory;
    /**
     * @psalm-suppress UndefinedClass
     */
    private $productRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $imageHelperFactory;

    /**
     * Option constructor.
     * @param Context $context
     * @param Product $Product
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param Product\Option $productOption
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress TypeCoercion
     */
    public function __construct(
        Context $context,
        Product $Product,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product\Option $productOption,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->Product = $Product;
        $this->ProductOption = $productOption;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedMethod
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $product_id = $this->getRequest()->getPost('productid', false);
        $product = $this->Product->load($product_id);
        $customOptions = $this->ProductOption->getProductOptionCollection($product);
        
        $html='';
        $hasnoOption=true;
         $response=[];
        // phpcs:disable
            $html .='<form id="customoption-form-'.$product_id.'" method="get" enctype="multipart/form-data" action="">
    <div class="modal-body">
        <div class="bootbox-body">';
        foreach ($customOptions as $optionKey => $optionVal) {
            if ($optionVal->getData("type") == "drop_down") {
                $hasnoOption=false;
                $html .= '<div class="field required" style="width: 50%;display: inline-block;">
    <label class="label" for="select_">
        <span>' . $optionVal->getData("title") . '</span>
    </label>
    <div class="control"><select name="options_' . $optionVal->getData("option_id") . '" id="select_' . $optionVal->getData("option_id") . '" class=" required product-custom-option admin__control-select" title="" data-selector="options[' . $optionVal->getData("option_id") . ']" aria-required="true">';

                foreach ($optionVal->getValues() as $valuesKey => $valuesVal) {
//                    var_dump($valuesVal->getData()); exit;
 
                    $html .= '<option value="' . $valuesVal->getData('option_type_id') . '" price="' . number_format((float)$valuesVal->getData('price'), 2, ".", '') . '">' . $valuesVal->getData('title') . '</option>';
                }
                $html .= "</select>   </div></div>";
            }
        }
         $html .='</div>
    </div>
   
</form>';
        if ($hasnoOption) {
            $html .="<b>This product don not have any drop down custom option.</b>";
        }
        $response["htmloption"] = $html;
        return $resultJson->setData(json_encode($response));
    }
}
