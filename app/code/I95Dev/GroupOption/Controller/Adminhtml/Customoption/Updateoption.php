<?php

namespace I95Dev\GroupOption\Controller\Adminhtml\Customoption;

use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\Product;

class Updateoption extends \Magento\Backend\App\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_registry;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_resultPageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $productRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $imageHelperFactory;

    /**
     * Updateoption constructor.
     * @param Context $context
     * @param Product $Product
     * @param \Magento\Framework\Session\SessionManagerInterface $registry
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param Product\Option $productOption
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress TypeCoercion
     */
    public function __construct(
        Context $context,
        Product $Product,
        \Magento\Framework\Session\SessionManagerInterface $registry,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product\Option $productOption,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->Product = $Product;
        $this->_registry = $registry;
        $this->ProductOption = $productOption;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedMethod
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $product_id = $this->getRequest()->getPost('productid', false);
        $option_data = $this->getRequest()->getPost('formData', false);
        $gpoption = [];
        $response = [];
        if (!$option_data) {
            return $resultJson->setData(json_encode($response));
        }
        $response["message"] = "You have Updated to Custom option for group product.";
        $this->_registry->start();
        $exitingOption = $this->_registry->getGpCustomOption();

        if (is_array($exitingOption)) {
            foreach ($option_data as $option) {
                $name = str_replace("options_", "", $option['name']);
                $exitingOption[$product_id][$option['value']] = $name;
            }

            $this->_registry->setGpCustomOption($exitingOption);
        } else {
            foreach ($option_data as $option) {
                $name = str_replace("options_", "", $option['name']);
                $gpoption[$product_id][$option['value']] = $name;
            }
            $this->_registry->setGpCustomOption($gpoption);
        }
        return $resultJson->setData(json_encode($response));
    }
}
