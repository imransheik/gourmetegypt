<?php

namespace I95Dev\GroupOption\Controller\Cart;

use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Add extends \Magento\Checkout\Controller\Cart\Add
{

    /**
     * I95Dev grouped product option helper.
     *
     * @var \I95Dev\GroupOption\Helper\Data
     */
    private $helperI95Dev;

    /**
     * Product repository.
     *
     * @var ProductRepositoryInterface
     */
    public $productRepository;

    /**
     * Registry model.
     *
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * Resolver.
     *
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    private $resolver;

    /**
     * Escaper.
     *
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * Checkout cart helper.
     *
     * @var \Magento\Checkout\Helper\Cart
     */
    private $cartHelper;

    /**
     * Logger.
     *
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $helperData;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $stockRegistryInterface;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param \I95Dev\GroupOption\Helper\Data $helperI95Dev
     * @param \Magento\Framework\Locale\ResolverInterface $resolver
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Gourmet\Location\Helper\Data $helperData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $registry,
        \I95Dev\GroupOption\Helper\Data $helperI95Dev,
        \Magento\Framework\Locale\ResolverInterface $resolver,
        \Magento\Framework\Escaper $escaper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Psr\Log\LoggerInterface $logger,
        \Gourmet\Location\Helper\Data $helperData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $productRepository
        );

        $this->productRepository = $productRepository;
        $this->helperI95Dev = $helperI95Dev;
        $this->registry = $registry;
        $this->resolver = $resolver;
        $this->escaper = $escaper;
        $this->cartHelper = $cartHelper;
        $this->logger = $logger;
        $this->helperData = $helperData;
        $this->stockRegistryInterface = $stockRegistryInterface;
    }

    /**
     * @param null $productId
     * @return mixed
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedFunction
     */
    public function deleteQuoteItems($productId = null)
    {
		$result = [];
        $checkoutSession = $this->getCheckoutSession();
        $allItems = $checkoutSession->getQuote()->getAllVisibleItems(); //returns all teh items in session
        foreach ($allItems as $item) {
            $itemId = $item->getItemId(); //item id of particular item
            if ($item->getProductId() == $productId):
                //load particular item which you want to delete by his item id
                $quoteItem = $this->getItemModel()->load($itemId);
                $this->cart->removeItem($itemId)->save();
                $result = ['success' => __('You have added maximum sale item quantity!')];
                return $this->getResponse()->representJson(
                    $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($result)
                );
            endif;
            return $result;
        }
    }

    /**
     * @return mixed
     * @psalm-suppress MissingReturnType
     */
    public function getCheckoutSession()
    {
//        $checkoutSession = $this->_objectManager->get(\Magento\Checkout\Model\Session::class); //checkout session
        $checkoutSession = $this->getCheckoutSession();//checkout session
        return $checkoutSession;
    }

    /**
     * @return mixed
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     */
    public function getItemModel()
    {
        $itemModel = $this->_objectManager->create(Magento\Quote\Model\Quote\Item::class);
        return $itemModel;
    }

    /**
     * @param null $coreRoute
     * @return Add|\Magento\Framework\Controller\Result\Redirect
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     */
    //phpcs:disable
    public function execute($coreRoute = null)
    {

        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        $product = $this->_initProduct();

        if (isset($params['deduct'])) {
            try {

                if (($quoteItem = $this->cart->getQuote()->getItemByProduct($product)) && $quoteItem->getQty() && ($itemId = $quoteItem->getItemId()) && !(in_array('options', $params))) {
                    unset($params['deduct']);
                    $itemQty = ((int) $quoteItem->getQty()) - 1;

                    $itemData = [$itemId => ['qty' => $itemQty]];
                    $this->cart->updateItems($itemData)->save();

                    $this->_eventManager->dispatch(
                        'checkout_cart_update_item_complete',
                        ['item' => $quoteItem, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
                    );

                    if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                        if (!$this->cart->getQuote()->getHasError()) {
                            $message = __(
                                'You have removed %1 from your shopping cart.',
                                $product->getName()
                            );
                            $this->messageManager->addError($message);
                        }
                    }
                }

                if (isset($params['options'])) {
                    $productId = $params['product'];
                    $this->deleteQuoteItems($productId);
                    if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                        if (!$this->cart->getQuote()->getHasError()) {
                            $message = __(
                                'You have removed %1 from your shopping cart.',
                                $product->getName()
                            );
                            $this->messageManager->addError($message);
                        }
                    }

                }
                return $this->goBack(null, $product);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->messageManager->addNotice(
//                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                        $this->escaper->escapeHtml($e->getMessage())
                    );
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $this->messageManager->addError(
                            $this->escaper->escapeHtml($message)
                        );
                    }
                }

                $url = $this->_checkoutSession->getRedirectUrl(true);

                if (!$url) {
                    //$cartUrl = $this->_objectManager->get(\Magento\Checkout\Helper\Cart::class)->getCartUrl();
                    $cartUrl = $this->cartHelper->getCartUrl();
                    $url = $this->_redirect->getRedirectUrl($cartUrl);
                }

            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t deduct this item to your shopping cart right now.'));
                $this->logger->critical($e);
                return $this->goBack();
            }
        } else {
            if (!$this->helperI95Dev->getConfig() || $product->getTypeId() != \I95Dev\GroupOption\Helper\Data::PRODUCT_TYPE_GROUPED) { //phpcs:ignore


                try {
                    //            echo "<pre>";
                    //            var_dump($params); exit;
                    //            $params["options"]=array(30722=>"68453");
                    if (isset($params['qty'])) {

                        $filter = new \Zend_Filter_LocalizedToNormalized(
                            ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                        );
                        $params['qty'] = $filter->filter($params['qty']);
                    }

                    $product = $this->_initProduct();
                    $related = $this->getRequest()->getParam('related_product');

                    $avilableqty=$this->checkAreaStock($product);

                    if (($quoteItem = $this->cart->getQuote()->getItemByProduct($product)) && $quoteItem->getQty() && ($itemId = $quoteItem->getItemId())) {


                        if (isset($params['qty'])) {

                            if ($avilableqty < ($quoteItem->getQty()+$params['qty'])) {

                                $this->messageManager->addErrorMessage(__('You have added maximum sale item quantity!'));
                                $result = ['error' => __('You have added maximum sale item quantity!')];
                                return $this->getResponse()->representJson(
                                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                                );

                                return $this->goBack();
                            }
                        }
                    }

                    /**
                     * Check product availability
                     */
                    if (!$product) {
                        return $this->goBack();
                    }

                    if ($this->isProductInventory($product) != 0) {
                        $result = ['error' => __('You have added maximum sale item quantity!')];
                        return $this->getResponse()->representJson(
                            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                        );
                        return $result;
                    }

                    $this->cart->addProduct($product, $params);
                    if (!empty($related)) {
                        $this->cart->addProductsByIds(explode(',', $related));
                    }

                    $this->cart->save();

                    /**
                     * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
                     */
                    $this->_eventManager->dispatch(
                        'checkout_cart_add_product_complete',
                        ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
                    );

                    if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                        if (!$this->cart->getQuote()->getHasError()) {
                            $message = __(
                                'You added %1 to your shopping cart.',
                                $product->getName()
                            );
                            $this->messageManager->addSuccessMessage($message);
                        }
                        return $this->goBack(null, $product);
                    }
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    //               echo "aaa";
                    if ($this->_checkoutSession->getUseNotice(true)) {
                        $this->messageManager->addNoticeMessage(
                            $this->escaper->escapeHtml($e->getMessage())
                        );
                    } else {
                        $messages = array_unique(explode("\n", $e->getMessage()));
                        foreach ($messages as $message) {
                            $this->messageManager->addErrorMessage(
                                $this->escaper->escapeHtml($message)
                            );
                        }
                    }

                    $url = $this->_checkoutSession->getRedirectUrl(true);

                    if (!$url) {
                        $cartUrl = $this->cartHelper->getCartUrl();
                        $url = $this->_redirect->getRedirectUrl($cartUrl);
                    }
                    //                var_dump($e->getMessage()); exit;
                    //                return $this->goBack($url);
                    return $this->goBack(null, $product);
                } catch (\Exception $e) {
                    $this->messageManager
                        ->addExceptionMessage($e, __('We can\'t add this item to your shopping cart right now.'));
                    $this->logger->critical($e);
                    return $this->goBack();
                }

            }

            // check in category
            if (!isset($params['I95Dev-option'])) {
                $redirect = false;
                $typeInstance = $product->getTypeInstance();
                $associatedProducts = $typeInstance->getAssociatedProducts($product);
                if ($associatedProducts) {
                    foreach ($associatedProducts as $associatedProduct) {
                        if ($associatedProduct->getRequiredOptions()) {
                            $redirect = true;
                            break;
                        }
                    }
                }
                if ($redirect) {
                    $url = $product->getUrlModel()->getUrl($product);
                    $this->messageManager->addNoticeMessage(__("Please specify product's required option(s)."));
                    return $this->goBack(null, $product);
                } else {
                    return parent::execute($coreRoute);
                }
            } else {
                // add to cart function
                $this->registry->register('I95Dev-option-group', $params['product']);
                try {
                    foreach ($params['super_group'] as $id => $qty) {
                        if (!isset($qty) || $qty <= 0 || $qty == '') {
                            continue;
                        }

                        $productChild = $this->productRepository->getById($id);

                        $avilableqty=$this->checkAreaStock($productChild);
                        if ($avilableqty < $qty) {
                            continue;
                        }
                        $paramsChild = [];
                        $paramsChild['uenc'] = $params['uenc'];
                        $paramsChild['product'] = $id;
                        $paramsChild['selected_configurable_option'] = $params['selected_configurable_option'];
                        if (isset($params['options_' . $id]) && !empty($params['options_' . $id]) > 0) {
                            $paramsChild['options'] = $params['options_' . $id];
                        }
                        if (isset($params['I95Dev-option-option-' . $id]) && !empty($params['I95Dev-option-option-' . $id]) > 0) {
                            foreach ($params['I95Dev-option-option-' . $id] as $name => $value) {
                                $paramsChild[$name] = $value;
                            }
                        }

                        $paramsChild['qty'] = $qty;
                        if (isset($paramsChild['qty'])) {
                            $filter = new \Zend_Filter_LocalizedToNormalized(
                                ['locale' => $this->resolver->getLocale()]
                            );
                            $paramsChild['qty'] = $filter->filter($paramsChild['qty']);
                        }

                        if ($productChild->getTypeId() == 'configurable') {
                            $paramsChild['super_attribute'] = $params['super_attribute'][$id];
                        } else {
                            $paramsChild['super_product_config'] = [
                                'product_type' => $product->getTypeId(),
                                'product_id' => $params['product']
                            ];
                        }

                        /**
                         * Check product availability
                         */
                        if (!$productChild) {
                            return $this->goBack();
                        }
                        $this->registry->unregister('I95Dev-option-group-add');
                        $this->registry->register('I95Dev-option-group-add', $id);
                        $this->cart->addProduct($productChild, $paramsChild);
                    }

                    $related = $this->getRequest()->getParam('related_product');
                    if (!empty($related)) {
                        $this->cart->addProductsByIds(explode(',', $related));
                    }

                    $this->cart->save();

                    $this->_eventManager->dispatch(
                        'checkout_cart_add_product_complete',
                        ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
                    );

                    if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                        if (!$this->cart->getQuote()->getHasError()) {
                            $message = __(
                                'You added %1 to your shopping cart.',
                                $product->getName()
                            );
                            $this->messageManager->addSuccessMessage($message);
                        }
                        return $this->goBack(null, $product);
                    }
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    //               echo "aaa";
                    if ($this->_checkoutSession->getUseNotice(true)) {
                        $this->messageManager->addNoticeMessage(
                            $this->escaper->escapeHtml($e->getMessage())
                        );
                    } else {
                        $messages = array_unique(explode("\n", $e->getMessage()));
                        foreach ($messages as $message) {
                            $this->messageManager->addErrorMessage(
                                $this->escaper->escapeHtml($message)
                            );
                        }
                    }

                    $url = $this->_checkoutSession->getRedirectUrl(true);

                    if (!$url) {
                        $cartUrl = $this->cartHelper->getCartUrl();
                        $url = $this->_redirect->getRedirectUrl($cartUrl);
                    }
                    //                var_dump($e->getMessage()); exit;
                    //                return $this->goBack($url);
                    return $this->goBack(null, $product);
                } catch (\Exception $e) {
                    $this->messageManager
                        ->addExceptionMessage($e, __('We can\'t add this item to your shopping cart right now.'));
                    $this->logger->critical($e);
                    return $this->goBack();
                }
            }
        }
    }
    //phpcs:disable

    /**
     * @param $product
     * @return \Magento\Framework\Controller\Result\Redirect
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress UndefinedVariable
     * @psalm-suppress MissingParamType
     * @psalm-suppress PossiblyFalseArgument
     */
    protected function isProductInventory($product)
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::_goBack($backUrl);
        }


        $StockState = $this->_objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        $inStock = $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
        if (($quoteItem = $this->cart->getQuote()->getItemByProduct($product)) && $quoteItem->getQty() && ($itemId = $quoteItem->getItemId())) {

            if ($quoteItem->getQty() == $inStock) {

            } else {
                echo false;
            }
        }
    }

    /**
     * @param $_product
     * @return int|mixed
     * @psalm-suppress MissingParamType
     */
    public function checkAreaStock($_product)
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $inventory = 0;
        $lochelper = $this->helperData;
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) {
            $cookie = $_COOKIE["Gourmet_Location"];

//            $lochelper = $objectManager->get('\Gourmet\Location\Helper\Data');
            $data = $lochelper->getLocationById($cookie);
            $store = $data->getStoreId();
//            $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($_product->getId());
            $productStockObj = $this->stockRegistryInterface->getStockItem($_product->getId());
            $storeId = 'store_' . $store;
            $inventory = $productStockObj[$storeId];
        } else {
            $cookie = 73;
//            $lochelper = $objectManager->get('\Gourmet\Location\Helper\Data');
            $data = $lochelper->getLocationById($cookie);
            $store = $data->getStoreId();
            $productStockObj = $this->stockRegistryInterface->getStockItem($_product->getId());
            $storeId = 'store_' . $store;
            $inventory = $productStockObj[$storeId];
        }

        $minQty = $productStockObj['min_qty'];

        if (((isset($inventory) && $inventory > $minQty)) && ($productStockObj->getQty()>0)) {

            return $inventory;
        } else {

            return 0;
        }
    }
}