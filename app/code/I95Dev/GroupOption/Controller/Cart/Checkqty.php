<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\GroupOption\Controller\Cart;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Action;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Checkqty extends \Magento\Framework\App\Action\Action
{


    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $helperData;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $stockRegistryInterface;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param \Gourmet\Location\Helper\Data $helperData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        ProductRepositoryInterface $productRepository,
        \Gourmet\Location\Helper\Data $helperData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface
    ) {
        parent::__construct(
            $context
        );
        $this->productRepository = $productRepository;
        $this->helperData = $helperData;
        $this->stockRegistryInterface = $stockRegistryInterface;
    }

    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @psalm-suppress PossiblyNullOperand
     * @psalm-suppress UndefinedMethod
     */
    public function execute()
    {
        // add to cart function
        $params = $this->getRequest()->getParams();
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $currentStore = $storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $productBlock = $this->_objectManager->create(\Magento\Catalog\Block\Product\ListProduct::class);
        $formKey= $this->_objectManager->create(\Magento\Framework\Data\Form\FormKey::class);
        $baseUrl = $currentStore->getBaseUrl() . 'checkout/cart/add/';
        $result=[];
        $html="";

        foreach ($params['super_group'] as $id => $qty) {
            if (!isset($qty) || $qty <= 0 || $qty == '') {
                continue;
            }
            $productChild = $this->productRepository->getById($id);
            $postParams = $productBlock->getAddToCartPostParams($productChild);
            $cartUrl = $baseUrl . $postParams['data'][Action::PARAM_NAME_URL_ENCODED] . '/' . $postParams['data'][Action::PARAM_NAME_URL_ENCODED] . '/product/' . $productChild->getId(); //phpcs:ignore
            $StockState = $this->_objectManager->get(\Magento\CatalogInventory\Api\StockStateInterface::class);
            $avilableqty=$this->checkAreaStock($productChild);

            if ($avilableqty < $qty) {
                $price=$productChild->getPrice();
                $name=$productChild->getName();
                $imageUrl=$this->getImageURL($productChild);
                $poductUrl=$productChild->getProductUrl();
                $priceHelper = $this->_objectManager->create(\Magento\Framework\Pricing\Helper\Data::class); //phpcs:ignore

                $formattedPrice = $priceHelper->currency($price, true, false);
                // phpcs:disable
                $html[$id]=  '<li class="item product product-item" data-role="product-item">
                                <div class="product ci__itemwrapper">

                                    <div class="ci__image">

                                        <a tabindex="-1" class="product-item-photo" href="'.$poductUrl.'" title="'.$name.'">

                                            <span class="product-image-container"  style="width: 78px;">
                                                <span class="product-image-wrapper"  style="padding-bottom: 100%;">
                                                    <img class="product-image-photo"  src="'.$imageUrl.'" alt="'.$name.'" style="width: 78px; height: 78px;">
                                                </span>
                                            </span>

                                        </a>

                                    </div>

                                    <div class="ci__content">
                                        <div class="ci__detail">
                                            <strong class="product-item-name block__row">

                                                <a  href="'.$poductUrl.'">'.$name.'</a>

                                            </strong>	
                                    <div class="availbale-qty">Available Qty:'.round($avilableqty, 0).'</div>

                                        </div>
                                        <div class="ci__action">
                                            <div class="product-item-pricing">

                                                <div class="price-container">
                                                    <span class="price-wrapper" >

                                                        <span class="price-excluding-tax" data-label="Excl. Tax">
                                                            <span class="minicart-price">
                                                                <span class="price">'.$formattedPrice.'</span>        </span>

                                                        </span>
                                                    </span>
                                                </div>
<div class="details-qty qty">
                                      <form id="product_addtocart_form_'.$id.'" data-role="tocart-form" action="'.$cartUrl.'" method="post">
                                        <input type="hidden" name="product" value="'.$id.'">
                                        <input type="hidden" name="uenc" value="'.$postParams['data'][Action::PARAM_NAME_URL_ENCODED].'">
                                                <input name="form_key" type="hidden" value="'.$formKey->getFormKey().'">
                                                                                
                                                                                    <div class="add-product">
                                           
                                                <div class="pull-right">
<input type="number" value="1" name="qty" min="1" max="'.$avilableqty.'" style="width: 25px;height: 27px;padding: 2px 2px 3px 8px!important;margin-right: 1px;"/>
                                                            <button type="submit" title="+ Add" class="action tocart primary" qty-incart="10030" style="padding:4px 2px 3px !important;">
                                                                <span>+Add</span>
                                                                <div class="messages-error notQty'.$id.' notify-box" style="display: none;"></div>
                                                            </button>
                                                         
                                                            
                                                                                                                                                            </div>
                                            </div>
                                                                            </form>
                                                                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>';

            }

        }
        // phpcs:enable
        $result['html']=$html;
        $resultJson=   $this->_objectManager->get(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        return $resultJson->setData(($result));
    }

    /**
     * @param $_product
     * @return int|mixed
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function checkAreaStock($_product)
    {
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $inventory = 0;
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) { //phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Location"]; //phpcs:ignore
            $lochelper = $this->helperData; //phpcs:ignore
            $data = $lochelper->getLocationById($cookie);
            $store = $data->getStoreId();
//            $productStockObj = $objectManager->get(\Magento\CatalogInventory\Api\StockRegistryInterface::class)->getStockItem($_product->getId());
            $productStockObj = $this->stockRegistryInterface->getStockItem($_product->getId());
            $storeId = 'store_' . $store;
            $inventory = $productStockObj[$storeId];
        } else {
            $cookie = 73;
//            $lochelper = $objectManager->get(\Gourmet\Location\Helper\Data::class);
            $data = $this->helperData->getLocationById($cookie);
            $store = $data->getStoreId();
            $productStockObj = $this->stockRegistryInterface->getStockItem($_product->getId());
            $storeId = 'store_' . $store;
            $inventory = $productStockObj[$storeId];
        }

        $minQty = $productStockObj['min_qty'];

        if (((isset($inventory) && $inventory > $minQty)) && ($productStockObj->getQty()>0)) {

            return $inventory;
        } else {

            return 0;
        }
    }

    /**
     * @param $product
     * @param array $attr
     * @return mixed
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedClass
     */
    public function getImageURL(
        $product,
        $attr = ['width'=>78,'height'=>78,'watermarked'=>true,'imageId'=>'product_thumbnail_image']
    ) {
        $imageFactory = $this->_objectManager->get(\Magento\Catalog\Helper\ImageFactory::class);
        $image = $imageFactory->create()->init($product, $attr['imageId'])
            ->constrainOnly(true)
            ->keepAspectRatio(true)
            ->keepTransparency(true)
            ->keepFrame(true)
            ->resize($attr['width'], $attr['height']);
        return $image->getUrl();
    }
}
