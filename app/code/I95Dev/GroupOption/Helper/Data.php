<?php

namespace I95Dev\GroupOption\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const PRODUCT_TYPE_CONFIGURABLE = 'configurable';
    const PRODUCT_TYPE_BUNDLE = 'bundle';
    const PRODUCT_TYPE_GROUPED = 'grouped';

    /**
     * Product drop-down option type.
     */
    const OPTION_TYPE_DROP_DOWN = 'drop_down';

    /**
     * Product multiple option type.
     */
    const OPTION_TYPE_MULTIPLE = 'multiple';

    /**
     * Product radio option type.
     */
    const OPTION_TYPE_RADIO = 'radio';

    /**
     * Product checkbox option type.
     */
    const OPTION_TYPE_CHECKBOX = 'checkbox';

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $storeManager;

    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $locationHelper;

    /**
     * @param Context $context
     * @param ProductMetadataInterface $productMetadata
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Gourmet\Location\Helper\Data $locationHelper
     */
    public function __construct(
        Context $context,
        ProductMetadataInterface $productMetadata,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Gourmet\Location\Helper\Data $locationHelper
    ) {
        parent::__construct($context);
        $this->productMetadata = $productMetadata;
        $this->storeManager = $storeManager;
        $this->locationHelper = $locationHelper;
    }

    /**
     * Get config values
     *
     * @param string $field
     * @return bool|string
     */
    public function getConfig($field = 'active')
    {
        return true;
    }

    /**
     *  Compare magento version
     *
     * @param string $version
     * @return bool
     */
    public function getMagentoVersion($version)
    {
        $dataVersion = $this->productMetadata->getVersion();
        if (version_compare($dataVersion, $version) >= 0) {
            return true;
        }
        return false;
    }

    /**
     * @return \Magento\Store\Model\StoreManagerInterface
     * @psalm-suppress MissingReturnType
     */
    public function getstoreManager()
    {
        return $this->storeManager;
    }
    /**
     * @return \Gourmet\Location\Helper\Data
     * @psalm-suppress MissingReturnType
     */
    public function getLocationHelper()
    {
        return  $this->locationHelper;
    }
}
