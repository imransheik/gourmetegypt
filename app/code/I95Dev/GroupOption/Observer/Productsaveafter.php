<?php

namespace I95Dev\GroupOption\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;

class Productsaveafter implements ObserverInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $sessionMenager;

    /**
     * Productsaveafter constructor.
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionMenager
     * @param Product $Product
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $sessionMenager,
        Product $Product
    ) {
        $this->sessionMenager = $sessionMenager;
        $this->Product = $Product;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    // phpcs:disable
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $_product = $observer->getProduct();  // you will get product object

//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->sessionMenager->start();
//        $this->sessionMenager->unsGpCustomOption();
        $customOptionData = $this->sessionMenager->getGpCustomOption();
        $myArray=[];
        if (is_array($customOptionData)) {
            foreach ($customOptionData as $optionproductid => $optionproductvalue) {
                $product = $this->Product->load($optionproductid);
                if (!empty($product->getOptions())) {
                    foreach ($product->getOptions() as $option) {
                        $optionValues = $option->getValues();
                        foreach ($optionValues as $value) {
                            if (array_key_exists($value->getOptionTypeId(), $optionproductvalue)) {
                                try {
                                    $exitingvalue = $value->getGroupProductId();
                                    if ($exitingvalue) {
                                        $myArray = explode(',', $exitingvalue);
                                   
                                        if (!in_array($_product->getId(), $myArray)) {
                                            $value->setGroupProductId($exitingvalue . ',' . $_product->getId())->save();
                                        }
                                    } else {
                                     
                                        $value->setGroupProductId($_product->getId())->save();
                                    }
                                } catch (\Exception $e) {
                                    $this->logger->critical($e->getMessage());
                                }
                            } else {
                                $exitingvalue = $value->getGroupProductId();
                                if ($exitingvalue) {
                                    $myArray = explode(',', $exitingvalue);
                                    if (($key = array_search($_product->getId(), $myArray)) !== false) {
                                        unset($myArray[$key]);
                                    }
                                    $value->setGroupProductId(implode($myArray))->save();
                                } else {
                                    $value->setGroupProductId('')->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->sessionMenager->unsGpCustomOption();
    }
}
