<?php

namespace I95Dev\GroupOption\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RemoveBlock implements ObserverInterface
{
    /**
     * @var \I95Dev\GroupOption\Helper\Data
     */
    private $helperI95Dev;

    /**
     * @param \I95Dev\GroupOption\Helper\Data $helperI95Dev
     */
    public function __construct(
        \I95Dev\GroupOption\Helper\Data $helperI95Dev
    ) {
        $this->helperI95Dev = $helperI95Dev;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\View\Layout $layout */
        $layout = $observer->getLayout();
        $block = $layout->getBlock('product.info.grouped');
        if ($block && $this->helperI95Dev->getConfig()) {
            $layout->unsetElement('product.info.grouped');
        }
    }
}
