<?php
namespace I95Dev\GroupOption\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class CustomOption extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$fieldName . '_html'] = "<button class='button'><span>Custom Options</span></button>";
                $item[$fieldName . '_title'] = __('Custom Option List');
                $item[$fieldName . '_submitlabel'] = __('Save Option');
                $item[$fieldName . '_cancellabel'] = __('Reset');
                $item[$fieldName . '_productid'] = $item['entity_id'];

                $item[$fieldName . '_formaction'] = $this->urlBuilder->getUrl('grid/customer/saveoption');
                $item[$fieldName . '_getoptionurl'] = $this->urlBuilder->getUrl('groupoption/customoption/option');
                $item[$fieldName . '_updateoptionurl'] = $this->urlBuilder->getUrl('groupoption/customoption/updateoption'); //phpcs:ignore
            }
        }

        return $dataSource;
    }
}
