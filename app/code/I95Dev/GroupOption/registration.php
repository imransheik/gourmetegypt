<?php
/**
 * I95Dev Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://I95Devcommerce.com/I95Dev-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * I95Dev Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * I95Dev Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   I95Dev
 * @package    I95Dev_GroupOption
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 I95Dev Commerce Co. ( http://I95Devcommerce.com )
 * @license    http://I95Devcommerce.com/I95Dev-Commerce-License.txt
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'I95Dev_GroupOption',
    __DIR__
);
