define([
    'Magento_Ui/js/grid/columns/column',
    'jquery',
    'mage/template',
    'Magento_Ui/js/modal/modal'
], function (Column, $, mageTemplate) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'ui/grid/cells/html',
            fieldClass: {
                'data-grid-html-cell': true
            }
        },
      
      
        getProductid: function (row) {
            return row[this.index + '_productid'];
        },
        getLabel: function (row) {
            return row[this.index + '_html']
        },
        getTitle: function (row) {
            return row[this.index + '_title']
        },
        getSubmitlabel: function (row) {
            return row[this.index + '_submitlabel']
        },
        getCancellabel: function (row) {
            return row[this.index + '_cancellabel']
        },
        getOptionURL: function (row) {
            return row[this.index + '_getoptionurl']
        },
          updateOptionURL: function (row) {
            return row[this.index + '_updateoptionurl']
        },
          updateOptions: function (row) {
              var pid=this.getProductid(row)
              var formData=$("#customoption-form-"+pid).serializeArray();
              
            var params = {'productid': pid,formData:formData};
           
            $.ajax({
                type: 'POST',
                url: this.updateOptionURL(row),
                data: params,
                showLoader: true,
                success: function (data) {
                     var resultData = JSON.parse(data);
                    
                }
            });
        },
        preview: function (row) {
            var params = {'productid': this.getProductid(row)};
            var self=this;
            $.ajax({
                type: 'POST',
                url: this.getOptionURL(row),
                data: params,
                showLoader: true,
                success: function (data) {
                    var htmlData = JSON.parse(data);
                                 
                    var previewPopup = $('<div/>').html(htmlData.htmloption);
                    previewPopup.modal({
                        title: self.getTitle(row),
                        innerScroll: true,
                        modalClass: '_image-box',
                         buttons: [{
                                                    text: 'Save Option',
                                                    class: 'save-option',
                                                    click: function () {
                                                        self.updateOptions(row);
                                                        this.closeModal();
                                                      

                                                    }
                                                }]}).trigger('openModal'); 



                }



            });

        },
        getFieldHandler: function (row) {

            return this.preview.bind(this, row);
        }
    });
});