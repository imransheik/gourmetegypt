
define([
    'jquery',
    'underscore',
    'jquery/ui',
    'Magento_Catalog/js/catalog-add-to-cart'
], function($, _) {
    'use strict';

    $.widget('I95Dev_GroupOption.catalogAddToCart', $.mage.catalogAddToCart, {
        /**
         * Handler for the form 'submit' event
         *
         * @param {Object} form
         */
        submitForm: function (form) {
            if ($('[name=I95Dev-option]').length) {
                var addToCartButton,
                    self = this,
                    fileOption = false;
                _.each($('#super-product-table tbody input.qty'), function (qty) {
                    if ($(qty).val() > 0) {
                        var optionEl = $(qty).closest('tbody').find('.I95Dev-option-custom-option');
                        if (optionEl.has('input[type="file"]').length && optionEl.find('input[type="file"]').val() !== '') {
                            fileOption = true;
                            return false;
                        }
                    }
                });
                if (fileOption) {
                    self.element.off('submit');
                    // disable 'Add to Cart' button
                    addToCartButton = $(form).find(this.options.addToCartButtonSelector);
                    addToCartButton.prop('disabled', true);
                    addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
                    form.submit();
                } else {
                    self.ajaxSubmit(form);
                }
            } else {
                this._super(form);
            }
        }
    });

    return $.I95Dev_GroupOption.catalogAddToCart;
});
