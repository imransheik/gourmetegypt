
define([
    'jquery',
    'I95Dev_GroupOption/js/price-box',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function ($, priceBox) {
    'use strict';

    $.widget('I95Dev.grouped', {
        options: {
            show_option: false,
            json: {}
        },

        _initPriceBox: function () {
            var options = this.options;
            var priceBoxes = $('[data-role=priceBox]');

            priceBoxes = priceBoxes.filter(function (index, elem) {
                return !$(elem).find('.price-from').length;
            });

            priceBoxes.priceBox({'priceConfig': options.json});
        },
        _create: function () {
            this._initGrouped();
            this._initPriceBox();
        },
        _initGrouped: function () {
            var options = this.options;
            var optionCache = {},
                    showOption = options.show_option;

            $.validator.addMethod(
                    'required-option-field',
                    function (value) {
                        return (value !== '');
                    },
                    $.mage.__('This is a required field.')
                    );

            $('#product_addtocart_form').attr('enctype', 'multipart/form-data');

            $('#super-product-table input.qty').change(function () {
                var childId,
                        element,
                        qty = $(this).val();
                childId = $(this).attr('data-product-id');
                element = $('#super-product-table .I95Dev-option-child-product-id-' + childId);
                if (!showOption) {
                    if (qty != 0) {
                        element.show();
                    } else {
                        $('#super-product-table .I95Dev-option-child-product-id-' + childId).hide();
                    }
                } else {
                    element.find('.product-custom-option').each(function () {
                        var $this = $(this);
                        if (qty == 0) {
                            if ($this.hasClass('required')) {
                                $this.addClass('I95Dev-required');
                                $this.removeClass('required');
                            }
                            if (typeof $this.attr('data-validate') !== typeof undefined && $this.attr('data-validate') !== false) {
                                var attrValue = $this.attr('data-validate');
                                $this.attr('data-validate-I95Dev', attrValue);
                                $this.removeAttr('data-validate');
                                if ($this.hasClass('required-option-field')) {
                                    $this.removeClass('required-option-field');
                                    $this.attr('required-option-field', '1');
                                }
                            }
                            $this.removeClass('mage-error');
                        } else {
                            if ($this.hasClass('I95Dev-required')) {
                                $this.addClass('required');
                                $this.removeClass('I95Dev-required');
                            }
                            if (typeof $this.attr('data-validate-I95Dev') !== typeof undefined && $this.attr('data-validate-I95Dev') !== false) {
                                var attrValue2 = $this.attr('data-validate-I95Dev');
                                $this.attr('data-validate', attrValue2);
                                $this.removeAttr('data-validate-I95Dev');
                                if ($this.attr('required-option-field') === '1') {
                                    $this.addClass('required-option-field');
                                }
                            }
                        }
                    });
                }
                element.find('.super-attribute-select').each(function () {
                    var $this = $(this);
                    if (qty == 0) {
                        if ($this.hasClass('required-option-field')) {
                            $this.removeClass('required-option-field');
                        }
                    } else {
                        if (!$this.hasClass('required-option-field')) {
                            $this.addClass('required-option-field');
                        }
                    }
                });


            });




            $('#super-product-table input.qty').change();

            $('.action.tocart').mousedown(function () {
                $('#super-product-table input.qty').change();
            })
        }


    });

    return $.I95Dev.grouped;
});