
define(
        [
            'jquery',
            'Magento_Catalog/js/price-utils',
            'underscore',
            'mage/template',
            'Magento_Catalog/js/price-box',
            'jquery/ui'
        ],
        function ($, utils, _, mageTemplate) {
            'use strict';
            var I95DevProductId = '';
            $.widget('I95Dev_GroupOption.priceBox', $.mage.priceBox, {
                groupItemPrices: [],
                originGroupItemPrices: [],
                groupElement: [],
                additionalPriceObjectCustom: {},
                
                _create: function createPriceBox()
                {
                    var box = this.element,
                            self = this;

                    self.cache = {};
                    self.cache.displayPrices = {};
                    self._setDefaultsFromPriceConfig();
                    self._setDefaultsFromDataSet();
                    $('.I95Dev-option-child-product-info .super-attribute-select').change(function () {
                        var id = $(this).closest('.product-options-wrapper').find('.I95Dev-option-custom-option');
                        if (id && $(id).length) {
                            I95DevProductId = $(id).attr('data-product-id');
                        } else {
                            I95DevProductId = '';
                        }
                    });
                    $('.I95Dev-option-child-product-info .super-attribute-select').on('option-update-attribute', function () {
                        var id = $(this).closest('.product-options-wrapper').find('.I95Dev-option-custom-option');
                        if (id && $(id).length) {
                            I95DevProductId = $(id).attr('data-product-id');
                        } else {
                            I95DevProductId = '';
                        }
                    });
                    box.on('reloadPrice', self.reloadPrice.bind(self));
                    box.on('updatePrice', self.onUpdatePrice.bind(self));
                    _.each(this.options.priceConfig, function (index, productId) {
                        self.groupItemPrices[productId] = utils.deepClone(index.prices);
                        self.originGroupItemPrices[productId] = utils.deepClone(index.prices);
                        self.groupElement[productId] = $('#super-product-table [data-role="priceBox"][data-product-id="' + productId + '"]');
                    });
                },

                _init: function initPriceBox() {
                    var box = this.element,
                            self = this;

                    box.trigger('updatePrice');
                    _.each(self.options.priceConfig, function (index, el) {
                        self.cache.displayPrices[el] = utils.deepClone(index.prices);
                    });
                },

                updatePrice: function updatePrice(newPrices)
                {
                    var prices = this.cache.displayPrices,
                            additionalPrice = {},
                            pricesCode = [],
                            priceValue, origin, finalPrice;
                    if (newPrices && typeof (newPrices.productId) != 'undefined') {
                        I95DevProductId = newPrices.productId;
                        delete newPrices.productId;
                    }

                    if (I95DevProductId) {
                        prices = this.cache.displayPrices[I95DevProductId];
                        this.additionalPriceObjectCustom[I95DevProductId] = this.additionalPriceObjectCustom[I95DevProductId] || {};
                    }
                    this.cache.additionalPriceObject = this.cache.additionalPriceObject || {};

                    if (newPrices) {
                        if (I95DevProductId) {
                            $.extend(this.additionalPriceObjectCustom[I95DevProductId], newPrices);
                            this.cache.additionalPriceObject = this.additionalPriceObjectCustom[I95DevProductId];
                        } else {
                            $.extend(this.cache.additionalPriceObject, newPrices);
                        }
                    }

                    if (!_.isEmpty(additionalPrice)) {
                        pricesCode = _.keys(additionalPrice);
                    } else if (!_.isEmpty(prices)) {
                        pricesCode = _.keys(prices);
                    }

                    _.each(this.cache.additionalPriceObject, function (additional, key) {
                        if (additional && !_.isEmpty(additional)) {
                            pricesCode = _.keys(additional);
                        }
                        _.each(pricesCode, function (priceCode) {
                            var priceValue = additional[priceCode] || {};
                            priceValue.amount = +priceValue.amount || 0;
                            priceValue.adjustments = priceValue.adjustments || {};

                            additionalPrice[priceCode] = additionalPrice[priceCode] || {
                                'amount': 0,
                                'adjustments': {}
                            };
                            additionalPrice[priceCode].amount = 0 + (additionalPrice[priceCode].amount || 0)
                                    + priceValue.amount;
                            _.each(priceValue.adjustments, function (adValue, adCode) {
                                additionalPrice[priceCode].adjustments[adCode] = 0
                                        + (additionalPrice[priceCode].adjustments[adCode] || 0) + adValue;
                            });
                        });
                    });

                    if (_.isEmpty(additionalPrice) && I95DevProductId) {
                        this.cache.displayPrices[I95DevProductId] = utils.deepClone(this.options.priceConfig[I95DevProductId].prices);
                    } else {
                        _.each(additionalPrice, function (option, priceCode) {
                            if (I95DevProductId != '') {
                                origin = this.originGroupItemPrices[I95DevProductId][priceCode] || {};
                                finalPrice = prices[priceCode] || {};
                                option.amount = option.amount || 0;
                                origin.amount = origin.amount || 0;
                                origin.adjustments = origin.adjustments || {};
                                finalPrice.adjustments = finalPrice.adjustments || {};

                                finalPrice.amount = 0 + origin.amount + option.amount;
                                _.each(option.adjustments, function (pa, paCode) {
                                    finalPrice.adjustments[paCode] = 0 + (origin.adjustments[paCode] || 0) + pa;
                                });
                            }
                        }, this);
                    }

                    this.element.trigger('reloadPrice');
                },

                reloadPrice: function reDrawPrices()
                {
                    var priceFormat = (this.options.priceConfig[I95DevProductId] && this.options.priceConfig[I95DevProductId].priceFormat) || {},
                            priceTemplate = mageTemplate(this.options.priceTemplate);

                    if (I95DevProductId != '') {
                        var prices = this.cache.displayPrices[I95DevProductId];
                    } else {
                        var prices = this.cache.displayPrices;
                    }
                    if (typeof prices.oldPrice !== 'undefined') {
                        $("#old-price-" + I95DevProductId + " .price").text(utils.formatPrice(prices.oldPrice.amount));
                    }
                    _.each(prices, function (price, priceCode) {
                        var defaultQty = $('.group-p-add-tocart-' + I95DevProductId).parent().siblings('.qty').find('input').attr('avlqty');
                        price.amount = price.amount * defaultQty;
                        if (price.adjustments) {
                            price.final = _.reduce(price.adjustments, function (memo, amount) {
                                return memo + amount;
                            }, price.amount);

                            price.formatted = utils.formatPrice(price.final, priceFormat);
                            if (this.cache && I95DevProductId) {
                                if (this.groupElement[I95DevProductId].attr('data-product-id') != I95DevProductId) {
                                    return;
                                }
                            }

                            if (I95DevProductId != '') {
                                var element = this.groupElement[I95DevProductId];
                            } else {
                                var element = this.element;
                            }
                            var elem = '#product-price-' + I95DevProductId + '[data-price-type="' + priceCode + '"]';


                            var tPrice = $('.addAndDeductDiv .actions .show-totalPrice ').attr('tprice') - $('.group-p-add-tocart-' + I95DevProductId).attr("price");
                            $('.group-p-add-tocart-' + I95DevProductId).attr("price", price.amount);
                            var cPrice = price.amount;
                            var nPrice = parseFloat(tPrice) + parseFloat(cPrice);
                            $('.addAndDeductDiv .actions .show-totalPrice ').attr('tprice', nPrice);
                            nPrice = nPrice.toFixed(2);
                            if (nPrice > 0)
                            {
                                $('.allPrice').text('EGP' + nPrice);
                            } else {
                                $('.allPrice').text('');
                            }
                            $(elem).html(priceTemplate({data: price}));
                        }
                    }, this);
                }
            });

            return $.I95Dev_GroupOption.priceBox;
        }
);
