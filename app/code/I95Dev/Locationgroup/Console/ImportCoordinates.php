<?php
namespace I95Dev\Locationgroup\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;

class ImportCoordinates extends Command
{
	const NAME = 'filename';
	const LOGNAME = 'logname';
	const WEBSITEMAPPING = 'websitemapping';

	protected $_directory;
	protected $_customerFactory;
	protected $_customerModel;
	protected $_address;
	protected $_addressCollectionFactory;
   
	public function __construct(
        DirectoryList $directory,
        CustomerFactory $customerFactory,
        Customer $customerModel,
		AddressFactory $address,
		\Magento\Customer\Model\Address $addressobj,
		CollectionFactory $addressCollectionFactory
        
    ) {
        $this->_directory = $directory;
        $this->_customerFactory = $customerFactory;
        $this->_customerModel = $customerModel;
        $this->_address = $address;
        $this->_addressobj = $addressobj;
        $this->_addressCollectionFactory = $addressCollectionFactory;
        
        parent::__construct();
    }
	
	protected function configure()
    {

        $commandoptions = [
			new InputOption(self::NAME, null, InputOption::VALUE_REQUIRED, 'filename')
		];

        $this->setName('i95devimport:coordinates')
            ->setDescription(' import zone from csv')
            ->setDefinition($commandoptions);

        parent::configure();
    }
	protected function execute(InputInterface $input, OutputInterface $output)
    {
		$jsonFile = $input->getOption(self::NAME);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

		$rootPath  =  $directory->getPath('var');
		
		$jsonFilePath = $rootPath.'/import/'.$jsonFile;
		//echo $jsonFilePath;
		// Checking the csv file is provide or empty
		if( empty($jsonFilePath)){
			$output->writeln("Please provide the Json file to import.");
			exit;
		}
		
		if (!file_exists($jsonFilePath)) {
			$output->writeln("Please provide proper JSON file on proper path to map location and store.");
			exit;
		}
		
		//Reading JSON file
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$array = $fields = array(); $i = 0;
		$handle = fopen($jsonFilePath, "r");
		if ($handle) {
			while (($row = fgetcsv($handle, 4096)) !== false) {
				if (empty($fields)) {
					$fields = $row;
					continue;
				}
				foreach ($row as $k=>$value) {
					$array[$i][$fields[$k]] = $value;
				}
				$i++;
			}
			if (!feof($handle)) {
				echo "Error: unexpected fgets() fail\n";
			}
			fclose($handle);
		}
		
		foreach($array as $coordinate){
			// print_r($coordinate['zone_name']);
			// print_r($coordinate['data_id']);
			// print_r($coordinate['coordination']);
			// exit;
			$coordintesVar = $coordinate['coordination'];
			$coords = explode(',', $coordintesVar);
			
			foreach($coords as $coord){
				$coordinateObj = $objectManager->create('I95Dev\Locationgroup\Model\Address');
				$coord =str_replace(' ', ',', $coord);
				$coord = trim($coord,",");
				//print_r($coord);
				$coordinateObj->setData('location_id', $coordinate['data_id']);
				$coordinateObj->setData('coordination', $coord);
				$coordinateObj->save();
			}
			
		}
	
		
		
		$output->writeln('The import is done');
	}
   
}