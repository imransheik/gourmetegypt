<?php
/*
 * I95Dev_Locationgroup
 */
namespace I95Dev\Locationgroup\Controller\Adminhtml\Data;

use I95Dev\Locationgroup\Controller\Adminhtml\Data;

class Add extends Data
{
    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
