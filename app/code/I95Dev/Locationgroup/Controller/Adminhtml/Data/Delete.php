<?php

/*
 * I95Dev_Locationgroup
 * @version    1.0.0
 */

namespace I95Dev\Locationgroup\Controller\Adminhtml\Data;

use I95Dev\Locationgroup\Controller\Adminhtml\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends \Magento\Backend\App\Action
{
      
    /**
     * @var array|null
    */
    protected $_collectionFactory;

    /**
     * @var array|null
     */
   // protected $_options;

    /**
     * Object initialization
     *
     * @return void
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context, \I95Dev\Locationgroup\Model\ResourceModel\Address\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    } 
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
     
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('data_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data');
                /*var_dump($model->getData());exit;*/
                $locationModel = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address');
                $model->load($id);
                $model->delete();
                
                $locationCollection = $this->_collectionFactory->create();
                $locationCollection->addFieldToFilter('location_id', array(array('finset' => array($id))));
//                echo '<pre>';var_dump($locationCollection->getData());exit;
                foreach ($locationCollection as $location) {
                 $locationId = $location->getLocationId();
                 $entityId = $location->getEntityId();
                }
                if($locationId == $id){
                $locationModel->load($entityId);
                $locationModel->delete();  
                }
                
                // display success message
                $this->messageManager->addSuccess(__('The item has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['data_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a item to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

