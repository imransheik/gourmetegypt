<?php

/*
 * I95Dev_Locationgroup
 */

namespace I95Dev\Locationgroup\Controller\Adminhtml\Data;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

/**
 * Class CustomerOrder
 * @package Efi\PurchaseLimits\Controller\Adminhtml\Customer
 */
class Edit extends Action
{
    /**
     * @var bool|PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @var Session
     */
    protected $backendSession;

    /**
     * CustomerOrder constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Session $backendSession
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Session $backendSession
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->backendSession = $backendSession;
        parent::__construct($context);
    }
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute() {
        $session = $this->backendSession;
        $session->setGroupId($this->getRequest()->getParam('data_id', null));
        
        $dataId = $this->getRequest()->getParam('data_id');
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev_Locationgroup::data')
                ->addBreadcrumb(__('Manage Location Mapping Rule'), __('Manage Location Mapping Rule'))
                ->addBreadcrumb(__('Manage Location Mapping Rule'), __('Manage Location Mapping Rule'));

        if ($dataId === null) {
            $resultPage->addBreadcrumb(__('New Location Mapping Rule'), __('New Location Mapping Rule'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Location Mapping Rule'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Location Mapping Rule'), __('Edit Location Mapping Rule'));
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            
            $customerInfoSession = $objectManager->create('Magento\Framework\Session\SessionManager');
            $customerInfoSession->setMyId($dataId);
        }

        return $resultPage;
    }

}
