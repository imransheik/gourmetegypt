<?php
/*
 * I95Dev_Locationgroup
 */

namespace I95Dev\Locationgroup\Controller\Adminhtml\Data;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Backend\App\Action {


    protected $groupNameCollection;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context,
                                \I95Dev\Locationgroup\Model\ResourceModel\Data\CollectionFactory $groupNameCollection) {
        $this->collectionFactory = $groupNameCollection;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $request = $this->getRequest();

        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data');
        $data = $request->getParams();
		//print_r(); exit;
        $id = (int) $request->getParam('data_id');
        if ($id) {
            $model->load($id);
        }
        try {
            $postGroupName = $request->getParam('group_name');
            $daysArray = $request->getParam('days');
			$days = implode(',', $daysArray);
            $storeId = $request->getParam('store_id');
            $time_from = $request->getParam('time_from');
            $time_to = $request->getParam('time_to');
            $groupNameCollection = $this->collectionFactory->create();
            $groupNameCollection->addFieldToFilter('group_name', array('finset' => $postGroupName));
            $groupNameCollection->addFieldToFilter('store_id', array('finset' => $storeId));
            $groupNameCollectionData = $groupNameCollection->getData();
            if (empty($groupNameCollectionData) || ($groupNameCollectionData[0]['data_id'] == $id)) {
                $model->setData('status', $data['status']);
                $model->setData('group_name', $data['group_name']);
                $model->setData('store_id', $data['store_id']);
                $model->setData('days', $days);
                $model->setData('time_from', $time_from);
                $model->setData('time_to', $time_to);
                $model->setData('cutoff_time', $data['cutoff_time']);
                $model->save();
            }else{
                $this->messageManager->addError('The Location Mapping Rule Name and Store Id should be unique.');
                return $resultRedirect->setPath('*/*/edit', ['data_id' => $this->getRequest()->getParam('data_id')]);

            }
            $this->messageManager->addSuccess(__('The Location Mapping Rule is saved successfully.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['data_id' => $model->getId(), '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/');

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the Location Mapping Rule.'));
            $this->_getSession()->setFormData(false);
            return $resultRedirect->setPath('*/*/edit', ['data_id' => $this->getRequest()->getParam('data_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
