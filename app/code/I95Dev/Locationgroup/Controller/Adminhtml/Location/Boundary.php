<?php

namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

class Boundary extends \Magento\Framework\App\Action\Action {
 protected $resultJsonFactory;
    protected $_pageFactory;
    //protected $_loyaltyHelper;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
            \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
       
        return parent::__construct($context);
    }

    public function execute() {
        $datas = $this->getRequest()->getParams();
        $boundarydatacoorinate = $datas['coordinate']; 
         $result = $this->resultJsonFactory->create();
        $coordinates= array();
        foreach ($boundarydatacoorinate as $coordinate){
           //$coordinates= explode(" ",$coordinate);
//           var_dump($coordinate);
          
           $coordinates[] = explode (",", $coordinate);     
          
          
        }
       // var_dump($coordinates);exit;
           //$response = array();
           $response['success'] = true;
           $response['coordinates'] = $coordinates;
           $response['message'] = "Coordinates data!";
//           var_dump($response);
           $result->setData(json_encode($response));
        return $result; 
    }

}
