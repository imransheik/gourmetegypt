<?php

namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

class BoundaryCollection extends \Magento\Framework\App\Action\Action {
    protected $resultJsonFactory;
    protected $_pageFactory;
    protected $collectionFactory;

    /**
     * @var Session
     */
    protected $backendSession;
    protected $requestInterface;

    public function __construct(
        \Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \I95Dev\Locationgroup\Model\ResourceModel\Address\CollectionFactory  $collectionFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,\Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Backend\Model\Session $backendSession) {
        $this->collectionFactory = $collectionFactory;
        $this->_pageFactory = $pageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->backendSession = $backendSession;
        $this->requestInterface = $requestInterface;

        return parent::__construct($context);
    }

    public function execute() {
        $request = $this->requestInterface;
        $session = $this->backendSession;
		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		$coorinates= array();
        
        $locationgroup = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->getCollection();
		foreach($locationgroup as $item){
			$locationgroupid=$item->getId();
			$boundarydata = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection()->addFieldToFilter('location_id',$locationgroupid);
			$boundarydatacoorinate= $boundarydata->getData();
			

			foreach ($boundarydatacoorinate as $coorinate){
				$coorinates[$locationgroupid][]= explode(",",trim($coorinate["coordination"]));
			}
			
		}
		
        
        $response=array();
        $response['success'] = true;
        $response['coordinates'] = $coorinates;
        $response['message'] = "Coordinates data!";
        $result->setData(json_encode($response));
        return $result;
    }

}
