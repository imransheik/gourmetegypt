<?php

namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

class BoundaryGrid extends \Magento\Framework\App\Action\Action {
    protected $resultJsonFactory;
    protected $_pageFactory;
    protected $collectionFactory;

    /**
     * @var Session
     */
    protected $backendSession;
    protected $requestInterface;

    public function __construct(
        \Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \I95Dev\Locationgroup\Model\ResourceModel\Address\CollectionFactory  $collectionFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,\Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Backend\Model\Session $backendSession) {
        $this->collectionFactory = $collectionFactory;
        $this->_pageFactory = $pageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->backendSession = $backendSession;
        $this->requestInterface = $requestInterface;

        return parent::__construct($context);
    }

    public function execute() {
        $request = $this->requestInterface;
        $session = $this->backendSession;
        $data_id = $session->getGroupId();
        //var_dump($data_id);exit;

        $datas = $this->getRequest()->getParams();


        $result = $this->resultJsonFactory->create();
        $model = $this->collectionFactory->create();
        $model->addFieldToSelect('coordination');
        //$model->addFieldToFilter('location_id', $data_id)
        $model->addFieldToFilter('location_id',array('in' => $data_id));
        $model ->load();
        // var_dump($model->getData());exit;
        $boundarydatacoorinate = $model->getData();

        $coordinates= array();
        foreach ($boundarydatacoorinate as $coordinate){
            $coordinatesArray = implode (",", $coordinate);

            $coordinates[] = explode (",", $coordinatesArray);
        }
        $response['success'] = true;
        $response['coordinates'] = $coordinates;
        $response['message'] = "Coordinates data!";
        $result->setData(json_encode($response));
        return $result;
    }

}
