<?php

/*
 * I95Dev_Locationgroup
 * @version    1.0.0
 */

namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

use I95Dev\Locationgroup\Controller\Adminhtml\Location;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var bool|PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @var Session
     */
    protected $backendSession;
    protected $requestInterface;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\RequestInterface $requestInterface,
        Session $backendSession
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->backendSession = $backendSession;
        $this->requestInterface = $requestInterface;
        parent::__construct($context);
    }
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $request = $this->requestInterface;
        $session = $this->backendSession;
        $data_id = $session->getGroupId();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('entity_id');

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address');
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The location has been deleted.'));
                return $resultRedirect->setPath('*/data/edit', ['data_id' => $data_id]);
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/data/edit', ['entity_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a location to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/data/edit');
    }
}

