<?php
/*
 * I95Dev_Locationgroup
 */
namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

use I95Dev\Locationgroup\Controller\Adminhtml\Location;

class Index extends Location
{
    
    
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        
        //return $this->resultPageFactory->create();
         $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev\Locationgroup::data_Tab');
        $resultPage->addBreadcrumb(__('Location Restrictions'), __('Location Restrictions'));
        $resultPage->addBreadcrumb(__('Location Restrictions'), __('Location Restrictions'));
        $resultPage->getConfig()->getTitle()->prepend(__('Location'));
        return $resultPage;
    }
}
