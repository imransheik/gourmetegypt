<?php
/*
 * I95Dev_Locationgroup
 */

namespace I95Dev\Locationgroup\Controller\Adminhtml\Location;

class Save extends \Magento\Backend\App\Action {
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $connection;
    protected $resource;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(\Magento\Backend\App\Action\Context $context,
                                \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,\Magento\Framework\App\ResourceConnection $resource) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->connection = $resource->getConnection();
        $this->resource = $resource;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $datas = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $zone = $datas['zone_id'];
        //$coordinate = $datas['vertices'];
        $importCoordinates = $datas['vertices-import'];
        $result = $this->resultJsonFactory->create();
        $boundry = 0;
        if($importCoordinates != ""){
            $coords = explode(',', $importCoordinates);
            try {
                $locationCollectionObj = $objectManager->get('I95Dev\Locationgroup\Model\ResourceModel\Address\CollectionFactory')->create();
                $locationCollection = $locationCollectionObj->addFieldToFilter('location_id',array('in' => $zone));
                $locationCollection->walk('delete');
                foreach($coords as $coord){
                    $model = $this->_objectManager->create('I95Dev\Locationgroup\Model\AddressFactory')->create();
                    $coord =str_replace(' ', ',', $coord);
                    $coord = trim($coord,",");
                    $model->setData('address', "");
                    $model->setData('coordination', $coord);
                    $model->setData('boundry', "");
                    $model->setData('location_id', $zone);
                    $model->save();
                    $model->unsetData();
                }
                $result->setData(array('type' => 'success', 'message' => 'Data saved Successfully !!'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(array('message' => $e->getMessage()));
            } catch (\RuntimeException $e) {
                $result->setData(array('type' => 'error', 'message' => $e->getMessage()));
            } catch (\Exception $e) {
                $result->setData(array('type' => 'error', 'message' =>$e->getMessage()));
                $this->_getSession()->setFormData(false);
            }
        }else{
            preg_match_all("/\((?:[^()]|(?R))+\)|'[^']*'|[^(),\s]+/",  $datas['vertices'], $coordinate);
            $coordinates = $coordinate[0];
            try {
                $locationCollectionObj = $objectManager->get('I95Dev\Locationgroup\Model\ResourceModel\Address\CollectionFactory')->create();
                $locationCollection = $locationCollectionObj->addFieldToFilter('location_id',array('in' => $zone));
                $locationCollection->walk('delete');

                foreach ($coordinates as $coordinate) {
                    $coordinate = str_replace(array( '(', ')' ), '', $coordinate);
                    $model = $this->_objectManager->create('I95Dev\Locationgroup\Model\AddressFactory')->create();
                    $model->setData('address', "");
                    $model->setData('coordination', $coordinate);
                    $model->setData('boundry', "");
                    $model->setData('location_id', $zone);
                    $model->save();
                    $model->unsetData();
                }

                $result->setData(array('type' => 'success', 'message' => 'Data saved Successfully !!'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(array('message' => $e->getMessage()));
            } catch (\RuntimeException $e) {
                $result->setData(array('type' => 'error', 'message' => $e->getMessage()));
            } catch (\Exception $e) {
                $result->setData(array('type' => 'error', 'message' =>$e->getMessage()));
                $this->_getSession()->setFormData(false);
            }
        }
        
        return $result;

    }
}
