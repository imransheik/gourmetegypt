<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Locationgroup\Controller\Cart;

class Index extends \Magento\Checkout\Controller\Cart 
{

    protected $resultPageFactory;
    protected $_registry;
    protected $_cartSession;
    //protected $helper;
    protected $customerData;
    protected $storeManager;
    protected $messageManager;
    //protected $purchaselimitCollection;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @codeCoverageIgnore
     */
    public function __construct(
    //\I95Dev\Purchaselimit\Model\ResourceModel\Data\CollectionFactory $purchaselimitCollection,
    //\I95Dev\Purchaselimit\Helper\Data $helper,
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \Magento\Checkout\Model\Cart $cart, 
    \Magento\Checkout\Model\Session $checkoutSession, 
    \Magento\Customer\Model\Session $customerData, 
    \Magento\Store\Model\StoreManagerInterface $storeManager, 
    \Magento\Framework\View\Result\PageFactory $resultPageFactory,
    \Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
    ) {
        //$this->purchaselimitCollection = $purchaselimitCollection;
        $this->customerData = $customerData;
        $this->resultPageFactory = $resultPageFactory;
        $this->cartSession = $cart;
        //$this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->storeManger = $storeManager;
        $this->messageManager = $messageManager;

        parent::__construct(
                $context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart
        );
    }

    public function execute()
    {


        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Shopping Cart'));

        if (!isset($_SESSION["store"])) {
            //var_dump($_SESSION["store"] );exit;
            $_SESSION["store"] = 7;
        }


        $items = $this->cartSession->getQuote()->getAllItems();
        foreach ($items as $item) {
           // var_dump($item);
            $sku = $item->getSku();
           // var_dump($sku);exit;
            $skuArray = $arr = preg_split("/[-+@#]/", $item->getSku());

            if (!empty($skuArray)) {
                $sku = $skuArray[0];
            }

            $inventory = 0;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) {
                $cookie = $_COOKIE["Gourmet_Location"];
                //$lochelper = $objectManager->get('\I95Dev\Locationgroup\Helper\Data');
                $store = $cookie;
                $productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($item->getSku());
                $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($productId);
                $storeId = 'store_' . $store;
                $inventory = $productStockObj[$storeId];
            } else {
                $cookie = 7;
                //$lochelper = $objectManager->get('\I95Dev\Locationgroup\Helper\Data');
                $store = $cookie;
                $productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($item->getSku());
                $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($productId);
                $storeId = 'store_' . $store;
                $inventory = $productStockObj[$storeId];
            }
            $minQty = $productStockObj['min_qty'];
			
            if (isset($_COOKIE['Gourmet_Location'])) {
               if(($inventory <= $item->getQty()) &&($inventory != 0) ) {

                    $qty = $inventory;
                   $finalPrice = $item->getPrice();
                   $itemData = [$item->getItemId() => ['qty' => $qty]];
                   $msg = __('The quantity associated with selected Location is ' . $qty . ', the maximum available product for selected location has been added to your cart');
                   $this->messageManager->addError($msg);
                   $item->setRowTotal($finalPrice * $qty);
                }
                else if($inventory == 0){
                    $qty = $item->getQty();
                    $itemData = [$item->getItemId() => ['qty' => $qty]];
                    $skuName = $item->getSku();
                    $itemName = $item->getName();

                    $msg = __('Some of the products are out of stock.');
                    $this->messageManager->addError($msg);

                }
                if (!empty($itemData)) {
                    $this->cartSession->updateItems($itemData)->save();
                }
            }
        }
		
        $items1 = $this->cartSession->getQuote()->getAllItems();
        if (count($items1) == 0) {
            $cartObject = $this->cartSession->truncate();
            $cartObject->saveQuote();
        }
        return $resultPage;
    }

}
