<?php

namespace I95Dev\Locationgroup\Controller\Index;

class Boundary extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
       
        return parent::__construct($context);
    }

    public function execute() {
        $location_id = $this->getRequest()->getPost('location', false);
       
        $boundarydata = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection()->addFieldToFilter('location_id',$location_id);
        $boundarydatacoorinate= $boundarydata->getData();
        $result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $coorinates= array();
       
        foreach ($boundarydatacoorinate as $coorinate){
            $coorinates[]= explode(",",trim($coorinate["coordination"]));
        }

        $response=array();
        $response['success'] = true;
        $response['coordinates'] = $coorinates;
        $response['message'] = "Coordinates data!";
        $result->setData(json_encode($response));
        return $result;
    }

}
