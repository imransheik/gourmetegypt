<?php

namespace I95Dev\Locationgroup\Controller\Index;

class BoundaryCollection extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
       
        return parent::__construct($context);
    }

    public function execute() {
		
		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		$coorinates= array();
        $city = $this->getRequest()->getPost('location', false);
        $locationgroup = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->getCollection()->addFieldToFilter('status',1);
		foreach($locationgroup as $item){
			$locationgroupid=$item->getId();
			$boundarydata = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection()->addFieldToFilter('location_id',$locationgroupid);
			$boundarydatacoorinate= $boundarydata->getData();
			

			foreach ($boundarydatacoorinate as $coorinate){
				$coorinates[$locationgroupid][]= explode(",",trim($coorinate["coordination"]));
			}
			
		}
		
        
        $response=array();
        $response['success'] = true;
        $response['coordinates'] = $coorinates;
        $response['message'] = "Coordinates data!";
        $result->setData(json_encode($response));
        return $result;
    }

}
