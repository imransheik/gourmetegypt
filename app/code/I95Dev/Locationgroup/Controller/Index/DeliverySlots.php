<?php

namespace I95Dev\Locationgroup\Controller\Index;
use Magento\Backend\Model\Session;

class DeliverySlots extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;
    /**
     * @var Session
     */
    protected $backendSession;
    public function __construct(
    \Magento\Framework\App\Action\Context $context,Session $backendSession, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->backendSession = $backendSession;

       
        return parent::__construct($context);
    }

    public function execute() {
		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		$date = $this->_objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('Y-m-d');
		$location_id = $this->getRequest()->getPost('location_id');
		$store_id = $this->getRequest()->getPost('store_id');
		
		$response = file_get_contents($this->getServerUrl().'mofluidapi2?callback=&currency=EGP&service=geodeliverySlot&city=&deliveryDate='.$date.'&location_id='.$location_id.'&store_id='.$store_id.'&quote_id=&store=1');
		$response = json_decode($response);
		$slots = $response->payload->data->regions;
		$rates = $this->_objectManager->create('\I95Dev\DeliverySlot\Model\DeliverySlot')->getCollection();
		$rates->addFieldToFilter('dest_zip',trim($location_id))->addFieldToFilter('dest_zip_to',trim($store_id));
		
		if(!empty($slots)){
			$delievrySlots= $slots[0]->slots;
			$city = $slots[0]->city;
		}else{
			foreach($rates as $rate){
				$delievrySlots= $rate->getShippingMethod();
				$city = $rate->getData('dest_city');
				break;
			}
			
		}
		if(empty($delievrySlots)){
			$delievrySlots = "";
		}
		
		$response=array();
        $response['success'] = true;
        $response['delivery'] = $delievrySlots;
        $response['message'] = "Delivery data!";
        $result->setData($response);
        return $result;
       
    }
	
	public function getServerUrl() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url = $storeManager->getStore()->getBaseUrl();
        return $url;
    }

}
