<?php

namespace I95Dev\Locationgroup\Controller\Index;
use Magento\Backend\Model\Session;

class GetAddressById extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;
    /**
     * @var Session
     */
    protected $backendSession;
    public function __construct(
    \Magento\Framework\App\Action\Context $context,Session $backendSession, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->backendSession = $backendSession;

       
        return parent::__construct($context);
    }

    public function execute() {
		
        $location_id = $this->getRequest()->getPost('address_id', false);
		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		//$addressData  = $this->_objectManager->create('Magento\Customer\Api\AddressRepositoryInterface')->getById($location_id);
		$addressData = $this->_objectManager->create('\Magento\Customer\Model\Address')->load($location_id);
		
        $response=array();
        if(!empty($addressData->getData('entity_id'))){
			$response["success"] = true;
			$response["address"] = $addressData->getData();
			$response["message"] = "Address data!";
		}else{
			$response["success"] = false;
			$response["address"] = "";
			$response["message"] = "No Address data!";
		}
        
        //$response['store_id'] = $locationgroup->getData('store_id');
        
        $result->setData($response);
        return $result;
    }

}
