<?php

namespace I95Dev\Locationgroup\Controller\Index;
use Magento\Backend\Model\Session;

class GetLocationDay extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;
    /**
     * @var Session
     */
    protected $backendSession;
    public function __construct(
    \Magento\Framework\App\Action\Context $context,Session $backendSession, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->backendSession = $backendSession;

       
        return parent::__construct($context);
    }

    public function execute() {
		//echo "dddddd"; exit;
        $location_id = $this->getRequest()->getPost('location_id');
        $result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		$locationgroup = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->load($location_id);
		$response=array();
		$days = $locationgroup->getData('days');
		$days = explode(',',$days);
		//print_r($days);
		foreach($days as $key => $value ){
			if($value == 7){
				$days[$key] = 0;
			}else{
				$days[$key] = (int)$value;
			}
			
		}
		
        $response['location'] = $locationgroup->getData();
        $response['location']['days'] = $days;
        
        $result->setData($response);
        return $result;
    }

}
