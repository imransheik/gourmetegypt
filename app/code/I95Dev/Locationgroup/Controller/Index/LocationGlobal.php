<?php

namespace I95Dev\Locationgroup\Controller\Index;
use Magento\Backend\Model\Session;

class LocationGlobal extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;
    /**
     * @var Session
     */
    protected $backendSession;
    public function __construct(
    \Magento\Framework\App\Action\Context $context,Session $backendSession, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->backendSession = $backendSession;

       
        return parent::__construct($context);
    }

    public function execute() {

        $location_id = $this->getRequest()->getPost('location_id', false);
       
		$locationgroup = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->load($location_id);
		$cartCount = $this->_objectManager->create('Magento\Checkout\Helper\Cart')->getItemsCount();
		
        setcookie("Gourmet_Location_Id", $location_id, time() + (10 * 365 * 24 * 60 * 60), "/", "", 0);

		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $response=array();
        $response["success"] = true;
        $response["group_name"] = $locationgroup->getData('group_name');
        $response["store_id"] = $locationgroup->getData('store_id');
        //$response['store_id'] = $locationgroup->getData('store_id');
        $response["cart_count"] = $cartCount;
        $response["message"] = "Coordinates data!";
        $result->setData($response);
        return $result;
    }

}
