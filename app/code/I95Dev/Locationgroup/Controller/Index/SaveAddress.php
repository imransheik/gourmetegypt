<?php

namespace I95Dev\Locationgroup\Controller\Index;
use Magento\Backend\Model\Session;

class SaveAddress extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_loyaltyHelper;
    /**
     * @var Session
     */
    protected $backendSession;
    public function __construct(
    \Magento\Framework\App\Action\Context $context,Session $backendSession, \Magento\Framework\View\Result\PageFactory $pageFactory) {
        $this->_pageFactory = $pageFactory;
        $this->backendSession = $backendSession;

       
        return parent::__construct($context);
    }

    public function execute() {
		
        $location_id = $this->getRequest()->getPost('address_id', false);
        $customer_id = $this->getRequest()->getPost('customer_id', false);
        $firstname = $this->getRequest()->getPost('firstname', false);
        $lastname = $this->getRequest()->getPost('lastname', false);
        $telephone = $this->getRequest()->getPost('telephone', false);
        $street = $this->getRequest()->getPost('street', false);
        $floor = $this->getRequest()->getPost('floor', false);
        $city = $this->getRequest()->getPost('city', false);
        $store_address_id = $this->getRequest()->getPost('store_address_id', false);
        $region = $this->getRequest()->getPost('region', false);
        $location_group_id = $this->getRequest()->getPost('location_group_id', false);
        $longitude = $this->getRequest()->getPost('longitude', false);
        $latitude = $this->getRequest()->getPost('latitude', false);
        $country = $this->getRequest()->getPost('country', false);
        $building = $this->getRequest()->getPost('building', false);
        $apartment = $this->getRequest()->getPost('apartment', false);
        $addresstype = $this->getRequest()->getPost('addresstype', false);
        $addressname = $this->getRequest()->getPost('addressname', false);
		$result = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		// exit;
		if(!empty($location_id)){
			
			//$addressData  = $this->_objectManager->create('Magento\Customer\Api\AddressRepositoryInterface')->getById($location_id);
			$addressData = $this->_objectManager->create('\Magento\Customer\Model\Address')->load($location_id);
			$addressData->setFirstname($firstname);
			$addressData->setLastname($lastname);
			$addressData->setTelephone($telephone);
			$addressData->setStreet($street);
			$addressData->setFloor($floor);
			$addressData->setCity($city);
			$addressData->setRegionId(0);
			$addressData->setRegion($region);
			$addressData->setStoreAddressId($store_address_id);
			$addressData->setLongitude($longitude);
			$addressData->setLatitude($latitude);
			$addressData->setCountryId($country);
			$addressData->setApartment($apartment);
			$addressData->setBuilding($building);
			$addressData->setAddresstype($addresstype);
			$addressData->setAddressname($addressname);
			$addressData->setLocationGroupId($location_group_id);
			$addressData->setIsDefaultShipping('1');
			$addressData->setIsDefaultBilling('1');

			$addressData->save();
		}else{
			$addressData = $this->_objectManager->create('\Magento\Customer\Model\Address');
			$addressData->setFirstname($firstname);
			$addressData->setLastname($lastname);
			$addressData->setTelephone($telephone);
			$addressData->setStreet($street);
			$addressData->setFloor($floor);
			$addressData->setCity($city);
			$addressData->setRegion($region);
			$addressData->setStoreAddressId($store_address_id);
			$addressData->setLongitude($longitude);
			$addressData->setLatitude($latitude);
			$addressData->setCountryId($country);
			$addressData->setAddresstype($addresstype);
			$addressData->setApartment($apartment);
			$addressData->setCustomerId($customer_id);
			$addressData->setBuilding($building);
			$addressData->setAddressname($addressname);
			//$addressData->setRegionId(0);
			$addressData->setLocationGroupId($location_group_id);
			$addressData->setIsDefaultShipping('1');
			$addressData->setIsDefaultBilling('1');
			$addressData->save();
		}
		
        $response=array();
        if($addressData){
			$response["success"] = true;
			$response["address"] = $addressData->getData();
			$response["message"] = "Address Saved!";
		}else{
			$response["success"] = false;
			$response["address"] = "";
			$response["message"] = "No Address data!";
		}
        
        //$response['store_id'] = $locationgroup->getData('store_id');
        
        $result->setData($response);
        return $result;
    }

}
