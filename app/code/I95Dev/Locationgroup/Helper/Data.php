<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\Locationgroup\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
protected $_objectManager;
    protected $customer;
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Filter\FilterManager $filter
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Customer\Model\Session $customer,
        \Magento\Framework\App\RequestInterface $requestInterface

    ) {
        $this->_objectManager = $objectmanager;
        $this->backendSession = $backendSession;
        $this->customer = $customer;
        $this->requestInterface = $requestInterface;

        parent::__construct($context);
    }
    public function getSessionData() {
        $request = $this->requestInterface;
        $sessionItem = $this->backendSession;
        return $sessionItem;
    }
    public function getCity() {
        $city =  $_COOKIE["Gourmet_Knockout_Location"];
        $citydatas = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection();
        $citydatas->addFieldToFilter('address', array('finset' => $city));
        $locationId = '';
        foreach ($citydatas as $citydata){
            if(isset($locationId)) {
                $locationId = $citydata->getData('location_id');
            }
        }
        if($locationId == "") {
            $locationId = '0';
        }
        return $locationId;
    }
    public function getStoreId() {
        $storeId = '';
        $storeDatas = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->getCollection();
        $storeDatas->addFieldToFilter('store_id',['eq'=> $this->getCity()]);
        foreach ($storeDatas as $storeData) {
            if (isset($storeId)) {
                $storeId = $storeData->getData('store_id');
            }
        }
        if($storeId == "") {
            $storeId = '0';
        }
        return $storeId;
    }
  /*  public function getCustomerData(){
        $customer = $this->customer;
       // var_dump($customer->getData());exit;
        //$customerName =  $customer->getName();
        $customerId = $customer->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $customerAddress = array();

        foreach ($customerObj->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }
        foreach ($customerAddress as $customerAddres) {

            // echo $customerAddres['street'];
            $city = $customerAddres['city'];
            //var_dump($city);
        }//exit;
       // echo $city;
        return $city;
    }*/
   /* public function getCityNew() {
        $storeId = 7;
        $storeDatas = $this->_objectManager->create('I95Dev\Locationgroup\Model\Data')->getCollection();
        $storeDatas->addFieldToFilter('store_id', array('finset' => $storeId));
        $dataId ='';
        foreach ($storeDatas as $storeData) {
                $dataId = $storeData->getData('data_id');
        }
        $citydatas = $this->_objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection();
        $citydatas->addFieldToFilter('location_id',['eq'=> $dataId]);
        $loc =[];
        foreach ($citydatas as $citydata){
            $locations = $citydata->getData('address');
            $locationsArray = explode (",", $locations);
            $loc[] = $locationsArray[2];
        }
        return $loc;

    }*/

}
