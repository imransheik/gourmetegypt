<?php

/*
 * I95Dev_Locationgroup
 */

namespace I95Dev\Locationgroup\Model;

class Address extends \Magento\Framework\Model\AbstractModel {
  
    protected function _construct()
    {
        $this->_init('I95Dev\Locationgroup\Model\ResourceModel\Address');
    }
}
