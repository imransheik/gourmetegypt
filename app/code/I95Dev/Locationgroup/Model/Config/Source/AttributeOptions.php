<?php
/**
 * i95Dev_Locationgroup
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2016 I95Dev, Inc.
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace I95Dev\Locationgroup\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class AttributeOptions implements ArrayInterface
{
    /**
     * @var array|null
     */
    protected $_options;
    protected $_attributeFactory;
    /**
     * Object initialization
     *
     * @return void
     */
    public function __construct(

    \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory)
    {
        $this->_attributeFactory = $attributeFactory;

    }
    /**
     * @return array
     */
    /*public function toOptionArray()
    {
        $collection = $this->_collectionFactory->create();
        $options = [['label' => 'Choose Tax Classification', 'value' => '']];
        foreach ($collection as $taxClassification) {
            $options[] = [
                'label' => __('%1 ',$taxClassification->getIncentiveLabel()),
                'value' => $taxClassification->getIncentiveName()
            ];
        }
        return $options;
    }*/
    public function toOptionArray()
    {
        return [
			['value' => "", 'label' => __('Select Store Id') ], 
			['value' => "7", 'label' => __('Zamalek Inventory_7') ],
			['value' => "9", 'label' => __('store_9') ], 
			['value' => "11", 'label' => __('Hacienda Inventory_11') ], 
			['value' => "12", 'label' => __('City Stars Inventory_12') ], 
			['value' => "13", 'label' => __('Bouri Inventory_13') ],
			['value' => "18", 'label' => __('Maadi Degla Inventory_18') ], 
			['value' => "19", 'label' => __('Dokki Inventory_19') ],
			['value' => "20", 'label' => __('Deplo Inventory_20') ], 
			['value' => "24", 'label' => __('Designia_24') ], 
			['value' => "27", 'label' => __('Stella Inventory_27') ], 
			['value' => "28", 'label' => __('Waterway Inventory_28') ],
			['value' => "32", 'label' => __('Kattemyza heights_32') ],
			['value' => "36", 'label' => __('Guizera Plaza Inventory_36') ],
			['value' => "38", 'label' => __('Safwa hub_38') ],
			['value' => "39", 'label' => __('Maadi hub_39') ],
			['value' => "40", 'label' => __('Tagamao hub_40') ],
			['value' => "41", 'label' => __('Arkan hub_41') ],
			['value' => "42", 'label' => __('Gouna_42') ],
			['value' => "44", 'label' => __('Almaza_44') ],
			['value' => "46", 'label' => __('Uptown_46') ],
            ['value' => "47", 'label' => __('Dunes_47') ]
		];
    }

}

