<?php

namespace I95Dev\Locationgroup\Model\Data;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
//use Magento\Ui\DataProvider\Modifier\PoolInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var PoolInterface
     */
    private $pool;

    /**
     * @var \I95Dev\Locationgroup\Model\ResourceModel\Data\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \I95Dev\Locationgroup\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory
     * @param PoolInterface $pool
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
            $name,
            $primaryFieldName,
            $requestFieldName, 
            \I95Dev\Locationgroup\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory, 
           // PoolInterface $pool, 
            DataPersistorInterface $dataPersistor, 
            \Psr\Log\LoggerInterface $logger, 
            array $meta = array(), 
            array $data = array()
    ) {
        $this->collection = $dataCollectionFactory->create();
        //$this->pool = $pool;
        $this->dataPersistor = $dataPersistor;
        $this->_logger = $logger;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * {@inheritdoc}
     * @since 101.0.0
     */
    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \I95Dev\Locationgroup\Api\Data\DataInterface $locationgroup */
        foreach ($items as $locationgroup) {
            $this->loadedData[$locationgroup->getId()] = $locationgroup->getData();
        }

//        $data = $this->dataPersistor->get('customModal');
//        if (!empty($data)) {
//            $locationgroup = $this->collection->getNewEmptyItem();
//            $locationgroup->setData($data);
//            $this->loadedData[$locationgroup->getId()] = $locationgroup->getData();
//            foreach ($this->pool->getModifiersInstances() as $modifier) {
//                $this->loadedData = $modifier->modifyData($this->loadedData);
//            }
//            $this->dataPersistor->clear('customModal');
//        }

//        $this->loadedData;
//        var_dump($this->loadedData[$locationgroup->getId()]);exit;

        /** @var ModifierInterface $modifier */
//        if (isset($this->loadedData)) {
//            foreach ($this->pool->getModifiersInstances() as $modifier) {
//                $this->loadedData = $modifier->modifyData();
//            }
//        }

//        \Zend_Debug::dump($this->loadedData);die();

        return $this->loadedData;
    }


    /**
     * {@inheritdoc}
     */
//    public function getMeta() {
//        $meta = parent::getMeta();
//
//        /** @var ModifierInterface $modifier */
//        foreach ($this->pool->getModifiersInstances() as $modifier) {
//            $meta = $modifier->modifyMeta($meta);
//        }
//
//        return $meta;
//    }

}
