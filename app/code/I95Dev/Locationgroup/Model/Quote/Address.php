<?php


namespace I95Dev\Locationgroup\Model\Quote;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Model\Address\Mapper;
use Magento\Directory\Helper\Data;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\Copy;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory;
use Magento\Quote\Model\Quote\Address\RateFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Quote\Model\Quote\Address\Total\CollectorFactory;
use Magento\Quote\Model\Quote\Address\TotalFactory;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Quote\Model\Quote\TotalsReader;
use Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory;
use Magento\Shipping\Model\CarrierFactoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Address
 * @package I95Dev\CustomAddress\Model
 */
class Address extends \Magento\Quote\Model\Quote\Address
{
    private $storeManager;
    public function __construct(Context $context, Registry $registry, ExtensionAttributesFactory $extensionFactory, AttributeValueFactory $customAttributeFactory, Data $directoryData, \Magento\Eav\Model\Config $eavConfig, \Magento\Customer\Model\Address\Config $addressConfig, RegionFactory $regionFactory, CountryFactory $countryFactory, AddressMetadataInterface $metadataService, AddressInterfaceFactory $addressDataFactory, RegionInterfaceFactory $regionDataFactory, DataObjectHelper $dataObjectHelper, ScopeConfigInterface $scopeConfig, \Magento\Quote\Model\Quote\Address\ItemFactory $addressItemFactory, \Magento\Quote\Model\ResourceModel\Quote\Address\Item\CollectionFactory $itemCollectionFactory, RateFactory $addressRateFactory, RateCollectorInterfaceFactory $rateCollector, CollectionFactory $rateCollectionFactory, RateRequestFactory $rateRequestFactory, CollectorFactory $totalCollectorFactory, TotalFactory $addressTotalFactory, Copy $objectCopyService, CarrierFactoryInterface $carrierFactory, \Magento\Quote\Model\Quote\Address\Validator $validator, Mapper $addressMapper, \Magento\Quote\Model\Quote\Address\CustomAttributeListInterface $attributeList, TotalsCollector $totalsCollector, TotalsReader $totalsReader, AbstractResource $resource = null, AbstractDb $resourceCollection = null, array $data = [], Json $serializer = null, StoreManagerInterface $storeManager = null)
    {
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        \Magento\Quote\Model\Quote\Address::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $directoryData, $eavConfig, $addressConfig, $regionFactory, $countryFactory, $metadataService, $addressDataFactory, $regionDataFactory, $dataObjectHelper, $scopeConfig, $addressItemFactory, $itemCollectionFactory, $addressRateFactory, $rateCollector, $rateCollectionFactory, $rateRequestFactory, $totalCollectorFactory, $addressTotalFactory, $objectCopyService, $carrierFactory, $validator, $addressMapper, $attributeList, $totalsCollector, $totalsReader, $resource, $resourceCollection, $data, $serializer, $storeManager);
    }

    /**
     * @param AbstractItem|null $item
     * @return bool
     */
    public function requestShippingRates(AbstractItem $item = null)
    {
        
        /** @var $request RateRequest */
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/geolog.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);

$logger->info('i m here model');
        $request = $this->_rateRequestFactory->create();
        $request->setAllItems($item ? [$item] : $this->getAllItems());
		
		$logger->info('-------');
		$logger->info($this->getCity());
		$logger->info($this->getStoreAddressId());
		$logger->info($this->getLocationGroupId());
		$logger->info($this->getQuote()->getEntityId());
		$logger->info('-------');
        $request->setDestCountryId($this->getCountryId());
        $request->setDestRegionId($this->getRegionId());
        $request->setDestRegionCode($this->getRegionCode());
        $request->setDestStreet($this->getStreetFull());
        $request->setDestCity($this->getCity());
		
		$request->setDestStoreAddressId($this->getStoreAddressId());
		$request->setDestLocationGroupId($this->getLocationGroupId());
		
        $request->setDestPostcode($this->getPostcode());
		
        $request->setDestPostcode($this->getPostcode());
        $request->setPackageValue($item ? $item->getBaseRowTotal() : $this->getBaseSubtotal());
        $packageWithDiscount = $item ? $item->getBaseRowTotal() -
            $item->getBaseDiscountAmount() : $this->getBaseSubtotalWithDiscount();
        $request->setPackageValueWithDiscount($packageWithDiscount);
        $request->setPackageWeight($item ? $item->getRowWeight() : $this->getWeight());
        $request->setPackageQty($item ? $item->getQty() : $this->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $item ? $item->getBaseRowTotal() : $this->getBaseSubtotal() -
            $this->getBaseVirtualAmount();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        $request->setFreeMethodWeight($item ? 0 : $this->getFreeMethodWeight());

        /**
         * Store and website identifiers specified from StoreManager
         */
        if ($this->getQuote()->getStoreId()) {
            $storeId = $this->getQuote()->getStoreId();
            $request->setStoreId($storeId);
            $request->setWebsiteId($this->storeManager->getStore($storeId)->getWebsiteId());
        } else {
            $request->setStoreId($this->storeManager->getStore()->getId());
            $request->setWebsiteId($this->storeManager->getWebsite()->getId());
        }
        $request->setFreeShipping($this->getFreeShipping());
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($this->storeManager->getStore()->getBaseCurrency());
        $request->setPackageCurrency($this->storeManager->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($this->getLimitCarrier());
        $baseSubtotalInclTax = $this->getBaseSubtotalTotalInclTax();
        $request->setBaseSubtotalInclTax($baseSubtotalInclTax);

        $result = $this->_rateCollector->create()->collectRates($request)->getResult();

        $found = false;
        if ($result) {
            $shippingRates = $result->getAllRates();

            foreach ($shippingRates as $shippingRate) {
                $rate = $this->_addressRateFactory->create()->importShippingRate($shippingRate);
                if (!$item) {
                    $this->addShippingRate($rate);
                }

                if ($this->getShippingMethod() == $rate->getCode()) {
                    if ($item) {
                        $item->setBaseShippingAmount($rate->getPrice());
                    } else {

                        /** @var StoreInterface */
                        $store = $this->storeManager->getStore();
                        $amountPrice = $store->getBaseCurrency()
                            ->convert($rate->getPrice(), $store->getCurrentCurrencyCode());
                        $this->setBaseShippingAmount($rate->getPrice());
                        $this->setShippingAmount($amountPrice);
                    }

                    $found = true;
                }
            }
        }

        return $found;
    }
}
