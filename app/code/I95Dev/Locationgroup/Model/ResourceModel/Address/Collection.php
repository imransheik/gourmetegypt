<?php
/*
 * I95Dev_Locationgroup
 */
namespace  I95Dev\Locationgroup\Model\ResourceModel\Address;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     * @codingStandardsIgnoreStart
     */
    protected $_idFieldName = 'entity_id';
    
    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        
        $this->_init('I95Dev\Locationgroup\Model\Address', 'I95Dev\Locationgroup\Model\ResourceModel\Address');
    }
}
