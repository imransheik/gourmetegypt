<?php
/*
 * I95Dev_Locationgroup
 */
namespace I95Dev\Locationgroup\Model\ResourceModel\Data;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     * @codingStandardsIgnoreStart
     */
    protected $_idFieldName = 'data_id';

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init('I95Dev\Locationgroup\Model\Data', 'I95Dev\Locationgroup\Model\ResourceModel\Data');
    }
}
