<?php

/*
 * I95Dev_Locationgroup
 */

namespace I95Dev\Locationgroup\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('i95dev_location_group');
        $tableNameLocation = $installer->getTable('i95dev_location_address');
        if (!$installer->tableExists('i95dev_location_group')) {
            $table = $installer->getConnection()
                            ->newTable($tableName)
                            ->addColumn(
                                    'data_id', Table::TYPE_INTEGER, null, [
                                'identity' => true,
                                'unsigned' => true,
                                'nullable' => false,
                                'primary' => true
                                    ], 'Data ID'
                            )->addColumn(
                            'group_name', Table::TYPE_TEXT, 100, ['nullable' => false], 'Location Mapping Rule Name'
                    )->addColumn(
                            'store_id', Table::TYPE_TEXT, 100, ['nullable' => false], 'Store ID'
                    )->addColumn(
                            'status', Table::TYPE_TEXT, '32', ['nullable' => false], 'group status'
                    )->addColumn(
                            'created_at', Table::TYPE_DATETIME, 255, ['nullable' => false], 'Created at'
                    )->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, 255, ['nullable' => false], 'Updated at'
            );
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('i95dev_location_address')) {
            $tableLocation = $installer->getConnection()
                            ->newTable($tableNameLocation)
                            ->addColumn(
                                    'entity_id', Table::TYPE_INTEGER, null, [
                                'identity' => true,
                                'unsigned' => true,
                                'nullable' => false,
                                'primary' => true
                                    ], 'ID'
                            )
                            ->addColumn(
                                    'location_id', Table::TYPE_TEXT, 255, ['nullable' => false], 'Address group Id'
                            )->addColumn(
                                    'address', Table::TYPE_TEXT, 255, ['nullable' => false], 'Address Location'
                            )
                            ->addColumn(
                                    'coordination', Table::TYPE_TEXT, 255, ['nullable' => false], 'Co-ordinates'
                            )->addColumn(
                    'boundry', Table::TYPE_TEXT, 255, ['nullable' => false], 'Boundry'
            );
            $installer->getConnection()->createTable($tableLocation);
        }
        $installer->endSetup();
    }

}
