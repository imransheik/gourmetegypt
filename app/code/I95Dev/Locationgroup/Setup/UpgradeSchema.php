<?php
namespace I95Dev\Locationgroup\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $setup->getConnection()->addIndex(
                $setup->getTable('i95dev_location_group'),
                'group_name_index',
                'group_name',
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            );
        }
        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $connection = $installer->getConnection();
            $connection->addColumn(
                $installer->getTable('quote_address'),
                'location_group_id',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'Location Group Id',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('sales_order_address'),
                'location_group_id',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'Location Group Id',
                        'after' => 'fax'
                    ]
            );
                $connection->addColumn(
                    $installer->getTable('quote_address'),
                    'store_address_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'Store Address Id',
                        'after' => 'fax'
                    ]
                );
            $connection->addColumn(
                $installer->getTable('sales_order_address'),
                'store_address_id',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'Store Address Id',
                        'after' => 'fax'
                    ]
            );
        }
        
        if (version_compare($context->getVersion(), '1.4.0', '<')) {
            $connection = $installer->getConnection();
            $connection->addColumn(
                $installer->getTable('i95dev_location_group'),
                'days',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => '',
                        'length' => 255,
                        'comment' => 'Days',
                        'after' => 'status'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('i95dev_location_group'),
                'time_from',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'default' => 0,
                        'length' => 10,
                        'comment' => 'Time_from',
                        'unsigned' => true,
                        'after' => 'days'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('i95dev_location_group'),
                'time_to',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'default' => 0,
                        'length' => 10,
                        'comment' => 'time_to',
                        'unsigned' => true,
                        'after' => 'time_from'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('i95dev_location_group'),
                'cutoff_time',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => 0,
                        'length' => 255,
                        'comment' => 'time_to',
                        'after' => 'time_from'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $connection = $installer->getConnection();
            $connection->addColumn(
                $installer->getTable('quote_address'),
                'longitude',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'longitude',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('sales_order_address'),
                'longitude',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'longitude',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('quote_address'),
                'latitude',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'latitude',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('sales_order_address'),
                'latitude',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'latitude',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('quote_address'),
                'addresstype',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'addresstype',
                        'after' => 'fax'
                    ]
            );
            $connection->addColumn(
                $installer->getTable('sales_order_address'),
                'addresstype',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'length' => 255,
                        'comment' => 'addresstype',
                        'after' => 'fax'
                    ]
            );
        }
		if(version_compare($context->getVersion(), '1.6.0', '<')) {
			$connection = $installer->getConnection();
			$connection->addColumn(
				$installer->getTable('sales_order'),
				'order_source',
				[
					'type' => \Magento\Framework\DB\Ddl\Table ::TYPE_TEXT,
					'nullable' => true,
					'default' => NULL,
					'length' => 255,
					'comment' => 'order source',
					'grid' => true,
					'after' => 'ecoupon_percentage'
				]
			);
			
		}
        $installer->endSetup();
    }
}
