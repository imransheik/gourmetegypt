<?php
namespace I95Dev\Locationgroup\Ui\DataProvider;

/**
 * Class LogsProvider
 */
class LogsProvider extends  \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider {

     protected $requestInterface;
    protected $backendSession;
   
    public function __construct(
            \Magento\Framework\App\RequestInterface $requestInterface,
            \Magento\Backend\Model\Session $backendSession,
            \Magento\Backend\Helper\Data $backendHelper,
            $name, 
            $primaryFieldName, 
            $requestFieldName, 
            \Magento\Framework\Api\Search\ReportingInterface $reporting, 
            \Magento\Framework\Api\Search\SearchCriteriaBuilder $searchCriteriaBuilder, 
            \Magento\Framework\App\RequestInterface $request, 
            \Magento\Framework\Api\FilterBuilder $filterBuilder, 
            array $meta = array(), 
            array $data = array()) {
        $this->requestInterface = $requestInterface;
        $this->backendSession = $backendSession;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }
    
    public function getData() {
        $request = $this->requestInterface;
        $session = $this->backendSession;
        $group_id = $session->getGroupId();

        $this->addFilter(
            $this->filterBuilder->setField('location_id')->setValue($group_id)->setConditionType('eq')->create()
        );
        $data = parent::getData();
        return $data;
    }

}
