<?php


namespace I95Dev\Loyalty\Api;

interface MyGourmetManagementInterface
{

    /**
     * POST for MyGourmet api
     * @param string $email

     * @return string
     */
    public function postMyGourmet($email);
}
