<?php


namespace I95Dev\Loyalty\Api;

interface MyRewardsManagementInterface
{

    /**
     * POST for MyRewards api
     * @param string $email
     * @param string $store
     * @return string
     */
    public function postMyRewards($email, $store = null);
}
