<?php


namespace I95Dev\Loyalty\Api;

interface OtpByIdManagementInterface
{

    /**
     * POST for Otp api
     * @param string $phone
     * @return string
     */
    public function postOtpById($phone);
}
