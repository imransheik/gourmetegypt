<?php


namespace I95Dev\Loyalty\Api;

interface OtpManagementInterface
{

    /**
     * POST for Otp api
     * @param string $customer
     * @return string
     */
    public function postOtp($customer);
}
