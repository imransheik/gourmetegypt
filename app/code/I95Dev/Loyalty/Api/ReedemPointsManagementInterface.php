<?php


namespace I95Dev\Loyalty\Api;

interface ReedemPointsManagementInterface
{

    /**
     * POST for ReedemPoints api
     * @param string $couponCode
     * @param string $couponType
     * @param string $quoteId
     * @param string $email
     * @param string $cartUid
     * @return string
     */
    public function postReedemPoints($couponCode, $couponType, $quoteId, $email, $cartUid);
}
