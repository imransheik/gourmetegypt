<?php


namespace I95Dev\Loyalty\Api;

interface TransactionsManagementInterface
{
    
    /**
     * POST for Otp api
     * @param string $email
     * @return string
     */
    public function postTransactions($email);
}
