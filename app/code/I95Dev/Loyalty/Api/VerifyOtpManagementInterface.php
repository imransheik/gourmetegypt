<?php


namespace I95Dev\Loyalty\Api;

interface VerifyOtpManagementInterface
{

    /**
     * GET for verifyOtp api
     * @param string $otp
     * @param string $pinId
     * @return string
     */
    public function verifyOtp($otp, $pinId);
}
