<?php

namespace I95Dev\Loyalty\Block\Adminhtml\Sales;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Totals extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\Currency $currency,
        array $data = []
    ) {
        parent::__construct($context, $data);
    
        $this->_currency = $currency;
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     * @psalm-suppress PossiblyInvalidMethodCall
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * @return mixed
     * @psalm-suppress PossiblyInvalidMethodCall
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->_currency->getCurrencySymbol();
    }

    /**
     *
     *
     * @return $this
     * @psalm-suppress UndefinedFunction
     */
    public function initTotals()
    {
        $this->getParentBlock();
        $this->getOrder();
        $this->getSource();

        if ($this->getSource()->getRedeemLoyaltyAmount()==0) {
            return $this;
        }
        $total = new \Magento\Framework\DataObject(
            [
                'code' => 'redeem_loyalty_amount',
                'value' => $this->getSource()->getRedeemLoyaltyAmount(),
                'label' =>  __('Redeem Points Value '),
            ]
        );
        $this->getParentBlock()->addTotalBefore($total, 'grand_total');

        return $this;
    }
}
