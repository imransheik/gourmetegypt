<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Acart
 */
/**
 * Copyright © 2019 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Loyalty\Block;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Customer extends \Magento\Framework\View\Element\Template
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customerSession;

    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customer;

    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $storeManager;

    /**
     * Customer constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {

        $this->customerSession = $customerSession;
        $this->customer = $customer;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     * @psalm-suppress MissingReturnType
     */
    public function isCustomerLoyaltyPoup()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer=$this->customerSession->getCustomer();
            if ($customer->getData('is_contactinfoupdated')) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @return string
     * @psalm-suppress MissingReturnType
     */
    public function getCustomerEmail()
    {
        if ($this->customerSession->isLoggedIn()) {
             $customer=$this->customerSession->getCustomer();
         
            return $customer->getEmail();
          
        } else {
            return '';
        }
    }

    /**
     * @psalm-suppress MissingReturnType
     */
    public function customerPrimaryMobile()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer=$this->customerSession->getCustomer();
         
            return $customer->getData('primary_mobile_number');
          
        } else {
            return '';
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function getCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomer();
            return $customer;
        } else {
            return '';
        }
    }

    /**
     * @psalm-suppress MissingReturnType
     */
    //phpcs:disable
    public function customerHasNewCario()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomer()->getId();
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $customerObj = $objectManager->create(\Magento\Customer\Model\Customer::class)->load($customerId);
            $customerObj = $this->customer->load($customerId);
            $customerAddress = [];
            $hascity = false;
            foreach ($customerObj->getAddresses() as $address) {
                $customerAddress[] = $address->toArray();
            }

            foreach ($customerAddress as $customerAddres) {

                if ($customerAddres['city'] == "New Cairo") {
                    $hascity = true;
                }
            }
            return $hascity;
        } else {
            return false;
        }
    }

    /**
     * @psalm-suppress MissingReturnType
     */
    public function getBaseUrl()
    {
       return $this->storeManager->getStore()->getBaseUrl();
    }
}
