<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Loyalty\Block\Order;

use \Magento\Framework\App\ObjectManager;

/**
 * Sales order history block
 *
 * @api
 * @since 100.0.2
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Transaction extends \Magento\Framework\View\Element\Template
{

    /**
     * @var string
     */
    protected $_template = 'gourmet.phtml';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $objectFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param array $data
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_orderConfig = $orderConfig;
        $this->objectFactory = $objectFactory;
        $this->collectionFactory=$collectionFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     * @psalm-suppress UndefinedFunction
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Orders'));
    }

    /**
     * @return mixed
     * @psalm-suppress MissingReturnType
     */
    public function getTransactions()
    {
         $responce = $this->_loyaltyHelper->BloyalPointsTransactions('abdelrahman.ismail@gourmetegypt.com');
         $collection = $this->collectionFactory->create();
          //get values of current page
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
    //get values of current limit
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 1;

          $obj = $this->objectFactory->create();
          $i=0;
          $a=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
        foreach ($a as $item) {
            $varienObject = new \Magento\Framework\DataObject();
            $varienObject->setData((array)$item);
            $collection->addItem($varienObject);
     
            $i++;
        }
          $collection->setPageSize(5);
        $collection->setCurPage(1);
        
        return $collection;
    }

    /**
     * @return $this
     * @psalm-suppress UndefinedMethod
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getTransactions()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'loyalty.gourmet.index.pager'
            )->setAvailableLimit([5=>5,10=>10,15=>15])->setShowPerPage(true)->setCollection(
                $this->getTransactions()
            );
            $this->setChild('pager', $pager);
            $this->getTransactions()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param object $order
     * @return string
     */
    public function getViewUrl($order)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $order->getId()]);
    }

    /**
     * @param object $order
     * @return string
     */
    public function getTrackUrl($order)
    {
        return $this->getUrl('sales/order/track', ['order_id' => $order->getId()]);
    }

    /**
     * @param object $order
     * @return string
     */
    public function getReorderUrl($order)
    {
        return $this->getUrl('sales/order/reorder', ['order_id' => $order->getId()]);
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
