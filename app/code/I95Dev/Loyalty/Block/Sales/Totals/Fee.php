<?php

namespace I95Dev\Loyalty\Block\Sales\Totals;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Fee extends \Magento\Framework\View\Element\Template
{
    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $_order;

    /**
     * @var \Magento\Framework\DataObject
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $_source;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    // @codingStandardsIgnoreStart
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
       
        parent::__construct($context, $data);
    }
// @codingStandardsIgnoreEnd
    /**
     * Check if we nedd display full tax total info
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     * @psalm-suppress MissingReturnType
     */
    public function getSource()
    {
        return $this->_source;
    }

    /**
     * @return mixed
     * @psalm-suppress MissingReturnType
     */
    public function getStore()
    {
        return $this->_order->getStore();
    }

    /**
     * @return Order
     * @psalm-suppress UndefinedClass
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @return array
     * @psalm-suppress PossiblyInvalidMethodCall
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     * @psalm-suppress PossiblyInvalidMethodCall
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     * @return $this
     * @psalm-suppress PossiblyInvalidMethodCall
     * @psalm-suppress UndefinedFunction
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
       // $store = $this->getStore();
        if ($this->_source->getRedeemLoyaltyAmount()==0) {
             return $this;
        }
        $fee = new \Magento\Framework\DataObject(
            [
                'code' => 'redeem_loyalty_amount',
                'strong' => false,
                'value' => $this->_source->getRedeemLoyaltyAmount(),
                'label' =>  __('Redeem Points Value '),
            ]
        );

        $parent->addTotal($fee, 'redeem_loyalty_amount');

        return $this;
    }
}
