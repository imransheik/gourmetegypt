<?php

namespace I95Dev\Loyalty\Controller\Customer;

use Webengage\Event\Helper\Data;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Customer extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;

    /**
     * Customer constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param Data $helper
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Data $helper,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->helper = $helper;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }
// @codingStandardsIgnoreStart
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedClass
     */
    public function execute()
    {
        $isd = $this->getRequest()->getPost('isd', false);
        $mobile = $this->getRequest()->getPost('mobile', false);
        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $customerCollection = $this->_objectManager
            ->get(\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory::class)->create()
            ->addAttributeToFilter('primary_mobile_number', ['eq' => $mobile]);
        $response = [];
        $customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
        if ($customerSession->isLoggedIn()) {
            $allIds = $customerCollection->getAllIds();
            if (empty($customerCollection->getAllIds()) ||
                in_array($customerSession->getCustomer()->getId(), $allIds)) {

                $response = [];
                $pinId = $this->getRequest()->getPost('pinId', false);
                $pin = $this->getRequest()->getPost('otp', false);
                if ($pinId && $pin) {
                    $responcePin = $this->_objectManager->get('\I95Dev\Loyalty\Helper\Data')->Verifypin($pin, $pinId);
                    if ($responcePin->verified) {
                        try {
                            $customermodel = $customerSession->getCustomer();
                            $customerData = $customermodel->getDataModel();
                            if ($mobile) {
                                $customerData->setCustomAttribute("primary_mobile_number", $mobile);
                            }
                            if ($isd) {
                                $customerData->setCustomAttribute("isd_code", $isd);
                            }
                            $customermodel->updateData($customerData);
                            $customermodel->save();
                            $response['type'] = "success";
                            $response = ['message' => "You saved the account information."];
                            $this->messageManager->addSuccessMessage('You saved the account information.');
                        } catch (\Exception $e) {
                            $response = ['type' => 'error', 'message' => $e->getMessage()];
                        }
                    } else {
                        $response['message'] = "An error occurred while verifying the PIN. Please try again later.";
                        $response['type'] = 'error';
                    }
                } else {
                    $response['message'] = "Please enter the OTP!";
                    $response['type'] = 'error';
                }
            } else {
                $response['type'] = "error";
                $response['message'] = "Mobile number already exists! It's associated with another account.";
            }
        } else {
            $response['type'] = "error";
            $response['message'] = "Customer is not loggedin!";
        }
        $result->setData(json_encode($response));

        return $result;
    }
}
// @codingStandardsIgnoreEnd