<?php

namespace I95Dev\Loyalty\Controller\Customer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Sentotpdefault extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * Sentotpdefault constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress UndefinedClass
     *
     */
    public function execute()
    {
        $phone = $this->getRequest()->getPost('phone', false);
        $isd = $this->getRequest()->getPost('isd', false);

        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $_coreSession = $this->_objectManager->create(\Magento\Framework\Session\SessionManagerInterface::class);
        $smsStatus = '';
        $pinId = '';
        $customerCollection = $this->_objectManager
            ->get(\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory::class)
            ->create()->addAttributeToFilter('primary_mobile_number', ['eq' => $phone]);
        $customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
        if (!empty($customerCollection->getAllIds()) &&
            (!in_array($customerSession->getCustomer()->getId(), $customerCollection->getAllIds()))) {
            $response['success'] = false;
            $response['message'] = "Mobile number already exists! It's associated with another account.";
            $result->setData(json_encode($response));
            return $result;
        }
        if ($phone) {

            $responce = $this->_loyaltyHelper->SendPinBySMS($isd . '' . $phone);
            $smsStatus = $responce->smsStatus;
            $pinId = $responce->pinId;
        }

        if ($smsStatus == "MESSAGE_SENT") {
            $_coreSession->start();
            $_coreSession->setPrimaryMobile($phone);
            $_coreSession->setIsd($isd);
            $response['success'] = true;
            $response['message'] = "We have sent the OTP";
            $response['pinId'] = $pinId;
        } else {
            $response['success'] = false;
            $response['message'] = "An error occurred while processing your request. Please try again later.";
        }

        $result->setData(json_encode($response));
        return $result;
    }
}
