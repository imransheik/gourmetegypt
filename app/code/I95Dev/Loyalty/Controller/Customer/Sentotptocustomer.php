<?php

namespace I95Dev\Loyalty\Controller\Customer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Sentotptocustomer extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */

    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */

    protected $_loyaltyHelper;

    /**
     * Sentotptocustomer constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress PossiblyUndefinedVariable
     */
    public function execute()
    {
        $result = $this->_objectManager
            ->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $isd = $this->_loyaltyHelper->getCustomer()->getData('isd_code');
        $phone = $this->_loyaltyHelper->getCustomer()->getData('primary_mobile_number');
        $responce = $this->_loyaltyHelper->SendPinBySMS($isd . '' . $phone);
        $smsStatus = $responce->smsStatus;
        $pinId = $responce->pinId;
        if ($smsStatus == "MESSAGE_SENT") {
            $response['success'] = true;
            $response['message'] = "We have sent the OTP";
            $response['pinId'] = $pinId;
             $this->messageManager->addSuccessMessage('We have sent the OTP to '.$isd . '' . $phone);
        } else {
            $response['success'] = false;
            $response['message'] = "An error occurred while processing your request. Please try again later.";
             $this->messageManager
                 ->addErrorMessage('An error occurred while processing your request. Please try again later.');
        }

        $result->setData(json_encode($response));
        return $result;
    }
}
