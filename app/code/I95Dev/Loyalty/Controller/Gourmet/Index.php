<?php

namespace I95Dev\Loyalty\Controller\Gourmet;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\RequestInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerSession;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_customerSession = $customerSession;
        return parent::__construct($context);
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $this->_redirect('customer/account/login');
        }
        return parent::dispatch($request);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('My Gourmet'));
      
        $this->_view->renderLayout();
    }
}
