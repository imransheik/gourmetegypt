<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Reset extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;

    /**
     * Reset constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyUndefinedVariable
     */
    //phpcs:disable
    public function execute()
    {
//        return true;
        $rmsconnectLogModel=$this->_objectManager->create(\I95Dev\RmsConnect\Model\Rmsconnect::class)
            ->setData("message", 'xyz');
        $rmsconnectLogModel->save();
        $reset = $this->getRequest()->getPost('reset', false);
        if ($reset) {
            $checkoutSession = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)
                ->setRedeemLoyaltyAmount("");
        }
        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $response['message'] = "Sent Otp";
        $result->setData(json_encode($response));
    }
}
