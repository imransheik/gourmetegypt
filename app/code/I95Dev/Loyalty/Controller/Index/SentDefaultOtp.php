<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class SentDefaultOtp extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * SentDefaultOtp constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
          $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress UndefinedMethod
     */
    public function execute()
    {
          $resultRedirect = $this->resultRedirectFactory->create();

//        return true;
        $mobile= $this->getRequest()->getPost('primary_mobile_number', false);
        $isd= $this->getRequest()->getPost('isd_code', false);
         
          $responce= $this->_loyaltyHelper->SendPinBySMS($isd.$mobile);
//        $responce->pinId;
         
        if ($responce->smsStatus=="MESSAGE_SENT") {
  
            return $resultRedirect->setPath('loyalty/index/validateDefaultOtp.php?pinId='.$responce->pinId);
 
        } else {
   
            return  $resultRedirect->setPath('loyalty/index/fail.php');
        }
   
//        return $result;
    }
}
