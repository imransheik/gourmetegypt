<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress  PropertyNotSetInConstructor
 */
class SentWebhukOtp extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * SentWebhukOtp constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @return false
     * @psalm-suppress UndefinedVariable
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute()
    {// @codingStandardsIgnoreStart
        header('Content-Type: application/json');
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->_objectManager->get(\Magento\Framework\App\RequestInterface::class);
        $body = json_decode($request->getContent());

        $urlInterface = $this->_objectManager->get(\Magento\Framework\UrlInterface::class);
        $loginDomain = $_GET['loginDomain'];
        $providerSecurityKey = $_GET['providerSecurityKey'];
        if ($providerSecurityKey != "i95wh2019") {
            $data = ['success' => false, 'errorCode' => null, 'message' => "Invalid provider Security Key!"];
            echo json_encode($data);
            return false;
        }
        if ($loginDomain != "GourmetEgypt") {
            $data = ['status' => "error", 'errorCode' => null, 'message' => "Invalid provider login Domain!"];
            echo json_encode($data);
            return false;
        }

        $mobile = '';
        if (isset($body->Cart->Customer->phone1)) {
            $mobile = $bodybody->Cart->Customer->phone1;
        } elseif (isset($body->Cart->Customer->MobilePhone)) {
            $mobile = $body->Cart->Customer->MobilePhone;
        } else {
            $data = ['status' => "error", 'errorCode' => null, 'message' => "Please send the MobilePhone param in your request"];
            echo json_encode($data);
            return false;
        }
        if ($mobile == "") {

            $data = ['status' => "error", 'errorCode' => null, 'message' => "MobilePhone is required field!"];
            echo json_encode($data);
            return false;
        }
        $responce = $this->_loyaltyHelper->SendPinBySMS($mobile);

        if (isset($responce->smsStatus) && $responce->smsStatus == "MESSAGE_SENT") {
            $resaraay = [
                
                "data"=>[
                "Alerts" => [[
                        "Category" => "Confirmation",
                        "Code" => "200",
                        "Uid" => $body->Cart->Uid,
                        "Message" => "We have sent you the OTP to your Mobile Number. Please enter the OTP.",
                        "SnippetUrl" => $urlInterface->getUrl('loyalty/index/validateDefaultOtp/pinId/' . $responce->pinId),
                        "AdditionalData" => [],
                    ]]],
                "status" => "success",
                "message" => "We have sent you the OTP to your Mobile Number. Please enter the OTP.",
                "code" => "200",
                "context-key" => "application/json"
            ];
            $data = json_encode($resaraay);
            echo $data;
            return false;
        } else {
              $resaraay = [
                "data"=>[
                "Alerts" => [[
                        "Category" => "Problem",
                        "Code" => "500",
                        "Uid" => $body->Cart->Uid,
                        "Message" => "An error occurred while processing your request. Please try again later.",
                        "SnippetUrl" => $urlInterface->getUrl('loyalty/index/index'),
                        "AdditionalData" => [],
                    ]]],
                "status" => "error",
                "message" => "An error occurred while processing your request. Please try again later.",
                "code" => "500",
                "context-key" => "application/json"
              ];
       
              $data = json_encode($resaraay);
              echo $data;
              return false;
        }
    }
}
// @codingStandardsIgnoreEnd