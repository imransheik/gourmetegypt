<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class SentWebhukSms extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * SentWebhukSms constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress InvalidFalsableReturnType
     */
    public function execute()
    {
        // @codingStandardsIgnoreStart
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->_objectManager->get(\Magento\Framework\App\RequestInterface::class);
        $body = json_decode($request->getContent());

        $urlInterface = $this->_objectManager->get(\Magento\Framework\UrlInterface::class);
        $loginDomain = $_GET['loginDomain'];
        $providerSecurityKey = $_GET['providerSecurityKey'];
        if ($providerSecurityKey != "i95wh2019") {
            $data = ['success' => false, 'errorCode' => null, 'message' => "Invalid provider Security Key!"];
            echo json_encode($data);
            return false;
        }
        if ($loginDomain != "GourmetEgypt") {
            $data = ['success' => false, 'errorCode' => null, 'message' => "Invalid provider login Domain!"];
            echo json_encode($data);
            return false;
        }
//        $isd= $this->getRequest()->getPost('isd_code', false);

        $mobile = '';
        if (isset($body->phone1)) {
            $mobile = $body->phone1;
        } elseif (isset($body->MobilePhone)) {
            $mobile = $body->MobilePhone;
        } else {
            $data = ['success' => false, 'errorCode' => null, 'message' => "Please send the MobilePhone param in your request"];
            echo json_encode($data);
            return false;
        }
        if ($mobile == "") {

            $data = ['success' => false, 'errorCode' => null, 'message' => "MobilePhone is required field!"];
            echo json_encode($data);
            return false;
        }
        $responce = $this->_loyaltyHelper->sentSMS($mobile, "Dear Customer, You redeemed " . $body->LoyaltyPointsRedeemed . " loyalty point successfully and current loyalty points balance is " . $body->LoyaltySummary->LoyaltyPoints);
//        $responce->pinId;
        $logger = $this->_objectManager->create('\Psr\Log\LoggerInterface');

        $logger->info(json_encode($responce));
        $logger->info("Dear Customer, You redeemed " . abs($body->LoyaltyPointsRedeemed) . " loyalty point successfully and current loyalty points balance is " . $body->LoyaltySummary->LoyaltyPoints);
        if (isset($responce->messages[0]->status->name) && $responce->messages[0]->status->name == "PENDING_ACCEPTED") {

            $resaraay = [
                "data" => [
                    "Alerts" => [
                        "Category" => "Confirmation",
                        "Code" => "",
                        "Uid" => '',
                        "Message" => "Dear Customer, You redeemed " . abs($body->LoyaltyPointsRedeemed) . " loyalty point successfully and current loyalty points balance is " . $body->LoyaltySummary->LoyaltyPoints,
                        "SnippetUrl" => "",
                        "AdditionalData" => [],
                    ],
                    "status" => "success",
                    "message" => "Dear Customer, You redeemed " . abs($body->LoyaltyPointsRedeemed) . " loyalty point successfully and current loyalty points balance is " . $body->LoyaltySummary->LoyaltyPoints,
                    "code" => 200,
                    "context-key" => "application/json"
                ]];
            $data = json_encode($resaraay);
            echo $data;
            return false;
        } else {

            $resaraay = [
                "data" => [
                    "Alerts" => [
                        "Category" => "Problem",
                        "Code" => "",
                        "Uid" => $body->Cart->Uid,
                        "Message" => "An error occurred while processing your request. Please try again later.",
                        "SnippetUrl" => '',
                        "AdditionalData" => [],
                    ],
                    "status" => "failure",
                    "message" => "An error occurred while processing your request. Please try again later.",
                    "code" => 500,
                    "context-key" => "application/json"
                ]];
            $data = json_encode($resaraay);
            echo $data;
            return false;
        }
    }
}
// @codingStandardsIgnoreEnd