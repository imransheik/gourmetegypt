<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Sentotp extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * Sentotp constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress InvalidOperand
     */
    public function execute()
    {
//        return true;
        $redemptions = $this->getRequest()->getPost('redemptions', false);
        $redemptionsType = $this->getRequest()->getPost('type', false);
        $i = 0;
        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $smsStatus = '';
        $pinId = '';
        $quote = $this->_loyaltyHelper->getQuote();
        $LoyaltyPointsUsed = $this->calculateCartBloyal($quote, '');
        $QuickSearch = $this->_loyaltyHelper->BloyalQuickSearch($quote->getCustomerEmail());
        $CurrentLoyaltyPoints = $QuickSearch->data[0]->CurrentLoyaltyPoints;
        if ($redemptionsType == "product") {
            $product = $this->_objectManager->create(\Magento\Catalog\Model\Product::class)->load($redemptions);
            if ((abs($LoyaltyPointsUsed) + $product->getPrice()) > $CurrentLoyaltyPoints) {
                $this->messageManager->addErrorMessage('You do not have enough points to redeem this Coupon.');
                $response['success'] = false;
                $response['message'] = "You do not have enough points to redeem this Coupon.";
                $result->setData(json_encode($response));
                return $result;
            }
            $checkoutSession = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)
                ->setRedeemLoyaltyCode($redemptions);
        } else {

            if ((abs($LoyaltyPointsUsed) + str_replace('EGP', '', $redemptions)) > $CurrentLoyaltyPoints) {
                $this->messageManager->addErrorMessage('You do not have enough points to redeem this Coupon.');
                $response['success'] = false;
                $response['message'] = "You do not have enough points to redeem this Coupon.";
                $result->setData(json_encode($response));
                return $result;
            }
            $redemptionsArray = json_decode($this->_objectManager->get(\Magento\Checkout\Model\Session::class)
                ->getCouponCodeArray());
            if (empty($redemptionsArray)) {
                $redemptionsArray = [];
            }
            array_push($redemptionsArray, $redemptions);
            $this->_objectManager->get(\Magento\Checkout\Model\Session::class)
                ->setCouponCodeArray(json_encode($redemptionsArray));
            $checkoutSession = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)
                ->setRedeemLoyaltyCode(implode(',', $redemptionsArray));
        }
        $checkoutSession = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)
            ->setRedeemtionType($redemptionsType);
        $customerSession = $this->_objectManager->create(\Magento\Customer\Model\Session::class);
        if ($customerSession->isLoggedIn()) {
            $mobileNo = $customerSession->getCustomer()->getIsdCode() .
                $customerSession->getCustomer()->getPrimaryMobileNumber();
            $responce = $this->_loyaltyHelper->SendPinBySMS($mobileNo);
            $smsStatus = $responce->smsStatus;
            $pinId = $responce->pinId;
        }

        if ($smsStatus == "MESSAGE_SENT") {
            $response['success'] = true;
            $response['message'] = "We have sent the OTP";
            $response['pinId'] = $pinId;
        } else {
            $response['success'] = false;
            $response['message'] = "An error occurred while processing your request. Please try again later.";
        }

        $result->setData(json_encode($response));
        return $result;
    }

    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function calculateCartBloyal($quote, $couponCode = '')
    {
        $items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
//    var_dump(get_class_methods($item)); exit;
            $items[] = [
                "ProductUid" => null,
                "ProductCode" => $item->getSku(),
                "ProductName" => $item->getName(),
                "CategoryCode" => null,
                "CategoryExternalId" => null,
                "DepartmentCode" => "Misc",
                "DepartmentExternalId" => null,
                "SalesRepCode" => null,
                "ShipmentUid" => null,
                "ShipmentNumber" => null,
                "Quantity" => $item->getQty(),
                "Price" => $item->getPrice() * $item->getQty(),
                "SalePrice" => 0.0,
                "SalePriceReasonCode" => null,
                "ExternallyAppliedDiscount" => false,
                "Discount" => 0.0,
                "DiscountReasonCode" => null,
                "NetPrice" => 0.0,
                "ExtendedNetPrice" => 0.0,
                "Cost" => 0.0,
                "OrderDiscount" => 0.0,
                "ExternallyAppliedTax" => false,
                "TaxDetails" => [
                ]
            ];
        }
        $BloyalCartUid = $quote->getData("bloyal_cartid");
        $BloyalCalculateCartResponse = $this->_loyaltyHelper
            ->BloyalCalculateCart(
                $items,
                $this->_loyaltyHelper->getCustomer()->getEmail(),
                $couponCode,
                $BloyalCartUid
            );
        $CartUid = $BloyalCalculateCartResponse->data->Cart->Uid;
        $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->setBloyalCartUid($CartUid);
        $LoyaltyPointsUsed = abs($BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed);
        if ($LoyaltyPointsUsed != 0 && $LoyaltyPointsUsed != "") {
            $quote->setData(
                "redeem_loyalty_amount",
                $BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed
            );
            $quote->setData("bloyal_cartid", $CartUid);
            $quote->collectTotals()->save();
        }
        return $LoyaltyPointsUsed;
    }
}
