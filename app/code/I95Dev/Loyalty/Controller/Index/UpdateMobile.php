<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class UpdateMobile extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;

    /**
     * UpdateMobile constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedClass
     */
    public function execute()
    {
        $isd = $this->getRequest()->getPost('isd', false);
        $mobile = $this->getRequest()->getPost('mobile', false);

        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $customerCollection = $this->_objectManager
            ->get(\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory::class)
            ->create()->addAttributeToFilter('primary_mobile_number', ['eq' => $mobile]);
        $response = [];

        if (empty($customerCollection->getAllIds())) {
            $customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
            if ($customerSession->isLoggedIn()) {
                try {
                    $customermodel = $customerSession->getCustomer();
                    $customerData = $customermodel->getDataModel();
                    $customerData->setCustomAttribute("primary_mobile_number", $mobile);
                    $customerData->setCustomAttribute("isd_code", $isd);

                    $customermodel->updateData($customerData);
                    $customermodel->save();
                    $response['type'] = "success";
                    $response = ['message' => "You saved the account information."];
                    $this->messageManager->addSuccessMessage('You saved the account information.');
                } catch (\Exception $e) {
                    $response = ['type' => 'error', 'message' => $e->getMessage()];
                }
            }
        } else {
            $response['type'] = "error";
            $response['message'] = "Mobile No already associated with some other account.";
        }
        $result->setData(json_encode($response));

        return $result;
    }
}
