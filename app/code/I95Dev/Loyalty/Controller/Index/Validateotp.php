<?php

namespace I95Dev\Loyalty\Controller\Index;

use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Validateotp extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $product;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $formKey;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $cart;

    /**
     * Validateotp constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @param FormKey $formKey
     * @param Cart $cart
     * @param Product $product
     * @psalm-suppress UndefinedClass
     */

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper,
        FormKey $formKey,
        Cart $cart,
        Product $product
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->product = $product;
        return parent::__construct($context);
    }

    /**
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress UndefinedMethod
     */
    public function execute()
    {
        $RedeemtionType = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getRedeemtionType();
        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $productId = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getRedeemLoyaltyCode();
        $quote = $this->_loyaltyHelper->getQuote();
        if ($RedeemtionType == "product") {
            $errorMessage = $this->addProduct();
            if ($errorMessage) {
                $response['message'] = $errorMessage;
                $response['success'] = false;
                $result->setData(json_encode($response));
                return $result;
            }

            $LoyaltyPointsUsed = $this->calculateCartBloyal($quote, '');
        } else {
            $setCoupon=$this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getRedeemLoyaltyCode();
            $LoyaltyPointsUsed = $this->calculateCartBloyal($quote, $setCoupon);
        }
        $QuickSearch = $this->_loyaltyHelper->BloyalQuickSearch($this->_loyaltyHelper->getCustomer()->getEmail());
        $CurrentLoyaltyPoints = $QuickSearch->data[0]->CurrentLoyaltyPoints;
        if ($CurrentLoyaltyPoints < $LoyaltyPointsUsed) {
            $response['message'] = "You do not have enough points to redeem this Coupon";
            $response['success'] = false;
            $result->setData(json_encode($response));
            return $result;
        }
        $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->setRedeemtionType("");
        $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->setRedeemLoyaltyCode("");
        $QuoteTotal = $quote->getSubtotal() + $quote->getShippingAddress()->getShippingAmount();
        if ($LoyaltyPointsUsed > $QuoteTotal) {
            $response['message'] = "Please add more items to the cart as the remaining amount is not refundable.";
        } else {
            $response['message'] = "";
        }

        $response = [];
        $pinId = $this->getRequest()->getPost('pinId', false);
        $pin = $this->getRequest()->getPost('otp', false);
        if ($pinId && $pin) {
            $responce = $this->_loyaltyHelper->Verifypin($pin, $pinId);
            if (isset($responce->verified)) {
                $response['message'] = "We have successfully verified your PIN!!!";
                $response['success'] = true;
                $response['amount'] = $LoyaltyPointsUsed;
                $response['points'] = $LoyaltyPointsUsed;
            } else {
				$QuoteTotal = $quote->getSubtotal() + $quote->getShippingAddress()->getShippingAmount();
				$quote->setData("bloyal_cartid", "")->setData("redeem_loyalty_amount", "");
                if ($RedeemtionType == "product") {
                    try {
                        $allItems = $quote->getAllVisibleItems();
                        foreach ($allItems as $item) {
                            $quoteProductId = $item->getProductId();
                            if($quoteProductId == $productId){
								$itemId = $item->getItemId();
								$item->delete();
								$quote->collectTotals()->save();
                            }
                        }

                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
				$quote->collectTotals()->save();
                $response['message'] = "An error occurred while verifying the PIN. Please try again later.";
                $response['success'] = false;
            }
        } else {
            $response['message'] = "Please enter the OTP!";
            $response['success'] = false;
        }
//
        $result = $this->_objectManager->create(\Magento\Framework\Controller\Result\JsonFactory::class)->create();
        $result->setData(json_encode($response));
        return $result;
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function addProduct()
    {
        $productId = $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getRedeemLoyaltyCode();
        try {
            $params = ['form_key' => $this->formKey->getFormKey(), 'product' => $productId, 'qty' => 1];
            $product = $this->product->load($productId);
            $this->cart->addProduct($product, $params);
            $this->cart->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function calculateCartBloyal($quote, $couponCode = '')
    {
        $items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $items[] = [
                "ProductUid" => null,
                "ProductCode" => $item->getSku(),
                "ProductName" => $item->getName(),
                "CategoryCode" => null,
                "CategoryExternalId" => null,
                "DepartmentCode" => "Misc",
                "DepartmentExternalId" => null,
                "SalesRepCode" => null,
                "ShipmentUid" => null,
                "ShipmentNumber" => null,
                "Quantity" => $item->getQty(),
                "Price" => $item->getPrice() * $item->getQty(),
                "SalePrice" => 0.0,
                "SalePriceReasonCode" => null,
                "ExternallyAppliedDiscount" => false,
                "Discount" => 0.0,
                "DiscountReasonCode" => null,
                "NetPrice" => 0.0,
                "ExtendedNetPrice" => 0.0,
                "Cost" => 0.0,
                "OrderDiscount" => 0.0,
                "ExternallyAppliedTax" => false,
                "TaxDetails" => [
                ]
            ];
        }
        $BloyalCartUid = $quote->getData("bloyal_cartid");
        $BloyalCalculateCartResponse = $this->_loyaltyHelper->BloyalCalculateCart(
            $items,
            $this->_loyaltyHelper->getCustomer()->getEmail(),
            $couponCode,
            $BloyalCartUid
        );
        $CartUid = $BloyalCalculateCartResponse->data->Cart->Uid;
        $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->setBloyalCartUid($CartUid);
        $LoyaltyPointsUsed = abs($BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed);
        if ($LoyaltyPointsUsed != 0 && $LoyaltyPointsUsed != "") {
            $quote->setData(
                "redeem_loyalty_amount",
                $BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed
            );
            $quote->setData("bloyal_cartid", $CartUid);
            $quote->collectTotals()->save();
        }
        return $LoyaltyPointsUsed;
    }
}
