<?php

namespace I95Dev\Loyalty\Controller\Index;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Verifypin extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * Verifypin constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress RedundantCondition
     */
    public function execute()
    {
//        return true;
        $resultRedirect = $this->resultRedirectFactory->create();
        $pinId = $this->getRequest()->getPost('pinId', false);
        $pin = $this->getRequest()->getPost('otp', false);
        if ($pinId && $pinId) {
            $responce = $this->_loyaltyHelper->Verifypin($pin, $pinId);
            if ($responce->verified) {
                
                return $resultRedirect->setPath('loyalty/index/success.php?id=webhook');
            } else {
               
                 return $resultRedirect->setPath('loyalty/index/fail.php?id=webhook');
            }
        } else {
            
            return $resultRedirect->setPath('loyalty/index/fail.php?id=webhook');
        }
    }
}
