<?php

namespace I95Dev\Loyalty\Controller\Savedcards;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\RequestInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Customer extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerSession;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $messageManager;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $request;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $resultRedirectFactory;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		\Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_pageFactory = $pageFactory;
		$this->request = $request;
		$this->messageManager = $messageManager;
		$this->resultRedirectFactory = $resultRedirectFactory;
        $this->_customerSession = $customerSession;
        return parent::__construct($context);
    }

    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $this->_redirect('customer/account/login');
        }
        return parent::dispatch($request);
    }

    /**
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        //ini_set('memory_limit', '2048M');
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer-order-1.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$now = new \DateTime();
		$logger->info('started');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
		$orderCollection = $OrderFactory->create()->addFieldToSelect(array('*'));
		$orderCollection->addFieldToFilter('created_at', ['lteq' => $now->format('Y-m-d H:i:s')])->addFieldToFilter('created_at', ['gteq' => $now->format('2021-11-26 H:i:s')]);
		$customerArray=[];

		foreach($orderCollection as $order){
			$customerArray[]= $order->getCustomerEmail();
			
		}
		//$logger->info($customerArray);
		foreach($customerArray as $customer){
			$orderColl = $OrderFactory->create()->addFieldToSelect(array('*'));
			$orderColl->addFieldToFilter('customer_email', array('eq' => $customer));
			if ($orderColl->getSize() == 1) {
				$logger->info('New customer and placed first order '.$customer);
				
			}
		}
		$logger->info('completed');
    }
}
