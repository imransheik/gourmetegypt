<?php

namespace I95Dev\Loyalty\Controller\Savedcards;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\RequestInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Remove extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_pageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerSession;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $messageManager;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $request;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $resultRedirectFactory;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		\Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_pageFactory = $pageFactory;
		$this->request = $request;
		$this->messageManager = $messageManager;
		$this->resultRedirectFactory = $resultRedirectFactory;
        $this->_customerSession = $customerSession;
        return parent::__construct($context);
    }

    public function dispatch(RequestInterface $request)
    {
        if (!$this->_customerSession->authenticate()) {
            return $this->_redirect('customer/account/login');
        }
        return parent::dispatch($request);
    }

    /**
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        $id = $this->request->getParam('cardid');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get(\Magento\Framework\App\ResourceConnection::class);
		$connection = $resource->getConnection();
		$sql = "SELECT * FROM Accept_Payments_Tokens WHERE id = $id";
		$deletedCardDetails = $connection->fetchRow($sql);
		
		$sql = "Delete FROM Accept_Payments_Tokens WHERE id = $id";
		$connection->query($sql);
		$message = __($deletedCardDetails['masked_pan'].' was sucessfully deleted from your account.');
			
		$this->messageManager->addSuccessMessage($message);
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath('loyalty/savedcards/index');
		return $resultRedirect;
    }
}
