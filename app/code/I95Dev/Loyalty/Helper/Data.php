<?php

namespace I95Dev\Loyalty\Helper;

/**
 * Returns base helper
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    // @codingStandardsIgnoreStart
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerRepo;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_logo;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $customerSession;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $cart;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_productCollectionFactory;

    /**
     * Data constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo
     * @param \Magento\Theme\Block\Html\Header\Logo $logo
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactor
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {

        $this->_customerRepo = $customerRepo;
        $this->_logo = $logo;
         $this->customerSession = $customerSession;
         $this->cart = $cart;
         
        $this->_productCollectionFactory = $productCollectionFactory;
    }

//    const APIKey = "App 514c1ea889edd234108e48dbe05cdcf7-a759d86a-b6df-4755-9c11-0fe7a6672b16";
//    const ApplicationId = "82409D8D6AD39695A8BA351712D4228A";
//    const MessageId = "2CD851BCB5ACF795241F59CD88B97544";
//    const FROM = "MyGourmet";
    const APIKEY = "App 7cea40c1413ae505efe2e4ba0622d550-cbf9e437-cff9-4afe-aac4-1496ecb90529";
    const APPLICATIONID = "845DF1B6CC1907F7C1B8C1F2ED48D0BC";
    //const MESSAGEID = "8503F8ACB5184C9936DE8A6242B9EEF1";
    const MESSAGEID = "09235C7F120298047DA547A17EA29458";
    const FROM = "Gourmet";

//    BLoyal APi Start
    const BLOYALAPIENDPOINT="https://loyaltyengineus1.bloyal.com/api/v4/733998b22f10acb136bd632f1f2bbd834304a6bc53d5d69048d56a8af7e040d3fc562435a6405eacbe90eff6/";

    /**
     * @psalm-suppress MissingReturnType
     */
    public function getLogoSrc()
    {
        return $this->_logo->getLogoSrc();
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function getProductCollection($categoryId)
    {
        $ids = [$categoryId];
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
         $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $collection->addCategoriesFilter(['in' => $ids]);
         return $collection;
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function getCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomer();

            return $customer;
        } else {
            return '';
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function isLoyalty()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomer();
            if ($customer->getData('is_loyalty')) {
                 return true;
            } else {
                return false;
            }
         
        } else {
            return false;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function isLoyaltyForAll()
    {
      
            return true;
    }
    /**
     * @psalm-suppress MissingReturnType
     */
    public function getQuote()
    {
        return $this->cart->getQuote();
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function SendPinBySMS($to)
    {
        $APIKey = self::APIKEY;
        $applicationId = self::APPLICATIONID;
        $messageId = self::MESSAGEID;
        $from = self::FROM;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://gvwg8.api.infobip.com/2fa/1/pin?ncNeeded=true",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"applicationId\": \"$applicationId\",\n    \"messageId\": \"$messageId\",\n    \"from\": \"$from\",\n    \"to\": \"$to\"\n}",
            CURLOPT_HTTPHEADER => [
                "Authorization: $APIKey",
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function Verifypin($pin, $pinId)
    {
        $APIKey = self::APIKEY;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://gvwg8.api.infobip.com/2fa/1/pin/" . $pinId . "/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"pin\": \"$pin\"\n}",
            CURLOPT_HTTPHEADER => [
                "Authorization: $APIKey",
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalQuickSearch($email)
    {
         $APIKey = self::BLOYALAPIENDPOINT;
         $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $APIKey."customers?quickSearch=".urlencode($email),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalAvailableCoupons($email)
    {
         $QuickSearchResponce= $this->BloyalQuickSearch($email);
        if (isset($QuickSearchResponce->data[0])) {
            $Uid = $QuickSearchResponce->data[0]->Uid;
        } else {
            $Uid='';
        }
         $APIKey = self::BLOYALAPIENDPOINT;
         $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $APIKey."customers/availablecoupons?uid=".$Uid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalCalculateCart($lineItems, $email, $CouponCodes = '', $cartUid = null)
    {
//         var_dump($cartUid);
        $Codes=explode(',', $CouponCodes);
//           var_dump($Codes); exit;
        $APIKey = self::BLOYALAPIENDPOINT;
        $curl = curl_init();
        $dataSet = [
        "CouponCodes"=> $Codes,
        "Cart"=> [
        "Uid"=> $cartUid,
        "GuestCheckout"=> false,
        "Comment"=> null,
        "Title"=> "Test - Do Not Ship!!!",
        "Customer"=> [
        "Phone1"=> null,
        "MobilePhone"=> null,
        "EmailAddress"=>$email
        ],
        "Lines"=> $lineItems
        ,
        "Payments"=> null,
        "ExternallyAppliedDiscount"=> false,
        "Discount"=> 0.0,
        "DiscountReasonCode"=> null,
        "ReturnUid"=> null,
        "ReturnExternalId"=> null,
        "ReturnSourceExternalId"=> null,
        "TotalCharge"=> 0.0
        ],
        "DeviceUid"=> null,
        "StoreCode"=> null,
        "DeviceCode"=> null,
        "CashierUid"=> null,
        "CashierCode"=> "",
        "CashierExternalId"=> null,
        "Uid"=> null,
        "ReferenceNumber"=> null]
        ;
        curl_setopt_array($curl, [
           CURLOPT_URL => $APIKey."carts/commands/calculates",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_POSTFIELDS => json_encode($dataSet),
           CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => [
               
               "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalApproveCart($cartUid)
    {
        $APIKey = self::BLOYALAPIENDPOINT;
        $curl = curl_init();
        $dataSet = [
        "CartUid"=> $cartUid,
        "CartExternalId"=> "",
        "CartSourceExternalId"=> "",
        "ProcessingOrder"=> false,
        "Cart"=> null,
        "Uid"=> null,
        "ReferenceNumber"=> null
        ]
        ;
        curl_setopt_array($curl, [
           CURLOPT_URL => $APIKey."carts/commands/approve",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_POSTFIELDS => json_encode($dataSet),
           CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => [
               
               "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalCommitCart($cartUid, $orderId, $amount)
    {
        $APIKey = self::BLOYALAPIENDPOINT;
        $curl = curl_init();
        $dataSet = [
         "CartUid" => $cartUid,
         "CartExternalId" => $orderId,
         "Payments" => [
             "TenderCode" => "1",
             "Amount" =>$amount,
         ],
         "Uid" => null,
         "ReferenceNumber" => null
        ];
        curl_setopt_array($curl, [
         CURLOPT_URL => $APIKey."carts/commands/commit",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_ENCODING => "",
         CURLOPT_MAXREDIRS => 10,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_POSTFIELDS => json_encode($dataSet),
         CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => [
               
             "Content-Type: application/json"
          ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     */
    public function BloyalPointsTransactions($email)
    {
        $QuickSearchResponce= $this->BloyalQuickSearch($email);
        if (isset($QuickSearchResponce->data[0])) {
            $Uid = $QuickSearchResponce->data[0]->Uid;
        } else {
            $Uid='';
        }
         
        $curl = curl_init();

        curl_setopt_array($curl, [
          CURLOPT_URL => "https://grid12.bloyal.com/api/v4/733998b22f10acb136bd632f1f2bbd834304a6bc53d5d69048d56a8af7e040d3fc562435a6405eacbe90eff6/Customers/".$Uid."/LoyaltyPointsTransactions",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
            
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidScalarArgument
     * @psalm-suppress UndefinedConstant
     */
    public function sentSMS($number, $text)
    {
         $username = 'GourmetTrans';
        $password = 'Abcd1234';
        $header = "Basic " . base64_encode($username . ":" . $password);
        $APIKey = self::APIKey;
        $from = self::FROM;
        $curl = curl_init();

        curl_setopt_array($curl, [
        CURLOPT_URL => "https://gvwg8.api.infobip.com/sms/2/text/single",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{ \"from\":\"$from\", \"to\":\"$number\", \"text\":\"$text\" }",
        CURLOPT_HTTPHEADER => [
        "accept: application/json",
        "authorization: $APIKey",
        "content-type: application/json"
        ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $decodedResponce = json_decode($response);
            return $decodedResponce;
        }
    }
}
// @codingStandardsIgnoreEnd