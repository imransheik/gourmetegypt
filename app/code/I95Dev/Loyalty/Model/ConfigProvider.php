<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Loyalty\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;

class ConfigProvider implements ConfigProviderInterface
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $loyaltyHelper;

    /**
     * @param CustomerSession $customerSession

     * @param CheckoutSession $checkoutSession
     * @psalm-suppress InvalidPropertyAssignmentValue
     */
    public function __construct(
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->customerSession = $customerSession;

        $this->checkoutSession = $checkoutSession;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedClass
     */
    public function getConfig()
    {
        $this->calculateCartBloyal();
        $config = [
            'payment' => [
                'loyalty' => [
                    'isAvailable' => $this->isAvailable(),
                    'usedAmount' => (float) $this->getUseCustomerRedeem(),
                    'usedPoints' => (float) $this->getUseCustomerPoints(),
                    'balance' => (float) $this->getBalance(),
                    'points' => (float) $this->getPoints(),
                    'enabled' => $this->loyaltyHelper->isLoyalty(),
					'ordertotals' => (float) $this->getorderTotals()
                ],
            ]
        ];
        return $config;
    }

    /**
     * Check if customer balance is available
     * @psalm-suppress UndefinedClass
     * @return bool
     */
    protected function isAvailable()
    {
        if (!$this->customerSession->getCustomerId()) {
            return false;
        }
        if (!$this->loyaltyHelper->getQuote()->getRedeemLoyaltyAmount()) {
            return false;
        }
        return true;
    }

    /**
     * Get balance amount
     * @psalm-suppress UndefinedClass
     * @return float
     */
    protected function getUseCustomerRedeem()
    {
        if (!$this->customerSession->getCustomerId()) {
            return 0;
        }

        return $this->loyaltyHelper->getQuote()->getRedeemLoyaltyAmount();
    }

    /**
     * Get balance amount
     * @psalm-suppress UndefinedClass
     * @return float
     */
    protected function getUseCustomerPoints()
    {
        if (!$this->customerSession->getCustomerId()) {
            return 0;
        }

        return -($this->loyaltyHelper->getQuote()->getRedeemLoyaltyAmount());
    }

    /**
     * Get balance amount
     * @psalm-suppress UndefinedClass
     * @return float
     */
    protected function getBalance()
    {
        if (!$this->customerSession->getCustomerId()) {
            return 0;
        }

        return 2000;
    }

    /**
     * Get balance amount
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress UndefinedClass
     * @return float
     */
    protected function getPoints()
    {
        if (!$this->customerSession->getCustomerId()) {
            return 0;
        }
    
        if (null !==$this->customerSession->getCustomer()->getEmail()) {
            $QuickSearch = $this->loyaltyHelper->BloyalQuickSearch($this->customerSession->getCustomer()->getEmail());
            $CurrentLoyaltyPoints='';
            if (isset($QuickSearch->data[0])) {
                $CurrentLoyaltyPoints = $QuickSearch->data[0]->CurrentLoyaltyPoints;
            }
            return $CurrentLoyaltyPoints;
        }
    }

    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress UndefinedVariable
     */
    public function calculateCartBloyal()
    {
        $quote = $this->loyaltyHelper->getQuote();

        $items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
//    var_dump(get_class_methods($item)); exit;
            $items[] = [
                "ProductUid" => null,
                "ProductCode" => $item->getSku(),
                "ProductName" => $item->getName(),
                "CategoryCode" => null,
                "CategoryExternalId" => null,
                "DepartmentCode" => "Misc",
                "DepartmentExternalId" => null,
                "SalesRepCode" => null,
                "ShipmentUid" => null,
                "ShipmentNumber" => null,
                "Quantity" => $item->getQty(),
                "Price" => $item->getPrice() * $item->getQty(),
                "SalePrice" => 0.0,
                "SalePriceReasonCode" => null,
                "ExternallyAppliedDiscount" => false,
                "Discount" => 0.0,
                "DiscountReasonCode" => null,
                "NetPrice" => 0.0,
                "ExtendedNetPrice" => 0.0,
                "Cost" => 0.0,
                "OrderDiscount" => 0.0,
                "ExternallyAppliedTax" => false,
                "TaxDetails" => [
                ]
            ];
        }
        $BloyalCartUid =  $quote->getData("bloyal_cartid");
        if (null !==$this->customerSession->getCustomer()->getEmail()) {
            $BloyalCalculateCartResponse = $this->loyaltyHelper
                ->BloyalCalculateCart($items, $this->loyaltyHelper->getCustomer()->getEmail(), '', $BloyalCartUid);
            return;
        }
        $CartUid='';
        if (isset($BloyalCalculateCartResponse->data->Cart->Uid)) {
            $CartUid = $BloyalCalculateCartResponse->data->Cart->Uid;
        }
        $LoyaltyPointsUsed='';
        if (isset($BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed)) {
            $LoyaltyPointsUsed= $BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed;
        }
         $quote->setData("bloyal_cartid", $CartUid);
        $quote->setData("redeem_loyalty_amount", $LoyaltyPointsUsed);
        
        $quote->collectTotals()->save();
    }
	
	/**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress UndefinedVariable
     */
    public function getorderTotals()
    {
        $quote = $this->loyaltyHelper->getQuote();
		$grandTotal = $quote->getGrandTotal();
		return $grandTotal;
	}
}
