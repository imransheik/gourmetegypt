<?php

namespace I95Dev\Loyalty\Model;

class MyGourmetManagement implements \I95Dev\Loyalty\Api\MyGourmetManagementInterface
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $data;

    /**
     * MyGourmetManagement constructor.
     * @param \I95Dev\Loyalty\Helper\Data $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\Loyalty\Helper\Data $data
    ) {
        $this->data= $data;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedClass
     */
    //phpcs:disable
    public function postMyGourmet($email)
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $i95DevHelper = $objectManager->get('I95Dev\Loyalty\Helper\Data');

        $i95DevHelper = $this->data;

        $QuickSearch = $i95DevHelper->BloyalQuickSearch($email);
        $firstName='';
        $LastName='';
        $CurrentLoyaltyPoints='';
        if (isset($QuickSearch->data[0])) {
            $firstName = $QuickSearch->data[0]->FirstName;
            $LastName = $QuickSearch->data[0]->LastName;
            $CurrentLoyaltyPoints = $QuickSearch->data[0]->CurrentLoyaltyPoints;
        }

        $res = [];
        $res['FirstName'] = $firstName;
        $res['LastName'] = $LastName;
        $res['CurrentLoyaltyPoints'] = $CurrentLoyaltyPoints;
        return json_encode($res);
//        return 'hello api POST return the $param '.$email;
    }
}
