<?php

namespace I95Dev\Loyalty\Model;

/**
 * @psalm-suppress PossiblyUndefinedVariable
 */
class MyRewardsManagement implements \I95Dev\Loyalty\Api\MyRewardsManagementInterface
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $data;

    /**
     * MyGourmetManagement constructor.
     * @param \I95Dev\Loyalty\Helper\Data $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\Loyalty\Helper\Data $data
    ) {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress UndefinedClass
     */
    //phpcs:disable
    public function postMyRewards($email, $store = null)
    {

        if (empty($store)) {
            $store=1;
        }
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      //  $i95DevHelper = $objectManager->get('I95Dev\Loyalty\Helper\Data');
        $i95DevHelper = $this->data;

        if(isset($i95DevHelper->BloyalAvailableCoupons($email)->data)){
			$availableCoupon = $i95DevHelper->BloyalAvailableCoupons($email)->data;
		}else{
			throw new \Magento\Framework\Exception\CouldNotDeleteException(__("Please contact support as loyalty was not applied to your order."));// phpcs:ignore
		}
        $noDisplayCouponcode = ['BAGFLOWER', 'BAGTREE', 'SPICEGURU', 'CANAPE'];
        foreach ($availableCoupon as $Coupon) {
            //if($store == 3){
            $CouponCode = str_replace('EGP', '', $Coupon->Code);
            $Coupon->Code = $CouponCode;
            $pts = str_replace('PTS', '', $Coupon->Name);
            $Coupon->Name = $CouponCode;
            //}
            if (!in_array($Coupon->Code, $noDisplayCouponcode)) {
                //print_r($Coupon->Code);
                $availableCoupons[str_replace('EGP', '', $Coupon->Code)] = $Coupon;
            }
            
        }
        ksort($availableCoupons);
        return json_encode(array_values($availableCoupons));
    }
}
