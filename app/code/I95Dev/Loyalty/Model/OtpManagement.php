<?php


namespace I95Dev\Loyalty\Model;

class OtpManagement implements \I95Dev\Loyalty\Api\OtpManagementInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $quoteFactory;
	/**
     * @psalm-suppress MissingPropertyType
     */
    protected $customer;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * OtpManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     * @runInSeparateProcess PossiblyUndefinedVariable
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
		\Magento\Customer\Model\Customer $customer,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {

        $this->quoteFactory = $quoteFactory;
        $this->_customer = $customer;
        $this->_loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyUndefinedVariable
     */
    public function postOtp($customer)
    {
		$customerObj = $this->_customer->load($customer);
		$primary_mobile_number = $customerObj->getPrimaryMobileNumber();
		$isd_code = $customerObj->getIsdCode();
		$phone = $isd_code.$primary_mobile_number;
        $responce = $this->_loyaltyHelper->SendPinBySMS($phone);
        if (isset($responce->smsStatus)) {
			$smsStatus = $responce->smsStatus;
		}else{
			$response['success'] = false;
            $response['message'] = "An error occurred while generating OTP for your request. Please try again later.";
			return json_encode($response);
		}

        if ($smsStatus == "MESSAGE_SENT") {
			$pinId = $responce->pinId;
            $response['success'] = true;
            $response['message'] = "We have sent the OTP!";
            $response['pinId'] = $pinId;
        } else {
            $response['success'] = false;
            $response['message'] = "An error occurred while sending the message on your mobile. Please try again later.";
        }
        return json_encode($response);
		
    }
}
