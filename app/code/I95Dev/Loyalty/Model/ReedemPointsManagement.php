<?php

namespace I95Dev\Loyalty\Model;

class ReedemPointsManagement implements \I95Dev\Loyalty\Api\ReedemPointsManagementInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $quoteFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * ReedemPointsManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {

        $this->quoteFactory = $quoteFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedConstant
     */
    public function postReedemPoints($couponCode, $couponType, $quoteId, $email, $BloyalCartUid)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/kamlesh-bl.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Test Info Message--');
        $logger->info($couponCode);
        $logger->info($couponType);
        $logger->info($quoteId);
        $logger->info($email);
        $logger->info($BloyalCartUid);
        $result = $this->calculateCartBloyal($quoteId, $email, $couponCode , $BloyalCartUid);

        return $result;
    }
    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress MisplacedRequiredParam
     */
    public function calculateCartBloyal($quoteId, $email, $couponCode = '', $BloyalCartUid = '')
    {
        $quote = $this->quoteFactory->create()->load($quoteId);

        $items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
//    var_dump(get_class_methods($item)); exit;
            $items[] = [
                "ProductUid" => null,
                "ProductCode" => $item->getSku(),
                "ProductName" => $item->getName(),
                "CategoryCode" => null,
                "CategoryExternalId" => null,
                "DepartmentCode" => "Misc",
                "DepartmentExternalId" => null,
                "SalesRepCode" => null,
                "ShipmentUid" => null,
                "ShipmentNumber" => null,
                "Quantity" => $item->getQty(),
                "Price" => $item->getPrice() * $item->getQty(),
                "SalePrice" => 0.0,
                "SalePriceReasonCode" => null,
                "ExternallyAppliedDiscount" => false,
                "Discount" => 0.0,
                "DiscountReasonCode" => null,
                "NetPrice" => 0.0,
                "ExtendedNetPrice" => 0.0,
                "Cost" => 0.0,
                "OrderDiscount" => 0.0,
                "ExternallyAppliedTax" => false,
                "TaxDetails" => [
                ]
            ];
        }

        $BloyalCartUidNew = $quote->getData("bloyal_cartid");

        $BloyalCalculateCartResponse = $this->_loyaltyHelper
            ->BloyalCalculateCart($items, $email, $couponCode, $BloyalCartUidNew);
        $CartUid = '';
        if (isset($BloyalCalculateCartResponse->data->Cart->Uid)) {
            $CartUid = $CartUid = $BloyalCalculateCartResponse->data->Cart->Uid;
        }
        if (isset($BloyalCalculateCartResponse->data)) {
            $LoyaltyPointsUsed = abs($BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed);
            $discount = $BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed;
        } else {
            $result = [
                "message" => "",
                "LoyaltyPointsUsed" => "0" . " Pts",
                "cartUid" => "",
                "quoteId" => $quoteId,
                "discount" => 0,
                "success" => true
            ];
            return json_encode($result);
        }
        if ($CartUid) {
            $QuoteTotal = $quote->getSubtotal() + $quote->getShippingAddress()->getShippingAmount();
            if ($LoyaltyPointsUsed > $QuoteTotal) {
                $message = "Please add more items to the cart as the remaining amount is not refundable.";
            } else {
                $message = "";
            }
            $quote->setData(
                "redeem_loyalty_amount",
                $BloyalCalculateCartResponse->data->LoyaltySummary->LoyaltyPointsUsed
            );
            $quote->setData("bloyal_cartid", $CartUid);
            $quote->collectTotals()->save();
            $result = [
                "message" => $message,
                "LoyaltyPointsUsed" => $LoyaltyPointsUsed . " Pts",
                "cartUid" => $CartUid,
                "quoteId" => $quoteId,
                "discount" => $discount,
                "success" => true
            ];
        } else {
            $result = ["message" => "Some issue with Bloyal!", "success" => false];
        }
        return json_encode($result);
    }
}
