<?php

/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Loyalty\Model\Total;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Fee extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency [description]
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    ) {
        $this->_priceCurrency = $priceCurrency;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this|Fee
     * @psalm-suppress UndefinedMethod
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        $address = $shippingAssignment->getShipping()->getAddress();
        $amount=abs($quote->getRedeemLoyaltyAmount());
        if ($address->getAddressType() == "shipping" && $amount) {
            $total->addTotalAmount('redeem_loyalty_amount', $quote->getRedeemLoyaltyAmount());
            $total->addBaseTotalAmount('redeem_loyalty_amount', $quote->getRedeemLoyaltyAmount());
        }
  
        if ($total->getGrandTotal() == 0) {
            $quote->setPaymentMethod('free')->save();
        }

//        var_dump($quote->getPaymentMethod()); exit;
        return $this;
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param Address\Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MismatchingDocblockParamType
     * @psalm-suppress MoreSpecificImplementedParamType
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        return [
            'code' => 'redeem_loyalty_amount',
            'title' => $this->getLabel(),
            'value' =>0
        ];
    }

    /**
     * get label
     * @return string
     * @psalm-suppress UndefinedFunction
     */
    public function getLabel()
    {
        return __('Redeem Amount');
    }
}
