<?php


namespace I95Dev\Loyalty\Model;

class TransactionsManagement implements \I95Dev\Loyalty\Api\TransactionsManagementInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $quoteFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * TransactionsManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {

        $this->quoteFactory = $quoteFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyUndefinedVariable
     */
    public function postTransactions($email)
    {
        try {
            $BloyalPointsTransactions = $this->_loyaltyHelper->BloyalPointsTransactions($email);
            if (isset($BloyalPointsTransactions) && $BloyalPointsTransactions) {
                $response['success'] = true;
                $response['message'] = "";
                $response['transactions'] = $BloyalPointsTransactions;
            } else {
                $response['success'] = false;
                $response['message'] = "You do not have any transaction details";
            }
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = "You do not have any transaction details";
        }
        return json_encode($response);
    }
}
