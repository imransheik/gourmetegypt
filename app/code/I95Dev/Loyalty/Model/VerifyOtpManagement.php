<?php


namespace I95Dev\Loyalty\Model;

class VerifyOtpManagement implements \I95Dev\Loyalty\Api\VerifyOtpManagementInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $quoteFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_loyaltyHelper;

    /**
     * VerifyOtpManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress PossiblyUndefinedVariable
     */
    public function verifyOtp($otp, $pinId)
    {
        if ($pinId && $otp) {
            $responce = $this->_loyaltyHelper->Verifypin($otp, $pinId);
            if (isset($responce->verified)) {
                $verified = $responce->verified;
            }else{
                $response['message'] = "An error occurred while verifying the PIN. Please try again later.";
                $response['success'] = false;
                return json_encode($response);
            }
            if ($verified) {
                $response['message'] = "We have successfully verified your PIN!!!";
                $response['success'] = true;
            } else {
                $response['message'] = "An error occurred while verifying the PIN. Please try again later.";
                $response['success'] = false;
            }
        } else {
            $response['message'] = "Please enter the OTP!";
            $response['success'] = false;
        }
        return json_encode($response);
    }
}
