<?php

namespace I95Dev\Loyalty\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddSelectToOrderObserver implements ObserverInterface
{

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $checkoutSession;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $loyaltyHelper;

    /**
     * @param CustomerSession $customerSession
     * @param CheckoutSession $checkoutSession
     * @psalm-suppress UndefinedClass
     * @psalm-suppress MismatchingDocblockParamType
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper,
        \Magento\Quote\Model\Quote $quote
    ) {

        $this->checkoutSession = $checkoutSession;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->quote = $quote;
    }

    /**
     * Set payment fee to order
     *
     * @param EventObserver $observer
     * @return $this
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress UndefinedClass
     * @psalm-suppress UndefinedFunction
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/bloyal.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $order = $observer->getOrder();
//		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//		$quote = $objectManager->create('\Magento\Quote\Model\Quote')->load($order->getQuoteId());
        $quote =  $this->quote->load($order->getQuoteId());
        $RedeemLoyaltyAmount = $quote->getRedeemLoyaltyAmount();
        if (!$RedeemLoyaltyAmount ||
            $RedeemLoyaltyAmount == "" ||
            $RedeemLoyaltyAmount == 0 || $RedeemLoyaltyAmount == "0.0000") {
            return $this;
        }
        $QuickSearch = $this->loyaltyHelper->BloyalQuickSearch($quote->getCustomerEmail());

		if($QuickSearch->data[0]->CurrentLoyaltyPoints){
			$CurrentLoyaltyPoints = $QuickSearch->data[0]->CurrentLoyaltyPoints;
		}else{
			throw new \Magento\Framework\Exception\CouldNotDeleteException(__("Please contact support as loyalty pints not avaliable."));// phpcs:ignore
		}
		
        if (abs($RedeemLoyaltyAmount) > $CurrentLoyaltyPoints) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("You do not have enough points to redeem this Coupon.")
            );
            // @codingStandardsIgnoreStart
            return;
            // @codingStandardsIgnoreEnd
        }
        $order = $observer->getOrder();
        $cartUid = $quote->getData("bloyal_cartid");
        if ($cartUid) {
            $ApproveCart = $this->loyaltyHelper->BloyalApproveCart($cartUid);
            $Approved = "";
            if (isset($ApproveCart->data->Approved)) {
                $Approved = $ApproveCart->data->Approved;
            }
            if ($ApproveCart->status == "success" && $Approved) {
                $commitCart = $this->loyaltyHelper
                    ->BloyalCommitCart($cartUid, $order->getIncrementId(), $order->getGrandTotal());
                $LoyaltyPointsRedeemedBloyal = "";
                if (isset($commitCart->data->LoyaltyPointsRedeemed)) {
                    $LoyaltyPointsRedeemedBloyal = $commitCart->data->LoyaltyPointsRedeemed;
                }
                if ($commitCart->status == "success" && ($LoyaltyPointsRedeemedBloyal == $RedeemLoyaltyAmount)) {

                    $this->checkoutSession->setCouponCodeArray('');
                    $this->checkoutSession->setRedeemtionType('');
                    $this->checkoutSession->setRedeemLoyaltyCode('');
                    $order->setData('redeem_loyalty_amount', $RedeemLoyaltyAmount);
                } else {
                    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/bloyal.log');
                    $logger = new \Zend\Log\Logger();
                    $logger->addWriter($writer);
                    $logger->info("commitCart: issue with bloyal customer " . $quote->getCustomerEmail());
					$logger->info("commitCart: issue with bloyal customer " . json_encode($commitCart));
                    $order->setData('redeem_loyalty_amount', '');
                    $quote->setData("bloyal_cartid", "")->setData("redeem_loyalty_amount", "");
                    $quote->collectTotals()->save();
                    throw new \Magento\Framework\Exception\CouldNotDeleteException(__("The order has been been cancel as loyalty points was not appiled, Please contact with admin."));// phpcs:ignore
                    return;// phpcs:ignore
                }
            } else {
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/bloyal.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $logger->info("approveCart: issue with bloyal customer " . $quote->getCustomerEmail());
                $logger->info("approveCartResponse: issue with bloyal customer " . json_encode($ApproveCart));
                // @codingStandardsIgnoreStart
                throw new \Magento\Framework\Exception\LocalizedException(__("Some isuse with BLoyal discount.Please contact with admin."));
                return;
                // @codingStandardsIgnoreEnd
            }
        }
        return $this;
    }
}
