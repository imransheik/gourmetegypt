<?php

namespace I95Dev\Loyalty\Observer;

class Customer implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_coreSession;

    /**
     * Customer constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    ) {
        $this->_request = $request;
        $this->_coreSession = $coreSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @psalm-suppress MissingPropertyType
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerMobile = $this->_request->getPost("primary_mobile_number");
        $countryCode = $this->_request->getPost("isd_code");
        $contact = $this->_request->getPost("prefered_contact");
        $language = $this->_request->getPost("prefered_language");
//        $mobile = $countryCode . $customerMobile;
         $customer = $observer->getCustomer();
        if ($this->_request->getFullActionName() == "sociallogin_popup_createAcc") {
             $this->_coreSession->start();
            $customer = $observer->getCustomer();
            $DataModel = $customer->getDataModel();
            $DataModel->setCustomAttribute("primary_mobile_number", $this->_coreSession->getPrimaryMobile());
            $DataModel->setCustomAttribute("isd_code", $this->_coreSession->getIsd());
            $DataModel->setCustomAttribute("prefered_contact", $contact);
            $DataModel->setCustomAttribute("prefered_language", $language);
            $DataModel->setCustomAttribute("is_contactinfoupdated", 1);
            $customer->updateData($DataModel);
        } else {
            $customer->setCustomAttribute("is_contactinfoupdated", 1);
            $customer->setCustomAttribute("primary_mobile_number", $customerMobile);
            $customer->setCustomAttribute("isd_code", $countryCode);
            $customer->setCustomAttribute("prefered_contact", $contact);
            $customer->setCustomAttribute("prefered_language", $language);
        }
        return $this;
    }
}
