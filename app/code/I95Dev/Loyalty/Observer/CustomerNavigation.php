<?php

namespace I95Dev\Loyalty\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerNavigation implements ObserverInterface
{

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $loyaltyHelper;

    /**
     * CustomerNavigation constructor.
     * @param \I95Dev\Loyalty\Helper\Data $loyaltyHelper
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\Loyalty\Helper\Data $loyaltyHelper
    ) {
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * @param Observer $observer
     * @psalm-suppress UndefinedClass
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\View\Layout $layout */
        $enableLoyalty=$this->loyaltyHelper->isLoyalty();
        if (!$enableLoyalty) {
            $layout = $observer->getLayout();
            $block = $layout->getBlock('customer-account-navigation-my-gourmet');
            $blockreward = $layout->getBlock('customer-account-navigation-my-gourmet');

            if ($block) {
                //you can apply or add you condition here.
                $layout->unsetElement('customer-account-navigation-my-gourmet');
            }
            if ($blockreward) {
                //you can apply or add you condition here.
                $layout->unsetElement('customer-account-navigation-my-rewards');
            }
        }
    }
}
