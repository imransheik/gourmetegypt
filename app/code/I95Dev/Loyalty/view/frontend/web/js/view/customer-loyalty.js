define(
        ['ko',
            'jquery',
            'mage/url',
            'Magento_Ui/js/form/form',
            'Magento_Checkout/js/model/quote',
            'Magento_Catalog/js/price-utils',
            'Magento_Customer/js/model/customer',
            'Magento_Checkout/js/model/cart/cache',
            'Magento_Checkout/js/model/cart/totals-processor/default',
            'Magento_Checkout/js/action/set-shipping-information',
            'Magento_Ui/js/modal/modal',
            'mage/mage'
        ],
        function (
                ko,
                $,
                url,
                Component,
                quote,
                priceUtils,
                customer,
                cartCache,
                totalsProcessor,
                setShippingInformationAction
                ) {
            'use strict';
            return Component.extend({
                defaults: {
                    template: 'I95Dev_Loyalty/checkout/loyalty',
                    listens: {
                        'update': 'update'
                    }
                },

                preferedValue: ko.observable(''),
                isCustomerLoggedIn: customer.isLoggedIn,
                isCustomerNotLoggedIn: window.isCustomerLoggedIn,
                initialize: function () {
                    self = this;
                    this._super();

                },

                myRewards: function () {
                    var rewardsPopup = $('div#gourmentrewardspopup')
                            .modal({
                                autoOpen: false,
                                buttons: [
                                ],
                                clickableOverlay: true,
                                focus: '',
                                innerScroll: false,
                                modalClass: 'loyalty-checkout-rewards-popup',
                                buttons: [{
                                        text: 'close',
                                        class: 'action-close',
                                        click: function () {
//                                                self.radioSelectedOptionValue('0');

                                            this.closeModal();
                                            $('.opc-payment-additional.loyalty').hide();

                                        }
                                    }],
                                responsive: true,
                                closeOnEscape: false,

                                type: 'popup'
                            });
                    rewardsPopup.modal("openModal");

                },
                validateOtp: function () {
                    var otp = $('#redemptions-otp').val();
                    var pinId = $('#redemptions-pinId').val();

                    if (otp) {
                        var params = {pinId: pinId, otp: otp};

                        $.ajax({
                            type: 'POST',
                            url: url.build('loyalty/index/validateotp'),
                            data: params,
                            showLoader: true,
                            success: function (data) {
                                var messageData = JSON.parse(data);
                                if (messageData.success == true) {
                                  
                                
                                    $('.opc-payment-additional.loyalty').show();
                                    $('.otp-success-block').show();
                                    $('.otp-block').hide();
                                    $('.reward.points-i95dev').show();
                                   $("div#message_loyalty_chechout_otp").html(messageData.message);
                                    $('.opc-payment-additional.loyalty').show();
                                    var fomatPoints = messageData.points.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    $('#loyalty-points-redeemed').text(fomatPoints);
                                    var pointsValue = priceUtils.formatPrice(-(messageData.amount), quote.getPriceFormat());
                                    var totals = quote.getTotals();
                                    var grandtotals = priceUtils.formatPrice(totals()['subtotal_with_discount'], quote.getPriceFormat());
                                    var amountToPay = priceUtils.formatPrice(totals()['subtotal_with_discount'] - messageData.amount, quote.getPriceFormat());
                                    $('#loyalty-points-value').text(pointsValue);
                                    $('.reward.points-i95dev .price').text(pointsValue);
                                    $('#loyalty-subtotal').text(grandtotals);
                                    $('#loyalty-amount-to-pay').text(amountToPay);
                                    window.checkoutConfig.payment.loyalty.points = messageData.points;
                                    window.checkoutConfig.payment.loyalty.usedAmount = messageData.amount;
                                    setTimeout(function() {
                                     $('div#gourmentpointotppoup').modal("closeModal");
                                    cartCache.clear('cartVersion');
                                   
                                        if (quote.shippingMethod()) {
                                            setShippingInformationAction().done(
                                                    function () {
                                                        totalsProcessor.estimateTotals(quote.shippingAddress());
                                                    }
                                            );
                                        } else {
                                            totalsProcessor.estimateTotals(quote.shippingAddress());
                                        }
                                    }, 5000);
                                } else {
                                    $("div#message_loyalty_chechout_otp").html(messageData.message);

                                }
                            }
                        });
                    } else {
                        $('div#error-message-redemptions-otp').show();
                    }
                }

            });
        }
);
