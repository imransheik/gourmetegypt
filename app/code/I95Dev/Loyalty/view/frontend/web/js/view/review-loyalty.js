/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Customer balance view model
 */
define([
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils'
  
], function (ko, component, quote, priceUtils) {
    'use strict';

 
    return component.extend({
        defaults: {
            template: 'I95Dev_Loyalty/payment/loyalty-summary',
            isEnabled: true
        },
         points: window.checkoutConfig.payment.loyalty.points,
         usedPoints: window.checkoutConfig.payment.loyalty.usedPoints,
        isAvailable: window.checkoutConfig.payment.loyalty.isAvailable,
      
        usedAmount: window.checkoutConfig.payment.loyalty.usedAmount,
        balance: window.checkoutConfig.payment.loyalty.balance,

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe('isEnabled');

            return this;
        },

        

       
        /**
         * Get active status
         *
         * @return {String}
         */
        subTotal: function () {
             var totals = quote.getTotals();

        
         return priceUtils.formatPrice( totals()['subtotal_with_discount'] , quote.getPriceFormat());
        },
        /**
         * Get active status
         *
         * @return {String}
         */
        toPay: function () {
              var totals = quote.getTotals();
         return priceUtils.formatPrice(totals()['grand_total'], quote.getPriceFormat()); 
        },
        /**
         * Get active status
         *
         * @return {String}
         */
        getShippingAmount: function () {
              var totals = quote.getTotals();
         return priceUtils.formatPrice(totals()['shipping_amount'], quote.getPriceFormat()); 
        },
        /**
         * Get active status
         *
         * @return {String}
         */
        getPoints: function () {
           return this.usedPoints;
        },
        /**
         * Get active status
         *
         * @return {String}
         */
        getUsedAmount: function () {
           return priceUtils.formatPrice(this.usedAmount, quote.getPriceFormat());
        }
    });
});
