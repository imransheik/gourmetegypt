<?php

namespace I95Dev\OrderDuplicatePatch\Observer;

class Order implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $_responseFactory;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_resource;

    /**
     * Order constructor.
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
        $this->_resource = $resource;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @psalm-suppress UndefinedMethod
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $order = $observer->getEvent()->getOrder();
        $quoteID = $order->getQuoteId();
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $resource = $objectManager->get(\Magento\Framework\App\ResourceConnection::class);
//        $connection = $resource->getConnection();
//        $tableName = $resource->getTableName('sales_order');
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getConnection()->getTableName('sales_order');
        $selectedTime = date('Y-m-d h:i:s');
        $endTime = strtotime("-15 seconds", strtotime($selectedTime));
        $last15Sec = date('Y-m-d h:i:s', $endTime);
        // @codingStandardsIgnoreStart
        $sql = "SELECT * FROM `" . $tableName . "` WHERE `quote_id` = " . $quoteID . " and `created_at` >= '$last15Sec'";
        if ($result = $connection->fetchRow($sql)) {
            $customerBeforeAuthUrl = $this->_url->getUrl('checkout');
            $this->_responseFactory->create()->setRedirect($customerBeforeAuthUrl)->sendResponse();
            exit();
        // @codingStandardsIgnoreEnd
        }
    }
}
