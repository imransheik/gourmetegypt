<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api\Data;

interface OutOfStockThresholdInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const STORE_28 = 'store_28';
    const STORE_20 = 'store_20';
    const OUTOFSTOCK_ID = 'outofstock_id';
    const STORE_24 = 'store_24';
    const STORE_18 = 'store_18';
    const STORE_19 = 'store_19';
    const STORE_7 = 'store_7';
    const STORE_9 = 'store_9';
    const STORE_13 = 'store_13';
    const STORE_12 = 'store_12';
    const STORE_27 = 'store_27';
    const SKU = 'sku';
    const OUTOFSTOCKTHRESHOLD_ID = 'outofstockthreshold_id';
    const PID = 'pid';
    const STORE_38 = 'store_38';
    const STORE_11 = 'store_11';
    const STORE_32 = 'store_32';
	const STORE_39 = 'store_39';
	const STORE_40 = 'store_40';
	const STORE_42 = 'store_42';
	const STORE_44 = 'store_44';
	const STORE_37 = 'store_37';
	const STORE_41 = 'store_41';
    const STORE_47 = 'store_47';

    /**
     * Get outofstockthreshold_id
     * @return string|null
     */
    public function getOutofstockthresholdId();

    /**
     * Set outofstockthreshold_id
     * @param string $outofstockthresholdId
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setOutofstockthresholdId($outofstockthresholdId);

    /**
     * Get outofstock_id
     * @return string|null
     */
    public function getOutofstockId();

    /**
     * Set outofstock_id
     * @param string $outofstockId
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setOutofstockId($outofstockId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdExtensionInterface $extensionAttributes
    );

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setSku($sku);

    /**
     * Get pid
     * @return string|null
     */
    public function getPid();

    /**
     * Set pid
     * @param string $pid
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setPid($pid);

    /**
     * Get store_7
     * @return string|null
     */
    public function getStore7();

    /**
     * Set store_7
     * @param string $store7
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore7($store7);
	/**
     * Get store_9
     * @return string|null
     */
    public function getStore9();

    /**
     * Set store_9
     * @param string $store9
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore9($store9);

    /**
     * Get store_11
     * @return string|null
     */
    public function getStore11();

    /**
     * Set store_11
     * @param string $store11
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore11($store11);

    /**
     * Get store_12
     * @return string|null
     */
    public function getStore12();

    /**
     * Set store_12
     * @param string $store12
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore12($store12);

    /**
     * Get store_13
     * @return string|null
     */
    public function getStore13();

    /**
     * Set store_13
     * @param string $store13
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore13($store13);

    /**
     * Get store_18
     * @return string|null
     */
    public function getStore18();

    /**
     * Set store_18
     * @param string $store18
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore18($store18);

    /**
     * Get store_19
     * @return string|null
     */
    public function getStore19();

    /**
     * Set store_19
     * @param string $store19
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore19($store19);

    /**
     * Get store_20
     * @return string|null
     */
    public function getStore20();

    /**
     * Set store_20
     * @param string $store20
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore20($store20);

    /**
     * Get store_24
     * @return string|null
     */
    public function getStore24();

    /**
     * Set store_24
     * @param string $store24
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore24($store24);

    /**
     * Get store_27
     * @return string|null
     */
    public function getStore27();

    /**
     * Set store_27
     * @param string $store27
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore27($store27);

    /**
     * Get store_28
     * @return string|null
     */
    public function getStore28();

    /**
     * Set store_28
     * @param string $store28
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore28($store28);

    /**
     * Get store_32
     * @return string|null
     */
    public function getStore32();

    /**
     * Set store_32
     * @param string $store32
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore32($store32);

    /**
     * Get store_38
     * @return string|null
     */
    public function getStore38();

    /**
     * Set store_38
     * @param string $store38
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore38($store38);
	
	/**
     * Get store_39
     * @return string|null
     */
    public function getStore39();

    /**
     * Set store_39
     * @param string $store39
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore39($store39);
	/**
     * Get store_40
     * @return string|null
     */
    public function getStore40();

    /**
     * Set store_40
     * @param string $store40
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore40($store40);
	
	/**
     * Get store_42
     * @return string|null
     */
    public function getStore42();

    /**
     * Set store_42
     * @param string $store42
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore42($store42);
	
	/**
     * Get store_44
     * @return string|null
     */
    public function getStore44();

    /**
     * Set store_44
     * @param string $store44
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore44($store44);
	/**
     * Get store_41
     * @return string|null
     */
    public function getStore41();

    /**
     * Set store_41
     * @param string $store41
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore41($store41);
	/**
     * Get store_37
     * @return string|null
     */
    public function getStore37();

    /**
     * Set store_37
     * @param string $store37
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore37($store37);
    /**
     * Get store_47
     * @return string|null
     */
    public function getStore47();

    /**
     * Set store_47
     * @param string $store47
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     */
    public function setStore47($store47);

}
