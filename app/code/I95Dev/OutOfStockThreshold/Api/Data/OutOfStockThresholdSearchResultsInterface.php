<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api\Data;

interface OutOfStockThresholdSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get OutOfStockThreshold list.
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface[]
     */
    public function getItems();

    /**
     * Set outofstock_id list.
     * @param \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
