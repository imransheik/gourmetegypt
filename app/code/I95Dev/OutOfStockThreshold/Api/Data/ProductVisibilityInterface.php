<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api\Data;

interface ProductVisibilityInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const OUTOFSTOCKTHRESHOLD_ID = 'outofstockthreshold_id';
    const STORE_27 = 'store_27';
    const STORE_12 = 'store_12';
    const STORE_19 = 'store_19';
    const STORE_18 = 'store_18';
    const STORE_24 = 'store_24';
    const PID = 'pid';
    const STORE_32 = 'store_32';
    const STORE_28 = 'store_28';
    const STORE_13 = 'store_13';
    const SKU = 'sku';
    const STORE_38 = 'store_38';
    const STORE_20 = 'store_20';
    const STORE_11 = 'store_11';
    const STORE_7 = 'store_7';
    const STORE_47 = 'store_47';

    /**
     * Get OUTOFSTOCKTHRESHOLD_ID
     * @return string|null
     */
    public function getProductvisibilityId();

    /**
     * Set outOfStockthreshold_id
     * @param string $productvisibilityId
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setProductvisibilityId($productvisibilityId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface $extensionAttributes
    );

    /**
     * Get pid
     * @return string|null
     */
    public function getPid();

    /**
     * Set pid
     * @param string $pid
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setPid($pid);

    /**
     * Get store_7
     * @return string|null
     */
    public function getStore7();

    /**
     * Set store_7
     * @param string $store7
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore7($store7);

    /**
     * Get store_11
     * @return string|null
     */
    public function getStore11();

    /**
     * Set store_11
     * @param string $store11
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore11($store11);

    /**
     * Get store_12
     * @return string|null
     */
    public function getStore12();

    /**
     * Set store_12
     * @param string $store12
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore12($store12);

    /**
     * Get store_13
     * @return string|null
     */
    public function getStore13();

    /**
     * Set store_13
     * @param string $store13
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore13($store13);

    /**
     * Get store_18
     * @return string|null
     */
    public function getStore18();

    /**
     * Set store_18
     * @param string $store18
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore18($store18);

    /**
     * Get store_19
     * @return string|null
     */
    public function getStore19();

    /**
     * Set store_19
     * @param string $store19
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore19($store19);

    /**
     * Get store_20
     * @return string|null
     */
    public function getStore20();

    /**
     * Set store_20
     * @param string $store20
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore20($store20);

    /**
     * Get store_24
     * @return string|null
     */
    public function getStore24();

    /**
     * Set store_24
     * @param string $store24
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore24($store24);

    /**
     * Get store_27
     * @return string|null
     */
    public function getStore27();

    /**
     * Set store_27
     * @param string $store27
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore27($store27);

    /**
     * Get store_28
     * @return string|null
     */
    public function getStore28();

    /**
     * Set store_28
     * @param string $store28
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore28($store28);

    /**
     * Get store_32
     * @return string|null
     */
    public function getStore32();

    /**
     * Set store_32
     * @param string $store32
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore32($store32);

    /**
     * Get store_38
     * @return string|null
     */
    public function getStore38();

    /**
     * Set store_38
     * @param string $store38
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore38($store38);

    /**
     * Get store_47
     * @return string|null
     */
    public function getStore47();

    /**
     * Set store_47
     * @param string $store47
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore47($store47);
}
