<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api\Data;

interface ProductVisibilitySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ProductVisibility list.
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
