<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface OutOfStockThresholdRepositoryInterface
{

    /**
     * Save OutOfStockThreshold
     * @param \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
    );

    /**
     * Retrieve OutOfStockThreshold
     * @param string $outofstockthresholdId
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($outofstockthresholdId);

    /**
     * Retrieve OutOfStockThreshold matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete OutOfStockThreshold
     * @param \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
    );

    /**
     * Delete OutOfStockThreshold by ID
     * @param string $outofstockthresholdId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($outofstockthresholdId);
}
