<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductVisibilityRepositoryInterface
{

    /**
     * Save ProductVisibility
     * @param \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface $productVisibility
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \I95Dev\OutOfStockThreshold\Api\Data\ProductVisibilityInterface $productVisibility
    );

    /**
     * Retrieve ProductVisibility
     * @param string $productvisibilityId
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($productvisibilityId);

    /**
     * Retrieve ProductVisibility matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilitySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ProductVisibility
     * @param \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface $productVisibility
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface $productVisibility
    );

    /**
     * Delete ProductVisibility by ID
     * @param string $productvisibilityId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productvisibilityId);
}
