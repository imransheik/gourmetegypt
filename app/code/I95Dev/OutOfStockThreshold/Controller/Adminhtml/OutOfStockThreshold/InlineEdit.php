<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Controller\Adminhtml\OutOfStockThreshold;

class InlineEdit extends \Magento\Backend\App\Action
{

    protected $jsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Inline edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        
        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelid) {
                    /** @var \I95Dev\OutOfStockThreshold\Model\OutOfStockThreshold $model */
                    $model = $this->_objectManager->create(\I95Dev\OutOfStockThreshold\Model\OutOfStockThreshold::class)->load($modelid); // phpcs:ignore
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$modelid])); // phpcs:ignore
                        $model->save();
                    } catch (\Exception $e) {
                        $messages[] = "[OutOfStockThreshold ID: {$modelid}]  {$e->getMessage()}";
                        $error = true;
                    }
                }
            }
        }
        
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
