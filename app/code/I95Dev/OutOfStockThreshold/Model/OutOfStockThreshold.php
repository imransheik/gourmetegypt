<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Model;

use I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface;
use I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class OutOfStockThreshold extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $outofstockthresholdDataFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_eventPrefix = 'i95dev_outofstock_threshold';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OutOfStockThresholdInterfaceFactory $outofstockthresholdDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold $resource
     * @param \I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold\Collection $resourceCollection
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        OutOfStockThresholdInterfaceFactory $outofstockthresholdDataFactory,
        DataObjectHelper $dataObjectHelper,
        \I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold $resource,
        \I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold\Collection $resourceCollection,
        array $data = []
    ) {
        $this->outofstockthresholdDataFactory = $outofstockthresholdDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve outofstockthreshold model with outofstockthreshold data
     * @return OutOfStockThresholdInterface
     */
    public function getDataModel()
    {
        $outofstockthresholdData = $this->getData();
        
        $outofstockthresholdDataObject = $this->outofstockthresholdDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $outofstockthresholdDataObject,
            $outofstockthresholdData,
            OutOfStockThresholdInterface::class
        );
        
        return $outofstockthresholdDataObject;
    }
}
