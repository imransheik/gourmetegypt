<?php
/**
 * A Magento 2 module named I95Dev/OutOfStockThreshold
 * Copyright (C) 2019
 *
 * This file included in I95Dev/OutOfStockThreshold is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Model;

use I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold as ResourceOutOfStockThreshold;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\DataObjectHelper;
use I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold\CollectionFactory as OutOfStockThresholdCollectionFactory; // phpcs:ignore
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdSearchResultsInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\CouldNotDeleteException;
use I95Dev\OutOfStockThreshold\Api\OutOfStockThresholdRepositoryInterface;
use I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterfaceFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Reflection\DataObjectProcessor;

class OutOfStockThresholdRepository implements OutOfStockThresholdRepositoryInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $collectionProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensibleDataObjectConverter;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $outOfStockThresholdFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $searchResultsFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataOutOfStockThresholdFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $outOfStockThresholdCollectionFactory;

    /**
     * @param ResourceOutOfStockThreshold $resource
     * @param OutOfStockThresholdFactory $outOfStockThresholdFactory
     * @param OutOfStockThresholdInterfaceFactory $dataOutOfStockThresholdFactory
     * @param OutOfStockThresholdCollectionFactory $outOfStockThresholdCollectionFactory
     * @param OutOfStockThresholdSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ResourceOutOfStockThreshold $resource,
        OutOfStockThresholdFactory $outOfStockThresholdFactory,
        OutOfStockThresholdInterfaceFactory $dataOutOfStockThresholdFactory,
        OutOfStockThresholdCollectionFactory $outOfStockThresholdCollectionFactory,
        OutOfStockThresholdSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->outOfStockThresholdFactory = $outOfStockThresholdFactory;
        $this->outOfStockThresholdCollectionFactory = $outOfStockThresholdCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataOutOfStockThresholdFactory = $dataOutOfStockThresholdFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function save(
        \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
    ) {
        /* if (empty($outOfStockThreshold->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $outOfStockThreshold->setStoreId($storeId);
        } */
        
        $outOfStockThresholdData = $this->extensibleDataObjectConverter->toNestedArray(
            $outOfStockThreshold,
            [],
            \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface::class
        );
        
        $outOfStockThresholdModel = $this->outOfStockThresholdFactory->create()->setData($outOfStockThresholdData);
        
        try {
            $this->resource->save($outOfStockThresholdModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the outOfStockThreshold: %1',
                $exception->getMessage()
            ));
        }
        return $outOfStockThresholdModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getById($outOfStockThresholdId)
    {
        $outOfStockThreshold = $this->outOfStockThresholdFactory->create();
        $this->resource->load($outOfStockThreshold, $outOfStockThresholdId);
        if (!$outOfStockThreshold->getId()) {
            throw new NoSuchEntityException(__('OutOfStockThreshold with id "%1" does not exist.', $outOfStockThresholdId)); // phpcs:ignore
        }
        return $outOfStockThreshold->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->outOfStockThresholdCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function delete(
        \I95Dev\OutOfStockThreshold\Api\Data\OutOfStockThresholdInterface $outOfStockThreshold
    ) {
        try {
            $outOfStockThresholdModel = $this->outOfStockThresholdFactory->create();
            $this->resource->load($outOfStockThresholdModel, $outOfStockThreshold->getOutofstockthresholdId());
            $this->resource->delete($outOfStockThresholdModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the OutOfStockThreshold: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($outOfStockThresholdId)
    {
        return $this->delete($this->getById($outOfStockThresholdId));
    }
}
