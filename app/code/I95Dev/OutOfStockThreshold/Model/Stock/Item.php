<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\OutOfStockThreshold\Model\Stock;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface as StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface as StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Catalog Inventory Stock Item Model
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Item extends \Magento\CatalogInventory\Model\Stock\Item
{
    /**
     * Stock item entity code
     */
    const ENTITY = 'cataloginventory_stock_item';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $eventPrefix = 'cataloginventory_stock_item';

    /**
     * @psalm-suppress MissingPropertyType
     */
    const WEBSITE_ID = 'website_id';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getItem() in this case
     *
     * @var string
     */
    protected $eventObject = 'item';

    /**
     * Store model manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var StockConfigurationInterface
     */
    protected $stockConfiguration;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var StockConfigurationInterface
     */
    protected $stockItemRepository;

    /**
     * @var float|false
     */
    protected $qtyIncrements;

    /**
     * Store id
     *
     * @var int|null
     */
    protected $storeId;

    /**
     * Customer group id
     *
     * @var int|null
     */
    protected $customerGroupId;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var ExtensionAttributesFactory
     */
    protected $extensionFactory;
    /**
     * @var AttributeValueFactory
     */
    protected $customAttributeFactory;
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockRegistryInterface $stockRegistry
     * @param StockItemRepositoryInterface $stockItemRepository
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @psalm-suppress InvalidPropertyAssignmentValue
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        StockConfigurationInterface $stockConfiguration,
        StockRegistryInterface $stockRegistry,
        StockItemRepositoryInterface $stockItemRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $customerSession,
            $storeManager,
            $stockConfiguration,
            $stockRegistry,
            $stockItemRepository,
            $resource,
            $resourceCollection,
            $data
        );
        $this->extensionFactory = $extensionFactory;
        $this->customAttributeFactory = $customAttributeFactory;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->stockConfiguration = $stockConfiguration;
        $this->stockRegistry = $stockRegistry;
        $this->stockItemRepository = $stockItemRepository;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magento\CatalogInventory\Model\ResourceModel\Stock\Item::class);
    }
    public function getItemId()
    {
        return $this->_getData(static::ITEM_ID);
    }

    /**
     * Retrieve Website Id
     *
     * @return int
     */
    public function getWebsiteId()
    {
        $websiteId = $this->getData(static::WEBSITE_ID);
        if ($websiteId === null) {
            $websiteId = $this->stockConfiguration->getDefaultScopeId();
        }
        return (int) $websiteId;
    }

    /**
     * Retrieve stock identifier
     *
     * @return int
     */
    public function getStockId()
    {
        $stockId = $this->getData(static::STOCK_ID);
        if ($stockId === null) {
            $stockId = $this->stockRegistry->getStock($this->getWebsiteId())->getStockId();
        }
        return (int) $stockId;
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     */
    public function getStore7()
    {
        return (int) $this->_getData(static::STORE_7);
    }

    /**
     * @param $store_7
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress MissingParamType
     * @psalm-suppress UndefinedConstant
     */
    public function setStore7($store_7)
    {
        return $this->setData(self::STORE_7, $store_7);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress InvalidReturnStatement
     * @psalm-suppress MissingReturnType
     * @psalm-suppress InvalidNullableReturnType
     * @psalm-suppress NullableReturnStatement
     */
    public function getStore9()
    {
        return null === $this->_getData(static::STORE_9) ? null : floatval($this->_getData(static::STORE_9));
    }

    /**
     * @param $store_9
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore9($store_9)
    {
        return $this->setData(self::STORE_9, $store_9);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore11()
    {
        return (int) $this->_getData(static::STORE_11);
    }

    /**
     * @param $store_11
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore11($store_11)
    {
        return $this->setData(self::STORE_11, $store_11);
    }

    /**
     * @return int
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function getStore12()
    {
        return (int) $this->_getData(static::STORE_12);
    }

    /**
     * @param $store_12
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore12($store_12)
    {
        return $this->setData(self::STORE_12, $store_12);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore13()
    {
        return (int) $this->_getData(static::STORE_13);
    }

    /**
     * @param $store_13
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore13($store_13)
    {
        return $this->setData(self::STORE_13, $store_13);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore18()
    {
        return (int) $this->_getData(static::STORE_18);
    }

    /**
     * @param $store_18
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore18($store_18)
    {
        return $this->setData(self::STORE_18, $store_18);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore19()
    {
        return (int) $this->_getData(static::STORE_19);
    }

    /**
     * @param $store_19
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore19($store_19)
    {
        return $this->setData(self::STORE_19, $store_19);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore20()
    {
        return (int) $this->_getData(static::STORE_20);
    }

    /**
     * @param $store_20
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore20($store_20)
    {
        return $this->setData(self::STORE_20, $store_20);
    }

    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore24()
    {
        return (int) $this->_getData(static::STORE_24);
    }

    /**
     * @param $store_24
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore24($store_24)
    {
        return $this->setData(self::STORE_24, $store_24);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore27()
    {
        return (int) $this->_getData(static::STORE_27);
    }

    /**
     * @param $store_27
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore27($store_27)
    {
        return $this->setData(self::STORE_27, $store_27);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore28()
    {
        return (int) $this->_getData(static::STORE_28);
    }

    /**
     * @param $store_28
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore28($store_28)
    {
        return $this->setData(self::STORE_28, $store_28);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore32()
    {
        return (int) $this->_getData(static::STORE_32);
    }

    /**
     * @param $store_32
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore32($store_32)
    {
        return $this->setData(self::STORE_32, $store_32);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore38()
    {
        return (int) $this->_getData(static::STORE_38);
    }

    /**
     * @param $store_38
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore38($store_38)
    {
        return $this->setData(self::STORE_38, $store_38);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore39()
    {
        return (int) $this->_getData(static::STORE_39);
    }

    /**
     * @param $store_39
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore39($store_39)
    {
        return $this->setData(self::STORE_39, $store_39);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore40()
    {
        return (int) $this->_getData(static::STORE_40);
    }

    /**
     * @param $store_40
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore40($store_40)
    {
        return $this->setData(self::STORE_40, $store_40);
    }
    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore42()
    {
        return (int) $this->_getData(static::STORE_42);
    }

    /**
     * @param $store_44
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore44($store_44)
    {
        return $this->setData(self::STORE_44, $store_44);
    }
	/**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore37()
    {
        return (int) $this->_getData(static::STORE_37);
    }

    /**
     * @param $store_37
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore37($store_37)
    {
        return $this->setData(self::STORE_37, $store_37);
    }

	/**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore41()
    {
        return (int) $this->_getData(static::STORE_41);
    }

    /**
     * @param $store_41
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore41($store_41)
    {
        return $this->setData(self::STORE_41, $store_41);
    }

    /**
     * Retrieve Product Id
     *
     * @return int
     */
    public function getStore47()
    {
        return (int) $this->_getData(static::STORE_47);
    }

    /**
     * @param $store_47
     * @return Item|AbstractExtensibleModel
     * @psalm-suppress UndefinedConstant
     * @psalm-suppress MissingParamType
     */
    public function setStore47($store_47)
    {
        return $this->setData(self::STORE_47, $store_47);
    }

    /**
     * Retrieve minimal quantity available for item status in stock
     *
     * @return float
     * @psalm-suppress UndefinedClass
     */
    public function getMinQty()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get(\Magento\Framework\App\Request\Http::class);
        $params = $request->getParam('id');
        $product = $objectManager->create(\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory::class)->create();// phpcs:ignore
        $product->addAttributeToSelect('*');   //semicolon is missing
        $product->addFieldToFilter('entity_id', $params);
        $sku = '';
        if (isset($product->getData()[0]) && $product->getData()[0] != null) {
            $sku = $product->getData()[0]['sku'];

        }

        $minQty = 1;
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) { // phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Location"]; // phpcs:ignore
            //$lochelper = $objectManager->get(\Gourmet\Location\Helper\Data::class);
            //$data = $lochelper->getLocationById($cookie);
            $store = $cookie;
            $storeId = 'store_' . $store;
            $resource = $objectManager->get(\Magento\Framework\App\ResourceConnection::class);
            $connection = $resource->getConnection();
            $outOfStockThreshold = $resource->getConnection()->describeTable('i95dev_outofstock_threshold');
            if (array_key_exists($storeId, $outOfStockThreshold)) {
                $outOfStockThresholdCollection = $objectManager->create(\I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold\Collection::class); // phpcs:ignore
                $outOfStockThresholdCollection->addFieldToFilter('sku', $sku)->load();
                $datas = $outOfStockThresholdCollection->getData();

                if (isset($datas[0]) && $datas[0] != null) {
                    $minQty = $datas[0][$storeId];
                }

            } else {
                if ($this->getUseConfigMinQty()) {
                    $minQty = $this->stockConfiguration->getMinQty($this->getStoreId());
                } else {
                    $minQty = (float)$this->getData(static::MIN_QTY);
                }
            }
        }
        return $minQty;
    }

    /**
     * Retrieve backorders status
     *
     * @return int
     */
    public function getBackorders()
    {
        if ($this->getUseConfigBackorders()) {
            return $this->stockConfiguration->getBackorders($this->getStoreId());
        }
        return (int) $this->getData(static::BACKORDERS);
    }

    /**
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigManageStock()
    {
        return (bool) $this->_getData(static::USE_CONFIG_MANAGE_STOCK);
    }

    /**
     * Retrieve Store Id (product or current)
     *
     * @return int
     */
    public function getStoreId()
    {
        if ($this->storeId === null) {
            $this->storeId = $this->storeManager->getStore()->getId();
        }
        return $this->storeId;
    }
    /**
     * Set minimal quantity available for item status in stock
     *
     * @param float $minQty
     * @return $this
     */
    public function setMinQty($minQty)
    {
        return $this->setData(self::MIN_QTY, $minQty);
    }

    /**
     * @param int $useConfigMinSaleQty
     * @return $this
     */
    public function setUseConfigMinSaleQty($useConfigMinSaleQty)
    {
        return $this->setData(self::USE_CONFIG_MIN_SALE_QTY, $useConfigMinSaleQty);
    }

    /**
     * @param bool $useConfigManageStock
     * @return $this
     */
    public function setUseConfigManageStock($useConfigManageStock)
    {
        return $this->setData(self::USE_CONFIG_MANAGE_STOCK, $useConfigManageStock);
    }

    //@codeCoverageIgnoreEnd
}
