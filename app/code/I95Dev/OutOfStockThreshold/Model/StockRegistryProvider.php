<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\OutOfStockThreshold\Model;

use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Api\StockRepositoryInterface;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockStatusRepositoryInterface;
use Magento\CatalogInventory\Api\Data\StockInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockStatusInterfaceFactory;
use Magento\CatalogInventory\Api\StockCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockStatusCriteriaInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class StockRegistryProvider extends \Magento\CatalogInventory\Model\StockRegistryProvider
{
    /**
     * @var StockRepositoryInterface
     */
    protected $stockRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockItemRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockItemFactory;

    /**
     * @var StockStatusRepositoryInterface
     */
    protected $stockStatusRepository;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockStatusFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockCriteriaFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockItemCriteriaFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $stockStatusCriteriaFactory;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $stockRegistryStorage;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $request;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @param StockRepositoryInterface $stockRepository
     * @param StockInterfaceFactory $stockFactory
     * @param StockItemRepositoryInterface $stockItemRepository
     * @param StockItemInterfaceFactory $stockItemFactory
     * @param StockStatusRepositoryInterface $stockStatusRepository
     * @param StockStatusInterfaceFactory $stockStatusFactory
     * @param StockCriteriaInterfaceFactory $stockCriteriaFactory
     * @param StockItemCriteriaInterfaceFactory $stockItemCriteriaFactory
     * @param StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        StockRepositoryInterface $stockRepository,
        StockInterfaceFactory $stockFactory,
        StockItemRepositoryInterface $stockItemRepository,
        StockItemInterfaceFactory $stockItemFactory,
        StockStatusRepositoryInterface $stockStatusRepository,
        StockStatusInterfaceFactory $stockStatusFactory,
        StockCriteriaInterfaceFactory $stockCriteriaFactory,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resource,
        StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory
    ) {

        $this->stockRepository = $stockRepository;
        $this->stockFactory = $stockFactory;
        $this->stockItemRepository = $stockItemRepository;
        $this->stockItemFactory = $stockItemFactory;
        $this->stockStatusRepository = $stockStatusRepository;
        $this->stockStatusFactory = $stockStatusFactory;
        $this->stockCriteriaFactory = $stockCriteriaFactory;
        $this->stockItemCriteriaFactory = $stockItemCriteriaFactory;
        $this->stockStatusCriteriaFactory = $stockStatusCriteriaFactory;
        $this->request = $request;
        $this->_resource = $resource;
    }

    /**
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockInterface
     * @psalm-suppress UndefinedClass
     */
    public function getStock($scopeId)
    {
        $stock = $this->getStockRegistryStorage()->getStock($scopeId);
        if (null === $stock) {
            $criteria = $this->stockCriteriaFactory->create();
            $criteria->setScopeFilter($scopeId);
            $collection = $this->stockRepository->getList($criteria);
            $stock = current($collection->getItems());
            if ($stock && $stock->getStockId()) {
                $this->getStockRegistryStorage()->setStock($scopeId, $stock);
            } else {
                $stock = $this->stockFactory->create();
            }
        }
        return $stock;
    }

    /**
     * @param int $productId
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @psalm-suppress UndefinedClass
     */
    public function getStockItem($productId, $scopeId)
    {
        $stockItem = $this->getStockRegistryStorage()->getStockItem($productId, $scopeId);
        if (null === $stockItem) {
            $criteria = $this->stockItemCriteriaFactory->create();
            $criteria->setProductsFilter($productId);
            $collection = $this->stockItemRepository->getList($criteria);
            $stockItem = current($collection->getItems());
            if ($stockItem && $stockItem->getItemId()) {
                $this->getStockRegistryStorage()->setStockItem($productId, $scopeId, $stockItem);
            } else {
                $stockItem = $this->stockItemFactory->create();
            }
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sku ='';
        $product = $objectManager->create(\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory::class)->create(); // phpcs:ignore
        $product->addAttributeToSelect('*');   //semicolon is missing
        $product->addFieldToFilter('entity_id', $productId);
        if (isset($product->getData()[0]) && $product->getData()[0] != null) {
            $sku = $product->getData()[0]['sku'];
        }

        $minQty = 1;
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) { // phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Location"]; // phpcs:ignore
            //$lochelper = $objectManager->get(\Gourmet\Location\Helper\Data::class);
            //$data = $lochelper->getLocationById($cookie);
            $store = $cookie;
            $storeId = 'store_' . $store;
            $resource = $objectManager->get(\Magento\Framework\App\ResourceConnection::class);
            $connection = $resource->getConnection();
            $outOfStockThreshold = $resource->getConnection()->describeTable('i95dev_outofstock_threshold');

            if (array_key_exists($storeId, $outOfStockThreshold)) {
                $outOfStockThresholdCollection = $objectManager->create(\I95Dev\OutOfStockThreshold\Model\ResourceModel\OutOfStockThreshold\Collection::class); // phpcs:ignore
                $outOfStockThresholdCollection->addFieldToFilter('sku', $sku)->load();
                $datas = $outOfStockThresholdCollection->getData();

                if (isset($datas[0]) && $datas[0] != null) {
                    $minQty = $datas[0][$storeId];
                }
            }
        }

        $stockItem->setMinQty($minQty);
        return $stockItem;
    }

    /**
     * @param int $productId
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockStatusInterface
     * @psalm-suppress UndefinedClass
     */
    public function getStockStatus($productId, $scopeId)
    {
        $stockStatus = $this->getStockRegistryStorage()->getStockStatus($productId, $scopeId);
        if (null === $stockStatus) {
            $criteria = $this->stockStatusCriteriaFactory->create();
            $criteria->setProductsFilter($productId);
            $criteria->setScopeFilter($scopeId);
            $collection = $this->stockStatusRepository->getList($criteria);
            $stockStatus = current($collection->getItems());
            if ($stockStatus && $stockStatus->getProductId()) {
                $this->getStockRegistryStorage()->setStockStatus($productId, $scopeId, $stockStatus);
            } else {
                $stockStatus = $this->stockStatusFactory->create();
            }
        }
        return $stockStatus;
    }

    /**
     * @return StockRegistryStorage
     * @psalm-suppress UndefinedClass
     * @psalm-suppress DocblockTypeContradiction
     */
    private function getStockRegistryStorage()
    {
        if (null === $this->stockRegistryStorage) {
            $this->stockRegistryStorage = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\CatalogInventory\Model\StockRegistryStorage::class);
        }
        return $this->stockRegistryStorage;
    }
}
