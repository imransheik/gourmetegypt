<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\OutOfStockThreshold\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_i95dev_outofstock_threshold = $setup->getConnection()->newTable($setup->getTable('i95dev_outofstock_threshold'));// phpcs:ignore

        $table_i95dev_outofstock_threshold->addColumn(
            'outofstockthreshold_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'sku'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'pid',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'pid'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_7',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_7'
        );
        $table_i95dev_outofstock_threshold->addColumn(
            'store_9',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_9'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_11',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_11'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_12',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_12'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_13',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_13'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_18',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_18'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_19',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_19'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_20',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_20'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_24',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_24'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_27',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_27'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_28',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_28'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_32',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_32'
        );

        $table_i95dev_outofstock_threshold->addColumn(
            'store_38',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_38'
        );
        $table_i95dev_outofstock_threshold->addColumn(
            'store_39',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_39'
        );
        
        $table_i95dev_outofstock_threshold->addColumn(
            'store_40',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_40'
        );
        
        $table_i95dev_outofstock_threshold->addColumn(
            'store_42',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_42'
        );
		$table_i95dev_outofstock_threshold->addColumn(
            'store_44',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_44'
        );

        $setup->getConnection()->createTable($table_i95dev_outofstock_threshold);
    }
}
