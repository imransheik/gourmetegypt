<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\ProductVisibility\Block\Adminhtml\ProductVisibility\Edit;

use Magento\Backend\Block\Widget\Context;

abstract class GenericButton
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $context;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @psalm-suppress MissingPropertyType
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('productvisibility_id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
