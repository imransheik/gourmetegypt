<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\ProductVisibility\Controller\Adminhtml\ProductVisibility;

class Edit extends \I95Dev\ProductVisibility\Controller\Adminhtml\ProductVisibility
{
    /**
     * @psalm-suppress  MissingPropertyType
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @psalm-suppress UndefinedFunction
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('productvisibility_id');
        $model = $this->_objectManager->create(\I95Dev\ProductVisibility\Model\ProductVisibility::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Productvisibility no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('i95dev_productvisibility_productvisibility', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Productvisibility') : __('New Productvisibility'),
            $id ? __('Edit Productvisibility') : __('New Productvisibility')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Productvisibilitys'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Productvisibility %1', $model->getId()) : __('New Productvisibility')); // phpcs:ignore
        return $resultPage;
    }
}
