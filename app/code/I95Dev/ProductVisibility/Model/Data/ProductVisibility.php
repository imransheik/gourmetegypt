<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\ProductVisibility\Model\Data;

use I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface;
/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ProductVisibility extends \Magento\Framework\Api\AbstractExtensibleObject implements ProductVisibilityInterface
{

    /**
     * Get productvisibility_id
     * @return string|null
     */
    public function getProductvisibilityId()
    {
        return $this->_get(self::PRODUCTVISIBILITY_ID);
    }

    /**
     * Set productvisibility_id
     * @param string $productvisibilityId
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setProductvisibilityId($productvisibilityId)
    {
        return $this->setData(self::PRODUCTVISIBILITY_ID, $productvisibilityId);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface|null
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidReturnStatement
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface $extensionAttributes
     * @return $this
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidArgument
     */
    public function setExtensionAttributes(
        \I95Dev\ProductVisibility\Api\Data\ProductVisibilityExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get pid
     * @return string|null
     */
    public function getPid()
    {
        return $this->_get(self::PID);
    }

    /**
     * Set pid
     * @param string $pid
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setPid($pid)
    {
        return $this->setData(self::PID, $pid);
    }

    /**
     * Get store_7
     * @return string|null
     */
    public function getStore7()
    {
        return $this->_get(self::STORE_7);
    }

    /**
     * Set store_7
     * @param string $store7
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore7($store7)
    {
        return $this->setData(self::STORE_7, $store7);
    }
		/**
     * Get store_9
     * @return string|null
     */
    public function getStore9()
    {
        return $this->_get(self::STORE_9);
    }

    /**
     * Set store_9
     * @param string $store9
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore9($store9)
    {
        return $this->setData(self::STORE_9, $store9);
    }
    /**
     * Get store_11
     * @return string|null
     */
    public function getStore11()
    {
        return $this->_get(self::STORE_11);
    }

    /**
     * Set store_11
     * @param string $store11
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore11($store11)
    {
        return $this->setData(self::STORE_11, $store11);
    }

    /**
     * Get store_12
     * @return string|null
     */
    public function getStore12()
    {
        return $this->_get(self::STORE_12);
    }

    /**
     * Set store_12
     * @param string $store12
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore12($store12)
    {
        return $this->setData(self::STORE_12, $store12);
    }

    /**
     * Get store_13
     * @return string|null
     */
    public function getStore13()
    {
        return $this->_get(self::STORE_13);
    }

    /**
     * Set store_13
     * @param string $store13
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore13($store13)
    {
        return $this->setData(self::STORE_13, $store13);
    }

    /**
     * Get store_18
     * @return string|null
     */
    public function getStore18()
    {
        return $this->_get(self::STORE_18);
    }

    /**
     * Set store_18
     * @param string $store18
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore18($store18)
    {
        return $this->setData(self::STORE_18, $store18);
    }

    /**
     * Get store_19
     * @return string|null
     */
    public function getStore19()
    {
        return $this->_get(self::STORE_19);
    }

    /**
     * Set store_19
     * @param string $store19
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore19($store19)
    {
        return $this->setData(self::STORE_19, $store19);
    }

    /**
     * Get store_20
     * @return string|null
     */
    public function getStore20()
    {
        return $this->_get(self::STORE_20);
    }

    /**
     * Set store_20
     * @param string $store20
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore20($store20)
    {
        return $this->setData(self::STORE_20, $store20);
    }

    /**
     * Get store_24
     * @return string|null
     */
    public function getStore24()
    {
        return $this->_get(self::STORE_24);
    }

    /**
     * Set store_24
     * @param string $store24
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore24($store24)
    {
        return $this->setData(self::STORE_24, $store24);
    }

    /**
     * Get store_27
     * @return string|null
     */
    public function getStore27()
    {
        return $this->_get(self::STORE_27);
    }

    /**
     * Set store_27
     * @param string $store27
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore27($store27)
    {
        return $this->setData(self::STORE_27, $store27);
    }

    /**
     * Get store_28
     * @return string|null
     */
    public function getStore28()
    {
        return $this->_get(self::STORE_28);
    }

    /**
     * Set store_28
     * @param string $store28
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore28($store28)
    {
        return $this->setData(self::STORE_28, $store28);
    }

    /**
     * Get store_32
     * @return string|null
     */
    public function getStore32()
    {
        return $this->_get(self::STORE_32);
    }

    /**
     * Set store_32
     * @param string $store32
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore32($store32)
    {
        return $this->setData(self::STORE_32, $store32);
    }
	/**
     * Get store_37
     * @return string|null
     */
    public function getStore37()
    {
        return $this->_get(self::STORE_37);
    }

    /**
     * Set store_37
     * @param string $store37
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore37($store37)
    {
        return $this->setData(self::STORE_37, $store37);
    }
    /**
     * Get store_38
     * @return string|null
     */
    public function getStore38()
    {
        return $this->_get(self::STORE_38);
    }

    /**
     * Set store_38
     * @param string $store38
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore38($store38)
    {
        return $this->setData(self::STORE_38, $store38);
    }
	/**
     * Get store_39
     * @return string|null
     */
    public function getStore39()
    {
        return $this->_get(self::STORE_39);
    }

    /**
     * Set store_39
     * @param string $store39
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore39($store39)
    {
        return $this->setData(self::STORE_39, $store39);
    }
	/**
     * Get store_40
     * @return string|null
     */
    public function getStore40()
    {
        return $this->_get(self::STORE_40);
    }

    /**
     * Set store_40
     * @param string $store40
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore40($store40)
    {
        return $this->setData(self::STORE_40, $store40);
    }
	/**
     * Get store_41
     * @return string|null
     */
    public function getStore41()
    {
        return $this->_get(self::STORE_41);
    }

    /**
     * Set store_41
     * @param string $store41
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore41($store41)
    {
        return $this->setData(self::STORE_41, $store41);
    }
	
	/**
     * Get store_42
     * @return string|null
     */
    public function getStore42()
    {
        return $this->_get(self::STORE_42);
    }

    /**
     * Set store_42
     * @param string $store42
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore42($store42)
    {
        return $this->setData(self::STORE_42, $store42);
    }
	/**
     * Get store_44
     * @return string|null
     */
    public function getStore44()
    {
        return $this->_get(self::STORE_44);
    }

    /**
     * Set store_44
     * @param string $store44
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore44($store44)
    {
        return $this->setData(self::STORE_44, $store44);
    }

    /**
     * Get store_47
     * @return string|null
     */
    public function getStore47()
    {
        return $this->_get(self::STORE_47);
    }

    /**
     * Set store_47
     * @param string $store47
     * @return \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface
     */
    public function setStore47($store47)
    {
        return $this->setData(self::STORE_47, $store47);
    }
}
