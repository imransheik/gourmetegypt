<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\ProductVisibility\Model;
/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Product extends \Magento\Catalog\Model\Product
{
    public function getStatus()
    {
        $status = $this->_getData(self::STATUS);
        $sku = $this->_getData(self::SKU);
        $pid = []; 
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) { // phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Location"]; // phpcs:ignore
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //$lochelper = $objectManager->get(\Gourmet\Location\Helper\Data::class);
            //$data = $lochelper->getLocationById($cookie);
            $store = $cookie;
            $storeId = 'store_' . $cookie;
            $productvisibility = $this->_resource->getConnection()->describeTable('i95dev_productvisibility_productvisibility'); // phpcs:ignore
			
            if (array_key_exists($storeId, $productvisibility)) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $ProductVisibilityCollection = $objectManager->create(\I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection::class); // phpcs:ignore
                $ProductVisibilityCollection->addFieldToFilter('sku', $sku)->load();
                $datas = $ProductVisibilityCollection->getData();
                $pid = [];
                foreach ($datas as $data) {
					if($data[$storeId] == 0){
						return \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
					}
                }
            }
        }
        return $status !== null ? $status : \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED; // phpcs:ignore
    }
}
