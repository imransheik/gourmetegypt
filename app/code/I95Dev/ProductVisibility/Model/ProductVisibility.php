<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\ProductVisibility\Model;

use Magento\Framework\Api\DataObjectHelper;
use I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterfaceFactory;
use I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface;
/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ProductVisibility extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $productvisibilityDataFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_eventPrefix = 'i95dev_productvisibility_productvisibility';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ProductVisibilityInterfaceFactory $productvisibilityDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility $resource
     * @param \I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection $resourceCollection
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ProductVisibilityInterfaceFactory $productvisibilityDataFactory,
        DataObjectHelper $dataObjectHelper,
        \I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility $resource,
        \I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection $resourceCollection,
        array $data = []
    ) {
        $this->productvisibilityDataFactory = $productvisibilityDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve productvisibility model with productvisibility data
     * @return ProductVisibilityInterface
     */
    public function getDataModel()
    {
        $productvisibilityData = $this->getData();
        
        $productvisibilityDataObject = $this->productvisibilityDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $productvisibilityDataObject,
            $productvisibilityData,
            ProductVisibilityInterface::class
        );
        
        return $productvisibilityDataObject;
    }
}
