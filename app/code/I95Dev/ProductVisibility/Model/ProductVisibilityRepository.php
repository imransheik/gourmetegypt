<?php
/**
 * A Magento 2 module named I95Dev/ProductVisibility
 * Copyright (C) 2019
 *
 * This file included in I95Dev/ProductVisibility is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace I95Dev\ProductVisibility\Model;

use I95Dev\ProductVisibility\Api\Data\ProductVisibilitySearchResultsInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use I95Dev\ProductVisibility\Api\ProductVisibilityRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\CollectionFactory as ProductVisibilityCollectionFactory; // phpcs:ignore
use I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility as ResourceProductVisibility;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterfaceFactory;

class ProductVisibilityRepository implements ProductVisibilityRepositoryInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $searchResultsFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $collectionProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $productVisibilityCollectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataProductVisibilityFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensibleDataObjectConverter;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $storeManager;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $productVisibilityFactory;

    /**
     * @param ResourceProductVisibility $resource
     * @param ProductVisibilityFactory $productVisibilityFactory
     * @param ProductVisibilityInterfaceFactory $dataProductVisibilityFactory
     * @param ProductVisibilityCollectionFactory $productVisibilityCollectionFactory
     * @param ProductVisibilitySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ResourceProductVisibility $resource,
        ProductVisibilityFactory $productVisibilityFactory,
        ProductVisibilityInterfaceFactory $dataProductVisibilityFactory,
        ProductVisibilityCollectionFactory $productVisibilityCollectionFactory,
        ProductVisibilitySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->productVisibilityFactory = $productVisibilityFactory;
        $this->productVisibilityCollectionFactory = $productVisibilityCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataProductVisibilityFactory = $dataProductVisibilityFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function save(
        \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface $productVisibility
    ) {
        /* if (empty($productVisibility->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $productVisibility->setStoreId($storeId);
        } */
        
        $productVisibilityData = $this->extensibleDataObjectConverter->toNestedArray(
            $productVisibility,
            [],
            \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface::class
        );
        
        $productVisibilityModel = $this->productVisibilityFactory->create()->setData($productVisibilityData);
        
        try {
            $this->resource->save($productVisibilityModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the productVisibility: %1',
                $exception->getMessage()
            ));
        }
        return $productVisibilityModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getById($productVisibilityId)
    {
        $productVisibility = $this->productVisibilityFactory->create();
        $this->resource->load($productVisibility, $productVisibilityId);
        if (!$productVisibility->getId()) {
            throw new NoSuchEntityException(__('ProductVisibility with id "%1" does not exist.', $productVisibilityId));
        }
        return $productVisibility->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->productVisibilityCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function delete(
        \I95Dev\ProductVisibility\Api\Data\ProductVisibilityInterface $productVisibility
    ) {
        try {
            $productVisibilityModel = $this->productVisibilityFactory->create();
            $this->resource->load($productVisibilityModel, $productVisibility->getProductvisibilityId());
            $this->resource->delete($productVisibilityModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ProductVisibility: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($productVisibilityId)
    {
        return $this->delete($this->getById($productVisibilityId));
    }
}
