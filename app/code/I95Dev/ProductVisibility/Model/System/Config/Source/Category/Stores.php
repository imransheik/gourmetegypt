<?php


namespace I95Dev\ProductVisibility\Model\System\Config\Source\Category;

class Stores extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @return array
     * @psalm-suppress UndefinedFunction
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
				['value' => '7', 'label' => __('Store 7')],
				['value' => '9', 'label' => __('Store 9')],
                ['value' => '11', 'label' => __('Store 11')],
                ['value' => '12', 'label' => __('Store 12')],
                ['value' => '13', 'label' => __('Store 13')],
                ['value' => '18', 'label' => __('Store 18')],
                ['value' => '19', 'label' => __('Store 19')],
                ['value' => '20', 'label' => __('Store 20')],
                ['value' => '24', 'label' => __('Store 24')],
                ['value' => '27', 'label' => __('Store 27')],
                ['value' => '28', 'label' => __('Store 28')],
                ['value' => '32', 'label' => __('Store 32')],
                ['value' => '37', 'label' => __('Store 37')],
                ['value' => '38', 'label' => __('Store 38')],
                ['value' => '39', 'label' => __('Store 39')],
                ['value' => '40', 'label' => __('Store 40')],
                ['value' => '41', 'label' => __('Store 41')],
                ['value' => '42', 'label' => __('Store 42')],
                ['value' => '44', 'label' => __('Store 44')],
                ['value' => '46', 'label' => __('Store 46')],
                ['value' => '47', 'label' => __('Store 47')]
            ];
        }

        return $this->_options;
    }
}
