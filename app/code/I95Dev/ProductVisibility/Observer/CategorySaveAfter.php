<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\ProductVisibility\Observer;

use Magento\Catalog\Model\Category;
use Magento\Framework\Event\ObserverInterface;

class Categorysaveafter implements ObserverInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
//      /** @var Category $category */
        $category = $observer->getEvent()->getCategory();
        /**
         * Create Permanent Redirect for old URL key
         */
       if ($category->getEntityId() && $this->_request->getPost('store_visibility')) {
           $stores= implode(',', $this->_request->getPost('store_visibility'));
           $category->setData('store_visibility', $stores);

       }
          
//        var_dump($category->getData('store_visibility')); exit;
    }
}
