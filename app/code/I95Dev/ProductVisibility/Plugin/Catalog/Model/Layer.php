<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Groupcat
 */


namespace I95Dev\ProductVisibility\Plugin\Catalog\Model;

class Layer
{
 
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * Restrict constructor.
     *
     * @param \Amasty\Groupcat\Model\ProductRuleProvider                  $ruleProvider
     * @param \Amasty\Groupcat\Helper\Data                                $helper
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Prepare Product Collection for layred Navigation.
     * Add restricted product filter to search engine.
     * In search_request.xml added filter for amasty_groupcat_entity_id
     *
     * @param \Magento\Catalog\Model\Layer                            $subject
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforePrepareProductCollection($subject, $collection)
    {

        return [$collection];

        $pid = [];
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) {
            $cookie = $_COOKIE["Gourmet_Location"];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //$lochelper = $objectManager->get('\Gourmet\Location\Helper\Data');
            //$data = $lochelper->getLocationById($cookie);
            $store = $cookie;
            $storeId = 'store_'.$store;
            $productvisibility = $this->_resource->getConnection()->describeTable('i95dev_productvisibility_productvisibility');

            if (array_key_exists($storeId, $productvisibility)) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $ProductVisibilityCollection = $objectManager->create('I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection');
                $ProductVisibilityCollection->addFieldToFilter($storeId, 0)->load();
                $datas = $ProductVisibilityCollection->getData();
                $pid = [];
                foreach ($datas as $data) {
                    $pid[] =  $data['pid'];

                }

            }
        }

        $productIds = $pid;
        if ($productIds) {
            // add filter to product fulltext search | catalog product collection
            $collection->addFieldToFilter('amasty_groupcat_entity_id', ['nin' => $productIds]);

            return [$collection];
        }

        return null;
    }
}
