<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Groupcat
 */


namespace I95Dev\ProductVisibility\Plugin\Catalog\Model\ResourceModel\Product;

use Magento\Catalog\Model\Product;

class Collection
{

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $helperData;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $collection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_resource;
    /**
     * Collection constructor.
     *
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Registry $coreRegistry,
        \Gourmet\Location\Helper\Data $helperData,
        \I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection $collection
    ) {
        $this->_resource = $resource;
        $this->coreRegistry = $coreRegistry;
        $this->helperData = $helperData;
        $this->collection = $collection;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @param  $printQuery
     * @param   $logQuery
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     */
    public function beforeLoad(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject,
        $printQuery = null,
        $logQuery = null
    ) {
        $this->addRestrictedProductFilter($subject, $subject->getSelect());
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @param \Magento\Framework\DB\Select  $productSelect
     *
     * @return \Magento\Framework\DB\Select
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSelect(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject,
        \Magento\Framework\DB\Select $productSelect
    ) {
        $this->addRestrictedProductFilter($subject, $productSelect);
        return $productSelect;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @psalm-suppress MissingReturnType
     */
    public function beforeGetSize(\Magento\Catalog\Model\ResourceModel\Product\Collection $subject)
    {
        $this->addRestrictedProductFilter($subject, $subject->getSelect());
    }

    /**
     * @psalm-suppress MissingReturnType
     * @psalm-suppress MissingParamType
     * @psalm-suppress InvalidArgument
     *
     */
    protected function addRestrictedProductFilter(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject,
        \Magento\Framework\DB\Select $productSelect
    ) {

        $pid = [];
        if (isset($_COOKIE['Gourmet_Location']) && !empty($_COOKIE['Gourmet_Location'])) { // phpcs:ignore
            $cookie = $_COOKIE["Gourmet_Location"]; // phpcs:ignore
            //$lochelper = $this->helperData;
            //$data = $lochelper->getLocationById($cookie);
            $store = $cookie;
            $storeId = 'store_'.$store;
            $productvisibility = $this->_resource->getConnection()->describeTable('i95dev_productvisibility_productvisibility'); // phpcs:ignore

            if (array_key_exists($storeId, $productvisibility)) {
//                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//                $ProductVisibilityCollection = $objectManager->create(\I95Dev\ProductVisibility\Model\ResourceModel\ProductVisibility\Collection::class); // phpcs:ignore
                $ProductVisibilityCollection = $this->collection;
                $ProductVisibilityCollection->addFieldToFilter($storeId, 0)->load();
                $datas = $ProductVisibilityCollection->getData();

                foreach ($datas as $data) {
                    $pid[] =  $data['entity_id'];
                }
            }
        }
        $productIds =  $pid;
        if ($productIds && $subject->getIdFieldName() == 'entity_id') {
            $idField = $subject::MAIN_TABLE_ALIAS . '.entity_id';
            $productSelect->where($idField . ' NOT IN (?)', $productIds);
        }
    }
}
