<?php


namespace I95Dev\ProductVisibility\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $eavSetupFactory;

    /**
     * Init.
     * @param EavSetupFactory $eavSetupFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dataSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $dataSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'store_visibility',
            [
                'label'                    => 'Visible to Stores',
                'note'                     => 'Baased on selection category will be visible',
                'type'                     => 'varchar',
                'input'                    => 'multiselect',
                'source'                   => 'I95Dev\ProductVisibility\Model\System\Config\Source\Category\Stores',
                'sort_order'               => 30,
                'visible'                  => true,
                'required'                 => false,
                'searchable'               => false,
                'filterable'               => false,
                'comparable'               => false,
                'user_defined'             => true,
                'visible_on_front'         => true,
                'wysiwyg_enabled'          => false,
                'is_html_allowed_on_front' => false,
                'global'                   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group'                    => 'General Information',
            ]
        );
		$i95dev_productvisibility_productvisibility = $setup->getConnection()->newTable($setup->getTable('i95dev_productvisibility_productvisibility'));// phpcs:ignore

        $i95dev_productvisibility_productvisibility->addColumn(
            'productvisibility_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'sku'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'pid',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'pid'
        );
		$i95dev_productvisibility_productvisibility->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'entity_id'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_7',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_7'
        );
        $i95dev_productvisibility_productvisibility->addColumn(
            'store_9',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_9'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_11',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_11'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_12',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_12'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_13',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_13'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_18',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_18'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_19',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_19'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_20',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_20'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_24',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_24'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_27',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_27'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_28',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_28'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_32',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_32'
        );

        $i95dev_productvisibility_productvisibility->addColumn(
            'store_38',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_38'
        );
        
        $i95dev_productvisibility_productvisibility->addColumn(
            'store_39',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_39'
        );
        
        $i95dev_productvisibility_productvisibility->addColumn(
            'store_40',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_40'
        );
        
        $i95dev_productvisibility_productvisibility->addColumn(
            'store_42',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_42'
        );
		$i95dev_productvisibility_productvisibility->addColumn(
            'store_44',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'store_44'
        );

        $setup->getConnection()->createTable($i95dev_productvisibility_productvisibility);
    }
}
