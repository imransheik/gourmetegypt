<?php


namespace I95Dev\RmsConnect\Api\Data;

interface RmsconnectInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const RMS_ID = 'rms_id';
    const RMSCONNECT_ID = 'rmsconnect_id';
    const UPDATED_AT = 'updated_at';
    const TYPE = 'type';
    const CUSTOMER_ID = 'customer_id';
    const CREATED_AT = 'created_at';
    const MESSAGE = 'message';

    /**
     * Get rmsconnect_id
     * @return string|null
     */
    public function getRmsconnectId();

    /**
     * Set rmsconnect_id
     * @param string $rmsconnectId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setRmsconnectId($rmsconnectId);

    /**
     * Get rms_id
     * @return string|null
     */
    public function getRmsId();

    /**
     * Set rms_id
     * @param string $rmsId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setRmsId($rmsId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface $extensionAttributes
    );

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get message
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     * @param string $message
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setMessage($message);

    /**
     * Get type
     * @return string|null
     */
    public function getType();

    /**
     * Set type
     * @param string $type
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setType($type);
}
