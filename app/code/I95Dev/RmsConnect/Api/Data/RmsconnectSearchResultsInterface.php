<?php


namespace I95Dev\RmsConnect\Api\Data;

interface RmsconnectSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get rmsconnect list.
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface[]
     */
    public function getItems();

    /**
     * Set rms_id list.
     * @param \I95Dev\RmsConnect\Api\Data\RmsconnectInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
