<?php


namespace I95Dev\RmsConnect\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RmsconnectRepositoryInterface
{

    /**
     * Save rmsconnect
     * @param \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
    );

    /**
     * Retrieve rmsconnect
     * @param string $rmsconnectId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($rmsconnectId);

    /**
     * Retrieve rmsconnect matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete rmsconnect
     * @param \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
    );

    /**
     * Delete rmsconnect by ID
     * @param string $rmsconnectId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($rmsconnectId);
}
