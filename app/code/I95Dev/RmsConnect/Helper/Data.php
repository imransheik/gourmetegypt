<?php

namespace I95Dev\RmsConnect\Helper;

/**
 * Returns base helper
 * @psalm-suppress  PropertyNotSetInConstructor
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \I95Dev\Fcm\lib\FcmAPI
     */
    protected $_api;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_fcmCollection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_guestCollection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_customerRepo;

    /**
     * Data constructor.
     * @param \I95Dev\Fcm\lib\FcmAPI $api
     * @param \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection
     * @param \Gourmet\Alert\Model\ResourceModel\GuestAlert\CollectionFactory $guestCollection
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \I95Dev\Fcm\lib\FcmAPI $api,
        \I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory $fcmCollection,
        \Gourmet\Alert\Model\ResourceModel\GuestAlert\CollectionFactory $guestCollection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo
    ) {
        $this->_api = $api;
        $this->_fcmCollection = $fcmCollection;
        $this->_guestCollection = $guestCollection;
        $this->_customerRepo = $customerRepo;
    }

    /**
     * Order status change notification
     * @psalm-suppress MissingReturnType
     */
    public function sendDataToRms()
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
}
