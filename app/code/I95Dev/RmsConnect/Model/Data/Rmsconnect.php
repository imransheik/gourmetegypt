<?php


namespace I95Dev\RmsConnect\Model\Data;

use I95Dev\RmsConnect\Api\Data\RmsconnectInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Rmsconnect extends \Magento\Framework\Api\AbstractExtensibleObject implements RmsconnectInterface
{

    /**
     * Get rmsconnect_id
     * @return string|null
     */
    public function getRmsconnectId()
    {
        return $this->_get(self::RMSCONNECT_ID);
    }

    /**
     * Set rmsconnect_id
     * @param string $rmsconnectId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setRmsconnectId($rmsconnectId)
    {
        return $this->setData(self::RMSCONNECT_ID, $rmsconnectId);
    }

    /**
     * Get rms_id
     * @return string|null
     */
    public function getRmsId()
    {
        return $this->_get(self::RMS_ID);
    }

    /**
     * Set rms_id
     * @param string $rmsId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setRmsId($rmsId)
    {
        return $this->setData(self::RMS_ID, $rmsId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface|null
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress InvalidReturnStatement
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface $extensionAttributes
     * @return $this
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidArgument
     */
    public function setExtensionAttributes(
        \I95Dev\RmsConnect\Api\Data\RmsconnectExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customerId
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get message
     * @return string|null
     */
    public function getMessage()
    {
        return $this->_get(self::MESSAGE);
    }

    /**
     * Set message
     * @param string $message
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get type
     * @return string|null
     */
    public function getType()
    {
        return $this->_get(self::TYPE);
    }

    /**
     * Set type
     * @param string $type
     * @return \I95Dev\RmsConnect\Api\Data\RmsconnectInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
}
