<?php


namespace I95Dev\RmsConnect\Model\ResourceModel;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Rmsconnect extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('i95dev_rmsconnect_rmsconnect', 'rmsconnect_id');
    }
}
