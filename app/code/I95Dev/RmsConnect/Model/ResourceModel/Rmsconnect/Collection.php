<?php


namespace I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \I95Dev\RmsConnect\Model\Rmsconnect::class,
            \I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect::class
        );
    }
}
