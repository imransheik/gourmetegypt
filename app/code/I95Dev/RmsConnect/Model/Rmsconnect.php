<?php


namespace I95Dev\RmsConnect\Model;

use Magento\Framework\Api\DataObjectHelper;
use I95Dev\RmsConnect\Api\Data\RmsconnectInterface;
use I95Dev\RmsConnect\Api\Data\RmsconnectInterfaceFactory;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Rmsconnect extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $rmsconnectDataFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var string
     */
    protected $_eventPrefix = 'i95dev_rmsconnect_rmsconnect';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RmsconnectInterfaceFactory $rmsconnectDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect $resource
     * @param \I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect\Collection $resourceCollection
     * @param array $data
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        RmsconnectInterfaceFactory $rmsconnectDataFactory,
        DataObjectHelper $dataObjectHelper,
        \I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect $resource,
        \I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect\Collection $resourceCollection,
        array $data = []
    ) {
        $this->rmsconnectDataFactory = $rmsconnectDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve rmsconnect model with rmsconnect data
     * @return RmsconnectInterface
     * @psalm-suppress UndefinedClass
     */
    public function getDataModel()
    {
        $rmsconnectData = $this->getData();
        
        $rmsconnectDataObject = $this->rmsconnectDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $rmsconnectDataObject,
            $rmsconnectData,
            RmsconnectInterface::class
        );
        
        return $rmsconnectDataObject;
    }
}
