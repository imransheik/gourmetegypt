<?php


namespace I95Dev\RmsConnect\Model;

use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect as ResourceRmsconnect;
use I95Dev\RmsConnect\Model\ResourceModel\Rmsconnect\CollectionFactory as RmsconnectCollectionFactory;
use I95Dev\RmsConnect\Api\Data\RmsconnectInterfaceFactory;
use I95Dev\RmsConnect\Api\RmsconnectRepositoryInterface;
use I95Dev\RmsConnect\Api\Data\RmsconnectSearchResultsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class RmsconnectRepository implements RmsconnectRepositoryInterface
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $rmsconnectFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $rmsconnectCollectionFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectHelper;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $collectionProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataObjectProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $dataRmsconnectFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $resource;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensibleDataObjectConverter;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $searchResultsFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * @psalm-suppress MissingPropertyType
     */
    private $storeManager;

    /**
     * @param ResourceRmsconnect $resource
     * @param RmsconnectFactory $rmsconnectFactory
     * @param RmsconnectInterfaceFactory $dataRmsconnectFactory
     * @param RmsconnectCollectionFactory $rmsconnectCollectionFactory
     * @param RmsconnectSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ResourceRmsconnect $resource,
        RmsconnectFactory $rmsconnectFactory,
        RmsconnectInterfaceFactory $dataRmsconnectFactory,
        RmsconnectCollectionFactory $rmsconnectCollectionFactory,
        RmsconnectSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->rmsconnectFactory = $rmsconnectFactory;
        $this->rmsconnectCollectionFactory = $rmsconnectCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRmsconnectFactory = $dataRmsconnectFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function save(
        \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
    ) {
        /* if (empty($rmsconnect->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $rmsconnect->setStoreId($storeId);
        } */
        
        $rmsconnectData = $this->extensibleDataObjectConverter->toNestedArray(
            $rmsconnect,
            [],
            \I95Dev\RmsConnect\Api\Data\RmsconnectInterface::class
        );
        
        $rmsconnectModel = $this->rmsconnectFactory->create()->setData($rmsconnectData);
        
        try {
            $this->resource->save($rmsconnectModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the rmsconnect: %1',
                $exception->getMessage()
            ));
        }
        return $rmsconnectModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getById($rmsconnectId)
    {
        $rmsconnect = $this->rmsconnectFactory->create();
        $this->resource->load($rmsconnect, $rmsconnectId);
        if (!$rmsconnect->getId()) {
            throw new NoSuchEntityException(__('rmsconnect with id "%1" does not exist.', $rmsconnectId));
        }
        return $rmsconnect->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->rmsconnectCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \I95Dev\RmsConnect\Api\Data\RmsconnectInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     * @psalm-suppress UndefinedFunction
     */
    public function delete(
        \I95Dev\RmsConnect\Api\Data\RmsconnectInterface $rmsconnect
    ) {
        try {
            $rmsconnectModel = $this->rmsconnectFactory->create();
            $this->resource->load($rmsconnectModel, $rmsconnect->getRmsconnectId());
            $this->resource->delete($rmsconnectModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the rmsconnect: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($rmsconnectId)
    {
        return $this->delete($this->getById($rmsconnectId));
    }
}
