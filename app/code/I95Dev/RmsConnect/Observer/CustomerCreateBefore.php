<?php

namespace I95Dev\RmsConnect\Observer;

class CustomerCreateBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Customer save after constructor.
     *
     */

    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_rmsConnection;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;

    /**
     * CustomerCreateBefore constructor.
     * @param \I95Dev\RmsConnect\lib\RmsConnection $rmsConnection
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \I95Dev\RmsConnect\lib\RmsConnection $rmsConnection,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_rmsConnection = $rmsConnection;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->_request = $request;
        $this->_coreSession = $coreSession;
        $this->messageManager = $messageManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedFunction
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!preg_match("/^[\p{L} ]+$/u", $this->_request->getPost("firstname")) ||
            !preg_match("/^[\p{L} ]+$/u", $this->_request->getPost("lastname"))) {
            $this->messageManager->addErrorMessage(__("Please enter the valid characters in first or last name."));
            $redirectionUrl = $this->url->getUrl('customer/account/create');
            $this->responseFactory->create()->setRedirect($redirectionUrl)->sendResponse();
            // @codingStandardsIgnoreStart
            exit;
            // @codingStandardsIgnoreEnd
        }
    }
}
