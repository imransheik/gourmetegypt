<?php
namespace I95Dev\RmsConnect\Observer;
class CustomerSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \I95Dev\Fcm\Helper\Data
     */
    protected $_rmsConnection;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_timezoneInterface;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_request;

    /**
     * CustomerSaveAfter constructor.
     * @param \I95Dev\RmsConnect\lib\RmsConnection $rmsConnection
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @psalm-suppress InvalidPropertyAssignmentValue
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \I95Dev\RmsConnect\lib\RmsConnection $rmsConnection,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \I95Dev\RmsConnect\Model\Rmsconnect $rmsConnect
    ) {
        $this->_rmsConnection = $rmsConnection;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_request = $request;
        $this->_coreSession = $coreSession;
        $this->rmsConnect = $rmsConnect;
    }

    /**
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress PossiblyUndefinedVariable
     * @psalm-suppress InvalidReturnType
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $today = $this->_timezoneInterface->date()->format('m/d/y H:i:s');
        $customer = $observer->getEvent()->getData("customer");
        $rmslog = [];
        $rmslog['customer_id'] = $customer->getId();

//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $CustomerEmail = $customer->getEmail();

        $billAddress = '';
        $billCompany = '';
        $billCity = '';
        $billState = '';
        if (strpos($this->_request->getRequestUri(), 'index.php/rest/V1/customers')) { //phpcs:ignore
            return $this;
        }

        if ($customer->getDefaultBillingAddress()) {
            $DefaultBillingAddress = $customer->getDefaultBillingAddress()->getData();
            $billAddress = (isset($DefaultBillingAddress['street'])) ? $DefaultBillingAddress['street']." ". $DefaultBillingAddress["apartment"]: ''; //phpcs:ignore
            $billCompany = (isset($DefaultBillingAddress['company'])) ? $DefaultBillingAddress['company'] : '';
            $billCity = (isset($DefaultBillingAddress['city'])) ? $DefaultBillingAddress['city'] : '';
            $billState = (isset($DefaultBillingAddress['region'])) ? $DefaultBillingAddress['region'] : '';
        }
        $firstName=$customer->getDataModel()->getFirstName();
        $lastName=$customer->getDataModel()->getLastName();
        if ($this->_request->getFullActionName()=="sociallogin_popup_createAcc"
            || $this->_request->getFullActionName()=="customer_account_createpost") {
            $primaryMobile = $this->_coreSession->getPrimaryMobile();
            $isdCode = $this->_coreSession->getIsd();
            $prefered_contact = $this->_request->getPost("prefered_contact");
            $prefered_language = $this->_request->getPost("prefered_language");
            if ($isdCode=="2") {
                $mobile = '+' . $isdCode . $primaryMobile;
            } else {
                $mobile =  $isdCode.$primaryMobile;
            }
            if ($prefered_contact == "211") {
                $preferedContact = "Email";
            } elseif ($prefered_contact == "210") {
                $preferedContact = "SMS";
            } else {
                $preferedContact = "Call";
            }

            if ($prefered_language == "213") {
                $preferedLanguage = "AR";
            } else {
                $preferedLanguage = "EN";
            }

        } else {
            $primaryMobile='';
            if ($customer->getDataModel()->getCustomAttribute("primary_mobile_number")) {
                $primaryMobile = $customer->getDataModel()->getCustomAttribute("primary_mobile_number")->getValue();
            }
            $isdCode='';
            if ($customer->getDataModel()->getCustomAttribute("isd_code")) {
                $isdCode = $customer->getDataModel()->getCustomAttribute("isd_code")->getValue();
            }

            if ($isdCode=="2") {
                $mobile = '+' . $isdCode . $primaryMobile;
            } else {
                $mobile =  $isdCode.$primaryMobile;
            }

            $prefered_contact='';
            if ($customer->getDataModel()->getCustomAttribute("prefered_contact")) {
                $prefered_contact = $customer->getDataModel()->getCustomAttribute("prefered_contact")->getValue();
            }
            if ($prefered_contact == "211") {
                $preferedContact = "Email";
            } elseif ($prefered_contact == "210") {
                $preferedContact = "SMS";
            } else {
                $preferedContact = "Call";
            }
            $prefered_language='';
            if ($customer->getDataModel()->getCustomAttribute("prefered_language")) {
                $prefered_language = $customer->getDataModel()->getCustomAttribute("prefered_language")->getValue();
            }
            if ($prefered_language == "213") {
                $preferedLanguage = "AR";
            } else {
                $preferedLanguage = "EN";
            }
        }
        $remove = [",", "!", "/"];
        $billAddress2 = '';

        $LoyaltyJoinSource = 'Website';
        $loyaltyPointsValue = '';
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/rms-cust.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info('----1');
        $conn = $this->_rmsConnection->connectToRms();
// @codingStandardsIgnoreStart
        if (($resultByEmail = sqlsrv_query($conn, "SELECT ID,Magento_Customer_ID,
             Address FROM Mcustomer WHERE (EmailAddress = '" . $CustomerEmail . "')")) !== false) {
            $resultByEmailObj=sqlsrv_fetch_object($resultByEmail);
            if (count($resultByEmailObj) > 0) {
                $customerObj=$resultByEmailObj;
            }else{
                if (($resultByPhone = sqlsrv_query($conn, "SELECT ID,Magento_Customer_ID,Address FROM Mcustomer WHERE (PhoneNumber = '" . $mobile . "')")) !== false) {
                    $customerObj=sqlsrv_fetch_object($resultByPhone);
                }
            }
			$logger->info($customer->getId());
			$logger->info(print_r($customerObj, true));
            if (count($customerObj) > 0 && !empty($customerObj->ID)) {
				if($customer->getId() != $customerObj->Magento_Customer_ID ){
					$customerMagentoId = $customer->getId();
				}else{
					$customerMagentoId = $customerObj->Magento_Customer_ID;
				}

                $tsql = "UPDATE Mcustomer  SET Magento_Customer_ID = ?,updated=? ,FirstName = ? ,
                    LastName = ? ,EmailAddress = ? ,PhoneNumber = ? ,Address = ? ,Address2 = ? ,Company = ? ,City = ? ,State = ?,LastUpdated = ?  WHERE Magento_Customer_ID=?";
                $var = array($customerMagentoId,1, $firstName, $lastName, $CustomerEmail, $mobile, str_replace($remove, '', mb_substr($billAddress, 0, 50, 'utf-8')), str_replace($remove, '', mb_substr($billAddress2, 0, 50, 'utf-8')),
                    $billCompany, $billCity, mb_substr($billState, 0, 20, 'utf-8'),$today, $customerObj->Magento_Customer_ID
                );
				$logger->info('----2');
				$logger->info(print_r($var, true));
                if (!sqlsrv_query($conn, $tsql, $var)) {
                    $rmslog['rms_id'] = $customerObj->ID;
                    $rmslog['message'] = "Updated customer1: " . json_encode(sqlsrv_errors());
                    $rmslog['type'] = "error";
                    $this->rmsConnect->setData($rmslog)->save();
                }
				$customerRMSID = $customerObj->ID;
				if($customerObj->ID == null){
					$resultByEmail = sqlsrv_query($conn, "SELECT ID,Magento_Customer_ID,Address FROM Mcustomer WHERE (EmailAddress = '" . $CustomerEmail . "')");
					$logger->info('----bjwfb');
					
					$resultByEmailObj=sqlsrv_fetch_object($resultByEmail);
					$logger->info(print_r($resultByEmailObj, true));
					$customerRMSID = $resultByEmailObj->ID;
				}
				
                $rmslog['rms_id'] = $customerRMSID;
                $rmslog['message'] = "Updated customer2: " . json_encode($var,JSON_UNESCAPED_UNICODE);
                $rmslog['type'] = "success";
//                $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                $this->rmsConnect->setData($rmslog)->save();

                if (($result = sqlsrv_query($conn, "SELECT Magento_Customer_ID FROM GO_Loyalty_Customer WHERE (Magento_Customer_ID = '" . $customerMagentoId . "')")) !== false) {
                    $obj = sqlsrv_fetch_object($result);
                    if (count($obj) > 0) {

                        $tsql = "UPDATE GO_Loyalty_Customer  SET Magento_Customer_ID = ? , FirstName = ? ,LastName = ? ,PhoneNumber = ? ,
                    EmailAddress = ? ,Loyalty_Join_Date = ? ,LastUpdated = ? ,Preferred_Contact_Method = ? ,Preferred_Language = ? ,Loyalty_Join_Source = ?  WHERE Magento_Customer_ID=?";
                        $var = array($customer->getId(),  $firstName, $lastName, $mobile,
                            $CustomerEmail, $today, $today, $preferedContact, $preferedLanguage, $LoyaltyJoinSource, $customerObj->Magento_Customer_ID
                        );
						$logger->info('----3');
						$logger->info(print_r($var, true));
                        if (!sqlsrv_query($conn, $tsql, $var)) {
                            $rmslog['rms_id'] = $customerObj->ID;
                            $rmslog['message'] = "Updated Loyalty: " . json_encode(sqlsrv_errors());
                            $rmslog['type'] = "error";
                            $this->rmsConnect->setData($rmslog)->save();
                            // $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                            return $this;
                        }
                        $rmslog['rms_id'] = $customerObj->ID;
                        $rmslog['message'] = "Updated Loyalty: " . json_encode($var,JSON_UNESCAPED_UNICODE);
                        $rmslog['type'] = "success";

                        $this->rmsConnect->setData($rmslog)->save();
                        //  $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                    } else {

                        $tsql = "INSERT INTO GO_Loyalty_Customer (
                    RMS_Customer_ID, Magento_Customer_ID, FirstName,LastName,PhoneNumber,
                    EmailAddress,Loyalty_Join_Date,LastUpdated,Preferred_Contact_Method,Preferred_Language,Loyalty_Join_Source
                    ) 
                  VALUES 
            (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?)";
                        $var = array($customerRMSID, $customer->getId(),  $firstName, $lastName, $mobile,
                            $CustomerEmail, $today, $today, $preferedContact, $preferedLanguage, $LoyaltyJoinSource
                        );
						$logger->info('----4');
						$logger->info(print_r($var, true));
                        if (!sqlsrv_query($conn, $tsql, $var)) {
                            $rmslog['rms_id'] = $customerObj->ID;
                            $rmslog['message'] = "Created Loyalty:::- " . json_encode(sqlsrv_errors());
                            $rmslog['type'] = "error";
                            //                            $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                            $this->rmsConnect->setData($rmslog)->save();
                            return $this;
                        }
                        $rmslog['rms_id'] = $customerObj->ID;
                        $rmslog['message'] = "Created Loyalty::: " . json_encode($var,JSON_UNESCAPED_UNICODE);
                        $rmslog['type'] = "success";
                        //                        $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                        $this->rmsConnect->setData($rmslog)->save();
                    }
                }
            } else {

                $tsql = "INSERT INTO Mcustomer (
                    Magento_Customer_ID,updated, FirstName,LastName,
                    EmailAddress,PhoneNumber,Address,Address2,Company,City,State,AccountOpened,LastUpdated
                    ) 
                  VALUES 
            (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?)";
                $var = array( $customer->getId(),1, $firstName, $lastName, $CustomerEmail, $mobile, str_replace($remove, '', mb_substr($billAddress, 0, 50, 'utf-8')), str_replace($remove, '', mb_substr($billAddress2, 0, 50, 'utf-8')),
                    $billCompany, $billCity, mb_substr($billState, 0, 20, 'utf-8'), $today,$today
                );
				$logger->info('----5');
				$logger->info(print_r($var, true));
                if (!sqlsrv_query($conn, $tsql, $var)) {
                    $rmslog['rms_id'] = $customerObj->ID;
                    $rmslog['message'] = "Created customer: " . json_encode(sqlsrv_errors());
                    $rmslog['type'] = "error";
//                    $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                    $this->rmsConnect->setData($rmslog)->save();
                    return $this;
                }
				if (($result = sqlsrv_query($conn, "SELECT ID FROM Mcustomer WHERE (EmailAddress = '" . $CustomerEmail . "')")) !== false) {
                    $customerObj = sqlsrv_fetch_object($result);
				}
                $rmslog['rms_id'] = $customerObj->ID;
                $rmslog['message'] = "Created customer: " . json_encode($var,JSON_UNESCAPED_UNICODE);
                $rmslog['type'] = "success";
                $this->rmsConnect->setData($rmslog)->save();
//                $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();

                if (($result = sqlsrv_query($conn, "SELECT ID FROM Mcustomer WHERE (EmailAddress = '" . $CustomerEmail . "')")) !== false) {
                    $customerObj = sqlsrv_fetch_object($result);

                    $tsql = "INSERT INTO GO_Loyalty_Customer (
                    RMS_Customer_ID, Magento_Customer_ID, FirstName,LastName,PhoneNumber,
                    EmailAddress,Loyalty_Join_Date,LastUpdated,Preferred_Contact_Method,Preferred_Language,Loyalty_Join_Source
                    ) 
                  VALUES 
            (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?)";
                    $var = array($customerObj->ID, $customer->getId(),  $firstName, $lastName, $mobile,
                        $CustomerEmail, $today, $today, $preferedContact, $preferedLanguage, $LoyaltyJoinSource
                    );
					$logger->info('----6');
					$logger->info(print_r($var, true));
                    if (!sqlsrv_query($conn, $tsql, $var)) {
                        $rmslog['rms_id'] = $customerObj->ID;
                        $rmslog['message'] = "Created loyalty:: " . json_encode(sqlsrv_errors());
                        $rmslog['type'] = "error";
                        $this->rmsConnect->setData($rmslog)->save();
//                    $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                        return $this;
                    }
                    $rmslog['rms_id'] =$customerObj->ID;
                    $rmslog['message'] = "Created loyalty:- " . json_encode($var,JSON_UNESCAPED_UNICODE);
                    $rmslog['type'] = "success";
//                    $objectManager->create("\I95Dev\RmsConnect\Model\Rmsconnect")->setData($rmslog)->save();
                    $this->rmsConnect->setData($rmslog)->save();
                }
            }
        }
        return $this;
    }

} 
