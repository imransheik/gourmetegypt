<?php


namespace I95Dev\RmsConnect\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_i95dev_rmsconnect_rmsconnect = $setup->getConnection()
            ->newTable($setup->getTable('i95dev_rmsconnect_rmsconnect'));

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'rmsconnect_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'rms_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => true],
            'rms_id'
        );

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'customer_id'
        );

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Creation Time'
        );
           $table_i95dev_rmsconnect_rmsconnect->addColumn(
               'updated_at',
               \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
               null,
               ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
               'Update Time'
           );

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'message',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'message'
        );

        $table_i95dev_rmsconnect_rmsconnect->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'type'
        );

        //Your install script

        $setup->getConnection()->createTable($table_i95dev_rmsconnect_rmsconnect);
    }
}
