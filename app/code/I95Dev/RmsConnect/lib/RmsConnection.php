<?php

namespace I95Dev\RmsConnect\lib;

class RmsConnection
{
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;

    /**
     * RmsConnection constructor.
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \I95Dev\RmsConnect\Model\Rmsconnect $rmsConnect
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Framework\HTTP\Client\Curl $curl,
        \I95Dev\RmsConnect\Model\Rmsconnect $rmsConnect
    ) {
        $this->_curl = $curl;
        $this->rmsConnect = $rmsConnect;
    }

    /**
     * @return resource
     * @psalm-suppress RedundantCondition
     * @psalm-suppress TypeDoesNotContainType
     * @psalm-suppress InvalidNullableReturnType
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function connectToRms()
    {
        $serverName = '41.33.113.26';

        $connectionInfo = ["Database" => "new_ge_hq", "UID" => "Magento", "PWD" => "BR.Magent0@0609"];
        $conn = sqlsrv_connect($serverName, $connectionInfo);
        if ($conn) {
            // echo "Connection established.<br/>";
            return $conn;
        } else {
            $rmslog = [];
            $rmslog['customer_id'] = null;
            $rmslog['rms_id'] = null;
            $rmslog['message'] = "RMS not connected: " . json_encode(sqlsrv_errors());
            $rmslog['type'] = "error";
            $this->rmsConnect ->setData($rmslog)->save();
        }
    }
}
