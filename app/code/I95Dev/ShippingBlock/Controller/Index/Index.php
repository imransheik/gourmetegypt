<?php

namespace I95Dev\ShippingBlock\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $_resultPageFactory;
    /**
     * @psalm-suppress MissingPropertyType
     */
    protected $quoteItemFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $customerFactory;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepositoryInterface;
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @psalm-suppress UndefinedClass
     */

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory
     * @param Session $checkoutSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface
     * @param \Magento\Checkout\Model\Cart $cart
     * @psalm-suppress UndefinedThisPropertyAssignment
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Magento\Checkout\Model\Session  $checkoutSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->checkoutSession = $checkoutSession;
        $this->customerFactory = $customerFactory;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cart = $cart;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @psalm-suppress UndefinedClass
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress UndefinedThisPropertyFetch
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress DeprecatedMethod
     */
    public function execute()
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $this->checkoutSession;  //$objectManager->get(\Magento\Checkout\Model\Session::class);
        $checkoutSession->start();
//        $customerFactory = $this->customerFactory->create();
//        $quoteRepository = $this->cartRepositoryInterface->create();
//        $quoteRepository = $objectManager
        //->create(\Magento\Quote\Api\CartRepositoryInterface::class);
        $params = $this->getRequest()->getParams();
        if (isset($params['preffered_select'])) {
            $checkoutSession->setCheckbox($params['preffered_select']);
            $prefferedselect = $checkoutSession->getCheckbox();
            //$quoteObj = $objectManager->create(\Magento\Checkout\Model\Cart::class)->getQuote();
            $quoteObj = $this->cart->getQuote();
            $quoteObj->setData("preffered_select", $prefferedselect)->save();
        }
    }
}
