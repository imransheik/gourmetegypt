<?php
namespace I95Dev\ShippingBlock\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddSelectToOrderObserver implements ObserverInterface
{
    /**
     * @param EventObserver $observer
     * @return $this|void
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $PrefferedSelect = $quote->getPrefferedSelect();
        if (!$PrefferedSelect) {
            return $this;
        }
        //Set fee data to order
        $order = $observer->getOrder();
        $order->setData('preffered_select', $PrefferedSelect);
        
        return $this;
    }
}
