<?php
namespace I95Dev\ShippingBlock\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddTemplateToShippingBlockObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager  $observer->getTransport()->getOutput() .
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function execute(EventObserver $observer)
    {
        $deliveryDateBlock = $this->objectManager->create(\Magento\Framework\View\Element\Template::class);
        $deliveryDateBlock->setTemplate('I95Dev_ShippingBlock::helloworld.phtml');
    }
}
