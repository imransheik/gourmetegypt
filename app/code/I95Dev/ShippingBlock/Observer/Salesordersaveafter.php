<?php

namespace I95Dev\ShippingBlock\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Captcha\Observer\CaptchaStringResolver;

class Salesordersaveafter implements ObserverInterface
{

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $historyFactory;

    /**
     * @psalm-suppress UndefinedClass
     */
    protected $orderFactory;

    /**
     * Salesordersaveafter constructor.
     * @param \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->historyFactory = $historyFactory;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @param EventObserver $observer
     * @psalm-suppress UndefinedClass
     */
    public function execute(EventObserver $observer)
    {

        $order = $observer->getEvent()->getOrder();
 
            $orderId = $order->getEntityId();
        if ($order->getPrefferedSelect()) {

            if ($orderId) {
                $status = $order->getData('status');
                $comment = $order->getPrefferedSelect();
                $data = $this->historyFactory->create()->load($orderId, 'parent_id');
                $data->setData('comment', "Prefered contact method: ".$comment);
                $data->setData('parent_id', $orderId);
                $data->setData('is_visible_on_front', 1);
                $data->setData('is_customer_notified', 1);
                $data->setData('status', $status);
                $data->setData('entity_name', 'order');
                $data->save();
            }
            
        }
    }
}
