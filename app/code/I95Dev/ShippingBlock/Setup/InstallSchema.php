<?php
namespace I95Dev\ShippingBlock\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

/**
 * {@inheritdoc}
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $quoteTable = 'quote';
        $orderTable = 'sales_order';

        $installer->getConnection()
            ->addColumn(
                $installer->getTable($quoteTable),
                'preffered_select',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '255',
                    'nullable' => true,
                    'comment' =>'Preffered Select'
                ]
            );

            $installer->getConnection()
            ->addColumn(
                $installer->getTable($orderTable),
                'preffered_select',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '255',
                    'nullable' => true,
                    'comment' =>'Preffered Select'
                ]
            );

        $installer->endSetup();
    }
}
