var config = {
    map: {
        '*': {
            'Amasty_Checkout/template/shipping-address/shipping.html':
                'I95Dev_UpdateAddress/template/shipping.html'
        }
    },
    config: {
        mixins: {
            'Magento_Customer/js/model/customer/address': {
                'I95Dev_UpdateAddress/js/model/customer/address-mixin': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'I95Dev_UpdateAddress/js/view/shipping-address/address-renderer/default-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'I95Dev_UpdateAddress/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/model/shipping-address/form-popup-state': {
                'I95Dev_UpdateAddress/js/model/shipping-address/form-popup-state-mixin': true
            }
        }
    }
};
