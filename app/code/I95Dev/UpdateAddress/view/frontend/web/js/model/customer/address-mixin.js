define(['mage/utils/wrapper'], function (wrapper) {

    return function (address) {

        address = wrapper.wrapSuper(address, function (addressData) {

            var result = this._super(addressData);

            result.isEditable = function () {
                return true;
            }
            return result;
        });

        return address;
    };

});

