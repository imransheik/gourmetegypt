define([
    'ko',
    'mage/utils/wrapper'
], function (ko, wrapper) {
    'use strict';

    return function (popup) {
        popup.isEditFormPopUpVisible = ko.observable(false);
        popup.editAddressData = {};
        return popup;
    };
});
