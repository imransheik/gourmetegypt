define([
    'jquery',
    'ko',
    'uiComponent',
    'underscore',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data'
], function ($, ko, Component, _, selectShippingAddressAction, quote, formPopUpState, checkoutData, customerData) {

    'use strict';

    return function (AddressInfo) {

        return AddressInfo.extend({
            editAddress: function () {
                if (typeof this.address().customerAddressId != 'undefined' && this.address().customerAddressId != null) {
                    formPopUpState.isEditFormPopUpVisible(true);
                    formPopUpState.editAddressData = this.address();
                    $('[data-open-modal="opc-edit-shipping-address"]').trigger('click');
                } else {
                    formPopUpState.isEditFormPopUpVisible(false);
                    formPopUpState.editAddressData = {};
                    this._super();
                }
            },
        });
    }
});
