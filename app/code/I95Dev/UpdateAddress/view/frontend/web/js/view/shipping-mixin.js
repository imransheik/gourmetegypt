define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/error-processor',
    'mage/storage',
    'mage/translate',
    'Magento_Checkout/js/model/shipping-rate-service'
], function (
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    urlBuilder,
    fullScreenLoader,
    errorProcessor,
    storage,
    $t
) {
    'use strict';

    var popEditUp = null;

    return function (shipping) {

        return shipping.extend({

            defaults: {
                shippingEditFormTemplate: 'I95Dev_UpdateAddress/shipping-address/editform'
            },

            isEditFormPopUpVisible: formPopUpState.isEditFormPopUpVisible,

            initialize: function () {
                var self = this;
                this._super();
                this.isEditFormPopUpVisible.subscribe(function (value) {
                    if (value) {
                        self.getEditPopUp().openModal();
                    }
                });
            },

            getEditPopUp: function () {

                var self = this,
                    buttons;
					var mapPopup = $('div#checkoutmapedit')
                            .modal({
                                autoOpen: false,
                                buttons: [
                                ],
                                clickableOverlay: true,
                                focus: '',
                                innerScroll: false,
                                modalClass: 'view-map-popup-edit',
                                responsive: true,
                                closeOnEscape: false,

                                type: 'popup'
                            });
                if (!popEditUp) {
                    buttons = this.popUpFormEdit.options.buttons;
					
                    this.popUpFormEdit.options.buttons = [
						{
                            text:  $t('View Map'),
                            class: 'action checkout-view-map',
                            click: function () {
                                mapPopup.modal("openModal"); 
                            }
                        },
                        {
                            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                            click: self.editAddress.bind(self)
                        },
                        {
                            text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
                            class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',

                            /** @inheritdoc */
                            click: this.onCloseEditPopUp.bind(this)
                        }
                    ];

                    /** @inheritdoc */
                    this.popUpFormEdit.options.closed = function () {
                        self.isEditFormPopUpVisible(false);
                        var formData = checkoutData.getNewCustomerShippingAddress();

                        self.populateFormData(formData, '#co-shipping-form');

                    };

                    this.popUpFormEdit.options.modalCloseBtnHandler = this.onCloseEditPopUp.bind(this);
                    this.popUpFormEdit.options.keyEventHandlers = {
                        escapeKey: this.onCloseEditPopUp.bind(this)
                    };

                    /** @inheritdoc */
                    this.popUpFormEdit.options.opened = function () {
                        var cid = formPopUpState.editAddressData.customerAddressId
						var i;
						$.each(customer.customerData.addresses, function (key, value) {
							var custID = customer.customerData.addresses[key].id;
							if(cid === customer.customerData.addresses[key].id){
								i = key;	
							}
						});
						var formData = customer.customerData.addresses[i];
                        self.populateFormData(formData, '#co-edit-shipping-form');
                    };
                    popEditUp = modal(this.popUpFormEdit.options, $(this.popUpFormEdit.element));
                }

                return popEditUp;
            },
            onCloseEditPopUp: function () {
                this.getEditPopUp().closeModal();
            },
            populateFormData: function (formData, identifier) {

                $(identifier).find("input[type=text], select").val("");
                if (formData != null) {
                    $.each(formData, function (key, value) {

                        if (key != 'street' && key != 'region_id') {
                            $(identifier).find('input[name="' + key + '"]').val(value).change();
                            $(identifier).find('select[name="' + key + '"]').val(value).change();
                        }
                        if (key == 'custom_attributes' && value != null) {
                            $.each(value, function (k, v) {
                                $(identifier).find('input[name="custom_attributes[' + k + ']"]').val(v.value).change();
								if(k == 'addresstype'){
									setTimeout(function () {
										$(identifier).find('select[name="custom_attributes[' + k + ']"]').val(v.value).change();
									}, 300);
								}
                            });
                        }
						if (key == 'region' && value != null) {
							if(jQuery.type( value ) === "string"){
								if(key == 'region'){
									$(identifier).find('input[name="' + key + '"]').val(value).change();
								}
							}else{
                            $.each(value, function (k, v) {
								if(k == 'region'){
									$(identifier).find('input[name="' + key + '"]').val(v).change();
								}
                            });
							}
                        }
                        if (key == 'street' && value != null) {
                            $.each(value, function (k, v) {
                                $(identifier).find('input[name="street[' + k + ']"]').val(v).change();
                            });
                        }
                        if (key == 'region_id') {
                            setTimeout(function () {
                                $(identifier).find('select[name="region_id"]').val(value).change();
                            }, 300);

                        }

                    });
                }
            },
            editAddress: function () {
				
				$(".action-save-address").attr("disabled", true);
                var customerInfo = customer.customerData.addresses;

                var self = this;
                var addressData, serviceUrl;
                var data = {}, postData = [], dataArr = [];

                data.customer = customer.customerData;
                var addressesArr = data.customer.addresses;
                $.each(addressesArr, function (key, value) {
                    delete data.customer.addresses[key].inline;
                });
                this.source.set('params.invalid', false);
                //this.triggerShippingDataValidateEvent();

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get('shippingAddress');
                    delete addressData.save_in_address_book;
                    if (customer.isLoggedIn()) {
                        serviceUrl = urlBuilder.createUrl('/customers/me', {});
                        addressData.id = formPopUpState.editAddressData.customerAddressId;

                        $.each(data.customer.addresses, function (key, value) {
                            if (key == formPopUpState.editAddressData.customerAddressId) {
                                postData.push($.extend(value, addressData));
                            } else {
                                postData.push(value);
                            }
                        });
                        $.each(postData, function (key, value) {
                            dataArr[value.id] = value;
                        });
                        
						setTimeout(function () {
							dataArr = Object.assign({}, dataArr);
							data.customer.addresses = dataArr;
							fullScreenLoader.startLoader();
							console.log('address sent');
							console.log(data);
						}, 300);
                        return storage.put(
                            serviceUrl, JSON.stringify(data)
                        ).fail(
                            function (response) {
								$(".action-save-address").attr("disabled", false);
								console.log('Error response');
                                console.log(response);
                            }
                        ).always(
                            function () {
                                var address = addressConverter.formAddressDataToQuoteAddress(addressData),
                                    isAddressUpdated = addressList().some(function (currentAddress, index, addresses) {

                                        if (typeof currentAddress.customerAddressId != 'undefined'
                                            && currentAddress.customerAddressId == formPopUpState.editAddressData.customerAddressId
                                            && currentAddress.customerAddressId != null) {
                                            address.customerAddressId = formPopUpState.editAddressData.customerAddressId;
                                            address.getKey = function () {
                                                return 'customer-address' + address.customerAddressId;
                                            };
                                            address.getType = function () {
                                                return 'customer-address';
                                            };
                                            address.getCacheKey = function () {
                                                return 'customer-address' + address.customerAddressId;
                                            };
                                            addresses[index] = address;
                                        }
                                    });

                                addressList.valueHasMutated();
                                selectShippingAddress(address);
                                checkoutData.setSelectedShippingAddress(address.getKey());
                                fullScreenLoader.stopLoader();
                                self.getEditPopUp().closeModal();
								console.log('success response');
								$(".action-save-address").attr("disabled", false);
                            }
                        );
                    }
                    fullScreenLoader.startLoader();
					$(".action-save-address").attr("disabled", false);
                    this.getEditPopUp().closeModal();
                }
            }
        });
    }
});
