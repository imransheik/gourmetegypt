<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Voucher\Model\Quote;

use Magento\Quote\Model\Quote\Address\Total\Collector;
use Magento\Quote\Model\Quote\Address\Total\CollectorFactory;
use Magento\Quote\Model\Quote\Address\Total\CollectorInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class TotalsCollector extends \Magento\Quote\Model\Quote\TotalsCollector
{

    /**
     * Total models collector
     *
     * @var \Magento\Quote\Model\Quote\Address\Total\Collector
     */
    protected $totalCollector;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $totalCollectorFactory;

    /**
     * Application Event Dispatcher
     *
     * @var \Magento\Framework\Event\ManagerInterface
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $eventManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $storeManager;

    /**
     * @var \Magento\Quote\Model\Quote\Address\TotalFactory
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $totalFactory;

    /**
     * @var \Magento\Quote\Model\Quote\TotalsCollectorList
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $collectorList;

    /**
     * Quote validator
     *
     * @var \Magento\Quote\Model\QuoteValidator
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $quoteValidator;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $shippingFactory;

    /**
     * @psalm-suppress UndefinedClass
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $shippingAssignmentFactory;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Request instance
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return $this
     * @psalm-suppress UndefinedClass
     * @psalm-suppress InvalidArgument
     */
    public function __construct(
        Collector $totalCollector, 
        CollectorFactory $totalCollectorFactory, 
        \Magento\Framework\Event\ManagerInterface $eventManager, 
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        \Magento\Quote\Model\Quote\Address\TotalFactory $totalFactory,
        \Magento\Quote\Model\Quote\TotalsCollectorList $collectorList, 
        \Magento\Quote\Model\ShippingFactory $shippingFactory, 
        \Magento\Quote\Model\ShippingAssignmentFactory $shippingAssignmentFactory,
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
        parent::__construct(
                $totalCollector, 
                $totalCollectorFactory, 
                $eventManager, 
                $storeManager, 
                $totalFactory, 
                $collectorList, 
                $shippingFactory, 
                $shippingAssignmentFactory, 
                $quoteValidator
        );
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return $this|TotalsCollector
     * @psalm-suppress UndefinedMethod
     */
    protected function _validateCouponCode(\Magento\Quote\Model\Quote $quote)
    {
        $code = $quote->getData('coupon_code');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$requestInterface = $objectManager->get(\Magento\Framework\App\RequestInterface::class);
        $requestInterface = $this->request;
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
//        $scopeConfig = $objectManager->create(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $ecouponPrefix = $this->scopeConfig->getValue("example_section/general/preForCoupon", $storeScope);
        if (strlen($code)) {
            $addressHasCoupon = false;
            $addresses = $quote->getAllAddresses();
            if (count($addresses) > 0) {
                foreach ($addresses as $address) {
                    if ($address->hasCouponCode()) {
                        $addressHasCoupon = true;
                    }
                }

                if ($requestInterface->getRequestUri() !== '/rest/default/V1/carts/mine/coupons/'.$code &&
                    !substr($code, 0, 2) === $ecouponPrefix) {

                    if (strpos($requestInterface->getRequestUri(), 'isapp') === false) {
                        if (!$addressHasCoupon) {
                            $quote->setCouponCode('');
                            $quote ->setEcouponValue(null);
                            $quote ->setEcouponPercentage(null);
                            $quote ->setEcoupon(null);
                            $quote ->setEcouponValueType(null);

                        }
                    }
                } elseif (strpos($requestInterface->getRequestUri(), 'isapp') === true &&
                    substr($code, 0, 2) != $ecouponPrefix) {
                    if (!$addressHasCoupon) {
                            $quote->setCouponCode('');
                            $quote ->setEcouponValue(null);
                            $quote ->setEcouponPercentage(null);
                            $quote ->setEcoupon(null);
                            $quote ->setEcouponValueType(null);
                    }
                } elseif (substr($code, 0, 2) != $ecouponPrefix) {
                    if (!$addressHasCoupon) {
                            $quote->setCouponCode('');
                            $quote ->setEcouponValue(null);
                            $quote ->setEcouponPercentage(null);
                            $quote ->setEcoupon(null);
                            $quote ->setEcouponValueType(null);
                    }
                }

            }
        }
        return $this;
    }
}
