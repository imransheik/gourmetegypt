<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\Voucher\Model\Total\Quote;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\SalesRule\Model\Validator;
use Magento\Store\Model\StoreManagerInterface;

class Ecoupon extends AbstractTotal
{
    /**
     * Total Code name
     *
     * @var string
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $_code;
    /**
     * @var Total
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $total;

    /**
     * Discount calculation object
     *
     * @var Validator
     */
    protected $calculator;

    /**
     * Core event manager proxy
     *
     * @var ManagerInterface
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     */
    protected $eventManager = null;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;
    /**
     * Request instance
     *
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Cart
     */
    protected $cart;
    /**
     * @psalm-suppress UndefinedClass
     */
    protected $_productloader;
    /**
     * @var Session
     */
    protected $session;

    /**
     * @param ManagerInterface $eventManager
     * @param StoreManagerInterface $storeManager
     * @param Validator $validator
     * @param PriceCurrencyInterface $priceCurrency
     * @param RequestInterface $request
     * @param Cart  $cart
     * @param ProductFactory  $_productloader
     * @param Session $session
     * @psalm-suppress UndefinedClass
     */
    public function __construct(
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        Validator $validator,
        PriceCurrencyInterface $priceCurrency,
        RequestInterface $request,
        Cart $cart,
        Product $_productloader,
        Session $session
    ) {
        $this->setCode('ecoupon');
        $this->eventManager = $eventManager;
        $this->calculator = $validator;
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        $this->request = $request;
        $this->cart = $cart;
        $this->_productloader = $_productloader;
        $this->session =$session;
    }

    /**
     * Collect address discount amount
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @psalm-suppress UndefinedMethod
     * @psalm-suppress UndefinedClass
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        // $objectManager = ObjectManager::getInstance();
//        $requestobj = $objectManager->create(\Magento\Framework\App\RequestInterface::class);
        $request = $this->request->getParams();
        $customerSession = $this->session;
//        $customerSession = $objectManager->create(Session::class);
//        $cart = $this->cart;
//        $cart = $objectManager->get(\Magento\Checkout\Model\Cart::class);
        $address = $shippingAssignment->getShipping()->getAddress();
        if ($address->getAddressType() == 'billing') {
            return $this;
        }
        $items = $shippingAssignment->getItems();
        if (!count($items)) {
            return $this;
        }
        $couponCode = $quote ->getCouponCode();
        $discountValue = $quote ->getEcouponValue();
        $discountPercentage = $quote ->getEcouponPercentage();
        $originalCode = $quote ->getEcoupon();
        $discountType = $quote ->getEcouponValueType();
        $remove = isset($request['remove']) ? $request['remove'] : '1';
        $updatecart = isset($request['update_cart_action']) ? $request['update_cart_action'] : '';

        if ($originalCode != null) {
            if ($remove == 0 || $updatecart == 'update_qty' || $customerSession->getEcoupon() ==0) {
                if ($discountType == 'EGP') {
                    $discount = $discountValue;
                    if ($quote->getSubtotal() < $discount) {
                        $discount =$quote->getSubtotal();
                    }
                    $label = 'ecoupon';

                    $total->addTotalAmount($label, -$discount);
                    $total->addBaseTotalAmount($label, -$discount);
                    $total->setDiscountDescription($couponCode);
                    //decline native discount if exists
                    $total->addTotalAmount('discount', -($total->getTotalAmount('discount')));
                    $total->addBaseTotalAmount('discount', -($total->getBaseTotalAmount('discount')));
                    foreach ($quote->getAllItems() as $item) {
                        $item->setDiscountAmount(0);
                        $item->setDiscountPercent(0);
                    }

                    $total->setDiscountAmount(-$discount);
                    $total->setBaseDiscountAmount(-$discount);
                    $total->setSubtotalWithDiscount($total->getSubtotal() - $discount);
                    $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() - $discount);
                }
                if ($discountType == 'Percentage') {
                    $discountAbleTotalPercentage =  $quote->getSubtotal() * ($discountPercentage/100);
                    // if ($discountAbleTotalPercentage > $discountValue) {
                        // $discount = $discountValue;
                    // } else {
                        // $discount = $discountAbleTotalPercentage;
                    // }
					$discount = $discountAbleTotalPercentage;
                    $label = 'ecoupon';
                    $total->addTotalAmount($label, -$discount);
                    $total->addBaseTotalAmount($label, -$discount);
                    $total->setDiscountDescription($couponCode);
                    //decline native discount if exists
                    $total->addTotalAmount('discount', -($total->getTotalAmount('discount')));
                    $total->addBaseTotalAmount('discount', -($total->getBaseTotalAmount('discount')));
                    foreach ($quote->getAllItems() as $item) {
                        $item->setDiscountAmount(0);
                        $item->setDiscountPercent(0);
                    }

                    $total->setDiscountAmount(-$discount);
                    $total->setBaseDiscountAmount(-$discount);
                    $total->setSubtotalWithDiscount($total->getSubtotal() - $discount);
                    $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() - $discount);
                }
            }
        }
		

        return $this;
    }

    /**
     * Add discount total information to address
     *
     * @param Quote $quote
     * @param Total $total
     * @return array|null
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @psalm-suppress UndefinedFunction
     */
    public function fetch(Quote $quote, Total $total)
    {
        $result = null;
        $amount = $total->getDiscountAmount();

        // ONLY return 1 discount. Need to append existing
        //see app/code/Magento/Quote/Model/Quote/Address.php

        if ($amount != 0) {
            $description = $total->getDiscountDescription();
            $result = [
                'code' => 'ecoupon',
                'title' => strlen($description) ? __('Discount (%1)', 'E-coupon') : __('E-coupon'),
                'value' => $amount
            ];
        }
        return $result;
        /* in magento 1.x
           $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $amount
            ));
         */
    }

    /**
     * @param Total $total
     * @psalm-suppress MissingReturnType
     * @psalm-suppress UndefinedClass
     */
    protected function clearValues(Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }
}
