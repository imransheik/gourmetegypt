<?php
namespace I95Dev\Voucher\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class SaveOrderAfterSubmitObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @param Redemption $redemption
     * @psalm-suppress UndefinedThisPropertyAssignment
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_request = $request;
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function execute(EventObserver $observer)
    {
        /* @var $order \Magento\Sales\Model\Order */
        $order = $observer->getEvent()->getData('order');
        /* @var $quote \Magento\Quote\Model\Quote */
        $quote = $observer->getEvent()->getData('quote');
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
//        $scopeConfig = $objectManager->create(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $ecouponUrl = $this->scopeConfig->getValue("example_section/general/url", $storeScope);
        $ecouponSecretCode = $this->scopeConfig->getValue("example_section/general/secretCode", $storeScope);
        $ecouponPrefix = $this->scopeConfig->getValue("example_section/general/preForCoupon", $storeScope);
        $ecouponCustomerCode = $this->scopeConfig->getValue("example_section/general/customerMsisdn", $storeScope);
        $couponCode = $quote ->getCouponCode();
		$couponCode1 = $couponCode;
		$ecouponPercentage = $quote->getEcouponPercentage();
		$ecouponValueType = $quote->getEcouponValueType();
		$ecouponValue = $quote->getEcouponValue();

        if (substr($couponCode, 0, 2) === $ecouponPrefix) {
            $couponCode = preg_replace('/^'.$ecouponPrefix.'/', '', $couponCode);
            $couponUrl = $ecouponUrl.'?couponNumber='.$couponCode1.'&msisdn='.
                $ecouponSecretCode.'&customerMSISDN=&Burn=true';
            // @codingStandardsIgnoreStart
            $xml = simplexml_load_string(file_get_contents($couponUrl));
            // @codingStandardsIgnoreEnd
			
            $order ->setEcouponValue($ecouponValue);
			$order ->setEcouponPercentage($ecouponPercentage);
			$order ->setEcoupon($couponCode1);
			$order ->setEcouponValueType($ecouponValueType);
			$order->save();

        }
        return $this;
    }
}
