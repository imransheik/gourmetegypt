<?php


namespace I95Dev\Voucher\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Sales\Setup\SalesSetup;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();
        $connection->addColumn($installer->getTable('quote'), 'ecoupon', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon'
        ]);
        $connection->addColumn($installer->getTable('quote'), 'ecoupon_value', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Value'
        ]);
        $connection->addColumn($installer->getTable('quote'), 'ecoupon_value_type', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Value Type'
        ]);
        $connection->addColumn($installer->getTable('quote'), 'ecoupon_percentage', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Percentage'
        ]);
        $connection->addColumn($installer->getTable('sales_order'), 'ecoupon', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon'
        ]);
        $connection->addColumn($installer->getTable('sales_order'), 'ecoupon_value_type', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Value Type'
        ]);
        $connection->addColumn($installer->getTable('sales_order'), 'ecoupon_value', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Value'
        ]);
        $connection->addColumn($installer->getTable('sales_order'), 'ecoupon_percentage', [
            'type'     => Table::TYPE_TEXT,
            'nullable' => true,
            'comment'  => 'E-coupon Percentage'
        ]);

        $installer->endSetup();
    }
}
