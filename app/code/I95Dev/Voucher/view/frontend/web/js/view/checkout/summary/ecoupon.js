define(
   [
       'jquery',
       'Magento_Checkout/js/view/summary/abstract-total'
   ],
   function ($,Component) {
       "use strict";
       return Component.extend({
           defaults: {
               template: 'I95Dev_Voucher/checkout/summary/ecoupon' 
           },
           isDisplayedCustomdiscount : function(){
               return true;
           },
           getCustomDiscount : function(){
               return '$10';
           }
       });
   }
);