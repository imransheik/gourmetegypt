<?php

namespace Klevu\Metadata;

class Constants
{
    const XML_PATH_METADATA_ENABLED = "klevu_search/metadata/enabled";
    const KLEVU_PLATFORM_TYPE = "magento2";
    const ID_SEPARATOR = "-";
}
