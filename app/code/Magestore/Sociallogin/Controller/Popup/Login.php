<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 *
 */
namespace Magestore\Sociallogin\Controller\Popup;

class Login extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {

            $this->_login();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function _login()
    {
		
        //$sessionId = session_id();
        $username = $this->getRequest()->getPost('socialogin_email', false);
        $password = $this->getRequest()->getPost('socialogin_password', false);

        $result = ['success' => false];
        $blockedemail=array("abolammoya@gmail.com","samar1933x@gmail.com","faressultan971@gmail.com");
if(in_array($username,$blockedemail)){
     $result['error'] = __(
                'Your account has been blocked by admin.');
  $jsonEncode = $this->_objectManager->create('Magento\Framework\Json\Helper\Data');
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));   
        return;
       
}
        if ($username && $password) {
            try {
                // $this->_getSession()->login($username, $password);
                $login = $this->_objectManager->create('Magento\Customer\Api\AccountManagementInterface');
                $customer = $login->authenticate(
                    $username,
                    $password
                );
                $this->_getSession()->setCustomerDataAsLoggedIn($customer);
                 $this->_getSession()->regenerateId();
                if ($this->cookieManager->getCookie('mage-cache-sessid')) {
                $metadata = $this->cookieMetadataFactory->createCookieMetadata();
                $metadata->setPath('/');
                $this->cookieManager->deleteCookie('mage-cache-sessid', $metadata);
            }
            } catch (\Exception $e) {
                $result['error'] = $e->getMessage();
            }
            if (!isset($result['error'])) {
                $customer=$this->_getSession()->getCustomer();
                $quote = $this->_objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($customer);
                $quote->setData("redeem_loyalty_amount", '');
                $quote->setData("bloyal_cartid", '');
                $quote->collectTotals()->save();
                $language=$customer->getData("prefered_language");
                if($language=="210"){
                     $redirecturl=$this->_loginPostRedirect()."?___store=arabic_store&___from_store=default";
                }else{
                      $redirecturl=$this->_loginPostRedirect()."?___store=default&___from_store=arabic_store";
                }
                $result['success'] = true;
                $result['url'] = $redirecturl;
            }
        } else {
            $result['error'] = __(
                'Please enter a username and password.');
        }
        $jsonEncode = $this->_objectManager->create('Magento\Framework\Json\Helper\Data');
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));
    }

}