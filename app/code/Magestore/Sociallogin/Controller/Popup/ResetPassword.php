<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 *
 */
namespace Magestore\Sociallogin\Controller\Popup;

use Magento\Customer\Model\AccountManagement;

class ResetPassword extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {

            $this->_resetpassword();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function _resetpassword()
    {
		
		$password = $this->getRequest()->getPost('socialogin_email_new_password', false);
        $email = $this->getRequest()->getPost('email-otp1', false);
        $model = $this->_objectManager->create(\Magento\Customer\Model\Customer::class);
		$customer = $model->setWebsiteId($this->_storeManager->getStore()->getWebsiteId())
            ->loadByEmail($email);
		if ($customer->getId()) {
			try {
				$customerRepositoryInterface = $this->_objectManager->get(\Magento\Customer\Api\CustomerRepositoryInterface::class);
				$customerRegistry = $this->_objectManager->get(\Magento\Customer\Model\CustomerRegistry::class);
				$encryptor = $this->_objectManager->get(\Magento\Framework\Encryption\EncryptorInterface::class);
				$credentialsValidator = $this->_objectManager->get(\Magento\Customer\Model\Customer\CredentialsValidator::class);
				$customerId = $customer->getEntityId();
				$credentialsValidator->checkPasswordDifferentFromEmail($email, $password);
				$customerAcc = $this->_objectManager->get(\Magento\Customer\Model\AccountManagement::class);
				
				$customer = $customerRepositoryInterface->getById($customerId); // _customerRepositoryInterface is an instance of \Magento\Customer\Api\CustomerRepositoryInterface
				$customerSecure = $customerRegistry->retrieveSecureData($customerId); // _customerRegistry is an instance of \Magento\Customer\Model\CustomerRegistry
				$customerSecure->setRpToken(null);
				$customerSecure->setRpTokenCreatedAt(null);
				$customerSecure->setPasswordHash($encryptor->getHash($password, true)); // here _encryptor is an instance of \Magento\Framework\Encryption\EncryptorInterface
				$customerRepositoryInterface->save($customer);
				
				$result = ['success' => true, 'message' => __('This password has been changed!')];
			} catch (\Exception $e) {
                $result = ['success' => false, 'error' => $e->getMessage()];
            }
		}else {
            $result = ['success' => false, 'error' => __('This email address does not exist!')];
        }
        // $this->messageManager->addSuccess(__('We\'ll email you a link to reset your password.'));
        $jsonEncode = $this->_objectManager->create(\Magento\Framework\Json\Helper\Data::class);
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));
    }

}