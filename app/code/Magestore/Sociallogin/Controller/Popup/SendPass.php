<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 *
 */
namespace Magestore\Sociallogin\Controller\Popup;

use Magento\Customer\Model\AccountManagement;

class SendPass extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {

            $this->_sendPass();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function _sendPass()
    {
        $email = $this->getRequest()->getPost('socialogin_email_forgot', false);
        $model = $this->_objectManager->create(\Magento\Customer\Model\Customer::class);
        $customer = $model->setWebsiteId($this->_storeManager->getStore()->getWebsiteId())
            ->loadByEmail($email);
		$resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
        $connection = $resource->getConnection();
        if ($customer->getId()) {
            try {
					$receiverInfo = [
						'name' => $customer->getFirstName(),
						'email' => $email
					];


					/* Sender Detail  */
					$senderInfo = [
						'name' => 'NO REPLY',
						'email' => 'orders@gourmetegypt.com',
					];
					$id = $customer->getEntityId();
					if($customer->getResetOtp()){
						$otp = $customer->getResetOtp();
					}else{
						$otp = $six_digit_random_number = random_int(100000, 999999);
						$sql = "update customer_entity SET  reset_otp='". $otp ."' where entity_id=$id";
						$connection->query($sql);
					}
					
					
					/* Assign values for your template variables  */
					$emailTemplateVariables = array();
					$emailTempVariables['email'] = $email; 
					$emailTempVariables['customer_name'] = $customer->getData('firstname'); 
					$emailTempVariables['otp'] = $otp; 
					
					/* We write send mail function in helper because if we want to 
					   use same in other action then we can call it directly from helper */  

					/* call send mail method from helper or where you define it*/  
					$this->_objectManager->get(\Magestore\Sociallogin\Helper\Email::class)->yourCustomMailSendMethod(
						  $emailTempVariables,
						  $senderInfo,
						  $receiverInfo
					  );
                $result = ['success' => true];
            } catch (\Exception $e) {
                $result = ['success' => false, 'error' => $e->getMessage()];
            }
        } else {
            $result = ['success' => false, 'error' => __('This email address does not exist!')];
        }
        // $this->messageManager->addSuccess(__('We\'ll email you a link to reset your password.'));
        $jsonEncode = $this->_objectManager->create(\Magento\Framework\Json\Helper\Data::class);
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));
    }

}