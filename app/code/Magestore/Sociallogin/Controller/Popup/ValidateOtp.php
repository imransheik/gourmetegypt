<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 *
 */
namespace Magestore\Sociallogin\Controller\Popup;

use Magento\Customer\Model\AccountManagement;

class ValidateOtp extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {

            $this->_otp();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function _otp()
    {
        $otp = $this->getRequest()->getPost('socialogin_email_otp', false);
        $email = $this->getRequest()->getPost('email-otp', false);
        $model = $this->_objectManager->create(\Magento\Customer\Model\Customer::class);
		$customer = $model->setWebsiteId($this->_storeManager->getStore()->getWebsiteId())
            ->loadByEmail($email);
		$resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
        $connection = $resource->getConnection();
		if ($customer->getId()) {
			$otpSaved = $customer->getResetOtp();
			$id = $customer->getEntityId();
			if(empty($otpSaved)){
				$result = ['success' => false, 'error' => __('Please enter correct otp.')];
			}
			if($otpSaved === $otp  ){
				
				$resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
				$connection = $resource->getConnection();
				$sql = "update customer_entity SET  reset_otp='' where entity_id=$id";
				$connection->query($sql);
				$result = ['success' => true];
			}else{
				$result = ['success' => false, 'error' => __('Please enter correct otp from registered email.')];
			}
		}else {
            $result = ['success' => false, 'error' => __('This email address does not exist!')];
        }
        // $this->messageManager->addSuccess(__('We\'ll email you a link to reset your password.'));
        $jsonEncode = $this->_objectManager->create(\Magento\Framework\Json\Helper\Data::class);
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));
    }

}