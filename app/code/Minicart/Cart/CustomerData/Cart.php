<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Minicart\Cart\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Cart source
 */
class Cart extends \Magento\Checkout\CustomerData\Cart
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $checkoutCart;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Url
     */
    protected $catalogUrl;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * @var ItemPoolInterface
     */
    protected $itemPoolInterface;

    /**
     * @var int|float
     */
    protected $summeryCount;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layout;


    /**
     * {@inheritdoc}
     */
	 
    public function getSectionData()
    {
        $totals = $this->getQuote()->getTotals();
		$grandTotal = $totals['subtotal']->getValue();		
		
		$discount = $this->getQuote()->getShippingAddress()->getData();
                $discountCode="Discount";
                $discountCodeonly="";
                if(isset($discount['discount_description'])){
                    $discountCode="Discount(".$discount['discount_description'].")";
                    $discountCodeonly=$discount['discount_description'];
                }
		if(isset($discount['discount_amount'])):
			$discountTotal = $this->checkoutHelper->formatPrice($discount['discount_amount']);
			$grandTotal = $grandTotal + $discount['discount_amount'];
		else:
			$discountTotal = '';
		endif;
		
		
		if($this->getQuote()->getShippingAddress()->getData('customer_balance_amount'))
		{
			$storeCredit = $this->getQuote()->getShippingAddress()->getData('customer_balance_amount');
			//$storeCredit = isset($discount['shipping_amount']) ? ($storeCredit - $discount['shipping_amount']) : $storeCredit ;
			$grandTotal = $grandTotal - $this->getQuote()->getShippingAddress()->getData('customer_balance_amount') ;
		}else{
			$storeCredit = '';
		}
		
		if(isset($discount['shipping_amount']) && $this->getQuote()->getShippingAddress()->getData('customer_balance_amount') && $totals['grand_total']->getValue() <= 0)
		{	
		
			$storeCredit = $storeCredit - $discount['shipping_amount'];
		}
		

		if($grandTotal < 0):
			$grandTotal = $totals['grand_total']->getValue();	
		endif;
		
		$storeCredit = ($storeCredit > 0) ? ($this->checkoutHelper->formatPrice('-'.$storeCredit)) : '';
		$grandTotal = $this->checkoutHelper->formatPrice($grandTotal);
		return [
            'summary_count' => $this->getSummaryCount(),
            'subtotal' => isset($totals['subtotal'])
                ? $this->checkoutHelper->formatPrice($totals['subtotal']->getValue())
                : 0,
			'location'=>__($this->currentStoreLocation()),
			'grandtotal'=> $grandTotal,
			'discount'=> $discountTotal ,			
                        'discount_code'=> $discountCode,			
                        'discountCode'=> $discountCodeonly,			
			'stroe_credit'=> $storeCredit ,			
            'possible_onepage_checkout' => $this->isPossibleOnepageCheckout(),
            'items' => $this->getRecentItems(),
            'extra_actions' => $this->layout->createBlock('Magento\Catalog\Block\ShortcutButtons')->toHtml(),
            'isGuestCheckoutAllowed' => $this->isGuestCheckoutAllowed(),
            'disableCheckoutButton' => $this->disableCheckoutButton(),
            'website_id' => $this->getQuote()->getStore()->getWebsiteId()			
        ];
    }
	function disableCheckoutButton()
	{
		$customerId = $this->getQuote()->getCustomer()->getId();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customerObj = $objectManager->create(\Magento\Customer\Model\Customer::class)->load($customerId);
		$addresses = $customerObj->getAddresses();
		$locationId = "";
		$addressEmptyFlag = 0;
		if(isset($_COOKIE["Gourmet_Location_Id"])){
			$locationId =$_COOKIE["Gourmet_Location_Id"];
		}
		foreach ($addresses as $addresse) {
			$flag = 1;
			if($addresse->getLocationGroupId() == $locationId ){
				$addressEmptyFlag = 1;
			}
		}
		
		if(isset($_COOKIE['Gourmet_Location_Id']) && $addressEmptyFlag ==1){
			return true;
		}
		return false;
	}
	function currentStoreLocation()
	{
		if(isset($_COOKIE['Gourmet_Knockout_Location'])){
			return $_COOKIE['Gourmet_Knockout_Location'];
		}
		
		return "Select address from the list.";
	}
        /**
     * Get array of last added items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    protected function getRecentItems() {
        $items = [];
        if (!$this->getSummaryCount()) {
            return $items;
        }

        foreach (array_reverse($this->getAllQuoteItems()) as $item) {
            /* @var $item \Magento\Quote\Model\Quote\Item */
            $items[] = $this->itemPoolInterface->getItemData($item);
        }
        return $items;
    }

}
