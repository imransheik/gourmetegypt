<?php

namespace Mofluid\Mofluidapi2\Helper;

//require_once(dirname(__FILE__) . '/Stripe.php');
use Magento\Customer\Model\AccountManagement;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Quote\Model\Quote\Address\ToOrderAddress as ToOrderAddressConverter;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment as ToOrderPaymentConverter;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Sales\Api\OrderManagementInterface as OrderManagement;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Order\Email\Sender;
use Magento\Sales\Model\Order\Invoice\SenderInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Search\Model\QueryFactory;
use Magento\Catalog\Model\Layer\Filter\DataProvider\Category as CategoryDataProvider;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use \Magento\Wishlist\Model\Wishlist as wishlist;
use Magento\Wishlist\Model\WishlistFactory;

class Geolocation extends \Magento\Framework\App\Helper\AbstractHelper {
	/**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $_orderRepository;
    /*     * FE
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    private $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    private $_transaction;

    /**
     * @var ToOrderConverter
     */
    private $quoteAddressToOrder;

    /**
     * @var ToOrderAddressConverter
     */
    private $quoteAddressToOrderAddress;

    /**
     * @var ToOrderItemConverter
     */
    private $quoteItemToOrderItem;

    /**
     * @var ToOrderPaymentConverter
     */
    private $quotePaymentToOrderPayment;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var \Mofluid\Mofluidapi2\Model\Catalog\Product
     */
    private $mproduct;

    /**
     * @var \Mofluid\Mofluidapi2\Model\Theme
     */
    private $_theme;

    /**
     * @var \Mofluid\Mofluidapi2\Model\Themeimage
     */
    private $_themeimage;

    /**
     * @var \Mofluid\Mofluidapi2\Model\Message
     */
    private $_mmessage;

    /**
     * @var \Mofluid\Mofluidapi2\Model\Themecolor
     */
    private $_themecolor;

    /**
     * @var \Mofluid\Payment\Model\Index
     */
    private $_mpayment;

    /**
     * @var \Magento\Catalog\Model\Category
     */
    private $_category;

    /**
     * @var \Magento\Framework\App\CacheInterface
     */
    private $_cache;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    private $_currency;

    /**
     * @var \Magento\Cms\Model\Page
     */
    private $_page;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $_product;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeconfig;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    private $_taxcalculation;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $_customer;

    /**
     * @var \Magento\Customer\Model\Address
     */
    private $_address;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistry
     */
    private $stock;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    private $_pagefilter;

    /**
     * @var \Magento\Store\Model\Store
     */
    private $_store;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    private $_directory;

    /**
     * @var \Magento\GroupedProduct\Model\Product\Type\Grouped
     */
    private $grouped;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    private $_accountManagementInterface;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_session;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_date;

    /**
     * @var \Magento\Framework\Escaper
     */
    private $_escaper;

    /**
     * @var \Magento\Directory\Model\Country
     */
    private $country;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $configurable;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    private $taxcalculation;

    /**
     * @var \Magento\Directory\Model\Region
     */
    private $region;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $_timezone;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $_urlinterface;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    private $_quote;
    /*     * ResourceConnection
     * @var \Magento\Quote\Model\Quote\Item
     */
    private $_quoteitem;

    /**
     * @var \Magento\Customer\Model\Address\Form
     */
    private $_addressform;

    /**
     * @var \Magento\Sales\Model\Order
     */
    private $_orderData;

    /**
     * @var \Magento\GiftMessage\Model\Message
     */
    private $_giftMessage;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $_categoryCollectionFactory;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    private $_orderSender;

    /**
     * @var \Magedelight\Stripe\Model\Cards
     */
    private $_stripeId;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resource;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $product;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;
    private $_wishlist;
    private $_wishlistRepository;

    public function __construct(
    BuilderInterface $transactionBuilder,
    // EventManager $eventManager,
            OrderFactory $orderFactory, OrderManagement $orderManagement, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \Magento\Sales\Model\Service\InvoiceService $invoiceService, \Magento\Framework\DB\Transaction $transaction, ToOrderConverter $quoteAddressToOrder, ToOrderAddressConverter $quoteAddressToOrderAddress, ToOrderItemConverter $quoteItemToOrderItem, ToOrderPaymentConverter $quotePaymentToOrderPayment, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper, \Magento\Framework\App\Helper\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Mofluid\Mofluidapi2\Model\Catalog\Product $Mproduct, \Mofluid\Mofluidapi2\Model\Theme $Mtheme, \Mofluid\Mofluidapi2\Model\Themeimage $Mimage, \Mofluid\Mofluidapi2\Model\Message $Mmessage, \Mofluid\Mofluidapi2\Model\Themecolor $Mcolor, \Mofluid\Payment\Model\Index $Mpayment, \Magento\Catalog\Model\Category $categorydata, \Magento\Framework\App\CacheInterface $cachedata, \Magento\Framework\Locale\CurrencyInterface $currencydata, \Magento\Cms\Model\Page $pagedata, \Magento\Catalog\Model\Product $productData,
    // \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigData,
            \Magento\Tax\Api\TaxCalculationInterface $taxcalculationData, \Magento\Customer\Model\Customer $customerData, \Magento\Customer\Model\Address $addressData, \Magento\CatalogInventory\Model\StockRegistry $stockRegistry, \Magento\Cms\Model\Template\FilterProvider $pagefilterData, \Magento\Store\Model\Store $storeData, \Magento\Directory\Helper\Data $directoryData, \Magento\GroupedProduct\Model\Product\Type\Grouped $groupedProductData, \Magento\Customer\Api\AccountManagementInterface $accountManagementInterfaceData, \Magento\Customer\Model\Session $sessionData, \Magento\Framework\Stdlib\DateTime\DateTime $date, \Magento\Framework\Escaper $escaperData, \Magento\Directory\Model\Country $country, \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProductData, \Magento\Tax\Api\TaxCalculationInterface $taxcalculation, \Magento\Directory\Model\Region $region, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
    // \Magento\Framework\UrlInterface $urlInterface,
            \Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Item $QuoteItem, \Magento\Customer\Model\Address\Form $AddressFrom, \Magento\Sales\Model\Order $orderData, \Magento\GiftMessage\Model\Message $giftMessage, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
    //   \Magento\Customer\Api\Data\CustomerInterface $customerInterface,
            \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory, \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender, \Magento\Framework\Data\Form\FormKey $formKey, \Magento\Quote\Model\QuoteFactory $quoteFactory, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Catalog\Model\ProductFactory $productFactory,
    // \Magedelight\Stripe\Model\Cards $stripeId,
            \Magento\Checkout\Model\Cart $cart, \Magento\Search\Model\QueryFactory $queryFactory,
    //\Magento\Framework\ObjectManagerInterface $objectInterface,
            InvoiceSender $invoicesender, wishlist $_wishlist, \Magento\Wishlist\Model\WishlistFactory $wishlistRepository
    ) {
        //$this->encryptor = $encryptor;
        $this->mproduct = $Mproduct;
        $this->_storeManager = $storeManager;
        $this->_category = $categorydata;
        $this->_cache = $cachedata;
        $this->_currency = $currencydata;
        $this->_page = $pagedata;
        $this->_product = $productData;
        $this->_scopeconfig = $context->getScopeConfig(); //$scopeConfigData;
        $this->_taxcalculation = $taxcalculationData;
        $this->customerRepository = $customerRepository;
        $this->_customer = $customerData;
        $this->stock = $stockRegistry;
        $this->_pagefilter = $pagefilterData;
        $this->taxcalculation = $taxcalculation;
        $this->_store = $storeData;
        $this->_directory = $directoryData;
        $this->configurable = $configurableProductData;
        $this->grouped = $groupedProductData;
        $this->_address = $customerData;
        $this->_accountManagementInterface = $accountManagementInterfaceData;
        $this->_session = $sessionData;
        $this->_escaper = $escaperData;
        $this->_date = $date;
        $this->_timezone = $timezone;
        $this->_country = $country;
        $this->_region = $region;
        $this->_address = $addressData;
        $this->_theme = $Mtheme;
        $this->_themeimage = $Mimage;
        $this->_mmessage = $Mmessage;
        $this->_themecolor = $Mcolor;
        $this->_urlinterface = $context->getUrlBuilder(); //$urlInterface;
        $this->_mpayment = $Mpayment;
        $this->_quote = $quote;
        $this->_quoteitem = $QuoteItem;
        $this->_addressform = $AddressFrom;
        $this->_orderData = $orderData;
        $this->_giftMessage = $giftMessage;
        $this->eventManager = $context->getEventManager(); //$eventManager;
        $this->orderFactory = $orderFactory;
        $this->orderManagement = $orderManagement;
        $this->quoteAddressToOrder = $quoteAddressToOrder;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        $this->quoteItemToOrderItem = $quoteItemToOrderItem;
        $this->quotePaymentToOrderPayment = $quotePaymentToOrderPayment;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->quoteRepository = $quoteRepository;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_orderSender = $orderSender;
        //$this->_objectManager=$objectInterface;
        // $this->_stripeId = $stripeId;
        $this->transactionBuilder = $transactionBuilder;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->invoiceSender = $invoicesender;
        $this->formKey = $formKey;
        $this->_cart = $cart;
        $this->quote = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
        $this->product = $productFactory;
        $this->_queryFactory = $queryFactory;
        $this->_wishlist = $_wishlist;
        $this->_wishlistRepository = $wishlistRepository;
        //$this->wishlist = $wishlist;


        parent::__construct($context);
    }
	
	public function allboundary() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$coorinates= array();
        $data = array();
        $locationgroup = $objectManager->create('I95Dev\Locationgroup\Model\Data')->getCollection()->addFieldToFilter('status',1);
		$result = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		foreach($locationgroup as $item){
			$locationgroupid=$item->getId();
			//print_r($item->getData());
			$storeid = $item->getStoreId();
			$boundarydata = $objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection()->addFieldToFilter('location_id',$locationgroupid);
			$boundarydatacoorinate= $boundarydata->getData();

			$coorinates['coordinates'] = array();
			foreach ($boundarydatacoorinate as $coorinate){
				$coorinates["location_id"] = $locationgroupid;
				$coorinates["store_id"] = $storeid;
				$temp = explode(",",trim($coorinate["coordination"]));
				
				$temp[0] = trim($temp[0]);
				$temp[1] = trim($temp[1]);
				$coorinates['coordinates'][]= $temp;
			}
			array_push($data, $coorinates);
		}

        $response=array();
        $response['success'] = true;
        $response['boundary'] = $data;
        $response['message'] = "Coordinates data!";
		$result->setData($response);
        return $response; 
    }
	public function boundary($location_id) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$locationgroup = $objectManager->create('I95Dev\Locationgroup\Model\Data')->load($location_id);
        $boundarydata = $objectManager->create('I95Dev\Locationgroup\Model\Address')->getCollection()->addFieldToFilter('location_id',$location_id);
        $boundarydatacoorinate= $boundarydata->getData();
        $result = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $coorinates= array();
		$storeid=$locationgroup->getStoreId();

        foreach ($boundarydatacoorinate as $coorinate){
			$coorinates["location_id"] = $location_id;
			$coorinates["store_id"] = $storeid;
            $coorinates['coordinates'][]= json_encode(explode(",",trim($coorinate["coordination"])));
        }

        $response=array();
        $response['success'] = true;
        $response['boundary'] = $coorinates;
        $response['message'] = "Coordinates data!";
        $result->setData(json_encode($response));
        return $response;
    }
	
	public function deliveryslot($location_id, $store_id) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$result = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
		$date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('Y-m-d');
		
		$response = file_get_contents($this->getServerUrl().'mofluidapi2?callback=&currency=EGP&service=geodeliverySlot&city=&deliveryDate='.$date.'&location_id='.$location_id.'&store_id='.$store_id.'&quote_id=&store=1');
		$response = json_decode($response);
		$slots = $response->payload->data->regions;
		$rates = $objectManager->create('\I95Dev\DeliverySlot\Model\DeliverySlot')->getCollection();
		$rates->addFieldToFilter('dest_zip',trim($location_id))->addFieldToFilter('dest_zip_to',trim($store_id));
		
		if(!empty($slots)){
			$delievrySlots= $slots[0]->slots;
			$city = $slots[0]->city;
		}else{
			foreach($rates as $rate){
				$delievrySlots= $rate->getShippingMethod();
				$city = $rate->getData('dest_city');
				break;
			}
			
		}
		if(empty($delievrySlots)){
			$delievrySlots = "";
		}
		
        $response=array();
        $response['success'] = true;
        $response['delievryslots'] = $delievrySlots;
		if(empty($city)){
			$response['message'] = "Delivery slot are missing for this location";
			$response['city'] = "";
		}else{
			 $response['city'] = $city;
			 $response['message'] = "Coordinates data!";
		}
       
        
        $result->setData(json_encode($response));
        return $response;
    }
	
	public function getaddresslistbycustomerid($customer_id) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$request = $objectManager->get('Magento\Framework\App\RequestInterface');
		$token_id = $request->getHeader('Authorization');
		//echo $token_id; exit;
		$url = $this->getServerUrl().'index.php/rest/V1/customers/'.$customer_id;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $options = array(CURLOPT_URL => $url,
            CURLOPT_VERBOSE => 0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => "Mozilla/4.0 (compatible;MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)",
            CURLOPT_POST => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET");

        curl_setopt_array($curl, $options);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: $token_id", "Accept: */*", "content-type: application/json"));
        
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($curl);
		$arrayData = json_decode($result);
		if(isset($arrayData->addresses)){
			$addresses = $arrayData->addresses;
		}else{
			
			return $arrayData;
		}
		
		$addressList =array();
		foreach($addresses as $address){
			$flag = 1;
			$custom_attributes =$address->custom_attributes;
			$addressname = $latitude = $addresstype = $longitude =$location_group_id =$floor = $store_address_id =$building= $apartment =$update_status="";
			foreach($custom_attributes as $custom_attribute){
				if($custom_attribute->attribute_code == 'addressname'){
					$addressname=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'addresstype'){
					$addresstype=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'latitude'){
					$latitude=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'longitude'){
					$longitude=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'location_group_id'){
					$location_group_id=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'store_address_id'){
					$store_address_id=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'floor'){
					$floor=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'building'){
					$building=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'apartment'){
					$apartment=$custom_attribute->value;
				}
				if($custom_attribute->attribute_code == 'update_status'){
					$update_status=$custom_attribute->value;
				}
			}
			if(empty($addressname) || empty($addresstype)  || empty($latitude) || empty($longitude) || empty($location_group_id) || empty($store_address_id)){
				$flag = 0;
				
			}
			$region_code = $address->region;
			// print_r($region_code->region_code);
			// exit;
			
			$street = $address->street[0];
			$address->entity_id = $address->id;
			$address->street = $street;
			unset($region);
			unset($address->id);
			unset($address->custom_attributes);
			$address->region = $region_code->region;
			$address->update_status = $update_status;
			$address->apartment = $apartment;
			$address->building = $building;
			$address->floor = $floor;
			$address->store_address_id = $store_address_id;
			$address->location_group_id = $location_group_id;
			$address->longitude = $longitude;
			$address->latitude = $latitude;
			$address->addresstype = $addresstype;
			$address->addressname = $addressname;
			
			$address->isValid = $flag;
			$addressList[] = $address;
		}
		$arrayData->addresses = $addressList;
        return $arrayData;
	}
	
	public function saveaddressbycustomerid($customer_id) {
		if(!$this->ws_validateAuthenticate()){
			$res["login_status"] = 0;
			$res["error_code"] = 101;
			$res["error_message"] = "Invalid Access.";
			return $res;
		}
		$res = array();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$request = $objectManager->get('Magento\Framework\App\RequestInterface');
		$data = $request->getContent(); 
		$data = json_decode($data);
		$location_id = $data->address_id;
        $customer_id = $data->customer_id;
        $firstname = $data->firstname;
        $lastname = $data->lastname;
        $telephone = $data->telephone;
        $street = $data->street;
        $floor = $data->floor;
        $city = $data->city;
        $store_address_id = $data->store_address_id;
        $region = $data->region;
        $location_group_id = $data->location_group_id;
        $longitude = $data->longitude;
        $latitude = $data->latitude;
        $country = $data->country;
        $building = $data->building;
        $apartment = $data->apartment;
        $addresstype = $data->addresstype;
        $addressname = $data->addressname;
        $store = $data->store;
		if(!empty($location_id)){
			//$addressData  = $this->_objectManager->create('Magento\Customer\Api\AddressRepositoryInterface')->getById($location_id);
			$addressData = $objectManager->create('\Magento\Customer\Model\Address')->load($location_id);
			$addressData->setFirstname($firstname);
			$addressData->setLastname($lastname);
			$addressData->setTelephone($telephone);
			$addressData->setStreet($street);
			$addressData->setFloor($floor);
			$addressData->setCity($city);
			$addressData->setRegionId(0);
			$addressData->setRegion($region);
			$addressData->setStoreAddressId($store_address_id);
			$addressData->setLongitude($longitude);
			$addressData->setLatitude($latitude);
			$addressData->setCountryId($country);
			$addressData->setApartment($apartment);
			$addressData->setBuilding($building);
			$addressData->setAddresstype($addresstype);
			$addressData->setAddressname($addressname);
			$addressData->setLocationGroupId($location_group_id);
			$addressData->setIsDefaultShipping('1');
			$addressData->setIsDefaultBilling('1');

			
		}else{
			$addressData = $objectManager->create('\Magento\Customer\Model\Address');
			$addressData->setFirstname($firstname);
			$addressData->setLastname($lastname);
			$addressData->setTelephone($telephone);
			$addressData->setStreet($street);
			$addressData->setFloor($floor);
			$addressData->setCity($city);
			$addressData->setRegion($region);
			$addressData->setStoreAddressId($store_address_id);
			$addressData->setLongitude($longitude);
			$addressData->setLatitude($latitude);
			$addressData->setCountryId($country);
			$addressData->setAddresstype($addresstype);
			$addressData->setApartment($apartment);
			$addressData->setCustomerId($customer_id);
			$addressData->setBuilding($building);
			$addressData->setAddressname($addressname);
			//$addressData->setRegionId(0);
			$addressData->setLocationGroupId($location_group_id);
			$addressData->setIsDefaultShipping('1');
			$addressData->setIsDefaultBilling('1');
		} 
		try{
			$addressData->save();
			$customerObj = $this->_customer;
			$storemodel = $this->_store;
			$websiteId = $storemodel->load($store)->getWebsiteId();
			$customerAddressList = "";

			$login_customer = $customerObj->setWebsiteId($websiteId);
			$login_customer->load($customer_id);
			$city = $error_message = "";
            if ($shippingAddress = $login_customer->getDefaultShippingAddress()) {
                $city = !empty($shippingAddress) ? $shippingAddress->getCity() : 0;
            }
            $customerAddressList =array();
			$res["username"] = $login_customer->getEmail();
			$res["email"] = $login_customer->getEmail();
			$res["firstname"] = $login_customer->getFirstname();
			$res["lastname"] = $login_customer->getLastname();
			$res["id"] = $customer_id;
            foreach ($login_customer->getAddresses() as $address) {
				
				$customerAddress = $address->getData();
				if(array_key_exists("latitude",$customerAddress) && array_key_exists("longitude",$customerAddress) && array_key_exists("location_group_id",$customerAddress) &&  array_key_exists("store_address_id",$customerAddress) && array_key_exists("addressname",$customerAddress) && array_key_exists("addresstype",$customerAddress) ){
					$flag = 1;
				}else{
					$flag = 0;
				}
				if(!array_key_exists("addresstype",$customerAddress)){
					$customerAddress["addresstype"] = "";
				}
				$customerAddress["isValid"] = $flag;
				$customerAddressList[] = $customerAddress;
                
            }

			if(empty($shippingAddress)){
				$defaultAdd = null;
			}else{
				$defaultAdd = $shippingAddress->getData();
				$flag = 0;
				if (!array_key_exists("location_group_id",$defaultAdd)){
					$defaultAdd["location_group_id"] = "";
				}
				if (!array_key_exists("store_address_id",$defaultAdd)){
					$defaultAdd["store_address_id"] = "";
				}
				if (!array_key_exists("longitude",$defaultAdd)){
					$defaultAdd["longitude"] = "";
				}
				if (!array_key_exists("latitude",$defaultAdd)){
					$defaultAdd["latitude"] = "";
				}
				if(array_key_exists("latitude",$customerAddress) && array_key_exists("longitude",$customerAddress) && array_key_exists("location_group_id",$customerAddress) &&  array_key_exists("store_address_id",$customerAddress) && array_key_exists("addressname",$customerAddress) && array_key_exists("addresstype",$customerAddress) ){
					$flag = 1;
				}else{
					$flag = 0;
				}
				$defaultAdd["isValid"] = $flag;
			}

            $res["default_shipping_address"] = $defaultAdd;
			$res["all_address"]['addresses'] = $customerAddressList;
			$res["all_address"]["id"] = $login_customer->getData('entity_id');
			$res["all_address"]["group_id"] = $login_customer->getData('group_id');
			$res["all_address"]["default_billing"] = $login_customer->getData('default_billing');
			$res["all_address"]["default_shipping"] = $login_customer->getData('default_shipping');
			$res["all_address"]["created_at"] = $login_customer->getData('created_at');
			$res["all_address"]["updated_at"] = $login_customer->getData('updated_at');
			$res["all_address"]["created_in"] = $login_customer->getData('created_in');
			$res["all_address"]["email"] = $login_customer->getData('email');
			$res["all_address"]["firstname"] = $login_customer->getData('firstname');
			$res["all_address"]["lastname"] = $login_customer->getData('lastname');
			$res["all_address"]["gender"] = $login_customer->getData('gender');
			$res["all_address"]["store_id"] = $login_customer->getData('store_id');
			$res["all_address"]["website_id"] = $login_customer->getData('website_id');
			
		}catch(\Exception $e){
			//print_r($e->getMessage());
			$res["error_code"] = 101;
			$res["error_message"] = $e->getMessage();
		}
		
		//$address = str_replace(" ", "+", $address);
		return $res;
	}
	public function customerDetails($customer_id, $username, $location_id, $store_id ,$store){
		$customerObj = $this->_customer;
        $storemodel = $this->_store;
        $websiteId = $storemodel->load($store)->getWebsiteId();
        $res = array();
		$customerAddressList = "";

        $login_customer = $customerObj->setWebsiteId($websiteId);
        $login_customer->load($customer_id);
        $error_code = "701";
		$error_message = "";
        $customerAddress = null;
		if(!$this->ws_validateAuthenticate()){
			$res["login_status"] = 0;
			$res["error_code"] = 101;
			$res["error_message"] = "Invalid Access.";
			return $res;
		}
		
		if($login_customer->getEmail() != $username){
			$res["login_status"] = 0;
			$res["error_code"] = 101;
			$res["error_message"] = "Invalid Customer.";
			return $res;
		}
		//adding IOS and android version
		$scopeConfig = $this->_scopeconfig;
        $IOS = $scopeConfig->getValue(
            'appversion/general/ios', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $Android = $scopeConfig->getValue(
            'appversion/general/android', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $res['playstore'] = $Android;
        $res['appstore'] = $IOS;
		$res["username"] = $login_customer->getEmail();
        $res["email"] = $login_customer->getEmail();
		
		$status = 0;
        if ($login_customer->getId()) {
            try {
				$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$helper = $_objectManager->create('\Mofluid\Mofluidapi2\Helper\Data');
                $quote = $_objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($login_customer);
                $quote->setData("redeem_loyalty_amount", '');
                $quote->setData("bloyal_cartid", '');
                $quote->collectTotals()->save();
                $res["websiteid"] = $websiteId;
                $login_status = 1;
                $res["firstname"] = $login_customer->getFirstname();
				
                $res["lastname"] = $login_customer->getLastname();
                $res["customergroupid"] = $login_customer->getGroupId();
                $res["store_credit"] = $helper->getStoreCreditByCustomerID($login_customer->getId(), $websiteId);
                $res["primary_mobile"] = $login_customer->getData("primary_mobile_number");
                $res["isd_code"] = $login_customer->getData("isd_code");
                if ($login_customer->getData("is_loyalty")) {
                    $res["is_loyalty"] = 1;
                } else {
                    $res["is_loyalty"] = 0;
                }
				if ($login_customer->getData("is_contactinfoupdated")) {
					$status = 1;
				} else {
					$status = 0;
				}
				$res["is_available"] = $status;
                $prefered_contact = array();
                $prefered_contact = explode(',', $login_customer->getPreferedContact());
                $preferedContact = array();
                foreach ($prefered_contact as $pc) {
                    if ($pc == "211") {
                        $preferedContact[] = 'Call';
                    } else if ($pc == "212") {
                        $preferedContact[] = 'SMS';
                    } else if ($pc == "213") {
                        $preferedContact[] = 'Email';
                    }
                }

                if (!empty($preferedContact)) {
                    $preferedContact = implode(',', $preferedContact);
                } else {

                    $preferedContact = 'Email';
                }

                $prefered_language = $login_customer->getPreferedLanguage();
                if ($prefered_language == "210") {
                    $preferedLanguage = "Arabic";
                } else {
                    $preferedLanguage = "English";
                }
                $res["prefered_contact"] = $preferedContact;
                $res["prefered_language"] = $preferedLanguage;


                $res["id"] = $login_customer->getId();
                $res["stripecustid"] = '0';
                if ($login_customer->getMdStripeCustomerId() != null) {
                    $res["stripecustid"] = $login_customer->getMdStripeCustomerId();
                }
                $city = $error_message = "";
                if ($shippingAddress = $login_customer->getDefaultShippingAddress()) {
                    $city = !empty($shippingAddress) ? $shippingAddress->getCity() : 0;
                }
                $customerAddressList['addresses'] =array();
				$customerAddressList["id"] = $login_customer->getData('entity_id');
				$customerAddressList["group_id"] = $login_customer->getData('group_id');
				$customerAddressList["default_billing"] = $login_customer->getData('default_billing');
				$customerAddressList["default_shipping"] = $login_customer->getData('default_shipping');
				$customerAddressList["created_at"] = $login_customer->getData('created_at');
				$customerAddressList["updated_at"] = $login_customer->getData('updated_at');
				$customerAddressList["created_in"] = $login_customer->getData('created_in');
				$customerAddressList["email"] = $login_customer->getData('email');
				$customerAddressList["firstname"] = $login_customer->getData('firstname');
				$customerAddressList["lastname"] = $login_customer->getData('lastname');
				$customerAddressList["gender"] = $login_customer->getData('gender');
				$customerAddressList["store_id"] = $login_customer->getData('store_id');
				$customerAddressList["website_id"] = $login_customer->getData('website_id');
                foreach ($login_customer->getAddresses() as $address) {
					
					$customerAddress = $address->getData();
					if(array_key_exists("latitude",$customerAddress) && array_key_exists("longitude",$customerAddress) && array_key_exists("location_group_id",$customerAddress) &&  array_key_exists("store_address_id",$customerAddress) && array_key_exists("addressname",$customerAddress) && array_key_exists("addresstype",$customerAddress) ){
						$flag = 1;
					}else{
						$flag = 0;
					}
					if(!array_key_exists("addresstype",$customerAddress)){
						$customerAddress["addresstype"] = "";
					}
					$customerAddress["isValid"] = $flag;
					$customerAddressList['addresses'][] = $customerAddress;
                    
                }

				if(empty($shippingAddress)){
					$defaultAdd = null;
				}else{
					$defaultAdd = $shippingAddress->getData();
					$flag = 0;
					if (!array_key_exists("location_group_id",$defaultAdd)){
						$defaultAdd["location_group_id"] = "";
					}
					if (!array_key_exists("store_address_id",$defaultAdd)){
						$defaultAdd["store_address_id"] = "";
					}
					if (!array_key_exists("longitude",$defaultAdd)){
						$defaultAdd["longitude"] = "";
					}
					if (!array_key_exists("latitude",$defaultAdd)){
						$defaultAdd["latitude"] = "";
					}
					if(!array_key_exists("latitude",$defaultAdd) || !array_key_exists("longitude",$defaultAdd) ||!array_key_exists("location_group_id",$defaultAdd) ||  !array_key_exists("store_address_id",$defaultAdd) && array_key_exists("addressname",$customerAddress) && array_key_exists("addresstype",$customerAddress) ){
						$flag = 1;
					}
					$defaultAdd["isValid"] = $flag;
				}

                $res["default_shipping_address"] = $defaultAdd;

                $res["all_address"] = $customerAddressList;
				//Delivery slot
				$date = $_objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('Y-m-d');
		
				$response = file_get_contents($this->getServerUrl().'mofluidapi2?callback=&currency=EGP&service=geodeliverySlot&city=&deliveryDate='.$date.'&location_id='.$location_id.'&store_id='.$store_id.'&quote_id=&store=1');
				$response = json_decode($response);
				$slots = $response->payload->data->regions;
				$rates = $_objectManager->create('\I95Dev\DeliverySlot\Model\DeliverySlot')->getCollection();
				$rates->addFieldToFilter('dest_zip',trim($location_id))->addFieldToFilter('dest_zip_to',trim($store_id));
		
				if(!empty($slots)){
					$delievrySlots= $slots[0]->slots;
					$city = $slots[0]->city;
				}else{
					foreach($rates as $rate){
						$delievrySlots= $rate->getShippingMethod();
						$city = $rate->getData('dest_city');
						break;
					}
				}
				if(empty($delievrySlots)){
					$delievrySlots = "";
				}
				if(empty($city)){
					$res['error_message'] = "Delivery slot are missing for this location";
					$res['shipping_slots']['city'] = "";
				}else{
					$res['shipping_slots']['city'] = $city;
				}

				$res['shipping_slots']['delievryslots'] = $delievrySlots;
				$logger = $_objectManager->get(\Magento\Customer\Model\Logger::class);
				$logger -> log($login_customer->getId(),
							['last_login_at' => (new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT)]);
            } catch (Exception $e) {
                $login_status = 0;
                $error_code = "500";
                $error_message = $e->getMessage();
            }
        } else {
            $login_status = 0;
            $error_code = "101";
            $error_message = "Invalid User.";
        }
        $res["login_status"] = $login_status;
        $res["error_code"] = $error_code;
        $res["error_message"] = $error_message;
		return $res;
	}
	public function getServerUrl() {
        //return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url = $storeManager->getStore()->getBaseUrl();
        return $url;
    }
	private function getAuthToken(){
        $adminUrl = $this->getServerUrl().'/index.php/rest/V1/integration/admin/token/';
        $ch = curl_init();
        $data = array("username" => $this->adminUserName, "password" => $this->adminPassword);
        $ch = curl_init($adminUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type=> application/json'));
        $token = curl_exec($ch);
        $token1 = json_decode($token);
        if(isset($token1->message) and $token1->message != ''){
            $info = 'There is an issue in loging into the system. Please check credentials.';
            return json_encode(array('status' => 'Failure', 'data' => '', 'info' => $info));
            exit;
        }else{
            return $token1;
        }
    }
	
	public function ws_validateAuthenticate()
	{	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$request = $objectManager->get('Magento\Framework\App\RequestInterface');
		$authappid = $request->getHeader('authappid');
		$token = $request->getHeader('token');
		$secretkey = $request->getHeader('secretkey');
		if(empty($authappid) || $authappid == null)
			  return false;
		if(empty($token) || $token == null)
			return false; 
		if(empty($secretkey) || $secretkey == null)
			return false;
		
		$resource =$objectManager->get(\Magento\Framework\App\ResourceConnection::class);
		$connection = $resource->getConnection();
		$sql = "select * from mofluid_authentication where appid='". $authappid."' and token='". $token."' and secretKey='". $secretkey."'";
		$response = $connection->fetchAll($sql);

		if(count($response) > 0){
			return true;
		}else{
			return false;
		}
		return false;
	}
	
}