<?php
namespace Mofluid\Mofluidapi2\Observer;

class Categoryattributes implements \Magento\Framework\Event\ObserverInterface
{
    private $category = null;
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helper = $objectManager->get('Mofluid\Mofluidapi2\Helper\Data');
		$fileSystem =  $objectManager->create('\Magento\Framework\Filesystem');
		$mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::PUB)->getAbsolutePath();
		$res = $helper->fetchInitialData(1,'initial', 'EGP');
		$array = array('metadata'=> array('success'=>true,'errorCode'=>Null,'message'=>null),'payload'=> array('data'=>$res) );
		$fp = fopen($mediaPath.'menu/results_en.json', 'w');
		fwrite($fp, json_encode($array,JSON_UNESCAPED_UNICODE));
		fclose($fp);
		$res = $helper->fetchInitialData(3,'initial', 'EGP');
		$array = array('metadata'=> array('success'=>true,'errorCode'=>Null,'message'=>null),'payload'=> array('data'=>$res) );
		$fp = fopen($mediaPath.'menu/results_eg.json', 'w');
		fwrite($fp, json_encode($array,JSON_UNESCAPED_UNICODE));
		fclose($fp);
    }
}