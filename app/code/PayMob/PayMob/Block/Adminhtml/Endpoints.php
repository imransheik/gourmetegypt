<?php

namespace PayMob\PayMob\Block\Adminhtml;

/**
 * TrustSpot_OrderNotifier Adminhtml Block for Connection Test
 *
 * @category    TrustSpot Extensions
 * @package     TrustSpot_OrderNotifier
 * @author      TrustSpot Inc
 */
class Endpoints extends \Magento\Config\Block\System\Config\Form\Field {

    protected $apiHelper;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_resourceConfig;

    public function __construct(
    \PayMob\PayMob\Helper\Api $apiHelper, \Magento\Config\Model\ResourceModel\Config $resourceConfig
    ) {
        $this->apiHelper = $apiHelper;
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * Prepares Top HTML for TrustSpot Settings page
     *
     * @access protected
     * @author TrustSpot Inc
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element) {
        $this->setElement($element);

        $baseURL = $this->apiHelper->getBaseURL();
        $notifyEndpoint = $baseURL . 'paymob/notification/callback';
        $txnEndpoint = $baseURL . 'paymob/txn/callback';
        
        $html = "<div style='margin: 10px 0;'> 
                    <p><b>Merchant Notification Endpoint:</b><br/>$notifyEndpoint</p>
                    <p><b>Transanction Response Endpoint:</b><br/>$txnEndpoint</p>
                </div>";

        return $html;
    }

    protected function _renderScopeLabel(\Magento\Framework\Data\Form\Element\AbstractElement $element) {
        $html = '<td class="scope-label">';
        if ($element->getScope()) {
            $html .= $element->getScopeLabel();
        }
        $html .= '</td>';
        return $html;
    }

}
