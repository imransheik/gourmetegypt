<?php

namespace PayMob\PayMob\Block;

class Config extends \Magento\Framework\View\Element\Template {

    protected $_cart;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Checkout\Model\Cart $cart
    ) {
        $this->_cart = $cart;
        parent::__construct($context);
    }

}
