<?php

/**
 * A Magento 2 module named TrustSpot/OrderNotifier
 * Copyright (C) 2016  2015
 * 
 * This file included in TrustSpot/OrderNotifier is licensed under OSL 3.0
 * 
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace PayMob\PayMob\Controller\Adminhtml\System\Config;
use Magento\Framework\Controller\ResultFactory;

class Iframe extends \Magento\Backend\App\Action {

    protected $resultPageFactory;

    /**
     * @var \TrustSpot\OrderNotifier\Helper\Api
     */
    protected $paymobApiHelper;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_resourceConfig;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \PayMob\PayMob\Helper\Api $paymobApiHelper, \Magento\Config\Model\ResourceModel\Config $resourceConfig, \Magento\Framework\App\Request\Http $request
    ) {
        $this->paymobApiHelper = $paymobApiHelper;
        $this->_resourceConfig = $resourceConfig;
        $this->request = $request;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $_helper = $this->paymobApiHelper;

        $html = $this->request->getParam('iframe_html');
        $css = $this->request->getParam('iframe_css');
        $js = $this->request->getParam('iframe_js');
        $data = array(
            'html' => $html,
            'css' => $css,
            'js' => $js,
        );


        
        $_helper->generateIframeToken();

        if ($_helper->uploadIframe($data)) {
            $message = "<span style='color:green'>Payment Iframe was successfully uploaded to Paymob Solutions.</span>";
        } else {
            $message = "<span style='color:#f00'>Error! Please check your account username and password.</span>";
        }


        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response->setData(['message' => $message]);
        $response->setHttpResponseCode(200);

        return $response;
    }

    public function __execute() {

        $_helper = $this->orderNotifierApiHelper;
        $email = $this->request->getParam('email');
        $key = $this->request->getParam('apikey');
        $data = array(
            'email' => $email,
            'key' => $key,
        );


        if ($_helper->verify($data)) {
            /**
             * Settings are verified
             * Now saving Email and Api Key
             */
            $this->_resourceConfig->saveConfig(
                    'trustspot_ordernotifier/general/email', $email, 'default', 0
            );
            $this->_resourceConfig->saveConfig(
                    'trustspot_ordernotifier/general/api_key', $key, 'default', 0
            );

            $this->messageManager->addSuccess('The extension is connected to your TrustSpot account.');
        } else {
            $this->messageManager->addError('Error! Please check your account email and api key then retry.');
        }

        $this->_redirect($this->_redirect->getRefererUrl());
    }

}
