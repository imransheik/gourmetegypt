<?php

namespace PayMob\PayMob\Controller\Checkout;

use Magento\Framework\Controller\ResultFactory;

class Callback extends \Magento\Framework\App\Action\Action {

    protected $_paymentMethod;
    protected $_notification;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $_orderSender;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $transactionFactory;
    protected $paymobApiHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager     
     * @param Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param  \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \PayMob\PayMob\Model\Payment\Acceptance $paymentMethod, \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender, \Magento\Checkout\Model\Session $checkoutSession, \Psr\Log\LoggerInterface $logger, \PayMob\PayMob\Helper\Api $paymobApiHelper, \Magento\Framework\DB\TransactionFactory $transactionFactory
    ) {
        $this->_paymentMethod = $paymentMethod;
        //$this->_client = $this->_paymentMethod->getClient();
        $this->transactionFactory = $transactionFactory;
        $this->_orderSender = $orderSender;
        $this->_checkoutSession = $checkoutSession;
        $this->paymobApiHelper = $paymobApiHelper;
        $this->_logger = $logger;

        parent::__construct($context);
    }

    public function execute() {

        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            $this->_logger->debug('gbgdn');
            $this->_loadOrder();
            $this->_payMobAuthenticate();
            $paymob_order = $this->_registerPayMobOrder();
            $this->_registerPayMobPaymentKey();

            $this->_logger->debug('after register_paymob order');

            $iframeID = $this->_paymentMethod->getConfigData('iframe_id');
            $iframeSrc = "https://accept.paymobsolutions.com/api/acceptance/iframes/$iframeID?payment_token=" . $this->_order->getPaymobPaymentKey();

            $responseContent = [
                'success' => true,
                'iframeSrc' => $iframeSrc,
                'url'=>$iframeSrc,
                'parameters' => []
            ];

            $response->setData($responseContent);
            $response->setHttpResponseCode(200);

            // $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            // $resultRedirect->setUrl($paymob_order->url);
            // return $resultRedirect;

            return $response;
        } catch (\Exception $e) {

            $responseContent = [
                'success' => false,
                'iframeSrc' => '',
                'message' => $e->getMessage(),
                'parameters' => []
            ];
            $response->setData($responseContent);
            $response->setHttpResponseCode(400);

            return $response;
        }
    }

    protected function _payMobAuthenticate() {
        $helper = $this->paymobApiHelper;
        $helper->generateCustomerToken();
    }

    protected function _registerPayMobOrder() {

        $order = $this->_order;
        
        $payMobOrderId = $order->getPaymobOrderId();
        $this->_logger->debug('registeer paymob order');

        $requestData = [
            'delivery_method' => (boolean) $this->_paymentMethod->getConfigData('enable_delivery'),
            'merchant_id' => $this->_paymentMethod->getConfigData('merchant_id'),
            'amount_cents' => $order->getGrandTotal() * 100,
            'currency' => $order->getBaseCurrencyCode(),
            'merchant_order_id' => $order->getId(),
            'items' => [],
        ];

        $requestData['shipping_data'] = $this->getShippingData();

        if ((boolean) $this->_paymentMethod->getConfigData('enable_delivery')) {

            $requestData['collector_id'] = $this->_paymentMethod->getConfigData('collector_id');
            
        }

        $helper = $this->paymobApiHelper;

        if (!$payMobOrderId) {
            $payMobOrderId = $helper->registerCustomerOrder($requestData);
            $order->setPaymobOrderId($payMobOrderId->id)->save();
            $order->setExtOrderId($payMobOrderId->id)->save();;
            return $payMobOrderId;
        }
    }

    protected function _registerPayMobPaymentKey() {

        $order = $this->_order;        
        $helper = $this->paymobApiHelper;


        $requestData = [
            'amount_cents' => $order->getGrandTotal() * 100,
            'currency' => $order->getBaseCurrencyCode(),
            'order_id' => $order->getPaymobOrderId(),
            "card_integration_id" => $this->_paymentMethod->getConfigData('card_integration_id')
        ];


        $requestData['billing_data'] = $this->getShippingData();

        $paymentToken = $helper->requestPaymentKey($requestData);
        if ($paymentToken) {
            $order->setPaymobPaymentKey($paymentToken)->save();
        }
    }

    protected function getShippingData() {

        $order = $this->_order;
        $billing = $order->getBillingAddress();
        $shipping = $order->getShippingAddress();
        
        $shippingData = [
            'apartment' => '42',
            'email' => $order->getCustomerEmail(),
            'floor' => '803',
            'first_name' => $billing->getFirstname(),
            'street' => $billing->getStreetLine(1),
            'building' => '8028',
//                ''     => $billing->getStreetLine(2),
            'phone_number' => $billing->getTelephone(),
            'city' => $billing->getCity(),
            'postal_code' => $billing->getPostcode(),
            'state' => $billing->getRegion(),
            'last_name' => $billing->getLastname(),
            'country' => $billing->getCountryId(),
            'shipping_method' => 'PKG', //$order->getShippingMethod()
        ];

        return $shippingData;
    }

    protected function _registerPaymentCapture() {

        $order = $this->_order;
        //echo $this->_order->getId(); exit;
        if ($order->getPayment()->getMethod() == $this->_paymentMethod->getCode()) {
            $qtys = [];
            foreach ($order->getAllItems() as $orderItem) {
                $qtys[$orderItem->getId()] = $orderItem->getQty();
            }

            $invoice = $order->prepareInvoice($qtys);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
            $invoice->register();
            $transaction = $this->transactionFactory->create();
            $transaction->addObject($invoice)
                    ->addObject($invoice->getOrder())
                    ->save();

            if ($invoice && !$this->_order->getEmailSent()) {
                $this->_orderSender->send($this->_order);
                $this->_order->addStatusHistoryComment(
                        __('You notified customer about invoice #%1.', $invoice->getIncrementId())
                )->setIsCustomerNotified(
                        true
                )->save();
            }
        }
    }

    protected function _loadOrder() {
        $session = $this->getCheckout();
        $order_id = $session->getLastOrderId();
        //$order_id = 78;
        $this->_order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);

        if (!$this->_order && $this->_order->getId()) {
            throw new Exception('Could not find Magento order with id $order_id');
        }
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    protected function getCheckout() {
        return $this->_checkoutSession;
    }

    protected function _redirect($path, $arguments = []) {
        $this->_redirect->redirect($this->getResponse(), $path, $arguments);
        return $this->getResponse();
    }

    /**
     * Generate an "IPN" comment with additional explanation.
     * Returns the generated comment or order status history object.
     *
     * @param string $comment
     * @param bool $addToHistory
     *
     * @return string|\Magento\Sales\Model\Order\Status\History
     */
    protected function _createIpnComment($comment = '', $addToHistory = false) {
        $message = __('IPN "%1"', $this->_notification->getType());
        if ($comment) {
            $message .= ' ' . $comment;
        }
        if ($addToHistory) {
            $message = $this->_order->addStatusHistoryComment($message);
            $message->setIsCustomerNotified(null);
        }

        return $message;
    }

}
