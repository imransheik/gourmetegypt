<?php

namespace PayMob\PayMob\Controller\Notification;

use Magento\Framework\Controller\ResultFactory;

class Callback extends \Magento\Framework\App\Action\Action {
    
    protected $_paymentMethod;
    
    protected $_notification;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $_orderSender;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $transactionFactory;
    protected $paymobApiHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager     
     * @param Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param  \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \PayMob\PayMob\Model\Payment\Acceptance $paymentMethod, \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender, \Magento\Checkout\Model\Session $checkoutSession, \Psr\Log\LoggerInterface $logger, \PayMob\PayMob\Helper\Api $paymobApiHelper, \Magento\Framework\DB\TransactionFactory $transactionFactory
    ) {
        $this->_paymentMethod = $paymentMethod;
        //$this->_client = $this->_paymentMethod->getClient();
        $this->transactionFactory = $transactionFactory;
        $this->_orderSender = $orderSender;
        $this->_checkoutSession = $checkoutSession;
        $this->paymobApiHelper = $paymobApiHelper;
        $this->_logger = $logger;

        parent::__construct($context);
    }

    public function execute() {
        
        $this->_logger->addDebug("PayMob Notification Callback");
        $this->_logger->addDebug(print_r($_REQUEST,true));
        $value = json_decode($this->getRequest()->getContent(), true);
        $this->_logger->addDebug(print_r($value,true));
        $this->_logger->addDebug(print_r($value['obj']['order']['merchant_order_id'],true));
        $this->_logger->addDebug(print_r($value['obj']['success'],true));
        if ($value['obj']['success']){
            $this->_loadOrder($value['obj']['order']['merchant_order_id']);
            $this->_logger->addDebug("load order done");
            $this->_logger->addDebug(print_r($this->_order->getPayment()->getMethod(),true));
            $this->_registerPaymentCapture();
            $this->_logger->addDebug("register invoice done");
            $orderState = \Magento\Sales\Model\Order::STATE_PROCESSING;
            $this->_order->setState($orderState)->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
            $this->_order->save();
            $this->_order->addStatusToHistory($this->_order->getStatus(), 'Order paid successfully');
        }
        else{
            $this->_logger->addDebug("if else");
            $this->_loadOrder($value['obj']['order']['merchant_order_id']);
            $this->_order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, true)->save();
            $this->_order->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, true)->save();
            $temp = $value['obj']['data']['message'];
            $this->_order->addStatusToHistory($this->_order->getStatus(), 'Order payment declined with message: ' . $temp)->save();
            $this->_logger->addDebug("else done");
        }
        exit;       
    }


    protected function _registerPaymentCapture() {

        $order = $this->_order;
        //echo $this->_order->getId(); exit;
        if ($order->getPayment()->getMethod() == $this->_paymentMethod->getCode()) {
            $qtys = [];
            $this->_logger->addDebug("creating invoice");
            $invoice = $this->_objectManager->create('Magento\Sales\Model\Service\InvoiceService')->prepareInvoice($order);

            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->save();
            $this->_logger->addDebug("invoice created");
            $transaction = $this->transactionFactory->create();
            $transaction->addObject($invoice)->addObject($invoice->getOrder());
            $transaction->save();

            $this->_logger->addDebug("transaction created");

            if ($invoice && !$this->_order->getEmailSent()) {
                $this->_orderSender->send($this->_order);
                $this->_order->addStatusHistoryComment(
                    __('You notified customer about invoice #%1.', $invoice->getIncrementId())
                )->setIsCustomerNotified(
                    true
                )->save();
            }
        }
    }

    protected function _loadOrder($order_id) {
        //$session = $this->getCheckout();
        //$order_id = $session->getLastOrderId();
        $this->_logger->addDebug("load order gettting order");
        $this->_order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);
        $this->_logger->addDebug("load order got order");
        if (!$this->_order && $this->_order->getId()) {
            throw new Exception('Could not find Magento order with id $order_id');
        }
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    protected function getCheckout() {
        return $this->_checkoutSession;
    }






}
