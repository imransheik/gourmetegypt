<?php

namespace PayMob\PayMob\Controller\Txn;

use Magento\Framework\Controller\ResultFactory;

class Callback extends \Magento\Framework\App\Action\Action {
    
    protected $_paymentMethod;
    
    protected $_notification;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $_orderSender;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $transactionFactory;
    protected $paymobApiHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager     
     * @param Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param  \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \PayMob\PayMob\Model\Payment\Acceptance $paymentMethod, \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender, \Magento\Checkout\Model\Session $checkoutSession, \Psr\Log\LoggerInterface $logger, \PayMob\PayMob\Helper\Api $paymobApiHelper, \Magento\Framework\DB\TransactionFactory $transactionFactory
    ) {
        $this->_paymentMethod = $paymentMethod;
        //$this->_client = $this->_paymentMethod->getClient();
        $this->transactionFactory = $transactionFactory;
        $this->_orderSender = $orderSender;
        $this->_checkoutSession = $checkoutSession;
        $this->paymobApiHelper = $paymobApiHelper;
        $this->_logger = $logger;


        parent::__construct($context);
    }

    public function execute() {
        
        $this->_logger->addDebug("PayMob Transanction Callback");
        $this->_logger->addDebug(print_r($_REQUEST,true));
        $this->_logger->addDebug(print_r($_REQUEST['success'],true));

        $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$this->_logger->addDebug(print_r("store manager",true));
        $response = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $baseURL = $storeManager->getStore()->getBaseUrl();
        $this->_logger->addDebug(print_r($baseURL,true));
        if ($_REQUEST['success'] == "true"){$this->_logger->addDebug("if true");
            $response_url = $baseURL . 'checkout/onepage/success';
        }
        else{$this->_logger->addDebug("else");
            $this->_logger->addDebug($_REQUEST['data_message']);
            $this->messageManager->addError( $_REQUEST['data_message']);
            $response_url = $baseURL . 'checkout/onepage/failure';
            $this->_logger->addDebug("else finish");
        }
        $this->_logger->addDebug(print_r($response_url,true));
        $responseContent = [
                'paymob_redirect_url' => $response_url,
            ];
        $response->setUrl($response_url);
        return $response;       
    }
}
