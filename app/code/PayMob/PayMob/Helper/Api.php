<?php

namespace PayMob\PayMob\Helper;

/**
 * TrustSpot_OrderNotifier extension
 * 
 * @category       TrustSpot Extensions
 * @package        TrustSpot_OrderNotifier
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
class Api extends \Magento\Framework\App\Helper\AbstractHelper {

    CONST API_URL = 'https://accept.paymobsolutions.com/api/';

    protected $logger;
    protected $orderNotifierHelper;
    protected $orderNotifierEntryFactory;
    protected $catalogImageHelper;
    protected $orderNotifierApiHelper;
    protected $backendSession;
    protected $checkoutSession;
    protected $messageManager;
    protected $imageBlock;
    protected $mediaUrl;
    protected $scopeConfig;
    protected $storeManager;
    protected $_resourceConfig;

    public function __construct(
    \Psr\Log\LoggerInterface $logger, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Backend\Model\Session $backendSession, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Config\Model\ResourceModel\Config $resourceConfig
    ) {

        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->backendSession = $backendSession;
        $this->scopeConfig = $scopeConfig;
        $this->_resourceConfig = $resourceConfig;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
    }

    /**
     *
     * @access public
     * @return string
     * @author 
     */
    public function getBaseURL() {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    public function getApiURL() {
        return self::API_URL;
    }

    public function getUsername() {
        return $this->scopeConfig->getValue('payment/acceptance/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get API Key
     *
     * @access public
     * @return string
     * @author 
     */
    public function getPassword() {
        return $this->scopeConfig->getValue('payment/acceptance/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function generateCustomerToken() {
        $authUrl = $this->getApiURL() . "auth/tokens";

        $authData = array(
            "username" => $this->getUsername(),
            "password" => $this->getPassword(),
        );


        $result = $this->call($authData, $authUrl);
        #       echo "<pre><hr>generate customer token<br>";print_r($authData);print_r($result);
        if ($result->token) {
            $this->checkoutSession->setPaymobToken($result->token);
        }
    }

    public function generateIframeToken() {
        $authUrl = $this->getApiURL() . "auth/tokens";

        $authData = array(
            "username" => $this->getUsername(),
            "password" => $this->getPassword(),
        );

        $result = $this->call($authData, $authUrl);

        if ($result->token) {
            $this->backendSession->setPaymobToken($result->token);
        }
    }

    public function registerCustomerOrder($requestData) {
        $apiUrl = $this->getApiURL() . "ecommerce/orders?token=" . $this->checkoutSession->getPaymobToken();

        $result = $this->call($requestData, $apiUrl);
        //echo "<pre><hr>registerCustomerOrder<br>";print_r($requestData);print_r($result);
        if (!empty($result->id)) {
            //$this->_logger->debug($result);
            return $result;
        }
        return false;
    }

    public function requestPaymentKey($requestData) {


        $apiUrl = $this->getApiURL() . "acceptance/payment_keys?token=" . $this->checkoutSession->getPaymobToken();

        $result = $this->call($requestData, $apiUrl);
        #echo "<pre><hr>request PaymentKey<br>";print_r($requestData);print_r($result);        
        if (!empty($result->token)) {
            $this->backendSession->setPaymobPaymentKey($result->token);
            return $result->token;
        }

        return false;
    }

    /**
     * @access public
     * @param array
     * @return bool
     * @author 
     */
    public function uploadIframe($data) {
        $iframeUrl = $this->getApiURL() . "acceptance/iframes?token=" . $this->backendSession->getPaymobToken();

        $iframeData = array(
            "name" => "test 1",
            "description" => "end to end iframe testing",
            "content_html" => $data['html'],
            "content_js" => $data['js'],
            "content_css" => $data['css']
        );

        $result = $this->call($iframeData, $iframeUrl);

        if ($result->id) { //uploaed successfully save to config table             
            $this->_resourceConfig->saveConfig(
                    'payment/acceptance/iframe_id', $result->id, 'default', 0
            );

            $this->_resourceConfig->saveConfig(
                    'payment/acceptance/merchant_id', $result->merchant, 'default', 0
            );

            $this->_resourceConfig->saveConfig(
                    'payment/acceptance/iframe_html', $data['html'], 'default', 0
            );

            $this->_resourceConfig->saveConfig(
                    'payment/acceptance/iframe_css', $data['css'], 'default', 0
            );

            $this->_resourceConfig->saveConfig(
                    'payment/acceptance/iframe_js', $data['js'], 'default', 0
            );

            return true;
        }

        return false;
    }

    public function call($data, $url) {
        $dataString = json_encode($data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($dataString))
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataString);  // Insert the data
        ob_start();
        $result = curl_exec($curl);
        $result = ob_get_clean();
        /* ob_flush(); */
        curl_close($curl);
        //var_dump(json_decode($result));
        //$this->logger->log(100, $result);

        if (!$result) {
            return false;
        }

        return json_decode($result);
    }

}
