<?php

namespace PayMob\PayMob\Helper;



class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    
      public function getUsername() {                        
        return $this->scopeConfig->getValue('payment/acceptance/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**     
     *
     * @access public
     * @return string
     * @author 
     */
    public function getPassword() {        
        return $this->scopeConfig->getValue('payment/acceptance/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
   
}
