<?php

namespace PayMob\PayMob\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;

class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var string[]
     */
    

    /**
     * @var \Razorpay\Magento\Model\Config
     */
    protected $method;
    protected $currentCustomer;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Url $urlBuilder
     */
    /**
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(        
        CurrentCustomer $currentCustomer
    ) {
        
        $this->currentCustomer = $currentCustomer;
    }


    /**
     * @return array|void
     */
    public function getConfig()
    {
        
        $config = [
            'payment' => [
                'paymob' => [
                    'iframeSrc' => 'gachi'
                ],
            ],
        ];

        return $config;
    }
}
