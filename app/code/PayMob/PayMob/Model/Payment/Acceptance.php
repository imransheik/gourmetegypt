<?php


namespace PayMob\PayMob\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Payment\Transaction;

class Acceptance extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "acceptance";
    protected $_isOffline = true;
    protected $_isGateway = true;
    protected $_isInitializeNeeded = true;
    protected $_canOrder = true;
    protected $_canRefund = true;
    protected $_canCapture = true;
    protected $_canAuthorize = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
