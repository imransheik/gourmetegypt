<?php

namespace PayMob\PayMob\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        $installer->getConnection()->addColumn(
                $installer->getTable('sales_order'), 'paymob_order_id', [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'length' => 11,
            'nullable' => true,
            'comment' => 'Paymob Order Id'
                ]
        );


        $installer->getConnection()->addColumn(
                $installer->getTable('sales_order'), 'paymob_payment_key', [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length' => 1024,
            'nullable' => true,
            'comment' => 'PayMOb Payment Key'
                ]
        );


        $installer->endSetup();
    }

}
