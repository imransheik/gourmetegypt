define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'acceptance',
                component: 'PayMob_PayMob/js/view/payment/method-renderer/acceptance-method'
            }
        );
        return Component.extend({});
    }
);