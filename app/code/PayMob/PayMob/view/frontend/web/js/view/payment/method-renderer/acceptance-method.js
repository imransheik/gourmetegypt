define(
        [
            'Magento_Checkout/js/view/payment/default',
            'Magento_Checkout/js/model/quote',
            'jquery',
            'ko',
            'Magento_Checkout/js/model/payment/additional-validators',
            'Magento_Checkout/js/action/set-payment-information',
            'mage/url',
            'Magento_Customer/js/model/customer',
            'Magento_Checkout/js/action/place-order',
            'Magento_Checkout/js/model/full-screen-loader',
            'Magento_Ui/js/model/messageList'
        ],
        function (Component, quote, $, ko, additionalValidators, setPaymentInformationAction, url, customer, placeOrderAction, fullScreenLoader, messageList) {
            return Component.extend({
                defaults: {
                    template: 'PayMob_PayMob/payment/acceptance',
                    paymobDataFrameLoaded: false,
                    pymbIframeSrc: ''
                },
                getMailingAddress: function () {
                    return window.checkoutConfig.payment.checkmo.mailingAddress;
                },
                redirectAfterPlaceOrder: false,
                afterPlaceOrder: function (data, event) {
                    var self = this;
                    fullScreenLoader.startLoader();
                    $.ajax({
                        type: 'POST',
                        url: url.build('paymob/checkout/callback'),
                        success: function (response) {
                            fullScreenLoader.stopLoader();
                            if (typeof response == "string"){
                                response = JSON.parse(response);
                            }
                            if (response.success) {
                                //this.pymbIframeSrc = response.iframeSrc;
                                //self.renderIframe(response);
                                //document.getElementsByClassName("action primary checkout").style.visibility = 'hidden';
                                //console.log(response.url);
                                window.location.replace(response.url);
                            } else {
                                self.isPaymentProcessing.reject(response.message);
                            }
                        },
                        error: function (response) {
                            fullScreenLoader.stopLoader();
                            self.isPaymentProcessing.reject(response.message);
                        }
                    });
                },
                isPlaceOrderActionAllowed: ko.observable(quote.billingAddress() != null),
                placeOrder: function (data, event) {
                    var self = this;

                    var self = this,
                            placeOrder;

                    if (event) {
                        event.preventDefault();
                    }

                    if (this.validate() && additionalValidators.validate()) {
                        this.isPlaceOrderActionAllowed(false);
                        placeOrder = placeOrderAction(this.getData(), this.redirectAfterPlaceOrder, this.messageContainer);

                        $.when(placeOrder).fail(function () {
                            self.isPlaceOrderActionAllowed(true);
                        }).done(this.afterPlaceOrder.bind(this));
                        return true;
                    }
                    return false;
                },
                getIframeUrl: function () {
                    return window.checkoutConfig.payment.paymob.iframeSrc;
                },
                /** Process Payment */
                preparePayMobPayment: function (context, event) {

                    var self = this,
                            billing_address,
                            pymb_order_id;

                    fullScreenLoader.startLoader();
                    this.messageContainer.clear();

                    this.amount = quote.totals()['base_grand_total'] * 100;
                    billing_address = quote.billingAddress();

                    this.user = {
                        name: billing_address.firstname + ' ' + billing_address.lastname,
                        contact: billing_address.telephone,
                        email: billing_address.email
                    };



                    if (!customer.isLoggedIn()) {
                        this.user.email = quote.guestEmail;
                    }

                    this.isPaymentProcessing = $.Deferred();
                    console.log(this.isPaymentProcessing);
                    $.when(this.isPaymentProcessing).done(
                            function () {
                                self.placeOrder();
                            }
                    ).fail(
                            function (result) {
                                self.handleError(result);
                            }
                    );


                    self.getPayMobOrderId();

                    return;
                },
                getPayMobOrderId: function () {

                    var self = this;

                    $.ajax({
                        type: 'POST',
                        url: url.build('paymob/checkout/order'),
                        success: function (response) {
                            fullScreenLoader.stopLoader();
                            if (response.success) {
                                self.renderIframe(response);
                            } else {
                                self.isPaymentProcessing.reject(response.message);
                            }
                        },
                        error: function (response) {
                            fullScreenLoader.stopLoader();
                            self.isPaymentProcessing.reject(response.message);
                        }
                    });
                },
                renderIframe: function (data) {
                    var self = this;
                    var iframeSrc = data.iframeSrc;                    
                    self.pymbIframeSrc = iframeSrc;                    
                    fullScreenLoader.stopLoader();
                    $('#paymob-checkout-iframe').attr('src', iframeSrc);
                },
                getData: function () {
                    return {
                        "method": this.item.method,
                        'additional_data': {
                            'paymob_txn_id': this.paylikeTxnId
                        }
                    };

                },
                validate: function () {
                    return true;
                },
            });
        }
);
