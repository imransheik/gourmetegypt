<?php
/**
 * WebShopApps
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * WebShopApps MatrixRate
 *
 * @category WebShopApps
 * @package WebShopApps_MatrixRate
 * @copyright Copyright (c) 2014 Zowta LLC (http://www.WebShopApps.com)
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @author WebShopApps Team sales@webshopapps.com
 *
 */
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace WebShopApps\MatrixRate\Model\Carrier;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Address\RateRequest;

class Matrixrate extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
\Magento\Shipping\Model\Carrier\CarrierInterface {

    /**
     * @var string
     */
    protected $_code = 'matrixrate';

    /**
     * @var bool
     */
    protected $_isFixed = false;

    /**
     * @var string
     */
    protected $defaultConditionName = 'package_weight';

    /**
     * @var array
     */
    protected $conditionNames = [];

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $resultMethodFactory;

    /**
     * @var \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory
     */
    protected $matrixrateFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory
     * @param \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory $matrixrateFactory
     * @param array $data
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory,
        \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory $matrixrateFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->resultMethodFactory = $resultMethodFactory;
        $this->matrixrateFactory = $matrixrateFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        foreach ($this->getCode('condition_name') as $k => $v) {
            $this->conditionNames[] = $k;
        }
    }

    /**
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function collectRates(RateRequest $request)
    {
		$destLocationGroupId = $request->getDestLocationGroupId();
		if(empty($destLocationGroupId)){
			if (!$this->getConfigFlag('active')) {
            return false;
        }

        // exclude Virtual products price from Package value if pre-configured
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        // Free shipping by qty
        $freeQty = 0;
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        if (!$request->getConditionMRName()) {
            $conditionName = $this->getConfigData('condition_name');
            $request->setConditionMRName($conditionName ? $conditionName : $this->defaultConditionName);
        }

        // Package weight and qty free shipping
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        $zipRange = $this->getConfigData('zip_range');
        $rateArray = $this->getRate($request, $zipRange);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        $foundRates = false;

        foreach ($rateArray as $rate) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $quote = $objectManager->create('Magento\Checkout\Model\Session')->getQuote();
//            var_dump(get_class_methods($quote)); exit;
            $city = $quote->getShippingAddress()->getCity();
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $area = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/cut_off_area', $storeScope);
            $areaArray = explode(',', $area);
			
			$ncCities = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/nc_city', $storeScope);
			$ncCitiesArray = explode(',', $ncCities);

			$redCities = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/red_city', $storeScope);
			$redCitiesArray = explode(',', $redCities);
			
            $requestInterface = $objectManager->get('Magento\Framework\App\RequestInterface');
           if ((($requestInterface->getRequestUri()!= '/rest/default/V1/carts/mine/estimate-shipping-methods')&&(strpos($requestInterface->getRequestUri(), 'rest/default/V1/guest-carts') == false) && ($requestInterface->getRequestUri()!= '/rest/arabic_store/V1/carts/mine/estimate-shipping-methods'))||($requestInterface->getFullActionName()=='sales_order_create_loadBlock')) { 
                if (!empty($rate) && $rate['price'] >= 0) {
                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                    $method = $this->resultMethodFactory->create();

                    $method->setCarrier('matrixrate');
                    $method->setCarrierTitle($this->getConfigData('title'));

                    $method->setMethod('matrixrate_' . $rate['pk']);
                    $method->setMethodTitle(__($rate['shipping_method']));

                    if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                        $shippingPrice = 0;
                    } else {
                        $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                    }

                    $method->setPrice($shippingPrice);
                    $method->setCost($rate['cost']);

                    $result->append($method);
                    $foundRates = true; // have found some valid rates
                }
            } else {
                $delivery = $objectManager->create('Amasty\Checkout\Model\Delivery')->findByQuoteId($quote->getId());
                    $deliveryDate = $delivery->getData('date');
                if ($deliveryDate == "2020-06-01") {
                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
                    $error = $this->_rateErrorFactory->create(
                            [
                                'data' => [
                                    'carrier' => $this->_code,
                                    'carrier_title' => $this->getConfigData('title'),
                                    'error_message' => $this->getConfigData('specificerrmsg'),
                                ],
                            ]
                    );
                    $result->append($error);
                     return $result;
                }
               
                if (in_array($rate["dest_city"], $areaArray)) {
                    if (!empty($rate) && $rate['price'] >= 0) {
                        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                        $method = $this->resultMethodFactory->create();

                        $method->setCarrier('matrixrate');
                        $method->setCarrierTitle($this->getConfigData('title'));

                        $method->setMethod('matrixrate_' . $rate['pk']);
                        $method->setMethodTitle(__($rate['shipping_method']));

                        if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                            $shippingPrice = 0;
                        } else {
                            $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                        }

                        $method->setPrice($shippingPrice);
                        $method->setCost($rate['cost']);

                        $result->append($method);
                        $foundRates = true; // have found some valid rates
                    }
                } else if ($rate["dest_city"] == "Ain Sokhna") {
                    $delivery = $objectManager->create('Amasty\Checkout\Model\Delivery')->findByQuoteId($quote->getId());
                    $deliveryDate = $delivery->getData('date');
                    $date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('m/d/y H:i:s');
                    $gouna_logic = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/gouna_logic', $storeScope);
                    $gouna_cut_off_time = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/gouna_cut_off_time', $storeScope);
                    $currentTimeFromated = date('h:00A', strtotime($date));
                   
                    $dayofweek = date('w', strtotime($deliveryDate));
                    $onepmTime = date('h:00A', strtotime($gouna_cut_off_time));
                    $displyRate = false;
//                    var_dump($rate['shipping_method']);exit;
                    if ($dayofweek == "5") { 
                        if (strpos($rate['shipping_method'], "FRIDAY") !== false) {
                            $displyRate = true;
                        }
                    } else if ($dayofweek == "2") {
                        if (strpos($rate['shipping_method'], "TUESDAY") !== false) {
                            $displyRate = true;
                        }
                   
                    } else if ($dayofweek == "4") {
                        if (strpos($rate['shipping_method'], "THURSDAY") !== false) {
                            $displyRate = true;
                        }
                    } 
                     else if ($dayofweek == "1") {
                        if (strpos($rate['shipping_method'], "MONDAY") !== false) {
                            $displyRate = true;
                        }
                    }
                     else if ($dayofweek == "0") {
                         if (strpos($rate['shipping_method'], "SUNDAY") !== false) {
                             $displyRate = true;
                         }
                     }
                    else {
                        $displyRate = true;
                    }
                    if(!$gouna_logic){
                         $displyRate = true;
                    }
                   
                     if(!$deliveryDate){
                         $displyRate = false;
                    }
                    if ($displyRate) {
                       
                       

                            $dateLabel = "(" . date('d/m/Y', strtotime($deliveryDate)) . ")";
                        
                        $method = $this->resultMethodFactory->create();

                        $method->setCarrier('matrixrate');
                        $method->setCarrierTitle($this->getConfigData('title'));

                        $method->setMethod('matrixrate_' . $rate['pk']);
                        $method->setMethodTitle(__($rate['shipping_method']) . $dateLabel);

                        if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                            $shippingPrice = 0;
                        } else {
                            $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                        }
                        $method->setPrice($shippingPrice);
                        $method->setCost($rate['cost']);
                        $result->append($method);
                        $foundRates = true; // have found some valid rates
                    }
                }  else {
                    $date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('m/d/y H:i:s');
					if (in_array($rate["dest_city"], $ncCitiesArray)) {
						$cutOffDeliverySlots = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/cut_off_delivery_slots_nc', $storeScope);
					}else{
						$cutOffDeliverySlots = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/cut_off_delivery_slots', $storeScope);
					}
					$cutOffTimeSameday ="";
					if (in_array($rate["dest_city"], $redCitiesArray)) {
						$cutOffTimeSameday = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/red_cut_off_time', $storeScope);
					}
										
                    $cutTimeDeliveryHours = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/cut_time_delivery_slots', $storeScope);
                    $startTimeDeliveryHours = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('checkout/options/start_time_delivery_slots', $storeScope);
                    $time = strtotime($date);
                    $time = $time + ($cutOffDeliverySlots * 60);

                    $currentDateTime = date("Y-m-d H:i:s", $time);

                    $delivery = $objectManager->create('Amasty\Checkout\Model\Delivery')->findByQuoteId($quote->getId());
                    $deliveryDate = $delivery->getData('date');
                    $currentTimeFromated = date('h:00A', strtotime($currentDateTime));
                    $DeliverySlotStartTime = date('h:00A', strtotime(substr($rate['shipping_method'], 0, 7)));
                    $cutTimeDeliveryWithinHour = strtotime($cutTimeDeliveryHours);
                    $startTimeDeliveryWithinHour = strtotime($startTimeDeliveryHours);
					$tweleveamcuoff=date('h:iA', strtotime("11:59PM")-($cutOffDeliverySlots * 60));
                    $tweleveam=date('h:iA', strtotime("11:59PM"));
                    if ((strtotime($date) >= strtotime($tweleveamcuoff))&&(strtotime($date)<=strtotime($tweleveam))&&(strtotime($currentDateTime) >= strtotime($deliveryDate))) {
                        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
                      $error = $this->_rateErrorFactory->create(
                                [
                                    'data' => [
                                        'carrier' => $this->_code,
                                        'carrier_title' => $this->getConfigData('title'),
                                        'error_message' => $this->getConfigData('specificerrmsg'),
                                    ],
                                ]
                        );
                        $result->append($error);


                        return $result;
                       
                    }
					if (in_array($rate["dest_city"], $redCitiesArray)) {
						if((strtotime($date) >= strtotime($cutOffTimeSameday)) && (strtotime($currentDateTime) >= strtotime($deliveryDate))){
							  /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
						  $error = $this->_rateErrorFactory->create(
									[
										'data' => [
											'carrier' => $this->_code,
											'carrier_title' => $this->getConfigData('title'),
											'error_message' => $this->getConfigData('specificerrmsg'),
										],
									]
							);
							$result->append($error);

							return $result;
						}
					}

                    if ((strtotime($DeliverySlotStartTime) > strtotime($currentTimeFromated)) || (substr($rate['shipping_method'], 0, 7) == "Deliver") && ((strtotime($date) >= $startTimeDeliveryWithinHour) && (strtotime($date) <= $cutTimeDeliveryWithinHour)) || (strtotime($currentDateTime) < strtotime($deliveryDate))) {
                        if (!empty($rate) && $rate['price'] >= 0) {
                            $deliveryDateFromated = date('Y-m-d', strtotime($deliveryDate));
                            if (!((substr($rate['shipping_method'], 0, 7) == "Deliver") && (strtotime(date("Y-m-d")) < strtotime($deliveryDateFromated)))) {
                                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                                $method = $this->resultMethodFactory->create();

                                $method->setCarrier('matrixrate');
                                $method->setCarrierTitle($this->getConfigData('title'));

                                $method->setMethod('matrixrate_' . $rate['pk']);
                                $method->setMethodTitle(__($rate['shipping_method']));

                                if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                                    $shippingPrice = 0;
                                } else {
                                    $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                                }

                                $method->setPrice($shippingPrice);
                                $method->setCost($rate['cost']);

                                $result->append($method);
                                $foundRates = true; // have found some valid rates
                            }
                        }
                    }
                }
            }
        }

        if (!$foundRates) {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
            $error = $this->_rateErrorFactory->create(
                    [
                        'data' => [
                            'carrier' => $this->_code,
                            'carrier_title' => $this->getConfigData('title'),
                            'error_message' => $this->getConfigData('specificerrmsg'),
                        ],
                    ]
            );
            $result->append($error);
        }

        return $result;
		}else{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->create('Magento\Checkout\Model\Session')->getQuote();
		$city = $quote->getShippingAddress()->getCity();
		$delivery = $objectManager->create('Amasty\Checkout\Model\Delivery')->findByQuoteId($quote->getId());
        $deliveryDate = $delivery->getData('date');
		$destLocationGroupId = $request->getDestLocationGroupId();
		$locationgroup = $objectManager->create('I95Dev\Locationgroup\Model\Data')->load($destLocationGroupId);
		
		if (!$this->getConfigFlag('active')) {
            return false;
        }
		
		$days = $locationgroup->getData('days');
		$time_from = $locationgroup->getData('time_from');
		$time_to = $locationgroup->getData('time_to');
		$cutoff_time = $locationgroup->getData('cutoff_time');
		
		$dayFlag = date('N', strtotime($deliveryDate));
		$validDays = explode(',',$days);
		if (!in_array($dayFlag, $validDays)) {
			return false;
		}

        // exclude Virtual products price from Package value if pre-configured
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        // Free shipping by qty
        $freeQty = 0;
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        if (!$request->getConditionMRName()) {
            $conditionName = $this->getConfigData('condition_name');
            $request->setConditionMRName($conditionName ? $conditionName : $this->defaultConditionName);
        }

        // Package weight and qty free shipping
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        $zipRange = $this->getConfigData('zip_range');
        $rateArray = $this->getRate($request, $zipRange);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        $foundRates = false;
		//$logger->info($rateArray);
		
		// Get Location information
		
		
		
        foreach ($rateArray as $rate) {
			$date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date()->format('m/d/y H:i:s');
			$cutOffDeliverySlots = $cutoff_time;
			$cutTimeDeliveryHours = $time_to;
			$startTimeDeliveryHours = $time_from;
			$time = strtotime($date);
            $time = $time + ($cutOffDeliverySlots * 60);
			
			$currentDateTime = date("Y-m-d H:i:s", $time);
			
			$currentTimeFromated = date('h:00A', strtotime($currentDateTime));
			$DeliverySlotStartTime = date('h:00A', strtotime(substr($rate['shipping_method'], 0, 7)));
			$cutTimeDeliveryWithinHour = strtotime($cutTimeDeliveryHours);
			$startTimeDeliveryWithinHour = strtotime($startTimeDeliveryHours);
			
			$requestInterface = $objectManager->get('Magento\Framework\App\RequestInterface');
			if ((($requestInterface->getRequestUri()!= '/rest/default/V1/carts/mine/estimate-shipping-methods')&&(strpos($requestInterface->getRequestUri(), 'rest/default/V1/guest-carts') == false) && ($requestInterface->getRequestUri()!= '/rest/arabic_store/V1/carts/mine/estimate-shipping-methods'))||($requestInterface->getFullActionName()=='sales_order_create_loadBlock')){ 
                if (!empty($rate) && $rate['price'] >= 0) {
                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                    $method = $this->resultMethodFactory->create();

                    $method->setCarrier('matrixrate');
                    $method->setCarrierTitle($this->getConfigData('title'));

                    $method->setMethod('matrixrate_' . $rate['pk']);
                    $method->setMethodTitle(__($rate['shipping_method']));

                    if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                        $shippingPrice = 0;
                    } else {
                        $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                    }

                    $method->setPrice($shippingPrice);
                    $method->setCost($rate['cost']);

                    $result->append($method);
                    $foundRates = true; // have found some valid rates
                }
            } else {
				if ((strtotime($DeliverySlotStartTime) > strtotime($currentTimeFromated)) || (substr($rate['shipping_method'], 0, 7) == "Deliver") && ((strtotime($date) >= $startTimeDeliveryWithinHour) && (strtotime($date) <= $cutTimeDeliveryWithinHour)) || (strtotime($currentDateTime) < strtotime($deliveryDate))) {
					if (!empty($rate) && $rate['price'] >= 0) {
						$deliveryDateFromated = date('Y-m-d', strtotime($deliveryDate));
						if (!((substr($rate['shipping_method'], 0, 7) == "Deliver") && (strtotime(date("Y-m-d")) < strtotime($deliveryDateFromated)))) {
							if($destLocationGroupId == 180 && (strtotime($currentDateTime) > strtotime($deliveryDate))){
								continue;
							}
							/** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
							$method = $this->resultMethodFactory->create();

							$method->setCarrier('matrixrate');
							$method->setCarrierTitle($this->getConfigData('title'));

							$method->setMethod('matrixrate_' . $rate['pk']);
							$method->setMethodTitle(__($rate['shipping_method']));

							if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
								$shippingPrice = 0;
							} else {
								$shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
							}

							$method->setPrice($shippingPrice);
							$method->setCost($rate['cost']);

							$result->append($method);
							$foundRates = true; // have found some valid rates
						}
					}
				}
			}
		}

        if (!$foundRates) {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
            $error = $this->_rateErrorFactory->create(
                    [
                        'data' => [
                            'carrier' => $this->_code,
                            'carrier_title' => $this->getConfigData('title'),
                            'error_message' => $this->getConfigData('specificerrmsg'),
                        ],
                    ]
            );
            $result->append($error);
        }

        return $result;
		}
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @param bool $zipRange
     * @return array|bool
     */
    public function getRate(\Magento\Quote\Model\Quote\Address\RateRequest $request, $zipRange)
    {
        return $this->matrixrateFactory->create()->getRate($request, $zipRange);
    }

    /**
     * @param string $type
     * @param string $code
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCode($type, $code = '')
    {
        $codes = $this->getRawCodes();

        if (!isset($codes[$type])) {
            throw new LocalizedException(__('Please correct Matrix Rate code type: %1.', $type));
        }

        if ('' === $code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw new LocalizedException(__('Please correct Matrix Rate code for type %1: %2.', $type, $code));
        }

        return $codes[$type][$code];
    }

    /**
     * Thanks to https://github.com/JeroenVanLeusden for this user submitted enhancement
     *
     * @return array
     */
    public function getRawCodes(): array
    {
        return [
            'condition_name' => [
                'package_weight' => __('Weight vs. Destination'),
                'package_value' => __('Order Subtotal vs. Destination'),
                'package_qty' => __('# of Items vs. Destination')
            ],
            'condition_name_short' => [
                'package_weight' => __('Weight'),
                'package_value' => __('Order Subtotal'),
                'package_qty' => __('# of Items')
            ]
        ];
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$connection  = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
		$tableName   = $connection->getTableName('webshopapps_matrixrate');
		$query = 'SELECT * FROM ' . $tableName;
		$results = $connection->fetchAll($query);
		$arr = [];
		foreach ($results as $result) {
            $arr['matrixrate_'.$result['pk']] = $this->getConfigData('title').' - '.$result['shipping_method'].' - '.$result['dest_city'];
        }
		
        //return ['matrixrate' => $this->getConfigData('name')];
        return $arr;
    }
}
