<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace WebShopApps\MatrixRate\Model;

use Magento\Checkout\Model\ConfigProviderInterface;


class ConfigProvider implements ConfigProviderInterface {


    protected $scopeConfigInterface;

    public function __construct(
     \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
    ) {
        $this->scopeConfigInterface = $scopeConfigInterface;

    }

    /**
     * {@inheritdoc}
     */
    public function getConfig() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $textone = $this->scopeConfigInterface->getValue('checkout/options/checkout_text_one', $storeScope);
        $texttwo = $this->scopeConfigInterface->getValue('checkout/options/checkout_text_two', $storeScope);
        $gouna_cut_off_time = $this->scopeConfigInterface->getValue('checkout/options/gouna_cut_off_time', $storeScope);
		$safaga_cut_off_time = $this->scopeConfigInterface->getValue('checkout/options/safaga_cut_off_time', $storeScope);
		$east_cut_off_time = $this->scopeConfigInterface->getValue('checkout/options/east_cut_off_time', $storeScope);
        
        $config = [
            'checkoutdata' => [
                'textone' =>$textone,
                'texttwo' =>$texttwo,
                'gouna_cut_off_time' =>date("H", strtotime($gouna_cut_off_time)),
				'safaga_cut_off_time' =>date("H", strtotime($safaga_cut_off_time)),
				'east_cut_off_time' =>date("H", strtotime($east_cut_off_time)),
            ]
        ];
        return $config;
    }

  

}
