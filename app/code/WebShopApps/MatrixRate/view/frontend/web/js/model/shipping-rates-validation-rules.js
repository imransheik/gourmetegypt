/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [],
    function () {
        "use strict";
        return {
            getRules: function () {
                return {
                    'telephone': {
                        'required': true
                    },
                    'country_id': {
                        'required': true
                    },
                    'region_id' : {
                        'required': true
                    },
                    'city' : {
                        'required': true
                    },
					'store_address_id' : {
                        'required': true
                    },
					'location_group_id' : {
                        'required': true
                    }
                };
            }
        };
    }
);
