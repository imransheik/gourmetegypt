<?php
namespace Webengage\Event\Controller\Index;
use Magento\Backend\App\Action;

class Refreshinfo extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $catalogSession = $objectManager->get('Magento\Catalog\Model\Session');
        if (isset($_POST) && !empty($_POST) && isset($_POST['weLuid']) && $_POST['weLuid'] != '') {
            $catalogSession->setLuid($_POST['weLuid']);
            $catalogSession->setSuid($_POST['suid']);

        }
        else {
            $catalogSession->setLuid($catalogSession->getLuid());
            $catalogSession->setSuid($catalogSession->getSuid());
        }


    }
}