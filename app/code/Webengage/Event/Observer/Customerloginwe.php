<?php

namespace Webengage\Event\Observer;

class Customerloginwe implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * Customerloginwe constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(\Magento\Customer\Model\Session $customerSession)
    {
        $this->customerSession = $customerSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helper = $objectManager->create(\Webengage\Event\Helper\Data::class);
		$customerSession = $this->customerSession;
        $customerSession->setCustomerloggedin('yes');
        setcookie('customerLoggedIn', 'yes', time() + (86400 * 30), "/"); // 86400 = 1 day
		$prepareJson = array(
                    'event_name' => 'User Logged In',
                    'event_data' => array(
                        'type' => 'Success',
                    )
                );
		/*Calling WE API*/
        $helper->apiCallToWebengage($prepareJson);
        /*Calling WE API*/
		
		$prepareJson1 = array(
                    'event_name' => 'User Login',
                    'event_data' => array(
                        'type' => 'Success',
                    )
                );
		/*Calling WE API*/
        $helper->apiCallToWebengage($prepareJson1);
        /*Calling WE API*/
        return $this;
    }


}