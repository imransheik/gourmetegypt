<?php

namespace Webengage\Event\Observer;

class Customerlogoutwe implements \Magento\Framework\Event\ObserverInterface {

    public function execute(\Magento\Framework\Event\Observer $observer) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helper = $objectManager->create(\Webengage\Event\Helper\Data::class);
        setcookie('customerLoggedOut', 'yes', time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('customerSignupLoggedIn', null, -1, '/');
        setcookie('customerLoggedIn', null, -1, '/');
		$prepareJson = array(
                    'event_name' => 'User LoggedOut',
                    'event_data' => array(
                        'Login Status' => 'Sign Out',
                    )
                );
		/*Calling WE API*/
        $helper->apiCallToWebengage($prepareJson);
        /*Calling WE API*/
		$prepareJson1 = array(
                    'event_name' => 'User Logout',
                    'event_data' => array(
                        'Login Status' => 'Sign Out',
                    )
                );
		/*Calling WE API*/
        $helper->apiCallToWebengage($prepareJson1);
        /*Calling WE API*/
        
        return $this;
    }

}
