<?php

namespace Webengage\Event\Observer;

use Webengage\Event\Helper\Data;


class Customerregistersuccesswe implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Catalog\Model\Session
     */
    private $session;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * Customerregistersuccesswe constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Customer $customer
     * @param Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Catalog\Model\Session $session,
        Data $helper)
    {
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->customer = $customer;
        $this->session = $session;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $event = $observer->getEvent();
        $storeManager = $this->storeManager;
        $storeName = $storeManager->getStore()->getName();
        $catalogSession = $this->session;
        $customer = $event->getCustomer();
        $customerEmail = $customer->getEmail();
        $customerSession = $this->customerSession;
        if ($customerSession->isLoggedIn()) {
            $customerEmail = $customerSession->getCustomer()->getEmail();
        }
        $_SESSION['customersignup'] = 'yes';
        $catalogSession->setSignuplogin('yes');
        $customerSession->setSignuplogindata('yes');
        setcookie('customerSignupLoggedIn', 'yes', time() + (86400 * 30), "/"); // 86400 = 1 day
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

//Load product by product id
        $coreSeesion = $objectManager->create('\Magento\Framework\Session\SessionManagerInterface');
        $coreSeesion->start();
        $primaryMobileNo = $coreSeesion->getPrimaryMobile() ? $coreSeesion->getPrimaryMobile() : '';
        $isdCode = $coreSeesion->getIsd() ? $coreSeesion->getIsd() : '';

        $email = $customer->getEmail();
        $firstName = $customer->getFirstname();
        $lastName = $customer->getLastname();

        $websiteId = $storeManager->getWebsite()->getWebsiteId();
        $CustomerModel = $this->customer;
        $CustomerModel->setWebsiteId($websiteId);
        $CustomerModel->loadByEmail($email);
        $customerData = (object)$CustomerModel->getData();

        $logger->info(print_r($_POST['prefered_contact'], true));

        $prefered_contact=isset($_POST['prefered_contact']) ? $_POST['prefered_contact'] : '213';
        $preferedContact = array();
        foreach($prefered_contact as $pc)
        {
            if ($pc == "211") {
                $preferedContact[]= 'Call';
            } else if ($pc == "212") {
                $preferedContact[] = 'SMS';
            } else if($pc == "213") {
                $preferedContact[] = 'Email';
            }


        }

        if(!empty($preferedContact)) {
            $preferedContact = implode(',',$preferedContact);
        }
        else {

            $preferedContact = 'Email';
        }

        $prefered_language = $_POST['prefered_language'];
        if ($prefered_language == "210") {
            $preferedLanguage = "Arabic";
        } else {
            $preferedLanguage = "English";
        }
        $prepareJson = array(
            'event_name' => 'User Signed Up',
            'cuid' => $customerEmail,
            'event_data' => array(
                'we_first_name' => $firstName,
                'we_last_name' => $lastName,
                'we_email' => strtolower($email),
                'we_phone' =>  $isdCode . '' . $primaryMobileNo,
                'Contact Number 1' => $isdCode . '' . $primaryMobileNo,
                'phone' =>'+'.$isdCode . '' . $primaryMobileNo,
                'customerCreatedStoreName' => $storeName,
                'customerPrimaryNumber' => $primaryMobileNo,
                'customerIsdCode' => $isdCode,
                'customerFullNumber' => $isdCode . $primaryMobileNo,
                'customerCreatedAt' => $customerData->created_at,
                'customerPreferedContact' => $preferedContact,
                'customerPreferedLanguage' => $preferedLanguage,
            )
        );

        /*Calling WE API*/
        $this->helper->apiCallToWebengage($prepareJson);
        /*Calling WE API*/
        return $this;
    }


}
