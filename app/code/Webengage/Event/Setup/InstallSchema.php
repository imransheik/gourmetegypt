<?php

namespace Webengage\Event\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '7.0.0') < 0) {
            $installer->run('create table IF NOT EXISTS webengage_configuration(weid int not null auto_increment, wekey varchar(100) DEFAULT NULL, wevalue varchar(100) DEFAULT NULL, primary key(weid))');
            $installer->run('insert into webengage_configuration values(null,\'we_licence_code\',\'\')');
            $installer->run('insert into webengage_configuration values(null,\'we_api_key\',\'\')');
            $installer->run('insert into webengage_configuration values(null,\'we_debug\',\'\')');
        }

        $installer->endSetup();
    }
}
