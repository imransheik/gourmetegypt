define([
    'jquery',
    'mage/translate',
    'jquery/ui',
    'Magento_Catalog/js/catalog-add-to-cart',
    "domReady!"
], function($, $t) {
    "use strict";

    $.widget('gourmet.catalogAddToCart', $.mage.catalogAddToCart, {
        

        _bindSubmit: function() {
            var self = this;
            
            this.element.on('click', '[data-action="addto-cart-deduct"]', $.proxy(this._beforeSubmit, self));

            this._super();
        },

        _beforeSubmit: function(e){
            e.preventDefault();
            this._removeHidden(this.element);
            this.options.formtype=1;

            $('<input>').attr({
                type: 'hidden',
                name: 'deduct',
                value: 1
            }).appendTo(this.element);

            this.element.submit();
            this._removeHidden(this.element);
        },

        submitForm: function (form) {
            if((typeof this.options.formtype === 'undefined')){
                this.options.formtype = 0;
            }
            this._super(form);
        },

        _removeHidden: function(form){
            $(form).find($('input[name="deduct"][type="hidden"]')).remove();
        },

        enableAddToCartButton: function(form) {
          this._super(form);
          $(form).find($('input[name="deduct"][type="hidden"]')).remove();
        }
    });

    return $.gourmet.catalogAddToCart;
});
