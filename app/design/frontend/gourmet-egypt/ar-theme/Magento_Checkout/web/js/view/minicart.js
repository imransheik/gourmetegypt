/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'mage/url',
    'jquery',
    'ko',
    'underscore',
    'sidebar'
], function (Component, customerData, url, $, ko, _) {

    var sidebarInitialized = false,
            addToCartCalls = 0,
            miniCart;

    miniCart = $('[data-block=\'minicart\']');
    miniCart.on('dropdowndialogopen', function () {
        initSidebar();
    });

    /**
     * @return {Boolean}
     */
    function initSidebar() {

        if (miniCart.data('mageSidebar')) {
            miniCart.sidebar('update');
        }

        if (!$('[data-role=product-item]').length) {
            return false;
        }
        miniCart.trigger('contentUpdated');

        if (sidebarInitialized) {
            return false;
        }
        sidebarInitialized = true;
        miniCart.sidebar({
            'targetElement': 'div.block.block-minicart',
            'url': {
                'checkout': window.checkout.checkoutUrl,
                'update': window.checkout.updateItemQtyUrl,
                'remove': window.checkout.removeItemUrl,
                'loginUrl': window.checkout.customerLoginUrl,
                'isRedirectRequired': window.checkout.isRedirectRequired
            },
            'button': {
                'checkout': '#top-cart-btn-checkout',
                'remove': '#mini-cart a.action.delete',
                'close': '#btn-minicart-close'
            },
            'showcart': {
                'parent': 'span.counter',
                'qty': 'span.counter-number',
                'label': 'span.counter-label'
            },
            'minicart': {
                'list': '#mini-cart',
                'content': '#minicart-content-wrapper',
                'qty': 'div.items-total',
                'subtotal': 'div.subtotal span.price',
                'maxItemsVisible': window.checkout.minicartMaxItemsVisible
            },
            'item': {
                'qty': ':input.cart-item-qty',
                'button': ':button.update-cart-item'
            },
            'confirmMessage': $.mage.__(
                    'Are you sure you would like to remove this item from the shopping cart?'
                    )
        });
    }

    return Component.extend({
        shoppingCartUrl: window.checkout.shoppingCartUrl,
        cart: {},

        /**
         * @override
         */
        initialize: function () {
            var self = this,
                    cartData = customerData.get('cart');
            this.update(cartData());
            cartData.subscribe(function (updatedCart) {
                addToCartCalls--;
                this.isLoading(addToCartCalls > 0);
                sidebarInitialized = false;
                this.update(updatedCart);
                initSidebar();
            }, this);
            $('[data-block="minicart"]').on('contentLoading', function (event) {
                addToCartCalls++;
                self.isLoading(true);
            });

            return this._super();
        },
        isLoading: ko.observable(false),
        initSidebar: initSidebar,

        /**
         * @return {Boolean}
         */
        closeSidebar: function () {
            var minicart = $('[data-block="minicart"]');
            minicart.on('click', '[data-action="close"]', function (event) {
                event.stopPropagation();
                minicart.find('[data-role="dropdownDialog"]').dropdownDialog('close');

            });

            return true;
        },

        /**
         * @param {String} productType
         * @return {*|String}
         */
        getItemRenderer: function (productType) {
            return this.itemRenderer[productType] || 'defaultRenderer';
        },

        /**
         * Update mini shopping cart content.
         *
         * @param {Object} updatedCart
         * @returns void	
         */
        update: function (updatedCart) {
            _.each(updatedCart, function (value, key) {

                if (!this.cart.hasOwnProperty(key)) {
                    this.cart[key] = ko.observable();
                }
                this.cart[key](value);
            }, this);
        },

        /**
         * Get cart param by name.
         * @param {String} name
         * @returns {*}
         */
        getCartParam: function (name) {
            if (!_.isUndefined(name)) {
                if (!this.cart.hasOwnProperty(name)) {
                    this.cart[name] = ko.observable();
                }
            }

            return this.cart[name]();
        },

        /**
         * Coupon code application procedure
         */
        apply: function () {
            if (this.validate()) {


                var postData = {remove: '0', coupon_code: $('#discount-code').val(), from_minicart: "1"}
                $.ajax({
                    url: url.build('checkout/cart/couponPost'),
                    type: 'POST',
                    dataType: "json",
                    data: postData,
                    showLoader: true,
                    success: function (result) {
                        message = result.message;
                        if (result.success == 'true') {
                            $('.discount-mimicart-message').css("color", "green");
                        } else {
                            $('.discount-mimicart-message').css("color", "red");

                        }

                        $('.discount-mimicart-message').html(message);
                        $('.discount-mimicart-message').css("display", "block");

                        return true;
                    }
                });
            }
        },

        /**
         * Cancel using coupon
         */
        cancel: function () {
            if (this.validate()) {
                $('.coupon-custom-label').css("padding", "10px 0");
                $('.coupon-custom-label').css("font-size", "14px");

                var postData = {remove: '1', coupon_code: $('#discount-code').val(), from_minicart: "1"}
                $.ajax({
                    url: url.build('checkout/cart/couponPost'),
                    type: 'POST',
                    dataType: "json",
                    data: postData,
                    showLoader: true,
                    success: function (result) {
                        message = result.message;
                        if (result.success == 'true') {
                            $('.discount-mimicart-message').css("color", "green");

                            isApplied(false);
                        } else {
                            $('.discount-mimicart-message').css("color", "red");
                        }

                        $('.discount-mimicart-message').html(message);
                        $('.discount-mimicart-message').css("display", "block");

                        return true;
                    }
                });
            }
        },

        /**
         * Coupon form validation
         *
         * @returns {Boolean}
         */
        validate: function () {
            var form = '#discount-form';

            return $(form).validation() && $(form).validation('isValid');
        }
    });
});

