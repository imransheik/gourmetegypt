/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, quote, priceUtils, totals) {
        "use strict";
        return Component.extend({
            defaults: {
                isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
                template: 'Magento_Tax/checkout/summary/grand-total'
            },
            totals: quote.getTotals(),
            isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,
             isAvailable: window.checkoutConfig.payment.loyalty.isAvailable,
              usedAmount: window.checkoutConfig.payment.loyalty.usedAmount,
            isDisplayed: function() {
                return true;
            },
            getValue: function() {
                var price = 0;
                if (this.totals()) {
                    price = totals.getSegment('grand_total').value;
                }
                return this.getFormattedPrice(price);
            },
			
			getCartValue: function() {
                var price = 0;
                if (this.totals()) {
					price = this.totals().subtotal;
					if(this.totals().discount_amount)
					{						
						price = parseFloat(price) + parseFloat(this.totals().discount_amount);												
					}
					
					if(totals.getSegment('customerbalance'))
					{
						price = parseFloat(price) + parseFloat(totals.getSegment('customerbalance')['value']);						
						if(price <= 0)
						{
							price = 0;
						}
					}
                }
                return this.getFormattedPrice(price);
            },
            getBaseValue: function() {
                var price = 0;
                if (this.totals()) {
                    price = this.totals().base_grand_total;
                }
                return priceUtils.formatPrice(price, quote.getBasePriceFormat());
            },
            getGrandTotalExclTax: function() {
                var totals = this.totals();
                if (!totals) {
                    return 0;
                }
                return this.getFormattedPrice(totals.grand_total);
            },
            isBaseGrandTotalDisplayNeeded: function() {
                var totals = this.totals();
                if (!totals) {
                    return false;
                }
                return totals.base_currency_code != totals.quote_currency_code;
            },
      
        getUsedAmount: function () {
           return priceUtils.formatPrice(this.usedAmount, quote.getBasePriceFormat());
        }
        });
    }
);
