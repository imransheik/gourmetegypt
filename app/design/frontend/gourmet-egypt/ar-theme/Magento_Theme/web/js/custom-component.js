define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magento_Theme/custom-location'
            },
            initialize: function () {
                this.locationData = ko.observable($.cookie('Gourmet_Knockout_Location'));
                this.slotData = ko.observable($.cookie('Gourmet_Knockout_Slot'));
				this._super();
				this.hideSlots();
            },
			
			hideSlots: function()
			{
				
			}
        });
    }
);