var config = {
    map: {
        '*': {
            nanoscroller: 'js/nanoscroller',
			bootstrapSelect: 'js/bootstrap.select',
            mousewheel: 'js/jquery.mousewheel.min',
            mCustomScrollbar: 'js/jquery.mCustomScrollbar.min',
            catalogAddToCart: 'Magento_Catalog/js/catalog-swatch-add-to-cart',
			googleMapApi: 'js/google-map-api'
        }
    },
	
	paths: {        
        "js/nanoscroller": "js/nanoscroller",
		"js/bootstrap.select": "js/bootstrap.select",
        "stickyheader": "js/sticky-header",
		"flickity": "js/flickity",
		"domReady": "js/domReady",
		"googleMapApi": "js/google-map-api",
		"mousewheel": 'js/jquery.mousewheel.min'
	},
	
	"shim": {
        "jquery/nanoscroller": ["jquery"],
        "mousewheel": ["jquery"],
        "mCustomScrollbar": ["mousewheel"],
		"bootstrapSelect": ["bootstrapSelect"],
		"googleMapApi": ["google-map-api"]	
    },

    deps: [
        "js/malihu"
    ]
};