define([
        'jquery',
        'mousewheel',
        'mCustomScrollbar',
        'ko'
    ],
    function ($, mousewheel, mCustomScrollbar, ko) {
        "use strict";
        (function ($) {


            /*$(".home-category-img img").each(function () {
             var imgulr = $(this).attr("src");
             $(this).parents(".home-category-img").css({"background-image": "url(" + imgulr + ")"});
             })*/

            function scrollBerScript() {
                if ($("html").hasClass("desktop") && $("body").hasClass("cms-home")) {
                    $(".product-listing .products-grid.grid").mCustomScrollbar("destroy");
                } else if (($(window).width() > 874) && $("body").hasClass("cms-home")) {
                    $(".product-listing .products-grid.grid").mCustomScrollbar("destroy");
                } else {
                    // for Ipad(portrait) and Mobiles)
                    $(".product-listing .products-grid.grid").mCustomScrollbar({
                        axis: "x",
                        theme: "dark-3",
                        advanced: {autoExpandHorizontalScroll: true},
                        mouseWheel: {preventDefault: true}
                    });
                }
            }

            // scrollBerScript();

            // Re-trigger on resize
            $(window).on("resize", function () {
                // scrollBerScript();
            });

            /**
             * Minicart binding
             * */
            ko.bindingHandlers.miniCartScroll = {
                init: function (element) {
                    $(element).mCustomScrollbar({
                        theme: "dark-3",
                        mouseWheel: {preventDefault: true},
                        setHeight: 400
                    });
                }
            };


            /**
             * Data binding for payment tabs
             * */
            ko.bindingHandlers.payTabs = {
                init: function (element) {
                    $(element).click(function () {
                        // Get target ID name.
                        var tab_id = $(this).attr('data-tab');
                        // Switch current classes
                        $('.payment__nav li').removeClass('current');
                        $('.payment__method').removeClass('current');
                        $(this).addClass('current');
                        $('#' + tab_id).addClass('current');

                        // Check payment method
                        $('#' + tab_id).find('input[name="payment[method]"]').trigger("click");
                    });
                }
            };

            // Check radio on selected tab by default
            ko.bindingHandlers.setChecked = {
                init: function (element) {
                    if ($(element).hasClass("current")) {
                        setTimeout(function () {
                            $(element).find('input[name="payment[method]"]').trigger("click");
                        }, 1000);
                    }
                }
            };

            // Focus on error fields in checkout
            ko.bindingHandlers.errorFocus = {
                init: function (element) {

                }
            };

            /**
             * Unchecked shipping slot
             * */
            ko.bindingHandlers.uncheckedTable = {
                init: function (element) {
                    $(element).find(".table-checkout-shipping-method input[type=radio]").prop("disabled", false);
                    $(element).find(".table-checkout-shipping-method input[type=radio]").prop("checked", false);

                }
            };

            /**
             * Check default address
             * */
            ko.bindingHandlers.checkDefaultAddress = {
                init: function (element) {
                    setTimeout(function () {
                        var selectedAddress = $(element).find(".shipping-address-item");

                        // If is selected
                        if (!selectedAddress.hasClass('selected-item')) {
                            $(".shipping-address-item:first").find(".action-select-shipping-item").trigger("click");
                        }
                    }, 3000);
                }
            };


            /**
             * Check shipping row
             * */
            ko.bindingHandlers.selectDefaultShipping = {
                init: function (element) {
                    // Target
                    var target = $(element)[0];

                    // Create an observer instance
                    var observer = new MutationObserver(function (mutations) {
                        // For all changes
                        mutations.some(function (mutation, ind) {
                            var newNodes = mutation.addedNodes;							
                            if (newNodes !== null) {
								if($.cookie('Slot_Id'))
								{ 
									var slotId = $.cookie('Slot_Id');
									$(element).find("tr").find("input#s_method_matrixrate_matrixrate_"+slotId).trigger("click"); }
								else
								{ 
									$(element).find("tr:first").find("input[type='radio']").trigger("click"); 
								}
                            }
                            return ind === 0;
                        });
                    });

                    // Configuration of the observer:
                    var config = {
                        attributes: false,
                        childList: true,
                        characterData: false
                    };

                    // Pass in the target node, as well as the observer options
                    observer.observe(target, config);
                }
            };
        })(jQuery);
    });