define(
        ['ko',
            'jquery',
            'mage/url',
            'Magento_Ui/js/form/form',
            'Amasty_Checkout/js/action/update-delivery',
            'Amasty_Checkout/js/model/delivery',
            'Amasty_Checkout/js/view/checkout/datepicker'
        ],
        function (
                ko,
                $,
                url,
                Component,
                updateAction,
                deliveryService
                ) {
            'use strict';

            return Component.extend({
                defaults: {
                    template: 'Amasty_Checkout/checkout/delivery_date',
                    listens: {
                        'update': 'update'
                    }
                },
                isLoading: deliveryService.isLoading,
                preferedContact: ko.observableArray(['Call', 'SMS', 'Email']),
                preferedValue: ko.observable(''),
                initialize: function () {
                    this._super();
                    this.preferedValue.subscribe(function (newValue) {
                        var params = {'preffered_select': newValue};
                        $.ajax({
                            type: 'POST',
                            url: url.build('prefferedselect/index/index'),
                            data: params,
                            showLoader: true,
                            success: function (data) {

                            }

                       
                        // This callback is executed if the post was successful     
                    });
                    });
                },

                update: function () {
                    this.source.set('params.invalid', false);
                    this.source.trigger('amcheckoutDelivery.data.validate');

                    if (!this.source.get('params.invalid')) {
                        var data = this.source.get('amcheckoutDelivery');

                        updateAction(data);
                    }
                }
            });
        }
);
