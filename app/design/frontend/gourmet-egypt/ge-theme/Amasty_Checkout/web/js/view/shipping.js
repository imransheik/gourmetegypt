define(
    [
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/view/shipping',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/cart/totals-processor/default'
    ],
    function (
        setShippingInformationAction,
        Shipping,
         quote,
        totalsDefaultProvider
    ) {
        'use strict';

        var instance = null;

        return Shipping.extend({
            setShippingInformation: function () {
                // if (this.validateShippingInformation()) {
                    setShippingInformationAction().done(
                        function () {
                                    totalsDefaultProvider.estimateTotals(quote.shippingAddress());

                            //stepNavigator.next();
                        }
                    );
                // }
            },
            initialize: function () {
                this._super();
                instance = this;
            },

            selectShippingMethod: function (shippingMethod) {
                this._super();

                instance.setShippingInformation();

                return true;
            }
        });
    }
);
