https://marketplace.magento.com/mageworkshop-module-shopping-cart-notifications.html

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/translate',
    'mage/url',
    'jquery/ui',
     'Magento_Ui/js/modal/modal'
], function($, $t,urlBuilder) {
    "use strict";

    $.widget('mage.catalogAddToCart', {

        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: '',
			qtySelector:'#qtyselect',			
			enableQtySelector:'#qtyinput',
			enableQtyBox:'#qtyinput #qty'
        },

        _create: function() {
            if (this.options.bindSubmit) {
                this._bindSubmit();
            }
        },

        _bindSubmit: function() {
            var self = this;
            this.element.on('submit', function(e) {
                e.preventDefault();
                self.submitForm($(this));
            });
        },

        isLoaderEnabled: function() {
            return this.options.processStart && this.options.processStop;
        },

        /**
         * Handler for the form 'submit' event
         *
         * @param {Object} form
         */
        submitForm: function (form) {
            var addToCartButton, self = this;

            if (form.has('input[type="file"]').length && form.find('input[type="file"]').val() !== '') {
                self.element.off('submit');
                // disable 'Add to Cart' button
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);
                addToCartButton.prop('disabled', true);
                addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
                form.submit();
            } else {
                self.ajaxSubmit(form);
            }
        },

        ajaxSubmit: function(form) {
			if($('#qty').length > 0 && !($.isNumeric($('#qty').val())))
			{
				return false;
			}
			var productID = $(form).find(this.options.addToCartButtonSelector).attr('qty-incart');
			
            var self = this;
            $(self.options.minicartSelector).trigger('contentLoading');
            self.disableAddToCartButton(form);	

            if(typeof self.option.msgTimeOut !== 'undefined'){
                clearTimeout(self.option.msgTimeOut);
            }

            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                type: 'post',
                dataType: 'json',
                beforeSend: function() {
                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStart);
                    }
                },
                success: function(res) {
					$("input#qty").val(1);
                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStop);
                    }				

                    if(res.error)
					{		
						$('.messages').hide(''); 
						if($('.messages-error').length > 1)
						{
                                                    $("#messages-cart").html(res.error).show();
							$('.notQty'+productID).show();
						}else{
							$('.messages-error').show();
						}
						$('.messages-error').html(res.error);							
						setTimeout(function() { 
                                                      $("#messages-cart").html(res.error).show(); 
							$('.messages-error').html('');						
							$('.notQty'+productID).hide();	
							$('.messages-error').hide();
						}, 4000);											
										
					}
					
					/* if(res.max-error)
					{		
						$('.messages').hide(''); 
						$('.messages-error').show();
						$('.messages-error').html(res.error);	
						setTimeout(function() { 							
							$('.notQty'+productID).hide();	
							$('.messages-error').hide();
						}, 4000);											
										
					} */
					
					
                    if (res.backUrl) {
                        window.location = res.backUrl;
                        return;
                    } 
                    if (res.messages) {
                        $(self.options.messagesSelector).html(res.messages);
                    }
                    if (res.minicart) {
                        $(self.options.minicartSelector).replaceWith(res.minicart);
                        $(self.options.minicartSelector).trigger('contentUpdated');
                    }
                    if (res.product && res.product.statusText) {
                        $(self.options.productStatusSelector)
                            .removeClass('available')
                            .addClass('unavailable')
                            .find('span')
                            .html(res.product.statusText);
                    }
                    self.enableAddToCartButton(form);
					
                    var msg = self.options.formtype ? 'Product removed from cart.' : 'Product successfully added in your cart.';
                    self.options.formtype = 0;
					if(!res.error)
					{
                                             var pid=form.attr('id').replace('product_addtocart_form_','');
//					$("[data-preorder-product-id="+pid+"]").show();
                                        
                                        var loyaltyPopup = $("[data-preorder-product-id="+pid+"]")
                                        .modal({
                                            autoOpen: false,
                                            buttons: [
                                            ],
                                            clickableOverlay: true,
                                            focus: '',
                                            innerScroll: false,
                                            modalClass: 'pre-order-note',
                                            responsive: true,
                                          
                                            closeOnEscape: false,

                                            type: 'popup'
                                        });
                                $("[data-preorder-product-id="+pid+"]").show();
                                loyaltyPopup.modal("openModal");
                                        
						$("#messages-cart").html(msg).show();
						setTimeout(function() {
							$('#qtyselect ul.dropdown-menu li:first-child').attr("class", "selected");
							$('select.drop option:first-child').attr("selected", "selected");
							$('#qtyselect button.dropdown-toggle span.filter-option').text(1);
							$('#qtyselect button.dropdown-toggle').attr('title',1);
						}, 1000);
					}					
					
					$(".page.messages").show();
					
					self.option.msgTimeOut = setTimeout(function() { 
                        $("#messages-cart").hide(); 
						
						if($(".page.messages").is(":visible")){
							setTimeout(function() { 
								$(".page.messages").fadeOut();
							}, 4000);
						}
						
                    }, 4000);
                }
            });
            if($('.showQtyerrorProduct').length){
               $.ajax({
                        url: urlBuilder.build("groupoption/cart/checkqty"),
                        data: form.serialize(),
                        type: 'post',
                        dataType: 'json',

                        success: function (res) {
                            $("#group-product-error").html("")
 
  
                            var diaplyPopup=false;
                            for (var key in res) {
                                // skip loop if the property is from prototype
                                if (!res.hasOwnProperty(key))
                                    continue;

                                var obj = res[key];
                                for (var prop in obj) {
                                    // skip loop if the property is from prototype
                                    if (!obj.hasOwnProperty(prop))
                                        continue;

                                    // your code
                                 if(obj[prop]){
                                     var diaplyPopup=true;
                                 }
                                    $("#group-product-error").append(obj[prop]);
                                }
                            }
                            if(diaplyPopup){
                            $('.showQtyerrorProduct').trigger('click');
                             $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                               $( "form[data-role='tocart-form']" ).catalogAddToCart(); 
                        }
                    });
            }
        },

        disableAddToCartButton: function(form) {
            var addToCartButtonTextWhileAdding = this.options.addToCartButtonTextWhileAdding || $t('+ Add');
            var addToCartButton = $(form).find(this.options.addToCartButtonSelector);
            addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
            /*addToCartButton.find('span').text(addToCartButtonTextWhileAdding);
            addToCartButton.attr('title', addToCartButtonTextWhileAdding);*/
        },

        enableAddToCartButton: function(form) {
            var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('+ Add');
            var self = this,
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);

            /*addToCartButton.find('span').text(addToCartButtonTextAdded);
            addToCartButton.attr('title', addToCartButtonTextAdded);*/			
            setTimeout(function() {
                var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('+ Add');
                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
				
                /*addToCartButton.find('span').text(addToCartButtonTextDefault);
                addToCartButton.attr('title', addToCartButtonTextDefault);*/
				/* $(self.options.qtySelector).attr('style','display:none'); */	
				/* $(self.options.enableQtySelector).attr('style','display:block');	
				$(self.options.enableQtyBox).removeAttr('disabled','disabled');	 */
				
            }, 1000);
        }
    });

    return $.mage.catalogAddToCart;
});
