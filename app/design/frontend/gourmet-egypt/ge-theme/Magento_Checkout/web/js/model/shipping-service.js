/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
        [
            'jquery', // For jQuery Added
            'Magento_Checkout/js/model/quote', // For Quote Added
            'ko',
            'Magento_Checkout/js/model/checkout-data-resolver'
        ],
        function ($, quote, ko, checkoutDataResolver) {
            "use strict";
            var shippingRates = ko.observableArray([]);
            return {
                isLoading: ko.observable(false),
                /**
                 * Set shipping rates
                 *
                 * @param ratesData
                 */

                setShippingRates: function (ratesData) {
                    shippingRates(ratesData);
                    shippingRates.valueHasMutated();
                    checkoutDataResolver.resolveShippingRates(ratesData);
                    jQuery(".table-checkout-shipping-method input[type=radio]").prop("disabled", false);
                    var address = quote.shippingAddress();
                    var gouna_cut_off_time=window.checkoutConfig.checkoutdata.gouna_cut_off_time;
					var safaga_cut_off_time=window.checkoutConfig.checkoutdata.safaga_cut_off_time;
					if(typeof address.customAttributes !== "undefined"){
						var location = address.customAttributes.location_group_id.value;
					}
                    var d = new Date();
                    var ch = d.getHours();
                    var day = d.getDay();
                    var arr = [];
                    var minDate = 0;
					var paramadd = {location_id:location};
					var origin   = window.location.origin + '/locationgroup/index/getlocationday';
						$.ajax({
							type: "POST",
							url:origin,
							dataType: "json",
							data: paramadd,
							showLoader: true,
						
						success: function (datareturn) {
							var days = datareturn.location.days;
							arr = days;
						},
						complete: function () {
							
						},
						});
                    var datepicker = $(document).find('._has-datepicker');

                    datepicker.datepicker("destroy");
                    datepicker.datepicker({
                        //dateFormat: 'mm\/dd\/yyyy',
                        showsTime: false,
                        timeFormat: null,
                        buttonImage: null,
                        buttonImageOnly: null,
                        buttonText: 'Select Date',
                        maxDate: "+8d",
                        minDate: minDate,
                        beforeShowDay: function (date) {
                            var day = date.getDay();
                            return [!(arr.indexOf(day) == -1)];
                        },
                        maxDate: "+8d",

                    });

                },

                /**
                 * Get shipping rates
                 *
                 * @returns {*}
                 */
                getShippingRates: function () {
                    return shippingRates;
                }
            };
        }
);
