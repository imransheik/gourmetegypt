/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'Magento_Checkout/js/view/minicart'
], function ($, Minicart) {
    'use strict';

    return Minicart.extend({
        update: function (updatedCart) {
            this.recurseSwatch(updatedCart);

            this._super(updatedCart);
        },

        recurseSwatch: function (cartValues) {
            if ((typeof cartValues === 'object') && cartValues.hasOwnProperty('items')) {
                var self = this;
                var retain = [];

                // Store individual items ini cart temporarily
                var cartItems = [];

                // For each item in cart
                $.each(cartValues.items, function (k, item) {
                    // Product id & quantity
                    var prod_id = item.product_id;
                    var prod_qty = item.qty;

                    // Push to object
                    cartItems.push({id: prod_id, prod_qty: prod_qty});

                    // Push to array
                    retain.push(prod_id);

                    // Added quantity
                    var qty = item.qty;
                });

                var calculatedData = {};

                if (jQuery.isEmptyObject(cartItems)) {
					self.clearSwatches(retain);
                    return;
                } else {
                    for (var i in cartItems) {
                        var item = cartItems[i];
                        if (calculatedData[item.id] === undefined) {
                            calculatedData[item.id] = 0;
                        }
                        calculatedData[item.id] += item.prod_qty;
                    }
                }

                $.each(calculatedData, function (prod_id, qty) {
                    if (prod_id !== 'undefined') {
                        self.showSwatch(prod_id, qty);
                    }
                });

                self.clearSwatches(retain);
            }
        },

        showSwatch: function (id, qty) {
            if ($('body .product-qty-wrapper').length && $('body').find('.prod-swatch-' + id)) {
                var prod_swatch_id = '.prod-swatch-' + id;
				var swatch_class = '.prod-swatch-' + id + ' .product-qty-wrapper';
                if ($(swatch_class).length) {
                    $(swatch_class).fadeIn(function () {
                        $(this).find('.product-qty-inCart').html(qty);
						$(prod_swatch_id).addClass("product-added-inCart");
                    });
                }

                var deduct_class = '.prod-swatch-' + id + ' .deduct';
                var qty_class = '.prod-swatch-' + id + ' .qty';
                if ($(deduct_class).length) {
                    $(deduct_class).removeClass('hidden');
                    $(qty_class).addClass('hidden');
                }
            }


        },

        clearSwatches: function (retain) {
            $('*[class*="prod-swatch-"]').each(function () {
				var prodmatch = $(this).attr("class").match(/\prod\-swatch\-([\d]*)\b/);
                if (prodmatch[1] && ($.inArray(prodmatch[1], retain) === -1)) {
                    var swatch_cl = $(this).find('.product-qty-wrapper');
                    if ($(swatch_cl).length && $(swatch_cl).is(':visible')) {
                        $(swatch_cl).fadeOut(function () {							
                            $(this).find('.product-qty-inCart').html(0);
                        });
                    }

                    var deduct_cl = $(this).find('.deduct');
                    var qty_cl = $(this).find('.qty');
                    if ($(deduct_cl).length && !$(deduct_cl).hasClass('hidden')) {
						//console.log(deduct_cl.context.className.match(/\d+/));
						//console.log(prodmatch[0])
						$('.'+prodmatch[0]).removeClass("product-added-inCart");
                        $(deduct_cl).addClass('hidden');
                        $(qty_cl).removeClass('hidden');
                        $('#qtyselect button.dropdown-toggle span.filter-option').text(1);
                        $('#qtyselect button.dropdown-toggle').attr('title', 1);
                        $('#qtyselect ul.dropdown-menu li').removeAttr("class");
                        $('#qtyselect ul.dropdown-menu li:first-child').attr("class", "selected");
                        $('select.drop option:first-child').attr("selected", "selected");
                        $('select#qty').removeAttr("disabled");
                        $('.qty #qtyinput').addClass('hidden');
                        $('.qty #qtyselect').attr('style', 'display:block');
                    }
                }
            });
        }
    });
});
