/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/model/address-list',
    'Magento_Ui/js/modal/confirm'
], function ($, ko, Component, selectShippingAddressAction, quote, formPopUpState, checkoutData, customerData, addressList, confirmation) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-address/address-renderer/default'
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super();
			this.isDisabled = ko.computed(function () {
				var configValues = window.checkoutConfig;
				var isDisabled = true;
				var location = configValues.location;
				var address = this.address().customAttributes;
				if (address.hasOwnProperty('location_group_id')){
					var city = this.address().customAttributes.location_group_id.value;
				}else{
					var city = 0;
				}
				
				if (city === location) {
					isDisabled = false;
				}else{
					isDisabled = true;
				}
                return isDisabled;
            }, this);
			
            this.isSelected = ko.computed(function () {
				var configValues = window.checkoutConfig;
				var location = configValues.location;
                var isSelected = false,
                    shippingAddress = quote.shippingAddress();

                if (shippingAddress) {
                    isSelected = shippingAddress.getKey() == this.address().getKey(); //eslint-disable-line eqeqeq
					if(isSelected){
						var address = shippingAddress.customAttributes;
						if (address.hasOwnProperty('location_group_id')){
							var city = shippingAddress.customAttributes.location_group_id.value;
						}else{
							var city = 0;
						}
						
					}
					
                }

                return isSelected;
            }, this);

            var neededAddress = "customer-address38829";
            
            _.each(addressList(), function(address) {

                if (address.city === location) {

                }
            }, this);

            return this;
        },

        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /** Set selected customer shipping address  */
        selectAddress: function () {
           selectShippingAddressAction(this.address());
           checkoutData.setSelectedShippingAddress(this.address().getKey());

        },

        /**
         * Edit address.
         */
        editAddress: function () {
            formPopUpState.isVisible(true);
            this.showPopup();

        },

        /**
         * Show popup.
         */
        showPopup: function () {
            $('[data-open-modal="opc-new-shipping-address"]').trigger('click');
        }
    });
});
