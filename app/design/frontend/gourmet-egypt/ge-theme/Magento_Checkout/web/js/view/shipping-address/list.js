/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'ko',
    'mageUtils',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/checkout-data',
    'uiComponent',
    'uiLayout',
    'Magento_Customer/js/model/address-list'
], function ($, _, ko, utils,  selectShippingAddressAction, checkoutData, Component, layout, addressList) {
    'use strict';

    var defaultRendererTemplate = {
        parent: '${ $.$data.parentName }',
        name: '${ $.$data.name }',
        component: 'Magento_Checkout/js/view/shipping-address/address-renderer/default'
    };

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-address/list',
            visible: addressList().length > 0,
            rendererTemplates: []
        },
       /* /!** @inheritdoc *!/
        initObservable: function () {
            var neededAddress = "38828";

            _.each(addressList(), function(address) {

                if (address.getKey() === neededAddress) {
                    selectShippingAddressAction(address);
                    checkoutData.setSelectedShippingAddress(address.getKey());
                }
            }, this);
            return this;
        },*/

        /** @inheritdoc */
        initialize: function () {
            this._super()
                .initChildren();

            addressList.subscribe(function (changes) {
                    var self = this;

                    changes.forEach(function (change) {
                        if (change.status === 'added') {
                            self.createRendererComponent(change.value, change.index);
                        }
                    });
                },
                this,
                'arrayChange'
            );

            return this;
        },

        /** @inheritdoc */
        initConfig: function () {
            this._super();
            // the list of child components that are responsible for address rendering
            this.rendererComponents = [];

            return this;
        },

        /** @inheritdoc */
        initChildren: function () {
            var configValues = window.checkoutConfig;
            _.each(addressList(), this.createRendererComponent, this);
           /* _.each(addressList._latestValue, function(address) {
                /!*var addressId = address.customerAddressId;
console.log(address);
                var city = address.city;
                var location = configValues.location;
                console.log('addressId '  +addressId + 'loaction value  '+location + 'city  '+city);
               // $('.action-select-shipping-item').removeClass('location-disable');
                if(city !== location){
                    console.log(56789);
                    //alert(12345);
                    //setTimeout(function(){

                        //$('.action-select-shipping-item').addClass('location-disable-' + addressId);
                        $('.action-select-shipping-item').prop('disabled', true);
                        //$('.location-disable-'+addressId).prop('disabled', true);
                        $('.edit-address-link').addClass('address-edit-disable-'+addressId);
                        $('.address-edit-disable-'+addressId).prop('disabled', true);

                    //}, 3000)
                } else {
                    $('.action-select-shipping-item').prop('disabled', false);
                }*!/

            });*/
            //var addressId = address.customerAddressId;
            /*var location = configValues.location;
           // var city = address.city;

            _.each(addressList(), function(address) {

                if (address.city === location) {
                    selectShippingAddressAction(address);
                    checkoutData.setSelectedShippingAddress(address.getKey());
                }
            }, this);*/

            return this;
        },

        /**
         * Create new component that will render given address in the address list
         *
         * @param {Object} address
         * @param {*} index
         */
        createRendererComponent: function (address, index) {
            var rendererTemplate, templateData, rendererComponent;

            if (index in this.rendererComponents) {
                this.rendererComponents[index].address(address);
            } else {
                // rendererTemplates are provided via layout
                rendererTemplate = address.getType() != undefined && this.rendererTemplates[address.getType()] != undefined ? //eslint-disable-line
                    utils.extend({}, defaultRendererTemplate, this.rendererTemplates[address.getType()]) :
                    defaultRendererTemplate;
                templateData = {
                    parentName: this.name,
                    name: index
                };
                rendererComponent = utils.template(rendererTemplate, templateData);
                utils.extend(rendererComponent, {
                    address: ko.observable(address)
                });
                layout([rendererComponent]);
                this.rendererComponents[index] = rendererComponent;
            }
        }
    });
});
