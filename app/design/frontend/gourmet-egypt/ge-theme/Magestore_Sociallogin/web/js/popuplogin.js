/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 *
 */

define([
    'jquery',
    "prototype"
], function (jQuery) {
    var LoginPopup = Class.create();
    LoginPopup.prototype = {
        initialize: function (options) {

            this.options = options;
            this.image_login = $('progress_image_login');
            this.invalid_email = $('magestore-invalid-email');
            this.baseurl = this.options.baseurl;
            this.login_form_div = $('magestore-login-form');
            this.forgotten_and_register_div = $('forgotten-and-register');
            this.forgotten_heading_h3 = $('magestore-forgot-h3');
            this.login_button = $('magestore-button-sociallogin');
            this.login_form = $('magestore-sociallogin-form');
            this.login_form_forgot = $('magestore-sociallogin-form-forgot');
            this.login_form_otp = $('magestore-sociallogin-form-otp');
            this.login_form_password = $('magestore-sociallogin-form-reset-password');
            this.forgot_a = $('magestore-forgot-password');
			this.reset_link = $('reset-link');
			
            this.forgot_title = $('sociallogin-forgot');
            this.forgot_button = $('magestore-button-sociallogin-forgot');
            this.otp_button = $('magestore-button-sociallogin-otp');
            this.password_button = $('magestore-button-sociallogin-reset-password');
            this.forgot_a_back = $('magestore-forgot-back');
            this.invalid_email_forgot = $('magestore-invalid-email-forgot');
            this.ajax_forgot = $('progress_image_login_forgot');
            this.login_social = $('magestore-login-social');

            this.create_customer = $('magestore-create-user');
            this.create_customer_click = $('magestore-sociallogin-create-new-customer');
            this.create_customer_form = $('magestore-sociallogin-form-create');
            this.create_form_backto_login = $('magestore-create-back');
            this.create_button = $('magestore-button-sociallogin-create');
            this.create_ajax = $('progress_image_login_create');
            this.create_invalid = $('magestore-invalid-create');
            this.create_success = $('magestore-invalid-create-success');

            this.mode = 'form_login';
            this.bindEventHandlers();


        },

        login_handler: function () {
            var login_validator = new Validation('magestore-sociallogin-form');
            if (login_validator.validate()) {
                var parameters = this.login_form.serialize(true);
                var url = this.options.login_url;
                if (window.location.href.slice(0, 5) == 'https') url = url.replace("http:", "https:");
                jQuery("<div id='page-loader' class='loading-mask'><div class='loader'></div></div>").appendTo("body");
                this.showLoginLoading();
                jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: parameters,
                    success: function (data, textStatus, xhr) {
                        var result = xhr.responseText.evalJSON();
                        jQuery('#progress_image_login').hide();
                        jQuery('#progress_image_login_forgot').hide();
                        jQuery('#progress_image_login_create').hide();

                        if (result.success) {
                            window.location = result.url;
                            setTimeout(function () {
                                jQuery('#page-loader.loading-mask').text('');
                            }, 5000);


                        } else {
                            jQuery("#page-loader.loading-mask").remove();
                            jQuery('#magestore-invalid-email').show();
                            jQuery('#magestore-invalid-email').text(result.error);
                            setTimeout(function () {
                                // jQuery('#magestore-invalid-email').text('');
                            }, 3000);


                        }
                    }

                });

            }
        },
        sendpass_handler: function () {
            var login_validator_forgot = new Validation('magestore-sociallogin-form-forgot');
            if (login_validator_forgot.validate()) {
                var parameters = this.login_form_forgot.serialize(true);
                var url = this.options.send_pass_url;
                if (window.location.href.slice(0, 5) == 'https') url = url.replace("http:", "https:");
                jQuery("<div id='page-loader' class='loading-mask'><div class='loader'></div></div>").appendTo("body");
                this.showLoginLoading();

                jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: parameters,
                    success: function (data, textStatus, xhr) {
                        var result = xhr.responseText.evalJSON();
                        jQuery('#progress_image_login').hide();
                        jQuery('#progress_image_login_forgot').hide();
                        jQuery('#progress_image_login_create').hide();
                        jQuery('#magestore-invalid-email').text('');
                        jQuery("#page-loader.loading-mask").remove();
                        if (result.success) {
                            jQuery('#magestore-sociallogin-form-forgot').attr('style', 'display:none');
                            jQuery('#forgotten-and-register').hide();
							
							jQuery('#magestore-sociallogin-form-otp').attr('style', 'display:block');
                            jQuery('#magestore-invalid-otp').show().text('We sent an email with instructions to reset your password. Please check your "Junk" or "Spam" email folders if you do not see the email within the next 10 minutes.');
							// window.location = window.location;
                            setTimeout(function () {
                                jQuery('#magestore-invalid-otp').attr('style', 'display:none');
                            }, 10000);
                            jQuery('#email-otp').val(parameters['socialogin_email_forgot']);
                            jQuery('#email-otp1').val(parameters['socialogin_email_forgot']);
                        } else {
							jQuery('#magestore-invalid-email').show();
                            jQuery('#magestore-invalid-email-forgot').show().text(result.error);
                            setTimeout(function () {
                                jQuery('#magestore-invalid-email-forgot').text('');
                            }, 3000);
                        }
                    }

                });


            }
        },
		validateotp_handler: function () {
            var login_validator_otp = new Validation('magestore-sociallogin-form-otp');
            if (login_validator_otp.validate()) {
                var parameters = this.login_form_forgot.serialize(true);
                var params = this.login_form_otp.serialize(true);
                var url = this.options.otp_url;
                if (window.location.href.slice(0, 5) == 'https') url = url.replace("http:", "https:");
                jQuery("<div id='page-loader' class='loading-mask'><div class='loader'></div></div>").appendTo("body");
                this.showLoginLoading();

				jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: params,
                    success: function (data, textStatus, xhr) {
                        var result = xhr.responseText.evalJSON();
                        jQuery('#progress_image_login').hide();
                        jQuery('#progress_image_login_forgot').hide();
                        jQuery('#progress_image_login_create').hide();
                        jQuery('#magestore-invalid-email').text('');
                        jQuery("#page-loader.loading-mask").remove();
                        if (result.success) {
                            jQuery('#magestore-sociallogin-form-forgot').attr('style', 'display:none');
                            jQuery('#magestore-sociallogin-form-reset-password').attr('style', 'display:block');
                            jQuery('#forgotten-and-register').hide();
							jQuery('#magestore-sociallogin-form-otp').attr('style', 'display:none');
                        } else {
							jQuery('#magestore-invalid-email').show();
							jQuery("#page-loader.loading-mask").remove();
                            jQuery('#magestore-invalid-otp').show().text(result.error);
                            setTimeout(function () {
                                jQuery('#magestore-invalid-otp').text('');
                            }, 3000);
                        }
                    }

                });


            }
        },
		resetpassword_handler: function () {
            var login_validator_reset_password = new Validation('magestore-sociallogin-form-reset-password');
            if (login_validator_reset_password.validate()) {
                var parameters = this.login_form_forgot.serialize(true);
                var params = this.login_form_password.serialize(true);
                var url = this.options.reset_url;
				if (window.location.href.slice(0, 5) == 'https') url = url.replace("http:", "https:");
                jQuery("<div id='page-loader' class='loading-mask'><div class='loader'></div></div>").appendTo("body");
                this.showLoginLoading();
				
				jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: params,
                    success: function (data, textStatus, xhr) {
                        var result = xhr.responseText.evalJSON();
                        jQuery('#progress_image_login').hide();
                        jQuery('#progress_image_login_forgot').hide();
                        jQuery('#progress_image_login_create').hide();
                        jQuery('#magestore-invalid-email').text('');
                        jQuery("#page-loader.loading-mask").remove();
                        if (result.success) {
                            jQuery('#magestore-sociallogin-form').attr('style', 'display:block');
                            jQuery('#magestore-sociallogin-form-reset-password').attr('style', 'display:none');
                            jQuery('#forgotten-and-register').hide();
							jQuery('#magestore-invalid-password').attr('style', 'display:none');
							
                       
                            
                        } else {
							jQuery('#magestore-invalid-email').show();
							jQuery("#page-loader.loading-mask").remove();
                            jQuery('#magestore-invalid-password').show().text(result.error);
                            setTimeout(function () {
                                // jQuery('#magestore-invalid-email-forgot').text('');
                            }, 3000);
                        }
                    }

                });


            }
        },
		
        forgot_handler: function () {
            this.hideFormLogin();
            this.mode = 'form_forgot';
			this.reset_link.style.display = "none";
            this.showFormForgot();
        },
        showLogin_handler: function () {
            this.hideFormForgot();
            this.hideCreateForm();
			this.reset_link.style.display = "inline";
			jQuery('#reset-link').show();
            this.mode = 'form_login';
            this.showFormLogin();
        },
        showCreate_handler: function () {
            this.hideFormLogin();
            this.hideFormForgot();
            this.mode = 'form_create';
            this.showCreateForm();
			this.reset_link.style.display = "inline";
            document.getElementById('socialogin.firstname').focus();

        },
        createAcc_handler: function () { 

            var login_validator_create = new Validation('magestore-sociallogin-form-create');
            if (login_validator_create.validate()) {

                var parameters = this.create_customer_form.serialize(true);

                var url = this.options.create_url;
                if (window.location.href.slice(0, 5) == 'https') url = url.replace("http:", "https:");
                this.showLoginLoading();
                jQuery("<div id='page-loader' class='loading-mask'><div class='loader'></div></div>").appendTo("body");
                this.showLoginLoading();
                jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: parameters,
                    success: function (data, textStatus, xhr) {
                        var result = xhr.responseText.evalJSON();
                        jQuery('#progress_image_login').hide();
                        jQuery('#progress_image_login_forgot').hide();
                        jQuery('#progress_image_login_create').hide();
                        if (result.success) {
                            setTimeout(function () {
                                jQuery('#page-loader.loading-mask').text('');
                            }, 5000);
                            jQuery('#magestore-invalid-create-success').show();
                            window.localStorage.setItem("welcome", 1);
                            window.location.href = "/customer/account/";
                        } else {
                            jQuery("#page-loader.loading-mask").remove();
                            jQuery('#magestore-invalid-create').show();
                            jQuery('#magestore-invalid-create').text(result.error);
                        }
                    }

                });


            }
        },
        bindEventHandlers: function () {
            /* Now bind the submit button for logging in */
            if (this.login_button) {
                this.login_button.observe(
                    'click', this.login_handler.bind(this));
            }
            if (this.forgot_a) {
                this.forgot_a.observe(
                    'click', this.forgot_handler.bind(this));
            }
            if (this.forgot_a_back) {
                this.forgot_a_back.observe(
                    'click', this.showLogin_handler.bind(this));
            }
            if (this.forgot_button) {
                this.forgot_button.observe(
                    'click', this.sendpass_handler.bind(this));
            }
			if (this.otp_button) {
                this.otp_button.observe(
                    'click', this.validateotp_handler.bind(this));
            }
			if (this.password_button) {
                this.password_button.observe(
                    'click', this.resetpassword_handler.bind(this));
            }
            if (this.create_customer_click) {
                this.create_customer_click.observe(
                    'click', this.showCreate_handler.bind(this));
            }
            if (this.create_form_backto_login) {
                this.create_form_backto_login.observe(
                    'click', this.showLogin_handler.bind(this));
            }
            if (this.create_button) {
                this.create_button.observe(
                    'click', this.createAcc_handler.bind(this));
            }
            document.observe('keypress', this.keypress_handler.bind(this));
        },
        keypress_handler: function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {

                if (this.mode == 'form_login') {
                    this.login_handler();
                } else if (this.mode == 'form_forgot') {
                    this.sendpass_handler();
                } else if (this.mode == 'form_create') {
                    this.createAcc_handler();
                } else {
                }
            }
        },
        showLoginLoading: function () {
            this.image_login.style.display = "block";
            this.ajax_forgot.style.display = "block";
            this.create_ajax.style.display = "block"
        },
        hideLoginLoading: function () {
            this.image_login.style.display = "none";
            this.ajax_forgot.style.display = "none";
            this.create_ajax.style.display = "none"
        },
        showLoginError: function (error) {
            this.invalid_email.show();
            this.invalid_email.update(error);
        },
        hideFormLogin: function () {
            this.login_form.style.display = "none";            
        },
        showFormLogin: function () {
			this.invalid_email_forgot.style.display = "none";
			this.login_form.style.display = "block";
            this.forgotten_and_register_div.style.display = "block";
            this.forgotten_heading_h3.style.display = "none";
            this.create_invalid.style.display = "none";
			
        },
        hideFormForgot: function () {
            this.forgot_title.style.display = "none";
            this.login_form_forgot.style.display = "none";
            this.login_social.style.display = "block";
            this.login_form_div.className = '';
            this.create_invalid.style.display = "none";
        },
        showFormForgot: function () {
            this.forgot_title.style.display = "block";
            this.login_form_forgot.style.display = "block";
            this.forgotten_and_register_div.style.display = "block";
            this.login_social.style.display = "none";
            this.login_form_div.className = 'hideAfter';
            this.create_invalid.style.display = "none";
        },
        showSendPassError: function (error) {
			this.invalid_email.show();
            this.invalid_email_forgot.show();
            this.invalid_email_forgot.update(error);
        },
        showCreateForm: function () {
            this.login_form_div.style.display = "none";
            //this.create_customer_click.style.display = "none";
            this.create_customer.style.display = "block";
            this.create_invalid.style.display = "block";
            this.forgotten_and_register_div.style.display = "none";
        },
        hideCreateForm: function () {
            this.create_customer.style.display = "none";
            this.login_form_div.style.display = "block";
            this.create_invalid.style.display = "none";
            //this.create_customer_click.style.display = "block";
        },
        showCreateError: function (error) {
            this.create_invalid.show();
            this.create_invalid.update(error);
        }
    };

    return LoginPopup;


});