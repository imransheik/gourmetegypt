// JavaScript Document
function openLocationPopup(evt, popupId) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("popupcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("popuplinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(popupId).style.display = "block";
    evt.currentTarget.className += " active";
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

/**
 * Sticky header
 * */

require(['domReady!', 'jquery', 'stickyheader'], function (doc, $, sticky) {

    var pageHeader = $(".page-header"),
        filterWrapper = $("#amasty-shopby-product-list .category-top-wrapper"),
        windowWidth = $(window).outerWidth(),
        windowSize = 874,
        pageHeaderheight = pageHeader.outerHeight();

    function mCustomScrollbarActive() {
        // Mega menu scroll
        $("#storeList").mCustomScrollbar({
            theme: "dark-3",
            mouseWheel: {preventDefault: true}
        });
    }

    function loadMoreCategories() {

        var catUl = $(".top-category-links ul:first-child");
        var singleCategoriesRow = catUl.height();

        if (singleCategoriesRow > 54) {
            catUl.addClass("topMoreCategories");
            $(".top-category-links .topMoreCategoriesLink").show();
        } else {
            if (singleCategoriesRow < 54) {
                catUl.removeClass("topMoreCategories");
                $(".top-category-links .topMoreCategoriesLink").hide();
            }
        }

        var subCatUl = $(".top-category-links ul.cat-sub-menu-nav");
        var singleSubCategoriesRow = subCatUl.height();

        if (singleSubCategoriesRow > 60) {
            subCatUl.addClass("topMoreSubCategories");
            $(".top-category-links .topMoreSubCategoriesLink").show();
        } else {
            if (singleSubCategoriesRow < 60) {
                subCatUl.removeClass("topMoreSubCategories");
                $(".top-category-links .topMoreSubCategoriesLink").hide();
            }
        }

    }

    $(".top-category-links .topMoreCategoriesLink").click(function () {
        $(".top-category-links ul:first-child").removeClass("topMoreCategories");
        $(".top-category-links .topCategoriesCollapseAll").show();
        $(this).hide();
    });

    $(".top-category-links .topMoreSubCategoriesLink").click(function () {
        $(".top-category-links ul.cat-sub-menu-nav").removeClass("topMoreSubCategories");
        $(".top-category-links .topCategoriesCollapseAll").show();
        $(this).hide();
    });

    $(".top-category-links .topCategoriesCollapseAll").click(function () {
        loadMoreCategories();
        $(this).hide();
    });


    loadMoreCategories();

    $(window).resize(function () {
        mCustomScrollbarActive();
    });

    $(window).load(function () {
        // If resolution matches
        mCustomScrollbarActive();

        if (windowWidth >= windowSize) {
            pageHeader.sticky({
                topSpacing: 0,
                center: true,
                zIndex: 999
            });

            // initiate sticky
            filterWrapper.sticky({
                topSpacing: pageHeaderheight,
                wrapperClassName: 'sticky__filter',
                center: true,
                zIndex: 999
            });

        } else {
            // destroy sticky
            pageHeader.unstick();
            filterWrapper.unstick();
        }

        // Scroll button
        $(".scroll__top").on("click", function (e) {
            // stop #
            e.preventDefault();

            // animate to top
            $('body, html').animate({scrollTop: 0});
        });


        $(".store-modal-popup").addClass("normalMap");

        $(".megamenu-f-product-listing").mCustomScrollbar({
            theme: "dark-3",
            mouseWheel: {preventDefault: true},
            setHeight: 560
        });

        $(".ms-maincontent .ms-category, li.ui-menu-item.first .ms-content").mCustomScrollbar({
            theme: "dark-3",
            mouseWheel: {preventDefault: true},
            setHeight: 630
        });
    });

    $(window).load(function () {
        if ($('body').hasClass('magento-giftcardaccount-customer-index')) {
            $('.sidebar-main ul li a[href*="storecredit/info"]').each(function () {
                $(this).parent().addClass('current');
            });
        }
    });

    // Filter
    $(document).on("click", ".mobFilterIcon .filter-icon", function () {
        $(".mobileFilterPopup").addClass("filtershown");
        $("header.page-header").addClass("phOnClick");
        $(".category-top-inner-wrapper > .row").append("<div class=\"black-overlay\" data-action=\"close\"></div>")
    });

    $(document).on("click", ".removeFiterShown, .black-overlay", function () {
        $(".mobileFilterPopup").removeClass("filtershown");
        $("header.page-header").removeClass("phOnClick");
        $(".black-overlay").remove();
    });

    // Vardhaman - Focue to on error field on checkout page //
    $(document).on("click", ".checkout.btn-block", function () {
        var errorField = $(".mage-error:first");

        // If there is any error
        if (errorField.length) {
            var errorPos = errorField.offset().top - 200;
            $("html,body").animate({scrollTop: errorPos}, 500);
        }
    });


    // Self close filters
//    $(document).click(function (e) {
//        if ($("#collapsed-sorter, .filter-options-item").is('.active') &&
//            $(e.target).parents('.right-filter-sortby').length === 0 &&
//            !$(e.target).is($(".right-filter-sortby"))) {
//            $("#collapsed-sorter, .filter-options-item").collapsible("deactivate");
//        }
//    });

    // timer for resize debounce (delay resize)
    var resizeTimer;

    // functions to run on resize events
    $(window).on("resize orientationchange", function () {
        if ($(window).outerWidth() != windowWidth) {
            // Update the window width for next time
            windowWidth = $(window).outerWidth();

            // clear timeout
            clearTimeout(resizeTimer);

            // call functions here in debounce
            resizeTimer = setTimeout(function () {
                /**
                 * Call resize functions here
                 * */
                // If resolution matches
                if (windowWidth >= windowSize) {
                    // Update height required
                    pageHeaderheight = pageHeader.outerHeight();

                    pageHeader.unstick();
                    filterWrapper.unstick();

                    pageHeader.sticky({
                        topSpacing: 0,
                        center: true,
                        zIndex: 999
                    });

                    filterWrapper.sticky({
                        topSpacing: pageHeaderheight,
                        wrapperClassName: 'sticky__filter',
                        center: true,
                        zIndex: 999
                    });

                } else {
                    pageHeader.unstick();
                    filterWrapper.unstick();
                }
            }, 100);
        }

        loadMoreCategories();
    });


});

/**
 * Homepage sliders
 * */
require(['domReady!', 'flickity', 'jquery'], function (doc, Flickity, $) {
    'use strict';

    var elem = document.querySelectorAll('#maincontent .product-items.widget-product-grid');

    // If homepage modules exists
    if (elem.length) {
        var flickity = [];

        // On window resize
        window.onresize = function () {
            refreshSlider();
        };
    }

    // Delay to avoid any UI issues because of loading { fallback }
    $(document).ready(function () {
        setTimeout(function () {
            // initiate plugin
            //createSlider();
        }, 1500);
    });

    // refresh slider to fix any loading issues
    $(window).load(function () {
        if (elem.length) {
            setTimeout(function () {
                // initiate plugin
                createSlider();
            }, 1500);
        }
    });

    // Create flickity
    function createSlider() {
        // Check if to enable drag or not
        var windowWidth = $(window).outerWidth(),
            widthDefault = 874,
            isDraggable = windowWidth <= widthDefault;

        var i;
        for (i = 0; i < elem.length; i++) {
            flickity[i] = new Flickity(elem[i], {
                cellAlign: 'left',
                contain: true,
                groupCells: true,
                pageDots: false,
                percentPosition: false,
                draggable: isDraggable,
                imagesLoaded: true
            });
        }
    }

    // Refresh slilder
    function refreshSlider() {
        var j;
        for (j = 0; j < flickity.length; j++) {
            flickity[j].destroy();
        }

        // re-initiate plugin
        createSlider();
    }
});