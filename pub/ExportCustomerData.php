<?php

error_reporting(1);
set_time_limit(0);
ini_set('memory_limit', '2048M');

use Magento\Framework\App\Bootstrap;

/**
 * If your external file is in root folder
 */
require __DIR__ . '/../app/bootstrap.php';

/**
 * If your external file is NOT in root folder
 * Let's suppose, your file is inside a folder named 'xyz'
 *
 * And, let's suppose, your root directory path is
 * /var/www/html/magento2
 */
// $rootDirectoryPath = '/var/www/html/magento2';
// require $rootDirectoryPath . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

// Set area code
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

try {
    $CustomerColection = $obj->create('Magento\Customer\Model\Customer')->getCollection();
    $fp = fopen("CustomerDataForWebengaged-jan16.csv","w+");
    foreach ($CustomerColection as $customer){
//        var_dump($customer->getData()); exit;
    $CustomerModel = $obj->create('Magento\Customer\Model\Customer');
    $CustomerModel->setWebsiteId(1);
    $CustomerModel->loadByEmail($customer->getEmail());
    $customerData = $CustomerModel->getDataModel();
    $pn=$customerData->getCustomAttribute("primary_mobile_number");
    $data = array();
    $data[] = $customer->getId();
    $data[] = $customer->getEmail();
    $data[] = $customer->getFirstname();
    $data[] = $customer->getLastname();
    if($pn){
    $data[] = "+".$customerData->getCustomAttribute("isd_code")->getValue().$pn->getValue();
    }
    if($customerData->getCustomAttribute("prefered_language")){
   $prefered_language= $customerData->getCustomAttribute("prefered_language")->getValue();
    }else{
        $prefered_language='';
    }

    if($prefered_language){
        if ($prefered_language == "210") {
             $data[] = "Arabic";
        } else {
             $data[] = "English";
        }
    }else{
        $data[]='';
    }
    fputcsv($fp, $data);
 
}
} catch (\Exception $e) {
    echo "No Number";
}
fclose($fp);

echo "Completed Customer Export!";