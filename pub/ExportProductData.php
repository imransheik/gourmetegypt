<?php

error_reporting(1);
set_time_limit(0);
ini_set('memory_limit', '2048M');

use Magento\Framework\App\Bootstrap;

/**
 * If your external file is in root folder
 */
require __DIR__ . '/../app/bootstrap.php';

/**
 * If your external file is NOT in root folder
 * Let's suppose, your file is inside a folder named 'xyz'
 *
 * And, let's suppose, your root directory path is
 * /var/www/html/magento2
 */
// $rootDirectoryPath = '/var/www/html/magento2';
// require $rootDirectoryPath . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

// Set area code
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

try {
    $productColection = $obj->create('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
    $collection = $productColection->create();
     $collection->addAttributeToFilter( 'status' ,\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $collection->addAttributeToSelect('*');
    
    $i=0;
    $data=array();
    
//      $b=array(1,2,3,"eeeeeeeeeeee");
       $fp = fopen("ProductExport-April22.csv","w+");
    foreach ($collection as $product){
      
//        var_dump($product->getSku());
//         $product = $obj->create("\Magento\Catalog\Model\ProductRepository")->get($product->getSku());
//            var_dump($product->getSku());
        
//          $a=array(1,2,3,"eeeeeeeeeeee");
    $data[] = str_replace(',', '|',$product->getSku());
    $data[] = str_replace(',', '|', $product->getName());
    $data[] =  str_replace(',', '|', $product->getData('product_weight'));
    $data[] = str_replace(',', '|', $product->getData('ingredients'));
    $data[] = str_replace(',', '|', $product->getResource()->getAttribute('tag')->getFrontend()->getValue($product)); 
//    if($product->getSku()=="6223001300519")
//    var_dump($data); exit;
   fputcsv($fp, $data); 
    $data=array();
//    if($i==20){
//        break;
//    }
//    $i++;
 
}

} catch (\Exception $e) {
    echo "No Number";
}

echo "Completed product Export!";