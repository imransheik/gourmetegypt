<?php

error_reporting(1);
set_time_limit(0);
ini_set('memory_limit', '2048M');

use Magento\Framework\App\Bootstrap;

/**
 * If your external file is in root folder
 */
require __DIR__ . '/../app/bootstrap.php';

/**
 * If your external file is NOT in root folder
 * Let's suppose, your file is inside a folder named 'xyz'
 *
 * And, let's suppose, your root directory path is
 * /var/www/html/magento2
 */
// $rootDirectoryPath = '/var/www/html/magento2';
// require $rootDirectoryPath . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

// Set area code
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
// Define Logger
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/update-cutomer-address.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);
//$CustomerAddd = $obj->create('Magento\Customer\Model\Address')->getCollection();
for ($x = 52460; $x <= 57758; $x++) { 
    try {
        $addressRepository = $obj->create('Magento\Customer\Api\AddressRepositoryInterface');
        $address = $addressRepository->getById($x);
        $street = $address->getStreet();
        if ($address->getCustomAttribute("floor") == null) { 
            if (isset($street['1'])) {
                $address->setCustomAttribute('floor', $street['1']);
            } else {
                $address->setCustomAttribute('floor', '-');
            }
        }
        $address->setStreet(array($street['0']));
        $msg = " Success Address Id: " . $address->getId();
        $logger->info($msg);
        $addressRepository->save($address);
    } catch (\Exception $e) {
        $msg = " Fail Address Id: " . $address->getId() . " Message: " . $e->getMessage();
        $logger->info($msg);
    }
}

$logger->info($msg);
echo "Completed Customer Export!";
