<div class="loyalty_container">
    <div id="Validate-oto-div">
        <div id="message_loyalty" style="color: red;font-size: large;font-weight: bold;padding: 1px;margin: 1px; text-align: center;"></div>
        <legend class="legend"><span>OTP Validation</span></legend>
        <form class="form" id="validate-otp-form" method="post" autocomplete="off" action="/loyalty/index/verifypin" >
            <fieldset class="fieldset">

                <li class="fields">
                   
                    <div class="sociallogin-field  form-group primary_mobile_number" style="width: 59%;clear: none;">
                        <div class="input-box">
                            <input type="text" id="primary_mobile_number" placeholder="Please enter your OTP" name="otp" value="" class="input-text required-entry validate-digits validate-length minimum-length-4 maximum-length-4" autocomplete="off" aria-required="true">
                        </div>
                    </div>
                </li>
                <input type="hidden" id="pinId" name="pinId" value="<?php echo rtrim($_GET['pinId'], '/')?>" >
            
            </fieldset>
            <div class="actions-toolbar">
                <div class="primary">
                    <button type="submit" class="action submit primary" title="<?php 'Verify OTP' ?>"><span><?php echo 'Verify OTP' ?></span></button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>

.loyalty_container {
  max-width:600px;
  margin:5% auto;
}
legend.legend {
    font-size: 18px;
    padding-bottom: 10px;
    color: #000;
}
li.fields {
    list-style: none;
}
form.form {
    border: 1px solid #ccc;
    padding: 15px;
    display: inline-block;
    width: 100%;
}
fieldset.fieldset {
    border: 0px solid #ccc;
	padding: 0;
}
input#primary_mobile_number {
    border: 1px solid #ccc;
    padding: 5px;
}
input#primary_mobile_number {
    border: 1px solid #ccc;
    padding: 5px;margin-bottom:10px;
	width:280px;
}
select#isd_code_loyalty {
    border: 1px solid #ccc;
    height: 28px;
    width: 280px;
    margin-bottom: 10px;
}
button.action.submit.primary {
    border: 1px solid #14b393;
    background-color: #14b393;
    padding: 0 15px;
    float: left;
    z-index: 1;
    text-decoration: none;
    color: #fff;
    font-size: 14px;    
    text-align: center;
    height: 30px;
    text-transform: uppercase;
    line-height: 28px;
} 
</style>