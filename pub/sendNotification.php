<?php

error_reporting(1);
set_time_limit(0);
ini_set('memory_limit', '2048M');

use Magento\Framework\App\Bootstrap;

/**
 * If your external file is in root folder
 */
require __DIR__ . '/../app/bootstrap.php';

/**
 * If your external file is NOT in root folder
 * Let's suppose, your file is inside a folder named 'xyz'
 *
 * And, let's suppose, your root directory path is
 * /var/www/html/magento2
 */
// $rootDirectoryPath = '/var/www/html/magento2';
// require $rootDirectoryPath . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

// Set area code
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

// Define Logger
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/new-cario-fcm-customerid.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);


$_fcmCollection=$obj->get('I95Dev\Fcm\Model\ResourceModel\Fcm\CollectionFactory');
$_api=$obj->get('I95Dev\Fcm\lib\FcmAPI');
$customerCollection = $obj->get('\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory')->create();
//$customerCollection->addAttributeToFilter('email', array('in' => array("kamlesh@jivainfotech.com","sheik.imran@jivainfotech.com","i95devtestuser@gmail.com")));

//    var_dump($customerCollection->getAllids()); exit;
foreach ($customerCollection as $customer){
//    var_dump($customer->getId()); exit;
            $customerAddress = array();
            $hascity = false;
            foreach ($customer->getAddresses() as $address) {
                $customerAddress[] = $address->toArray();
            }

            foreach ($customerAddress as $customerAddres) {
                if ($customerAddres['city'] == "New Cairo") {
                    $hascity = true;
                }
            }
            if($hascity){
        $orderStatusMessage = array(
            'title' => 'Location Update',
            'categoryIdentifier' => 'normal',
          'body' => 'Hello, thank you for choosing Gourmet Egypt. We now have two stores in New Cairo, please update your address accordingly. Happy shopping.',
        );
        $customerId=$customer->getId();
//        $customerId = $order->getCustomerId();
        //var_dump($customerId);exit;
        $fcmCollection = $_fcmCollection->create();

//        var_dump($fcmCollection->getData());exit;
        $fcmCollectionAndroid = $fcmCollection->addFieldToFilter('customerid', $customerId)->addFieldToFilter('platform', 'ANDROID');
        $fcmAndroid = $fcmCollectionAndroid->getData();
        $fcmAndroidArr = [];
        foreach ($fcmAndroid as $fcmData) {
            $fcmAndroidArr[] = $fcmData['fcmtoken'];
        }

        $responce =$_api->getApiCall($fcmAndroidArr, $orderStatusMessage);
        $fcmCollection = $_fcmCollection->create();
        $fcmCollectionIos = $fcmCollection->addFieldToFilter('customerid', $customerId)->addFieldToFilter('platform', 'ios');
        $fcmIos = $fcmCollectionIos->getData();
        $fcmIosArr = [];
        foreach ($fcmIos as $fcmData) {
            $fcmIosArr[] = $fcmData['fcmtoken'];
        }
        $responceios =$_api->getApiCallforIos($fcmIosArr, $orderStatusMessage);
        $logger->info($customer->getId());
}

}

echo "Completed the Send Notification";
//                        $msg = 'Product ID: ' . $product->getId() .
//                                ' || Updated Option ID: ' . $value->getId();
//
//                        echo $msg . '<br />';
