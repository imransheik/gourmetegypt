<?php

error_reporting(1);
set_time_limit(0);
ini_set('memory_limit', '2048M');

use Magento\Framework\App\Bootstrap;

/**
 * If your external file is in root folder
 */
require __DIR__ . '/../app/bootstrap.php';

/**
 * If your external file is NOT in root folder
 * Let's suppose, your file is inside a folder named 'xyz'
 *
 * And, let's suppose, your root directory path is
 * /var/www/html/magento2
 */
// $rootDirectoryPath = '/var/www/html/magento2';
// require $rootDirectoryPath . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

// Set area code
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

// Define Logger
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/update-customer-mobile.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);


if (($handle = fopen("mobile.csv", "r")) !== FALSE) {
  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
if($data[0]&&$data[1]){
try {
    $customerEmail = trim($data[0]);
    $CustomerModel = $obj->create('Magento\Customer\Model\Customer');
    $CustomerModel->setWebsiteId(1);
    $CustomerModel->loadByEmail($customerEmail);
    $customerData = $CustomerModel->getDataModel();
    $customerData->setCustomAttribute("primary_mobile_number", "0".$data[1]);
    $customerData->setCustomAttribute("isd_code", "2");
    $CustomerModel->updateData($customerData);
    $CustomerModel->save();
   
    $msg = $customerEmail . " Success Message: " . $CustomerModel->getData("primary_mobile_number");
    $logger->info($msg);
} catch (\Exception $e) {
    $msg = $customerEmail . " Mobile:". $data[1]."Error Message: " . $e->getMessage();
    $logger->info($msg);
}
  }} }
echo "Import Completed!!!!!!";
